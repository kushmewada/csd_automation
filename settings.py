import os
from pathlib import Path


"""
Server Startup Settings Start.
"""


# SERVER_IP = "0.0.0.0"
SERVER_IP = "localhost"   # ChangedForTest
SERVER_PORT = 5000

SERVER_DEBUG_MODE = 1
SERVER_PRODUCTION_MODE = 2

SERVER_MODE_DICT = {
    SERVER_DEBUG_MODE: True,
    SERVER_PRODUCTION_MODE: False
}

SERVER_MODE = SERVER_DEBUG_MODE 

TRACK_MODIFICATIONS = False
SERVER_RELOADING = False

MAX_UPLOAD_SIZE = 30000000  # 30 Megabytes

TEXT_FILE_EXTENSIONS = ['txt', 'doc', 'docm', 'docx', 'dot', 'dotx']
SPREADSHEET_FILE_EXTENSIONS = ['csv', 'xlsx', 'xlsm', 'xlsb', 'xltx', 'xltm', 'xls', 'xlt', 'xml', 'xlam', 'xla', 'xlw',
                               'xlr']
IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png']
PDF_EXTENSION = ['pdf']

UPLOAD_EXTENSIONS = TEXT_FILE_EXTENSIONS + SPREADSHEET_FILE_EXTENSIONS + IMAGE_EXTENSIONS + PDF_EXTENSION

ATTACHMENT_DIRECTORY_MAX_SIZE = 5000000000  # 5000 Megabytes
MAX_FILES_IN_ATTACHMENT_DIRECTORY = 1000

DOCUMENT_STORE = 'document_store'
PARENT_FOLDER_PATH = os.path.join(Path(os.path.abspath(__file__)).parent.parent)
DOCUMENT_STORE_PATH = os.path.join(PARENT_FOLDER_PATH, DOCUMENT_STORE)
if os.path.exists(DOCUMENT_STORE_PATH) is False:
    os.mkdir(DOCUMENT_STORE_PATH)

"""
Server Startup Settings End.
"""

"""
MS - SQL Settings Start
"""

# USER_NAME = 'admin'
USER_NAME = 'root'      # Changed
# PASSWORD = 'uh8qBD4255jzA3Lr'
PASSWORD = ''       # Changed
# MS_SQL_SERVER_IP = 'scd-auto-ms.cjob0teuevpo.ap-south-1.rds.amazonaws.com'
MS_SQL_SERVER_IP = 'localhost'
# MS_SQL_SERVER_PORT = '1433'
MS_SQL_SERVER_PORT = ''
# DATABASE_NAME = 'scd_iis'
DATABASE_NAME = 'scd_iis'
ODBC_DRIVER = 'ODBC Driver 17 for SQL Server'

"""
MS - SQL Settings End
"""

"""
Logging Settings Start.
"""

ROOT_ROTATING_BACKUP_COUNT = 5
DEFAULT_ROTATING_BACKUP_COUNT = 100
MAX_BYTES = 10240000

"""
Logging Settings End.
"""
