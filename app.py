from concurrent.futures import ThreadPoolExecutor

import constants
import settings as settings_file
from common_packages import init_app
from logging_module import setup_logger
from scd_automation_app import *

server_logger = setup_logger(constants.LOGGER_DICT['SERVER_LOG'])
server_logger.info('Starting SCD Automation server.....')
server_logger.info('Server URL: ' + settings_file.SERVER_IP + ' : ' + str(settings_file.SERVER_PORT))
server_logger.info('Server Mode: ' + str(settings_file.SERVER_MODE))


def register_blueprints(scd_app):
    server_logger.info('Registering blueprints....')
    scd_app.register_blueprint(common_bp)
    scd_app.register_blueprint(scr_bp)
    scd_app.register_blueprint(cod_bp)
    scd_app.register_blueprint(pmdr_bp)
    scd_app.register_blueprint(csvr_bp)
    scd_app.register_blueprint(attachment_bp)
    scd_app.register_blueprint(pending_actions_bp)
    scd_app.register_blueprint(users_bp)
    scd_app.register_blueprint(plant_bp)
    scd_app.register_blueprint(group_bp)
    scd_app.register_blueprint(import_data_bp)
    scd_app.register_blueprint(update_data_bp)
    scd_app.register_blueprint(dashboard_bp)
    scd_app.register_blueprint(settings_bp)
    server_logger.info('Blueprints registered.')


def start_background_task():
    server_logger.info('Starting background task...')
    executor = ThreadPoolExecutor(max_workers=constants.DEFAULT_BACKGROUND_TASK_WORKERS)
    executor.submit(background_tasks)
    server_logger.info('Background task started.')


app = init_app()
register_blueprints(app)
start_background_task()


if settings_file.SERVER_MODE == settings_file.SERVER_DEBUG_MODE:
    app.run(
        host=settings_file.SERVER_IP,
        port=settings_file.SERVER_PORT,
        debug=settings_file.SERVER_MODE_DICT[settings_file.SERVER_MODE],
        use_reloader=settings_file.SERVER_RELOADING
    )
server_logger.info('Server is up and running.')


@app.route('/')
def index():
    try:
        from flask import request
        import json
        import os

        b = dict()
        username = request.environ.get('REMOTE_USER')
        for a in request.environ:
            b[a] = request.environ[a]
        return str(username) + " Here we go: " + str(b)
    except:
        from traceback import format_exc
        return str(format_exc())
