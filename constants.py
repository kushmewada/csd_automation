"""
All constants of the project are defined in this file.
"""

import os
from pathlib import Path


"""
System Status constants start.
"""

SYSTEM_DISABLED = 0
SYSTEM_ENABLED = 1

"""
System Status constants end.
"""


"""
Logging Constants Start.
"""

LOGGING_LEVEL_DICT = {
    'INFO': 'INFO',
    'DEBUG': 'DEBUG',
    'ERROR': 'ERROR'
}

LOGGER_DICT = {
    'SERVER_LOG': 'server_log',
    'ARGS_LOG': 'args_log',
    'COMMON_LOG': 'common_log',
    'RESPONSE_LOG': 'response_log',
    'BACKGROUND_TASKS_LOG': 'background_tasks_log',
    'SCR_LOG': 'scr_log',
    'COD_LOG': 'cod_log',
    'PMDR_LOG': 'pmdr_log',
    'CSVR_LOG': 'csvr_log',
    'ATTACHMENT_LOG': 'attachment_log',
    'PENDING_ACTION_LOG': 'pending_action_log',
    'USER_LOG': 'user_log',
    'PLANT_LOG': 'plant_log',
    'GROUP_LOG': 'group_log',
    'IMPORT_DATA_LOG': 'import_data_log',
    'UPDATE_DATA_LOG': 'update_data_log',
    'DASHBOARD_LOG': 'dashboard_log',
    'SETTING_LOG': 'setting_log'
}

ABSOLUTE_PROJECT_PATH = os.path.join(Path(os.path.abspath(__file__)).parent)
LOGGER_PATH = os.path.join(ABSOLUTE_PROJECT_PATH, 'app_logs')
CONFIG_FOLDER = 'config'
MAPPING_LIST_FOLDER = 'mapping_list'
LOGS_FOLDER = 'logs'

ROOT_LOG_FILE = 'root.log'
CONFIG_JSON_FILE = 'config.json'
MAPPING_LIST_JSON_FILE = 'mapping_list.json'

IS_LOG_SETUP_IMPORTED = False
FORM_ROOT = 'form_root'
LOG_FORMAT = '%(asctime)-15s %(name)-5s %(levelname)-8s %(message)s'

ROTATING_FILE_HANDLER = 'RotatingFileHandler'
FORMATTER_CLASS = 'logging.Formatter'
ROTATING_FILE_HANDLER_CLASS = 'logging.handlers.RotatingFileHandler'

"""
Logging Constants End.
"""


"""
URL Constants Start.
"""

BLUE_PRINT_DICT = {
    'SCR': {
        'BLUE_PRINT': 'scr',
        'URL_PREFIX': '/scr'
    },
    'COD': {
        'BLUE_PRINT': 'cod',
        'URL_PREFIX': '/cod'
    },
    'PMDR': {
        'BLUE_PRINT': 'pmdr',
        'URL_PREFIX': '/pmdr'
    },
    'CSVR': {
        'BLUE_PRINT': 'csvr',
        'URL_PREFIX': '/csvr'
    },
    'ATTACHMENTS': {
        'BLUE_PRINT': 'attachments',
        'URL_PREFIX': '/attachments'
    },
    'PENDING_ACTIONS': {
        'BLUE_PRINT': 'pending_actions',
        'URL_PREFIX': '/pending_actions'
    },
    'COMMON': {
        'BLUE_PRINT': 'common',
        'URL_PREFIX': '/common'
    },
    'DASHBOARDS': {
        'BLUE_PRINT': 'dashboards',
        'URL_PREFIX': '/dashboards'
    },
    'MASTERS': {
        'USERS': {
            'BLUE_PRINT': 'masters_users',
            'URL_PREFIX': '/masters/users'
        },
        'PLANT': {
            'BLUE_PRINT': 'masters_plant',
            'URL_PREFIX': '/masters/plant'
        },
        'GROUPS': {
            'BLUE_PRINT': 'groups',
            'URL_PREFIX': '/masters/groups'
        },
        'IMPORT_DATA': {
            'BLUE_PRINT': 'masters_import_data',
            'URL_PREFIX': '/masters/import_data'
        },
        'UPDATE_DATA': {
            'BLUE_PRINT': 'masters_update_data',
            'URL_PREFIX': '/masters/update_data'
        }
    },
    'SETTINGS': {
        'BLUE_PRINT': 'settings',
        'URL_PREFIX': '/settings'
    }
}


API_DICT = {
    'COMMON': {
        'ENABLE_SYSTEM': 'enable_system',
        'DISABLE_SYSTEM': 'disable_system',
        'GET_USER_AREAS': 'get_user_areas',
        'GET_AREA_UNITS': 'get_area_units',
        'GET_FL_LIST': 'get_fl_list',
        'GET_ALL_FL_LIST': 'get_all_fl_list',
        'GET_FL_DETAILS_MASTER_DATA': 'get_fl_details_master_data',
        'GET_CAR_SEAL_MASTER_DATA': 'get_car_seal_master_data',
        'GET_FL_SUGGESTION_DATA': 'get_fl_suggestion_data',
        'CHECK_FOR_ACTIVE_REQUEST': 'check_for_active_request',
        'GET_LAST_CAR_SEAL_INDEX': 'get_last_car_seal_index'
    },
    'SCR': {
        'GET_FL_LEAD_CONFIRMATION_LIST': 'get_fl_lead_confirmation_list',
        'GET_SCR_REQUEST_LIST': 'get_scr_request_list',
        'GET_SCR_REQUEST_DETAILS': 'get_scr_request_details',
        'GET_FL_PROPERTY_HISTORY': 'get_fl_property_history/<int:record_id>',
        'SCR_REQUEST': 'scr_request',
        'SHE_REVIEW': 'she_review',
        'OIMS_ADMIN_APPROVAL': 'oims_admin_approval',
        'AREA_BTM_APPROVAL': 'area_btm_approval',
        'CREATE_RA': 'create_reliability_assessment',
        'CREATE_LEAD_CONFIRMATION': 'lead_confirmation',
        'UPDATE_LEAD_CONFIRMATION': 'lead_confirmation/<int:record_id>',
        'UPDATE_FL_PROPERTY': 'function_location/<int:record_id>',
        'DISCARD_FORM': 'discard_form'
    },
    'COD': {
        'GET_COD_REQUEST_LIST': 'get_cod_request_list',
        'GET_COD_REQUEST_DETAILS': 'get_cod_request_details',
        'CREATE_COD_REQUEST': 'create_cod_request',
        'INITIAL_DEFEAT_APPROVAL': 'initial_defeat_approval',
        'DEFEAT_EXTENSION_APPROVAL': 'defeat_extension_approval',
        'CREATE_DEFEAT_ACKNOWLEDGEMENT': 'create_defeat_acknowledgement',
        'LONG_TERM_DEFEAT': 'long_term_defeat',
        'DISCARD_FORM': 'discard_form'
    },
    'PMDR': {
        'GET_PMDR_REQUEST_LIST': 'get_pmdr_request_list',
        'GET_UNIT_WORK_ORDER_DETAILS': 'get_unit_work_order_details',
        'GET_PMDR_REQUEST_DETAILS': 'get_pmdr_request_details',
        'CREATE_PMDR_REQUEST': 'create_pmdr_request',
        'CREATE_INITIAL_DEFERMENT': 'create_initial_deferment',
        'AREA_BTM_APPROVAL': 'area_btm_approval',
        'DEFERMENT_EXTENSION': 'deferment_extension',
        'GM_REAPPROVAL': 'gm_reapproval',
        'CREATE_JOB_SCHEDULER_REQUEST': 'create_job_scheduler_request',
        'DISCARD_FORM': 'discard_form'
    },
    'CSVR': {
        'GET_CAR_SEAL_LIST': 'car_seal',
        'GET_CSVR_REQUEST_DETAILS': 'get_csvr_request_details',
        'CREATE_CSVR_REQUEST': 'create_csvr_request',
        'OCL_REVIEW': 'ocl_review',
        'AREA_BTM_APPROVAL': 'area_btm_approval',
        'DISCARD_FORM': 'discard_form'
    },
    'ATTACHMENTS': {
        'GET_ATTACHMENT': 'get_attachment/<int:document_id>',
        'UPLOAD_ATTACHMENT': 'upload_attachment',
        'DELETE_ATTACHMENT': 'delete_attachment/<int:document_id>'
    },
    'PENDING_ACTIONS': {
        'GET_PENDING_ACTIONS': 'pending_action',
        'CREATE_PENDING_ACTION': 'pending_action'
    },
    'DASHBOARDS': {
        'GET_SCR_REQUEST_COUNT': 'get_scr_request_count',
        'GET_ACTIVE_COD_REQUEST_COUNT': 'get_active_cod_request_count',
        'GET_ACTIVE_PMDR_REQUEST_COUNT': 'get_active_pmdr_request_count'
    },
    'MASTERS': {
        'USERS': {
            'GET_USER': 'user/<int:record_id>',
            'GET_USER_LIST': 'user',
            'ADD_USER': 'user',
            'UPDATE_USER': 'user/<int:record_id>',
            'UPDATE_USER_STATUS': 'update_user_status/<int:record_id>',
            'DELETE_USERS': 'delete_users',

            'GET_SHIFT': 'shift/<int:shift_id>',
            'GET_SHIFT_LIST': 'shift',
            'CREATE_SHIFT': 'shift',
            'UPDATE_SHIFT': 'shift/<int:shift_id>',
            'DELETE_SHIFT': 'shift/<int:shift_id>',

            'ROLES_AND_PERMISSIONS': {
                'GET_ROLE_LIST': 'role',
                'GET_ROLE': 'role/<int:record_id>',
                'GET_PERMISSIONS': 'get_permissions',
                'CREATE_ROLE': 'role',
                'UPDATE_ROLE': 'role/<int:record_id>',
                'DELETE_ROLE': 'role/<int:record_id>'
            },

            'USER_ACTIVE_WORKFLOW': {
                'GET_USER_ACTIVE_WORKFLOW': 'get_user_active_work_flow'
            }
        },
        'PLANT': {
            # Master SCD URLs.
            'GET_MASTER_FL_LIST': 'get_master_fl_list',

            # Defeat Reasons URLs.
            'GET_REASON_LIST': 'reason',
            'CREATE_REASON': 'reason',
            'UPDATE_REASON': 'reason/<int:record_id>',
            'DELETE_REASON': 'reason/<int:record_id>',

            'CATEGORY': {
                'GET_CATEGORY_LIST': 'category',
                'GET_CATEGORY_WORK_CENTERS': 'get_category_work_centers/<int:record_id>',
                'CREATE_CATEGORY': 'category',
                'UPDATE_CATEGORY': 'category/<int:record_id>',
                'DELETE_CATEGORY': 'category/<int:record_id>'
            },

            'CHANGE_SOURCE': {
                'GET_CHANGE_SOURCE_LIST': 'change_source',
                'CREATE_CHANGE_SOURCE': 'change_source',
                'UPDATE_CHANGE_SOURCE': 'change_source/<int:record_id>',
                'DELETE_CHANGE_SOURCE': 'change_source/<int:record_id>'
            },

            'CAR_SEAL_PROPERTY': {
                'GET_CS_PROPERTY_LIST': 'car_seal_property',
                'CREATE_CS_PROPERTY': 'car_seal_property',
                'UPDATE_CS_PROPERTY': 'car_seal_property/<int:record_id>',
                'DELETE_CS_PROPERTY': 'car_seal_property/<int:record_id>'
            }
        },
        'GROUPS': {
            'GET_GROUP_LIST': 'group',
            'GET_EXCLUDED_GROUP_MEMBER_LIST': 'get_excluded_group_member_list/<int:record_id>',
            'CREATE_GROUP': 'group',
            'UPDATE_GROUP': 'group/<int:record_id>',
            'ADD_USERS': 'add_users/<int:record_id>',
            'DELETE_USERS': 'delete_users/<int:record_id>',
            'DELETE_GROUP': 'group/<int:record_id>'
        },
        'IMPORT_DATA': {
            'GET_TASK_STATUS': 'get_task_status',
            'GET_TASK_TYPE_LIST': 'get_task_type_list',
            'IMPORT_DATA': 'import_data'
        },
        'UPDATE_DATA': {
            'FUNCTION_LOCATION': 'function_location/<int:record_id>'
        },
    },
    'SETTINGS': {
        'GET_SETTING_LIST': 'setting',
        'UPDATE_SETTING': 'setting/<int:record_id>'
    }
}

"""
URL Constants End.
"""


"""
User Constants Start.
"""

USER_STATUS = {
    'DELETED': -1,
    'DISABLED': 0,
    'ACTIVE': 1
}

COD_ACKNOWLEDGEMENT_STATUS = {
    'SUPPRESSED': 0,
    'UNSUPPRESSED': 1
}

"""
User Constants End.
"""


"""
Function Location Constants Start.
"""

DEMOLISHED_FL = -2
DELETED_FL = -1
NON_SCD = 0
SCD = 1

# Function Location Types.
FL_TYPE_DICT = {
    DEMOLISHED_FL: 'Demolished FL',
    DELETED_FL: 'Deleted FL',
    NON_SCD: 'Non SCD',
    SCD: 'SCD'
}

# Function Location Change Types.
FL_ADDITION = 1
FL_DELETION = 2
FL_DEMOLITION = 3

PM_PLAN_SCHEDULE_NOT_REQUIRED = -1
PM_PLAN_NOT_SCHEDULED = 0
PM_PLAN_SCHEDULED = 1

PID_DRAWING_UPDATE_NOT_REEQUIRED = -1
PID_DRAWING_NOT_UPDATED = 0
PID_DRAWING_UPDATED = 1

SAP_SCD_CLASS_UPDATE_NOT_REQUIRED = -1
SAP_SCD_CLASS_NOT_UPDATED = 0
SAP_SCD_CLASS_UPDATED = 1


PM_PLAN_SCHEDULED_ID = 1
PID_DRAWING_UPDATED_ID = 2
SAP_CLASS_UPDATED_ID = 3


CATEGORY_SUGGESTION_ID = 1
SCD_PURPOSE_SUGGESTION_ID = 2
PID_DRAWING_SUGGESTION_ID = 3


"""
Function Location Constants End.
"""


"""
Request Statuses Start.
"""

SUBMITTED = 1
PENDING = 2
APPROVED = 3
REJECTED = 4
RECYCLED = 5
CONFIRMED = 6
ENDORSED = 7
ACKNOWLEDGED = 8
COMPLETED = 9
CLOSED = 10

REQUEST_STATUS_DICT = {
    SUBMITTED: 'Submitted',
    PENDING: 'Pending',
    APPROVED: 'Approved',
    REJECTED: 'Rejected',
    RECYCLED: 'Recycled',
    CONFIRMED: 'Confirmed',
    ENDORSED: 'Endorsed',
    ACKNOWLEDGED: 'Acknowledged',
    COMPLETED: 'Completed',
    CLOSED: 'Closed'
}

DEFEAT_STATUS_DICT = {
    'NEW': 'New',
    'DEFEAT_APPROVED': 'Approved',
    'DEFEAT_EXPIRED': 'Expired',
    'DEFEAT_CLOSED': 'Closed'
}

PMDR_REQUEST_STATUS_DICT = {
    'NEW': 'New',
    'APPROVED': 'Approved',
    'EXPIRED': 'Expired',
    'REJECTED': 'Rejected',
    'CLOSED': 'Closed'
}


TEXTUAL_DATA_SUBMITTED = 1
ATTACHMENTS_SUBMITTED = 2

"""
Request Statuses End.
"""


"""
Tasks Constants Start.
"""

IMPORT_USER_DATA_TASK = 1
IMPORT_MASTER_SCD_DATA_TASK = 2
IMPORT_PM_OVERDUE_DATA_TASK = 3
IMPORT_SCD_PURPOSE_DATA_TASK = 4
IMPORT_WORK_CENTER_CATEGORY_DATA = 5

IMPORT_TASK_DICT = {
    IMPORT_USER_DATA_TASK: 'Employee Data',
    IMPORT_MASTER_SCD_DATA_TASK: 'Master SCD Data',
    IMPORT_PM_OVERDUE_DATA_TASK: 'PM Overdue Data',
    IMPORT_SCD_PURPOSE_DATA_TASK: 'SCD Purpose Data',
    IMPORT_WORK_CENTER_CATEGORY_DATA: 'Work Center Category Data'
}

NO_IMPORT_TASK_IN_PROGRESS = 0
IMPORT_TASK_IN_PROGRESS = 1

DEFAULT_BACKGROUND_TASK_WORKERS = 1
DEFAULT_IMPORT_DATA_WORKERS = 1

"""
Tasks Constants End.
"""


"""
Workflow & Section Codes Start.
"""

SCR = 10
SCR_SECTION_1 = 11
SCR_SECTION_2 = 12
SCR_SECTION_3 = 13
SCR_SECTION_4 = 14
SCR_SECTION_5 = 15
SCR_MAINTENANCE_LEAD_CONFIRMATION = 16
SCR_DRAFTING_LEAD_CONFIRMATION = 17

COD = 20
COD_SECTION_1 = 21
COD_SECTION_2 = 22
COD_SECTION_3 = 23
COD_SECTION_4 = 24
COD_SECTION_5 = 25

PMDR = 30
PMDR_SECTION_1 = 31
PMDR_SECTION_2 = 32
PMDR_SECTION_3 = 33
PMDR_SECTION_4 = 34
PMDR_SECTION_5 = 35
PMDR_SECTION_6 = 36

CSVR = 40
CSVR_SECTION_1 = 41
CSVR_SECTION_2 = 42
CSVR_SECTION_3 = 43

MAIN_DASHBOARD = 101
AREA_SHIFT_TEAM_DASHBOARD = 102
MAINTENANCE_DASHBOARD = 103

MASTERS = 200

WORK_FLOW_DICT = {
    SCR: 'SCR',
    SCR_SECTION_1: 'SCD Change Request',
    SCR_SECTION_2: 'SHE Review',
    SCR_SECTION_3: 'OIMS Admin Approval',
    SCR_SECTION_4: 'Area BTM Approval',
    SCR_SECTION_5: 'Reliability Assessment',
    SCR_MAINTENANCE_LEAD_CONFIRMATION: 'Maintenance Lead Confirmation',
    SCR_DRAFTING_LEAD_CONFIRMATION: 'Drafting Lead Confirmation',

    COD: 'COD',
    COD_SECTION_1: 'Defeat Initiation',
    COD_SECTION_2: 'Initial Defeat Approval',
    COD_SECTION_3: 'Defeat Extension Approval',
    COD_SECTION_4: 'Defeat / Extension Acknowledgement',
    COD_SECTION_5: 'Long Term Defeat',

    PMDR: 'PMDR',
    PMDR_SECTION_1: 'PM Deferment Request',
    PMDR_SECTION_2: 'Initial Deferment',
    PMDR_SECTION_3: 'Area BTM Approval',
    PMDR_SECTION_4: 'Deferment Extension',
    PMDR_SECTION_5: 'Operations GM Re-Approval',
    PMDR_SECTION_6: 'Job Scheduler',

    CSVR: 'CSVR',
    CSVR_SECTION_1: 'CSV Change Request',
    CSVR_SECTION_2: 'OCL Review / Endorsement',
    CSVR_SECTION_3: 'Area BTM Approval',

    MAIN_DASHBOARD: 'Main Dashboard',
    AREA_SHIFT_TEAM_DASHBOARD: 'Area Shift Team Dashboard',
    MAINTENANCE_DASHBOARD: 'Maintenance Dashboard',

    MASTERS: 'Masters'
}

WORK_FLOW_CONTROLLER_DICT = {
    SCR: {
        SCR_SECTION_1: 'scr_record',
        SCR_SECTION_2: 'she_review_record',
        SCR_SECTION_3: 'oims_admin_approval_record',
        SCR_SECTION_4: 'area_btm_approval_record',
        SCR_SECTION_5: 'create_ra_record'
    },
    COD: {
        COD_SECTION_1: 'create_cod_record',
        COD_SECTION_2: 'initial_defeat_approval_record',
        COD_SECTION_3: 'defeat_extension_approval_record',
        COD_SECTION_4: 'create_defeat_acknowledgement_record',
        COD_SECTION_5: 'create_long_term_defeat_record',
    },
    PMDR: {
        PMDR_SECTION_1: 'create_pmdr_record',
        PMDR_SECTION_2: 'create_initial_deferment_record',
        PMDR_SECTION_3: 'area_btm_record',
        PMDR_SECTION_4: 'deferment_extension_record',
        PMDR_SECTION_5: 'create_gm_reapproval_record',
        PMDR_SECTION_6: 'create_job_scheduler_record'
    },
    CSVR: {
        CSVR_SECTION_1: 'create_csvr_record',
        CSVR_SECTION_2: 'ocl_review_record',
        CSVR_SECTION_3: 'create_area_btm_approval_record'
    },
    'BACKGROUND_TASKS': 'background_tasks'
}


"""
Workflow & Section Codes Start.
"""


"""
Roles & Permissions Constants Start.
"""

BASIC_PERMISSIONS_DETAILS = {
    1: {
        'ACTION': 'view',
        'SORT_ORDER': 1
    },
    2: {
        'ACTION': 'create',
        'SORT_ORDER': 2
    },
    3: {
        'ACTION': 'edit',
        'SORT_ORDER': 3
    },
    4: {
        'ACTION': 'close',
        'SORT_ORDER': 4
    },
    5: {
        'ACTION': 'delete',
        'SORT_ORDER': 5
    }
}


MODULE_DETAILS = {
    'SCR': {
        'title': 'SCR Form',
        'identity': 'scr',
        'sort_order': 1
    },
    'COD': {
        'title': 'COD Form',
        'identity': 'cod',
        'sort_order': 2
    },
    'PMDR': {
        'title': 'PMDR Form',
        'identity': 'pmdr',
        'sort_order': 3
    },
    'CSVR': {
        'title': 'CSVR Form',
        'identity': 'csvr',
        'sort_order': 4
    },
    'MAIN DASHBOARD': {
        'title': 'Main Dashboard',
        'identity': 'main_dashboard',
        'sort_order': 5
    },
    'AREA SHIFT TEAM DASHBOARD': {
        'title': 'Area Shift Team Dashboard',
        'identity': 'area_shift_team_dashboard',
        'sort_order': 6
    },
    'MAINTENANCE DASHBOARD': {
        'title': 'Maintenance Dashboard',
        'identity': 'maintenance_dashboard',
        'sort_order': 7
    },
    'MASTERS': {
        'title': 'Masters',
        'identity': 'masters',
        'sort_order': 8
    }
}


SUB_MODULE_DETAILS = {
    SCR: {
        'SECTION_1': {
            'title': 'Section 1',
            'module': 'section_1',
            'permission_id': 1
        },
        'SECTION_2': {
            'title': 'Section 2',
            'module': 'section_2',
            'permission_id': 2
        },
        'SECTION_3': {
            'title': 'Section 3',
            'module': 'section_3',
            'permission_id': 3
        },
        'SECTION_4': {
            'title': 'Section 4',
            'module': 'section_4',
            'permission_id': 4
        },
        'SECTION_5': {
            'title': 'Section 5',
            'module': 'section_5',
            'permission_id': 5
        },
        'MAINTENANCE_LEAD_CONFIRMATION': {
            'title': 'Maintenance Lead Confirmation',
            'module': 'maintenance_lead_confirmation',
            'permission_id': 6
        },
        'DRAFTING_LEAD_CONFIRMATION': {
            'title': 'Drafting Lead Confirmation',
            'module': 'drafting_lead_confirmation',
            'permission_id': 7
        }
    },

    COD: {
        'SECTION_1': {
            'title': 'Section 1',
            'module': 'section_1',
            'permission_id': 8
        },
        'SECTION_2': {
            'title': 'Section 2',
            'module': 'section_2',
            'permission_id': 9
        },
        'SECTION_3': {
            'title': 'Section 3',
            'module': 'section_3',
            'permission_id': 10
        },
        'SECTION_4': {
            'title': 'section_4',
            'module': 'section_4',
            'permission_id': 11
        },
        'SECTION_5': {
            'title': 'Section 5',
            'module': 'section_5',
            'permission_id': 12
        }
    },

    PMDR: {
        'SECTION_1': {
            'title': 'Section 1',
            'module': 'section_1',
            'permission_id': 13
        },
        'SECTION_2': {
            'title': 'Section 2',
            'module': 'section_2',
            'permission_id': 14
        },
        'SECTION_3': {
            'title': 'Section 3',
            'module': 'section_3',
            'permission_id': 15
        },
        'SECTION_4': {
            'title': 'Section 4',
            'module': 'section_4',
            'permission_id': 16
        },
        'SECTION_5': {
            'title': 'Section 5',
            'module': 'section_5',
            'permission_id': 17
        },
        'SECTION_6': {
            'title': 'Section 6',
            'module': 'section_6',
            'permission_id': 18
        }
    },

    CSVR: {
        'SECTION_1': {
            'title': 'Section 1',
            'module': 'section_1',
            'permission_id': 19
        },
        'SECTION_2': {
            'title': 'Section 2',
            'module': 'section_2',
            'permission_id': 20
        },
        'SECTION_3': {
            'title': 'Section 3',
            'module': 'section_3',
            'permission_id': 21
        }
    },

    MAIN_DASHBOARD: {
        'ACTIVE_COD_REQUESTS': {
            'title': 'Active COD Requests',
            'module': 'active_cod_requests',
            'permission_id': 22
        },
        'COD_REQUEST_LIST': {
            'title': 'COD Request List',
            'module': 'cod_request_list',
            'permission_id': 23
        },
        'ACTIVE_PMDR_REQUESTS': {
            'title': 'Active PMDR Requests',
            'module': 'active_pmdr_requests',
            'permission_id': 24
        },
        'PMDR_DEFERMENT_REQUEST_LIST': {
            'title': 'PMDR Deferment Request List',
            'module': 'pm_deferment_request_list',
            'permission_id': 25
        },
        'SCD_CHANGE_REQUESTS': {
            'title': 'SCD Change Requests',
            'module': 'scd_change_requests',
            'permission_id': 26
        },
        'SCD_CHANGE_REQUEST_LIST': {
            'title': 'SCD Change Request',
            'module': 'pm_deferment_request_list',
            'permission_id': 27
        },
        'DOCUMENT_UPDATION_STATUS': {
            'title': 'Document Updation Status',
            'module': 'document_updation_status',
            'permission_id': 28
        },
    },

    AREA_SHIFT_TEAM_DASHBOARD: {
        'ACTIVE_SCD_DEFEATS': {
            'title': 'Active SCD Defeats',
            'module': 'active_scd_defeats',
            'permission_id': 29
        },
        'REASON_WISE_ACTIVE_DEFEATS': {
            'title': 'Reason Wise Active Defeats',
            'module': 'reason_wise_active_defeats',
            'permission_id': 30
        },
        'CATEGORY_WISE_ACTIVE_DEFEATS': {
            'title': 'Category Wise Active Defeats',
            'module': 'category_wise_active_defeats',
            'permission_id': 31
        },
        'COD_REQUEST_LIST': {
            'title': 'COD Request List',
            'module': 'cod_request_list',
            'permission_id': 32
        },
        'CAR_SEAL_VALVES_LIST': {
            'title': 'Car Seal Valves List',
            'module': 'car_seal_valves_list',
            'permission_id': 33
        }
    },

    MAINTENANCE_DASHBOARD: {
        'ACTIVE_PM_DEFERMENTS': {
            'title': 'Active PM Deferments',
            'module': 'active_pm_deferments',
            'permission_id': 34
        },
        'REASON_WISE_ACTIVE_PM_DEFERMENTS': {
            'title': 'Reason Wise Active PM Deferments',
            'module': 'reason_wise_active_pm_deferments',
            'permission_id': 35
        },
        'CATEGORY_WISE_ACTIVE_PM_DEFERMENTS': {
            'title': 'Category Wise Active PM Deferments',
            'module': 'category_wise_active_pm_deferments',
            'permission_id': 36
        },
        'PM_DEFERMENT_REQUEST_LIST': {
            'title': 'PM Deferment Request List',
            'module': 'pm_deferment_request_list',
            'permission_id': 37
        },
        'SCD_AS_FOUND_AS_LEFT_TEST_KPI': {
            'title': 'SCD As Found / As Left Test KPI',
            'module': 'scd_as_found_as_left_test_kpi',
            'permission_id': 38
        },
        'As_Found_As_Left_KPIS_Results': {
            'title': 'As_Found_As_Left_KPIS_Results',
            'module': 'as_found_as_left_kpis_results',
            'permission_id': 39
        }
    },

    MASTERS: {
        'EMPLOYEES': {
            'title': 'Employees',
            'module': 'employees',
            'permission_id': 40
        },
        'GROUPS': {
            'title': 'Groups',
            'module': 'groups',
            'permission_id': 41
        },
        'SHIFTS': {
            'title': 'Shifts',
            'module': 'shifts',
            'permission_id': 42
        },
        'ROLES_AND_PERMISSIONS': {
            'title': 'Roles & Permissions',
            'module': 'roles_and_permissions',
            'permission_id': 43
        },
        'PLANNER_GROUPS': {
            'title': 'Planner Groups',
            'module': 'planner_groups',
            'permission_id': 44
        },
        'WORK_CENTERS': {
            'title': 'Work Centers',
            'module': 'work_centers',
            'permission_id': 45
        },
        'MASTER_SCD_LIST': {
            'title': 'Master SCD List',
            'module': 'master_scd_list',
            'permission_id': 46
        },
        'CATEGORIES': {
            'title': 'Categories',
            'module': 'categories',
            'permission_id': 47
        },
        'DEFEAT_REASONS': {
            'title': 'Defeat Reasons',
            'module': 'defeat_reasons',
            'permission_id': 48
        },
        'DEFERMENT_REASONS': {
            'title': 'Deferment Reasons',
            'module': 'deferment_reasons',
            'permission_id': 49
        },
        'CHANGE_SOURCE': {
            'title': 'Change Source',
            'module': 'change_source',
            'permission_id': 50
        },
        'CAR_SEAL_POSITION': {
            'title': 'Car Seal Position',
            'module': 'car_seal_position',
            'permission_id': 51
        },
        'CAR_SEAL_VALVE': {
            'title': 'Car Seal Valve',
            'module': 'car_seal_valve',
            'permission_id': 52
        },
        'IMPORT_DATA': {
            'title': 'Import Data',
            'module': 'import_data',
            'permission_id': 53
        },
        'UPDATE_DATA': {
            'title': 'Update Data',
            'module': 'update_data',
            'permission_id': 54
        }
    }
}

"""
Roles & Permissions Constants End.
"""


"""
KEY DICTIONARY Start.
"""

KEY_DICT = {
    # Common Keys
    'ID': 'id',
    'AREA': 'area',
    'AREA_ID': 'area_id',
    'UNIT_NUMBER': 'unit_number',
    'UNIT_LIST': 'unit_list',
    'NAME': 'name',
    'UNIQUE_RGN_ID': 'unique_rgn_id',
    'MODE': 'mode',
    'CATEGORY': 'category',
    'CATEGORY_LIST': 'category_list',
    'SCD_PURPOSE': 'scd_purpose',
    'SCD_PURPOSE_UPPER': 'SCD_PURPOSE',
    'SCD_PURPOSE_LIST': 'scd_purpose_list',
    'PID_DRAWING': 'pid_drawing',
    'PID_DRAWING_LIST': 'pid_drawing_list',
    'CHANGE_SOURCE_LOWER': 'change_source',
    'CHANGE_SOURCE_LIST': 'change_source_list',
    'PID_DRAWING_UPPER': 'PID_DRAWING',
    'WORK_ORDER_ID': 'work_order_id',
    'WORK_ORDER_LIST': 'work_order_list',
    'AREA_BTM_APPROVAL_ID': 'area_btm_approval_id',
    'CREATED_ON_UPPER': 'CREATED_ON',
    'CREATED_BY_UPPER': 'CREATED_BY',
    'CHANGED_ON_UPPER': 'CHANGED_ON',
    'CHANGED_BY_UPPER': 'CHANGED_BY',
    'SYSTEM_STATUS_UPPER': 'SYSTEM_STATUS',
    'REMARKS': 'remarks',
    'REMARKS_UPPER': 'REMARKS',
    'COMMENTS': 'comments',
    'RECYCLE_COMMENTS': 'recycle_comments',
    'SECTION_STATUS': 'section_status',
    'WORK_FLOW_SECTION': 'work_flow_section',
    'PENDING_ACTION_LIST': 'pending_action_list',

    # Function Location Keys.
    'FL_DATA': 'fl_data',
    'FL_NAME': 'fl_name',
    'FL_ID': 'fl_id',
    'FUNCTION_LOCATION': 'function_location',
    'FUNCTION_LOCATION_ID': 'function_location_id',
    'FL_DESCRIPTION_UPPER': 'FL_DESCRIPTION',
    'MASTER_FL_LIST': 'master_fl_list',
    'FUNCTION_LOCATION_LIST': 'function_location_list',
    'PM_PLAN_SCHEDULED': 'pm_plan_scheduled',
    'PID_DRAWING_UPDATED': 'pid_drawing_updated',
    'SAP_SCD_CLASS_UPDATED': 'sap_scd_class_updated',
    'ABC_INDICATOR_UPPER': 'ABC_INDICATOR',
    'AREA_BTM_COMMENTS': 'area_btm_comments',
    'CHANGE_HISTORY': 'change_history',

    # SCR Keys.
    'SHE_ENGINEER_COMMENTS': 'she_engineer_comments',
    'OIMS_ADMIN_COMMENTS': 'oims_admin_comments',
    'CONSEQUENCE_LEVEL': 'consequence_level',
    'CONSEQUENCE_OF_FAILURE': 'consequence_of_failure',
    'REQUIRED_AVAILABILITY_TARGET': 'availability_target',
    'PM_PLAN_DEACTIVATED': 'pm_plan_deactivated',
    'RECOMMENDED_PM_INTERVAL': 'recommended_pm_interval',
    'NEW_OR_UPDATED_EQUIPMENT_STRATEGY': 'new_or_updated_equipment_strategy',
    'RECOMMENDED_PLAN': 'recommended_plan',
    'ADDITIONAL_RISK_MANAGEMENT_OPTIONS': 'additional_risk_management_options',
    'SCR_REQUEST_LIST': 'scr_request_list',
    'SHE_REVIEW_ID': 'she_review_id',
    'OIMS_ADMIN_APPROVAL_ID': 'oims_admin_approval_id',
    'RELIABILITY_ASSESSMENT_ID': 'reliability_assessment_id',

    # COD Keys.
    'COD_REQUEST_LIST': 'cod_request_list',
    'INITIAL_DEFEAT_APPROVAL_ID': 'initial_defeat_approval_id',
    'DEFEAT_EXTENSION_APPROVAL_ID': 'defeat_extension_approval_id',
    'LONG_TERM_DEFEAT_ID': 'long_term_defeat_id',
    'LONG_TERM_DEFEAT_REASON': 'long_term_defeat_reason',
    'EXPIRY_DATE': 'expiry_date',
    'DEFEAT_EXTENSION_REASON': 'defeat_extension_reason',

    # PMDR Keys.
    'PMDR_REQUEST_LIST': 'pmdr_request_list',
    'LAST_CAR_SEAL_INDEX': 'last_car_seal_index',
    'FL_WORK_ORDER_DATA': 'fl_work_order_data',
    'INITIAL_DEFERMENT_ID': 'initial_deferment_id',
    'DEFERMENT_EXTENSION_ID': 'deferment_extension_id',
    'OPERATIONS_GM_REAPPROVAL_ID': 'operations_gm_reapproval_id',
    'JOB_SCHEDULER_ID': 'job_scheduler_id',
    'PROPOSED_DEFERMENT_DATE': 'proposed_deferment_date',

    # Car Seal Keys.
    'CAR_SEAL_ID': 'car_seal_id',
    'CAR_SEAL_NO': 'car_seal_no',
    'POSITION_ID': 'position_id',
    'VALVE_ID': 'valve_id',
    'CAR_SEAL_POSITION': 'car_seal_position',
    'CAR_SEAL_VALVE': 'car_seal_valve',

    # CSVR Keys.
    'CS_POSITION_LIST': 'car_seal_position_list',
    'CS_VALVE_LIST': 'cs_valve_list',
    'CAR_SEAL_DATA': 'car_seal_data',
    'CAR_SEAL_LIST': 'car_seal_list',
    'OCL_REVIEW_ID': 'ocl_review_id',

    # User Keys.
    'USER_ID': 'user_id',
    'USER_LIST': 'user_list',
    'SHIFT': 'shift',
    'SHIFT_LIST': 'shift_list',

    # Group Keys.
    'GROUP': 'group',
    'GROUP_ID': 'group_id',
    'GROUP_NAME': 'group_name',
    'GROUP_LIST': 'group_list',
    'GROUP_MEMBER_LIST': 'group_member_list',

    # Planner Group Keys.
    'PLANNER_GROUP_ID': 'planner_group_id',
    'PLANNER_GROUP_UPPER': 'PLANNER_GROUP',
    'PLANNER_GROUP_NAME': 'planner_group_name',

    # Department Keys.
    'DEPARTMENT_ID': 'department_id',
    'DEPARTMENT_NAME': 'department_name',

    # Section Keys.
    'SECTION_ID': 'section_id',
    'SECTION_NAME': 'section_name',

    # Work Center Keys.
    'WORK_CENTER_ID': 'work_center_id',
    'WORK_CENTER_NAME': 'work_center_name',
    'WORK_CENTER_LIST': 'work_center_list',
    'MAIN_WORK_CENTER_UPPER': 'MAIN_WORK_CENTER',

    # Reason Keys.
    'REASON': 'reason',
    'REASON_LIST': 'reason_list',
    'DEFEAT_REASON_ID': 'defeat_reason_id',
    'DEFEAT_REASON': 'defeat_reason',
    'DEFERMENT_REASON_ID': 'deferment_reason_id',
    'DEFERMENT_REASON': 'deferment_reason',

    # Roles & Permissions Keys.
    'ROLE_ID': 'role_id',
    'ROLE_NAME': 'role_name',
    'ROLE_LIST': 'role_list',
    'PERMISSION_ID': 'permission_id',

    # Settings Keys
    'PAGE_SIZE': 'page_size',
    'TEXTBOX_CHARACTER_LIMIT': 'textbox_character_limit',
    'TEXTAREA_CHARACTER_LIMIT': 'textarea_character_limit'
}

"""
KEY DICTIONARY End.
"""


"""
SCR Constants Start.
"""

NORMAL = 1
TRANSIENT = 2

OPERATION_MODE_DICT = {
    NORMAL: 'Normal',
    TRANSIENT: 'Transient'
}

ACTIVE_SCR_REQUESTS = 1

LEAD_UNCONFIRMED_REQUESTS = 1
LEAD_CONFIRMED_REQUESTS = 2

ALL_LEAD_CONFIRMATIONS_NOT_RECEIVED = 0
ALL_LEAD_CONFIRMATIONS_RECEIVED = 1

"""
SCR Constants Start.
"""


"""
COD Constants Start.
"""

ONCE_PER_HOUR = 1
ONCE_PER_SHIFT = 2
OTHERS = 3
NA = 4

MONITORING_FREQUENCY_DICT = {
    ONCE_PER_HOUR: 'Once Per Hour',
    ONCE_PER_SHIFT: 'Once Per Shift',
    OTHERS: 'Others',
    NA: 'Not Applicable'
}

APPROVED_DEFEATS = 1
EXTENDED_DEFEATS = 2
LONG_TERM_DEFEATS = 3

ACTIVE_COD_REQUESTS = 1

"""
COD Constants End.
"""


"""
PMDR Constants Start.
"""

DURING_TURNAROUND = -1
NOT_APPLICABLE = 0
ONE_MONTH = 1
TWO_MONTHS = 2
THREE_MONTHS = 3
SIX_MONTHS = 6
TWELVE_MONTHS = 12
TWENTY_FOUR_MONTHS = 24
THIRTY_SIX_MONTHS = 36

RECOMMENDED_PM_INTERVAL_DICT = {
    DURING_TURNAROUND: 'During Turnaround',
    NOT_APPLICABLE: 'Not Applicable',
    ONE_MONTH: '1 Month',
    TWO_MONTHS: '2 Months',
    THREE_MONTHS: '3 Months',
    SIX_MONTHS: '6 Months',
    TWELVE_MONTHS: '12 Months',
    TWENTY_FOUR_MONTHS: '24 Months',
    THIRTY_SIX_MONTHS: '36 Months'
}

APPROVED_DEFERMENTS = 1
OVERDUE_DEFERMENTS = 2
DEFERRED_DEFERMENTS = 3
DUE_IN_ONE_WEEK = 3
DUE_IN_TWO_WEEKS = 4
DUE_IN_THREE_WEEKS = 5
DUE_IN_FOUR_WEEKS = 6
DUE_IN_FOUR_TO_TWELVE_WEEKS = 7

"""
PMDR Constants End.
"""


"""
Car Seal Constants Start.
"""

PERMANENT_CAR_SEAL_ID = 1
TEMPORARY_CAR_SEAL_ID = 2

CAR_SEAL_TYPE_DICT = {
    PERMANENT_CAR_SEAL_ID: 'PERMANENT',
    TEMPORARY_CAR_SEAL_ID: 'TEMPORARY'
}

CAR_SEAL_DELETION_ID = 0
CAR_SEAL_ADDITION_ID = 1

CHANGE_TYPE_DICT = {
    CAR_SEAL_DELETION_ID: 'DELETION',
    CAR_SEAL_ADDITION_ID: 'ADDITION'
}

CAR_SEAL_POSITION_PROPERTY = 1
CAR_SEAL_VALVE_PROPERTY = 2

CAR_SEAL_PROPERTY_DICT = {
    CAR_SEAL_POSITION_PROPERTY: 'Car Seal Position',
    CAR_SEAL_VALVE_PROPERTY: 'Car Seal Valve'
}

"""
Car Seal Constants End.
"""


"""
Master Groups constants start.
"""

GROUP = 1
PLANNER_GROUP = 2
WORK_CENTER = 3
DEPARTMENT = 4
SECTION = 5

MASTER_GROUP_DICT = {
    GROUP: 'Group',
    PLANNER_GROUP: 'Planner Group',
    WORK_CENTER: 'Work Center',
    DEPARTMENT: 'Department',
    SECTION: 'Section'
}

"""
Master Groups constants end.
"""


"""
Reason constants start.
"""

DEFEAT_REASON = 1
DEFERMENT_REASON = 2

"""
Reason constants end.
"""


"""
Pending Action Statuses Start.
"""

PENDING_ACTION = 0
PENDING_ACTION_COMPLETED = 1

"""
Pending Action Statues End.
"""


"""
Message Constants Start.
"""

INVALID_USERNAME_OR_PASSWORD = 'Invalid user name or password.'
PERMISSION_DENIED = 'You do not have permission to access this module.'

DEFAULT_GET_API_SUCCESS_MESSAGE = 'Data fetched successfully.'
DEFAULT_POST_API_SUCCESS_MESSAGE = 'Data successfully stored in database.'
DEFAULT_UPDATE_API_SUCCESS_MESSAGE = 'Data successfully updated in database.'
DEFAULT_DELETE_API_SUCCESS_MESSAGE = 'Data successfully deleted from database.'
DEFAULT_FAILURE_MESSAGE = 'Unexpected Error.'

CATEGORY_ALREADY_EXISTS = 'A category with this name already exists.'
CATEGORY_DOES_NOT_EXISTS = 'Category does not exits.'

WORK_CENTER_ASSOCIATED_WITH_CATEGORY = 'One or more work centers are present in this category. Please remove all the ' \
                                       'work centers from the category.'

CHANGE_SOURCE_ALREADY_EXISTS = 'This change source already_exists.'
CHANGE_SOURCE_ASSOCIATED_WITH_FUNCTION_LOCATION = 'This change source is associated with one or more function ' \
                                                  'locations. Please remove it from all associated function locations.'
CHANGE_SOURCE_DOES_NOT_EXISTS = 'This change source does not exists.'

CAR_SEAL_POSITION_ALREADY_EXISTS = 'This car seal position already exists.'
CAR_SEAL_POSITION_DOES_NOT_EXISTS = 'This car seal position does not exists.'
CS_POSITION_ASSOCIATED_WITH_CAR_SEAL = 'This car seal position is associated with one or more car seals. Please ' \
                                       'remove it from all associated car seals and then try again.'

CAR_SEAL_VALVE_ALREADY_EXISTS = 'This car seal valve already exists.'
CAR_SEAL_VALVE_DOES_NOT_EXISTS = 'This car seal valve does not exists.'
CS_VALVE_ASSOCIATED_WITH_CAR_SEAL = 'This car seal valve is associated with one or more car seals. Please remove it ' \
                                    'from all associated car seals and then try again.'

DEFEAT_REASON_EXISTS_ERROR_MESSAGE = 'This defeat reason already exists.'
DEFEAT_REASON_NOT_FOUND = 'This defeat reason does not exists.'
DEFEAT_REASON_ASSOCIATED_WITH_REQUEST = 'This defeat reason has been used in one or more of the COD requests and ' \
                                        'therefore cannot be deleted. Only defeat reasons not used in any COD ' \
                                        'requests can be deleted.'

DEFERMENT_REASON_EXISTS_ERROR_MESSAGE = 'This deferment reason already exists.'
DEFERMENT_REASON_NOT_FOUND = 'This deferment reason does not exists.'
DEFERMENT_REASON_ASSOCIATED_WITH_REQUEST = 'This deferment reason has been used in one or more of the PMDR requests ' \
                                           'and therefore cannot be deleted. Only deferment reasons not used in any ' \
                                           'PMDR requests can be deleted.'

GROUP_EXISTS_ERROR_MESSAGE = 'A group with this name already exists.'
GROUP_NOT_FOUND = 'This group does not exists.'
USERS_PRESENT_IN_GROUP = 'There are 1 or more users in the group. Please remove all users from the group.'

PLANNER_GROUP_EXISTS_ERROR_MESSAGE = 'A planner group with this name already exists.'
PLANNER_GROUP_NOT_FOUND = 'This planner group does not exists.'
FL_ASSOCIATED_WITH_PLANNER_GROUP = 'One or more function locations are present in this planner group. Please remove ' \
                                   'all the function locations from the planner group.'
USERS_PRESENT_IN_PLANNER_GROUP = \
    'There are 1 or more users in the planner group. Please remove all users from the planner group.'

WORK_CENTER_EXISTS_ERROR_MESSAGE = 'A work center with this name already exists.'
WORK_CENTER_NOT_FOUND = 'This work center does not exists.'
FL_ASSOCIATED_WITH_WORK_CENTER = 'One or more function locations are present in this work center. Please remove all ' \
                                 'the function locations from the work center.'
USERS_PRESENT_IN_WORK_CENTER = \
    'There are 1 or more users in the work center. Please remove all users from the work center.'

DEPARTMENT_EXISTS_ERROR_MESSAGE = 'A department with this name already exists.'
DEPARTMENT_NOT_FOUND = 'This department does not exists.'
USERS_PRESENT_IN_DEPARTMENT = \
    'There are 1 or more users in the department. Please remove all users from the department.'

SECTION_EXISTS_ERROR_MESSAGE = 'A section with this name already exists.'
SECTION_NOT_FOUND = 'This section does not exists.'
USERS_PRESENT_IN_SECTION = 'There are 1 or more users in the section. Please remove all users from the section.'

SHIFT_ASSIGNED_TO_USERS = 'This shift is assigned to 1 or more users. Please remove all users from this shift.'
ROLE_ASSIGNED_TO_USERS = 'This role is assigned to 1 or more users. Please remove all users from this role.'
ROLE_ALREADY_EXISTS = 'This role already exists.'

NULL_BASIC_START_DATE = 'Work Order Plan Date is null. Hence, due status could not be calculated.'
OVER_DUE_MESSAGE_DICT = {
    'OVERDUE': 'Overdue',
    'ONE_WEEK': 'One Week to Overdue.',
    'TWO_WEEKS': 'Two Weeks to Overdue.',
    'THREE_WEEKS': 'Three Weeks to Overdue.',
    'FOUR_WEEKS': 'Four Weeks to Overdue.',
    'TWO_MONTHS': '2 Months to Overdue.',
    'THREE_MONTHS': '3 Months to Overdue.',
    'NOT_OVERDUE': 'Preventive Maintenance is not overdue.'
}

REQUEST_EXISTS_FOR_FL = 'There is an active request involving 1 or more function locations of this request.'
ADDITION_REQUEST_EXISTS_FOR_CAR_SEAL = 'One or more car seals in this addition request already exists.'
CAR_SEALS_DO_NOT_EXISTS = 'One or more car seals do not exists.'
DELETION_REQUEST_EXISTS_FOR_CAR_SEAL = 'Deletion request for one or more car seals already exists.'
REQUEST_NOT_ALLOWED = 'This request is not allowed.'
INCORRECT_REQUEST_TYPE_FOR_REQUEST = 'The API type is incorrect for the request.'

FILE_NOT_FOUND = 'File not found.'
MAX_UPLOAD_LIMIT_REACHED = 'Max. upload limit reached. You have already uploaded 5 attachments.'

UNIT_NOT_SELECTED = 'Please select unit number.'

"""
Message Constants End.
"""


"""
File Constants Start.
"""

SUPPORTED_FILE_FORMAT = 1
UNSUPPORTED_FILE_FORMAT = 2
FILE_CODES = {SUPPORTED_FILE_FORMAT: 'Supported File Format', UNSUPPORTED_FILE_FORMAT: 'Unsupported File Format'}

MIME_TYPES = {
    # Text MIME Types.
    'txt': ['text/plain'],
    'doc': ['application/msword'],
    'docm': ['application/vnd.ms-word.document.macroEnabled.12'],
    'docx': ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
    'dot': ['application/msword'],
    'dotx': ['application/vnd.openxmlformats-officedocument.wordprocessingml.template'],

    # Spreadsheet MIME Types.
    'csv': ['text/csv', 'application/vnd.ms-excel'],
    'xlsx': ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
    'xlsm': ['application/vnd.ms-excel.sheet.macroEnabled.12'],
    'xlsb': ['application/vnd.ms-excel.sheet.binary.macroEnabled.12'],
    'xltx': ['application/vnd.openxmlformats-officedocument.spreadsheetml.template'],
    'xltm': ['application/vnd.ms-excel.template.macroEnabled.12'],
    'xls': ['application/vnd.ms-excel'],
    'xlt': ['application/excel'],
    'xml': ['application/xml'],
    'xlam': ['application/vnd.ms-excel.addin.macroEnabled.12'],

    # Image MIME Types.
    'jpg': ['image/jpeg'],
    'jpeg': ['image/jpeg'],
    'png': ['image/png'],

    # PDF MIME Type.
    'pdf': ['application/pdf']
}

"""
File Constants End.
"""


"""
Settings Constants Start.
"""

PAGE_SIZE_ID = 1
TEXTBOX_CHARACTER_LIMIT_ID = 2
TEXTAREA_CHARACTER_LIMIT_ID = 3
ATTACHMENTS_PER_SECTION_ID = 4
MAX_ATTACHMENT_SIZE_ID = 5

SETTING_DICT = {
    PAGE_SIZE_ID: 'Page Size',
    TEXTBOX_CHARACTER_LIMIT_ID: 'Textbox Character Limit',
    TEXTAREA_CHARACTER_LIMIT_ID: 'Textarea Character Limit',
    ATTACHMENTS_PER_SECTION_ID: 'Attachments Per Section',
    MAX_ATTACHMENT_SIZE_ID: 'Maximum Attachment Size (in MB)'
}

"""
Settings Constants End.
"""


"""
Miscellaneous Constants Start.
"""

SUCCESS = True
FAILURE = False

GET = 'GET'
POST = 'POST'
PUT = 'PUT'
PATCH = 'PATCH'
DELETE = 'DELETE'

ISO_DATE_FORMAT = '%Y-%m-%d'

NEW = 'New'
NOT_APPLICABLE = 'N / A'

BACKGROUND_TASK_SLEEP_INTERVAL = 5

APPEND_MODE = 'a'
READ_MODE = 'r'
WRITE_MODE = 'w'

SYSTEM_STATUS_CNF = 'CNF'
NULL = 'null'
NAN = 'nan'
UNDEFINED = 'undefined'


START_DATE_ASCENDING = 1
START_DATE_DESCENDING = 2
REQUEST_ID_ASCENDING = 3
REQUEST_ID_DESCENDING = 4

CAR_SEAL_NO_ASCENDING = 1
CAR_SEAL_NO_DESCENDING = 2

ASCENDING = 1
DESCENDING = 2

NOT_DELETED = 0
DELETED = 1

"""
Miscellaneous Constants End.
"""
