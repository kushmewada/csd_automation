from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

import constants
import settings
from logging_module import setup_logger

db = SQLAlchemy()
args_logger = setup_logger(constants.LOGGER_DICT['ARGS_LOG'])
resp_logger = setup_logger(constants.LOGGER_DICT['RESPONSE_LOG'])


def init_app():
    app = Flask(__name__)
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.config['CORS_ORIGIN_ALLOW_ALL'] = True
    app.config['ALLOWED_HOSTS'] = "*"
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.TRACK_MODIFICATIONS
    app.config['UPLOAD_PATH'] = settings.DOCUMENT_STORE_PATH
    app.config['UPLOAD_EXTENSIONS'] = settings.UPLOAD_EXTENSIONS

    # app.config['SQLALCHEMY_DATABASE_URI'] = \
    #     'mssql+pyodbc://{}:{}@{}:{}/{}?driver={}'.format(
    #         settings.USER_NAME,
    #         settings.PASSWORD,
    #         settings.MS_SQL_SERVER_IP,
    #         settings.MS_SQL_SERVER_PORT,
    #         settings.DATABASE_NAME,
    #         settings.ODBC_DRIVER
    #     )
    app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://{}:@localhost/{}".format(settings.USER_NAME,
                                                                                      settings.DATABASE_NAME)

    if settings.SERVER_MODE == settings.SERVER_DEBUG_MODE:
        CORS(app)
    else:
        CORS(app, resources={r"/": {"origins": "*"}})
    db.init_app(app)
    return app
