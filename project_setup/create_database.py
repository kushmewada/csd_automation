# TODO: Add code to create database.
from common_packages import db, init_app
from scd_automation_app import *  # This import is used to import tables in this file.


app = init_app()
with app.app_context():
    db.create_all()
