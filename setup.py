import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="scd-automation-python",
    version="0.0.1",
    author="Ravi Bhalani",
    author_email="ravi.b@peerbits.com",
    description="SCD Automation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(where="project_setup"),
    install_requires=[
        "Flask-SQLAlchemy==2.5.1",
        "SQLAlchemy==1.4.10",
        "wheel==0.36.2",
        "pyodbc==4.0.30",
        "Flask-Cors==3.0.10",
        "Flask-SocketIO==5.0.1"
    ],
    python_requires=">=3.6",
)
