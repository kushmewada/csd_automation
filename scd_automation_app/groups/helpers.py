import constants
from .models import (
    TblGroupMaster,
    TblUserGroups,
    TblPlannerGroupMaster,
    TblUserPlannerGroups,
    TblWorkCenterMaster,
    TblUserWorkCenters,
    TblDepartmentMaster,
    TblUserDepartments,
    TblSectionMaster,
    TblUserSections
)


def get_group_data(
        group_type=None,
        group_table_required=True,
        user_group_table_required=True,
        group_id_field_required=True,
        group_name_field_required=True,
        group_exists_error_message_required=False,
        group_not_found_error_message_required=False,
        user_error_message_required=False,
        fl_error_message_required=False
):
    if group_type == constants.GROUP:
        group_table, user_group_table = TblGroupMaster, TblUserGroups
        group_id_field, group_name_field = constants.KEY_DICT['GROUP_ID'], constants.KEY_DICT['GROUP_NAME']
        group_exists_error_message = constants.GROUP_EXISTS_ERROR_MESSAGE
        group_not_found_error_message = constants.GROUP_NOT_FOUND
        user_error_message = constants.USERS_PRESENT_IN_GROUP
        fl_error_message = None
    elif group_type == constants.PLANNER_GROUP:
        group_table, user_group_table = TblPlannerGroupMaster, TblUserPlannerGroups
        group_id_field, group_name_field = \
            constants.KEY_DICT['PLANNER_GROUP_ID'], constants.KEY_DICT['PLANNER_GROUP_NAME']
        group_exists_error_message = constants.PLANNER_GROUP_EXISTS_ERROR_MESSAGE
        group_not_found_error_message = constants.PLANNER_GROUP_NOT_FOUND
        user_error_message = constants.USERS_PRESENT_IN_PLANNER_GROUP
        fl_error_message = constants.FL_ASSOCIATED_WITH_PLANNER_GROUP
    elif group_type == constants.WORK_CENTER:
        group_table, user_group_table = TblWorkCenterMaster, TblUserWorkCenters,
        group_id_field, group_name_field = constants.KEY_DICT['WORK_CENTER_ID'], constants.KEY_DICT['WORK_CENTER_NAME']
        group_exists_error_message = constants.WORK_CENTER_EXISTS_ERROR_MESSAGE
        group_not_found_error_message = constants.WORK_CENTER_NOT_FOUND
        user_error_message = constants.USERS_PRESENT_IN_WORK_CENTER
        fl_error_message = constants.FL_ASSOCIATED_WITH_WORK_CENTER
    elif group_type == constants.DEPARTMENT:
        group_table, user_group_table = TblDepartmentMaster, TblUserDepartments
        group_id_field, group_name_field = constants.KEY_DICT['DEPARTMENT_ID'], constants.KEY_DICT['DEPARTMENT_NAME']
        group_exists_error_message = constants.DEPARTMENT_EXISTS_ERROR_MESSAGE
        group_not_found_error_message = constants.DEPARTMENT_NOT_FOUND
        user_error_message = constants.USERS_PRESENT_IN_DEPARTMENT
        fl_error_message = None
    elif group_type == constants.SECTION:
        group_table, user_group_table = TblSectionMaster, TblUserSections
        group_id_field, group_name_field = constants.KEY_DICT['SECTION_ID'], constants.KEY_DICT['SECTION_NAME']
        group_exists_error_message = constants.SECTION_EXISTS_ERROR_MESSAGE
        group_not_found_error_message = constants.SECTION_NOT_FOUND
        user_error_message = constants.USERS_PRESENT_IN_SECTION
        fl_error_message = None

    return_list = list()
    if group_table_required:
        return_list.append(group_table)
    if user_group_table_required:
        return_list.append(user_group_table)
    if group_id_field_required:
        return_list.append(group_id_field)
    if group_name_field_required:
        return_list.append(group_name_field)
    if group_exists_error_message_required:
        return_list.append(group_exists_error_message)
    if group_not_found_error_message_required:
        return_list.append(group_not_found_error_message)
    if user_error_message_required:
        return_list.append(user_error_message)
    if fl_error_message_required:
        return_list.append(fl_error_message)
    return tuple(return_list)
