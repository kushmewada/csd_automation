import constants
from common_packages import db
from scd_automation_app.common.models import TblAreaMaster, TblFunctionLocationMaster
from scd_automation_app.samref.common_functions import get_list_from_comma_separated_str
from scd_automation_app.samref.common_helpers import (
    format_list_api_data, check_for_existing_master_record, add_or_update_record, create_response
)
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster, TblRoleMaster, TblUserAreas, TblUserRoles
from .helpers import get_group_data


def get_group_records(request_data=None, logger=None):
    filters = list()

    group_type = int(request_data['group_type'])
    group_table, user_group_table, group_id_field, group_name_field = get_group_data(group_type=group_type)

    search_text = request_data.get('search_text', None)
    if search_text:
        filters.append(getattr(group_table, group_name_field).contains(search_text))

    records = group_table.query.outerjoin(
        user_group_table, getattr(user_group_table, group_id_field) == group_table.id
    ).add_columns(
        group_table.id,
        getattr(user_group_table, group_id_field),
        getattr(group_table, group_name_field),
        user_group_table.user_id
    ).filter(
        *filters
    ).order_by(
        getattr(group_table, group_name_field)
    ).all()

    group_user_data_dict = dict()
    for record in records:
        group_id = getattr(record, constants.KEY_DICT['ID'])
        if group_id in group_user_data_dict and record.user_id:
            group_user_data_dict[group_id]['user_count'] += 1
        else:
            group_user_data_dict[group_id] = {
                'id': group_id, 'name': getattr(record, group_name_field), 'user_count': 1 if record.user_id else 0
            }

    data = format_list_api_data(
        records=group_user_data_dict,
        list_name=constants.KEY_DICT['GROUP_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_excluded_group_member_records(request_data=None, group_id=None, logger=None):
    excluded_member_data_dict = dict()
    included_member_id_list = list()
    group_type = int(request_data['group_type'])
    group_table, user_group_table, group_id_field, group_name_field = get_group_data(group_type=group_type)

    user_id_list = [record.id for record in TblUserMaster.query.all()]
    user_group_user_id_list = [record.user_id for record in user_group_table.query.all()]
    ungrouped_user_id_list = list(set(user_id_list) - set(user_group_user_id_list))

    excluded_employee_records = TblUserMaster.query.join(
        user_group_table, getattr(user_group_table, 'user_id') == TblUserMaster.id
    ).join(
        TblUserAreas, TblUserAreas.user_id == TblUserMaster.id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUserAreas.area_id
    ).join(
        TblUserRoles, TblUserRoles.user_id == TblUserMaster.id
    ).join(
        TblRoleMaster, TblRoleMaster.id == TblUserRoles.role_id
    ).add_columns(
        getattr(user_group_table, group_id_field),
        getattr(user_group_table, 'user_id'),
        TblAreaMaster.area,
        TblRoleMaster.role_name,
        TblUserMaster.name
    ).all()

    for record in excluded_employee_records:
        area = None
        role = None
        group_id_list = None
        user_id = record.user_id

        if getattr(record, group_id_field) == group_id:
            included_member_id_list.append(user_id)
            included_member_id_list = list(set(included_member_id_list))

        if user_id in excluded_member_data_dict:
            area = excluded_member_data_dict[user_id]['area']
            role = excluded_member_data_dict[user_id]['role']
            group_id_list = excluded_member_data_dict[user_id]['group_id_list']

            if record.area not in area:
                area += ', ' + record.area

            if record.role_name not in role:
                role += ', ' + record.role_name

            if getattr(record, group_id_field) not in group_id_list:
                group_id_list.append(getattr(record, group_id_field))

        excluded_member_data_dict[user_id] = {
            'id': user_id,
            'name': record.name,
            'area': area if area else record.area,
            'role': role if role else record.role_name,
            'group_id_list': group_id_list if group_id_list else [getattr(record, group_id_field)]
        }

    excluded_member_data_dict = {
        excluded_member_data_dict[record]['id']: {
            'id': excluded_member_data_dict[record]['id'],
            'name': excluded_member_data_dict[record]['name'],
            'area': excluded_member_data_dict[record]['area'],
            'role': excluded_member_data_dict[record]['role'],
        }
        for record in excluded_member_data_dict if group_id not in excluded_member_data_dict[record]['group_id_list']
    }

    if ungrouped_user_id_list:
        ungrouped_employee_records = TblUserMaster.query.join(
            TblUserAreas, TblUserAreas.user_id == TblUserMaster.id
        ).join(
            TblAreaMaster, TblAreaMaster.id == TblUserAreas.area_id
        ).join(
            TblUserRoles, TblUserRoles.user_id == TblUserMaster.id
        ).join(
            TblRoleMaster, TblRoleMaster.id == TblUserRoles.role_id
        ).add_columns(
            TblAreaMaster.area,
            TblRoleMaster.role_name,
            TblUserAreas.user_id,
            TblUserMaster.name
        ).filter(
            TblUserMaster.id.in_(ungrouped_user_id_list)
        ).all()

        for record in ungrouped_employee_records:
            excluded_member_data_dict[record.user_id] = {
                'id': record.user_id, 'name': record.name, 'area': record.area, 'role': record.role_name,
            }

    data = format_list_api_data(
        records=excluded_member_data_dict,
        sort_key=constants.KEY_DICT['NAME'],
        list_name=constants.KEY_DICT['USER_LIST'],
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def create_or_update_group_record(request_data=None, group_id=None, request_type=None, logger=None):
    group_name = request_data['group_name'].title()
    group_type = int(request_data['group_type'])
    group_table, group_name_field, error_message = get_group_data(
        group_type=group_type,
        user_group_table_required=False,
        group_id_field_required=False,
        group_exists_error_message_required=True
    )

    status = check_for_existing_master_record(
        table=group_table, id_value=group_id, key_field=group_name_field, key_field_value=group_name)
    if status:
        return create_response(status=constants.FAILURE, error_message=error_message)

    add_or_update_record(table=group_table, id_value=group_id, field=group_name_field, field_value=group_name)
    if request_type == constants.POST:
        response_params = {'post_success_response': True}
    else:
        response_params = {'put_success_response': True}

    return create_response(**response_params)


def add_members(request_data=None, group_id=None, logger=None):
    group_type = int(request_data['group_type'])
    user_group_table, group_id_field = get_group_data(
        group_type=group_type, group_table_required=False, group_name_field_required=False
    )
    records = [user_group_table(**{group_id_field: group_id, 'user_id': user_id})
               for user_id in get_list_from_comma_separated_str(comma_separated_str=request_data['user_ids'])]
    db.session.add_all(records)
    db.session.commit()
    return create_response(post_success_response=True)


def delete_members(request_data=None, group_id=None, logger=None):
    group_type = int(request_data['group_type'])
    user_group_table, group_id_field = get_group_data(
        group_type=group_type, group_table_required=False, group_name_field_required=False
    )
    for user_id in get_list_from_comma_separated_str(comma_separated_str=request_data['user_ids']):
        user_group_table.query.filter_by(**{group_id_field: group_id, 'user_id': user_id}).delete()
    db.session.commit()
    return create_response(delete_success_response=True)


def delete_group_record(request_data=None, group_id=None, logger=None):
    group_type = int(request_data['group_type'])
    group_table, user_group_table, group_id_field, group_error_msg, user_error_msg, fl_error_msg = get_group_data(
        group_type=group_type,
        group_name_field_required=False,
        group_not_found_error_message_required=True,
        user_error_message_required=True,
        fl_error_message_required=True
    )

    record = group_table.query.filter_by(**{'id': group_id}).first()
    if record is None:
        return create_response(status=constants.FAILURE, error_message=group_error_msg)

    records = user_group_table.query.filter_by(**{group_id_field: group_id}).all()
    if records:
        return create_response(status=constants.FAILURE, error_message=user_error_msg)

    if group_type in [constants.PLANNER_GROUP, constants.WORK_CENTER]:
        record = TblFunctionLocationMaster.query.filter_by(**{group_id_field: group_id}).first()
        if record:
            return create_response(status=constants.FAILURE, error_message=fl_error_msg)

    group_table.query.filter_by(id=group_id).delete()
    db.session.commit()
    return create_response(delete_success_response=True)
