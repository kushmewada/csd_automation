from common_packages import db
from scd_automation_app.samref.base_model import BaseModel


class TblGroupMaster(BaseModel):
    group_name = db.Column(db.String(250), nullable=False, unique=True)


class TblUserGroups(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    group_id = db.Column(db.Integer, db.ForeignKey('tbl_group_master.id'), nullable=False)


class TblPlannerGroupMaster(BaseModel):
    planner_group_name = db.Column(db.String(250), nullable=False, unique=True)


class TblUserPlannerGroups(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    planner_group_id = db.Column(db.Integer, db.ForeignKey('tbl_planner_group_master.id'), nullable=False)


class TblWorkCenterMaster(BaseModel):
    work_center_name = db.Column(db.String(250), nullable=False, unique=True)
    category_id = db.Column(db.Integer, db.ForeignKey('tbl_category_master.id'), nullable=True)


class TblUserWorkCenters(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    work_center_id = db.Column(db.Integer, db.ForeignKey('tbl_work_center_master.id'), nullable=False)


class TblDepartmentMaster(BaseModel):
    department_name = db.Column(db.String(250), nullable=False, unique=True)


class TblUserDepartments(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('tbl_department_master.id'), nullable=False)


class TblSectionMaster(BaseModel):
    section_name = db.Column(db.String(250), primary_key=True, nullable=False, unique=True)


class TblUserSections(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    section_id = db.Column(db.Integer, db.ForeignKey('tbl_section_master.id'), nullable=False)
