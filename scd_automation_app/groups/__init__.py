from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_group_records,
    get_excluded_group_member_records,
    create_or_update_group_record,
    add_members,
    delete_members,
    delete_group_record
)

blueprint_name = constants.BLUE_PRINT_DICT['MASTERS']['GROUPS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['MASTERS']['GROUPS']['URL_PREFIX']
group_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['GROUP_LOG'])


api_get_group_list = constants.API_DICT['MASTERS']['GROUPS']['GET_GROUP_LIST']
api_get_excluded_group_member_list = constants.API_DICT['MASTERS']['GROUPS']['GET_EXCLUDED_GROUP_MEMBER_LIST']
api_create_group = constants.API_DICT['MASTERS']['GROUPS']['CREATE_GROUP']
api_update_group = constants.API_DICT['MASTERS']['GROUPS']['UPDATE_GROUP']
api_add_users = constants.API_DICT['MASTERS']['GROUPS']['ADD_USERS']
api_delete_users = constants.API_DICT['MASTERS']['GROUPS']['DELETE_USERS']
api_delete_group = constants.API_DICT['MASTERS']['GROUPS']['DELETE_GROUP']


@group_bp.route(api_get_group_list, methods=[constants.GET])
def get_group_list():
    return base_api(request=request, api_name=api_get_group_list, api=get_group_records, logger=logger)


@group_bp.route(api_get_excluded_group_member_list, methods=[constants.GET])
def get_excluded_group_member_list(record_id):
    return base_api(request=request, api_name=api_get_excluded_group_member_list,
                    api=get_excluded_group_member_records, api_params={'group_id': int(record_id)},
                    logger=logger)


@group_bp.route(api_create_group, methods=[constants.POST])
@group_bp.route(api_update_group, methods=[constants.PUT])
def create_or_update_group(record_id=None):
    return base_api(request=request, api_name=constants.KEY_DICT['GROUP'], api=create_or_update_group_record,
                    api_params={'group_id': int(record_id) if record_id else None, 'request_type': request.method},
                    logger=logger)


@group_bp.route(api_add_users, methods=[constants.POST])
def add_users(record_id):
    return base_api(request=request, api_name=api_add_users, api=add_members,
                    api_params={'group_id': int(record_id) if record_id else None}, logger=logger)


@group_bp.route(api_delete_users, methods=[constants.DELETE])
def delete_users(record_id):
    return base_api(request=request, api_name=api_delete_users, api=delete_members,
                    api_params={'group_id': int(record_id) if record_id else None}, logger=logger)


@group_bp.route(api_delete_group, methods=[constants.DELETE])
def delete_group(record_id):
    return base_api(request=request, api_name=api_delete_group, api=delete_group_record,
                    api_params={'group_id': int(record_id) if record_id else None}, logger=logger)
