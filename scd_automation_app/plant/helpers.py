import constants
from scd_automation_app.common.models import TblFunctionLocationMaster
from scd_automation_app.pmdr.models import TblPmdrMaster
from .models import TblCarSealPositionMaster, TblCarSealValveMaster
from .models import TblDefeatReasonMaster, TblDefermentReasonMaster


def get_reason_data(
        reason_type=None,
        reason_table_required=True,
        reason_field_required=True,
        reason_exists_error_message_required=False,
        reason_not_found_error_message_required=False,
        request_master_table_required=False
):
    if reason_type == constants.DEFEAT_REASON:
        reason_table = TblDefeatReasonMaster
        reason_field = constants.KEY_DICT['DEFEAT_REASON']
        reason_exists_error_message = constants.DEFEAT_REASON_EXISTS_ERROR_MESSAGE
        reason_not_found_error_message = constants.DEFEAT_REASON_NOT_FOUND
        request_master_table = TblFunctionLocationMaster
        request_master_table_reason_id = constants.KEY_DICT['DEFEAT_REASON_ID']
        reason_associated_with_request_error_message = constants.DEFEAT_REASON_ASSOCIATED_WITH_REQUEST
    elif reason_type == constants.DEFERMENT_REASON:
        reason_table = TblDefermentReasonMaster
        reason_field = constants.KEY_DICT['DEFERMENT_REASON']
        reason_exists_error_message = constants.DEFERMENT_REASON_EXISTS_ERROR_MESSAGE
        reason_not_found_error_message = constants.DEFERMENT_REASON_NOT_FOUND
        request_master_table = TblPmdrMaster
        request_master_table_reason_id = constants.KEY_DICT['DEFERMENT_REASON_ID']
        reason_associated_with_request_error_message = constants.DEFERMENT_REASON_ASSOCIATED_WITH_REQUEST

    return_list = list()
    if reason_table_required:
        return_list.append(reason_table)
    if reason_field_required:
        return_list.append(reason_field)
    if reason_exists_error_message_required:
        return_list.append(reason_exists_error_message)
    if reason_not_found_error_message_required:
        return_list.append(reason_not_found_error_message)
    if request_master_table_required:
        return_list.extend([
            request_master_table, request_master_table_reason_id, reason_associated_with_request_error_message
        ])
    return tuple(return_list)


def get_cs_property_data(
        property_type=None,
        property_field_required=True,
        list_name_required=False,
        property_exists_message_required=False,
        property_does_not_exists_message_required=False,
        property_associated_with_cs_message_required=False,
        property_id_field_required=False
):
    if property_type == constants.CAR_SEAL_POSITION_PROPERTY:
        property_table = TblCarSealPositionMaster
        property_field = constants.KEY_DICT['CAR_SEAL_POSITION']
        list_name = constants.KEY_DICT['CS_POSITION_LIST']
        property_exists_message = constants.CAR_SEAL_POSITION_ALREADY_EXISTS
        property_does_not_exists_message = constants.CAR_SEAL_POSITION_DOES_NOT_EXISTS
        property_associated_with_cs_message = constants.CS_POSITION_ASSOCIATED_WITH_CAR_SEAL
        property_id_field = constants.KEY_DICT['POSITION_ID']
    elif property_type == constants.CAR_SEAL_VALVE_PROPERTY:
        property_table = TblCarSealValveMaster
        property_field = constants.KEY_DICT['CAR_SEAL_VALVE']
        list_name = constants.KEY_DICT['CS_VALVE_LIST']
        property_exists_message = constants.CAR_SEAL_VALVE_ALREADY_EXISTS
        property_does_not_exists_message = constants.CAR_SEAL_VALVE_DOES_NOT_EXISTS
        property_associated_with_cs_message = constants.CS_VALVE_ASSOCIATED_WITH_CAR_SEAL
        property_id_field = constants.KEY_DICT['VALVE_ID']

    return_list = [property_table]
    if property_field_required:
        return_list.append(property_field)
    if list_name_required:
        return_list.append(list_name)
    if property_exists_message_required:
        return_list.append(property_exists_message)
    if property_does_not_exists_message_required:
        return_list.append(property_does_not_exists_message)
    if property_associated_with_cs_message_required:
        return_list.append(property_associated_with_cs_message)
    if property_id_field_required:
        return_list.append(property_id_field)
    return tuple(return_list)
