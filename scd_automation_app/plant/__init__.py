from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_master_fl_records,
    get_reason_records,
    create_or_update_reason_record,
    delete_reason_record,

    get_category_records,
    get_category_work_center_records,
    create_or_update_category,
    delete_category_record,

    get_change_source_records,
    create_or_update_change_source_record,
    delete_change_source_record,

    get_cs_property_records,
    create_or_update_cs_property_record,
    delete_cs_property_record
)

blueprint_name = constants.BLUE_PRINT_DICT['MASTERS']['PLANT']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['MASTERS']['PLANT']['URL_PREFIX']
plant_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['PLANT_LOG'])


"""
Master Function Location List APIs Start.
"""


api_get_master_fl_list = constants.API_DICT['MASTERS']['PLANT']['GET_MASTER_FL_LIST']


@plant_bp.route(api_get_master_fl_list, methods=[constants.GET])
def get_master_fl_list():
    return base_api(request=request, api_name=api_get_master_fl_list, api=get_master_fl_records, logger=logger)


"""
Master Function Location List APIs End.
"""


"""
Reasons APIs start.
"""


api_get_reason_list = constants.API_DICT['MASTERS']['PLANT']['GET_REASON_LIST']
api_create_reason = constants.API_DICT['MASTERS']['PLANT']['CREATE_REASON']
api_update_reason = constants.API_DICT['MASTERS']['PLANT']['UPDATE_REASON']
api_delete_reason = constants.API_DICT['MASTERS']['PLANT']['DELETE_REASON']


@plant_bp.route(api_get_reason_list, methods=[constants.GET])
def get_reason_list():
    return base_api(request=request, api_name=api_get_reason_list, api=get_reason_records, logger=logger)


@plant_bp.route(api_create_reason, methods=[constants.POST])
@plant_bp.route(api_update_reason, methods=[constants.PUT])
def create_or_update_reason(record_id=None):
    return base_api(request=request, api_name=constants.KEY_DICT['REASON'], api=create_or_update_reason_record,
                    api_params={'request_type': request.method, 'reason_id': int(record_id) if record_id else None},
                    logger=logger)


@plant_bp.route(api_delete_reason, methods=[constants.DELETE])
def delete_reason(record_id):
    return base_api(request=request, api_name=constants.KEY_DICT['REASON'], api=delete_reason_record,
                    api_params={'reason_id': int(record_id)}, logger=logger)


"""
Reasons APIs end.
"""


"""
Category APIs start.
"""


api_get_category_list = constants.API_DICT['MASTERS']['PLANT']['CATEGORY']['GET_CATEGORY_LIST']
api_get_category_work_centers = constants.API_DICT['MASTERS']['PLANT']['CATEGORY']['GET_CATEGORY_WORK_CENTERS']
api_create_category = constants.API_DICT['MASTERS']['PLANT']['CATEGORY']['CREATE_CATEGORY']
api_update_category = constants.API_DICT['MASTERS']['PLANT']['CATEGORY']['UPDATE_CATEGORY']
api_delete_category = constants.API_DICT['MASTERS']['PLANT']['CATEGORY']['DELETE_CATEGORY']


@plant_bp.route(api_get_category_list, methods=[constants.GET])
def get_category_list():
    return base_api(request=request, api_name=api_get_category_list, api=get_category_records, logger=logger)


@plant_bp.route(api_get_category_work_centers, methods=[constants.GET])
def get_category_work_centers(record_id):
    return base_api(request=request, api_name=api_get_category_work_centers, api=get_category_work_center_records,
                    api_params={'category_id': int(record_id)}, logger=logger)


@plant_bp.route(api_create_category, methods=[constants.POST])
@plant_bp.route(api_update_category, methods=[constants.PUT])
def category(record_id=None):
    category_id, api_name = (int(record_id), api_update_category) if record_id else (None, api_create_category)
    return base_api(request=request, api_name=api_name, api=create_or_update_category,
                    api_params={'category_id': category_id}, logger=logger)


@plant_bp.route(api_delete_category, methods=[constants.DELETE])
def delete_category(record_id):
    return base_api(request=request, api_name=api_delete_category, api=delete_category_record,
                    api_params={'category_id': int(record_id)}, logger=logger)


"""
Category APIs end.
"""


"""
Change Source APIs Start.
"""


api_get_change_source_list = constants.API_DICT['MASTERS']['PLANT']['CHANGE_SOURCE']['GET_CHANGE_SOURCE_LIST']
api_create_change_source = constants.API_DICT['MASTERS']['PLANT']['CHANGE_SOURCE']['CREATE_CHANGE_SOURCE']
api_update_change_source = constants.API_DICT['MASTERS']['PLANT']['CHANGE_SOURCE']['UPDATE_CHANGE_SOURCE']
api_delete_change_source = constants.API_DICT['MASTERS']['PLANT']['CHANGE_SOURCE']['DELETE_CHANGE_SOURCE']


@plant_bp.route(api_get_change_source_list, methods=[constants.GET])
def get_change_source_list():
    return base_api(request=request, api_name=api_get_change_source_list, api=get_change_source_records, logger=logger)


@plant_bp.route(api_create_change_source, methods=[constants.POST])
@plant_bp.route(api_update_change_source, methods=[constants.PUT])
def create_or_update_change_source(record_id=None):
    if record_id:
        api_name, api_params = api_update_change_source, {'change_source_id': int(record_id)}
    else:
        api_name, api_params = api_create_change_source, None
    return base_api(request=request, api_name=api_name, api=create_or_update_change_source_record,
                    api_params=api_params, logger=logger)


@plant_bp.route(api_delete_change_source, methods=[constants.DELETE])
def delete_change_source(record_id):
    return base_api(request=request, api_name=api_delete_change_source, api=delete_change_source_record,
                    api_params={'change_source_id': int(record_id)}, logger=logger)


"""
Change Source APIs End.
"""


"""
Change Source APIs Start.
"""


api_get_cs_property_list = constants.API_DICT['MASTERS']['PLANT']['CAR_SEAL_PROPERTY']['GET_CS_PROPERTY_LIST']
api_create_cs_property = constants.API_DICT['MASTERS']['PLANT']['CAR_SEAL_PROPERTY']['CREATE_CS_PROPERTY']
api_update_cs_property = constants.API_DICT['MASTERS']['PLANT']['CAR_SEAL_PROPERTY']['UPDATE_CS_PROPERTY']
api_delete_cs_property = constants.API_DICT['MASTERS']['PLANT']['CAR_SEAL_PROPERTY']['DELETE_CS_PROPERTY']


@plant_bp.route(api_get_cs_property_list, methods=[constants.GET])
def get_cs_property_list():
    return base_api(request=request, api_name=api_get_cs_property_list, api=get_cs_property_records, logger=logger)


@plant_bp.route(api_create_cs_property, methods=[constants.POST])
@plant_bp.route(api_update_cs_property, methods=[constants.PUT])
def create_or_update_cs_property(record_id=None):
    if record_id:
        api_name, api_params = api_update_cs_property, {'cs_property_id': int(record_id)}
    else:
        api_name, api_params = api_create_cs_property, None
    return base_api(request=request, api_name=api_name, api=create_or_update_cs_property_record, api_params=api_params,
                    logger=logger)


@plant_bp.route(api_delete_cs_property, methods=[constants.DELETE])
def delete_cs_property(record_id):
    return base_api(request=request, api_name=api_delete_cs_property, api=delete_cs_property_record,
                    api_params={'cs_property_id': int(record_id)}, logger=logger)


"""
Change Source APIs End.
"""
