from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblDefeatReasonMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    defeat_reason = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String(250), nullable=False)


class TblDefermentReasonMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    deferment_reason = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String(250), nullable=False)


class TblChangeSourceMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    change_source = db.Column(db.String(250), unique=True, nullable=False)


class TblCarSealPositionMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    car_seal_position = db.Column(db.String(250), nullable=False)


class TblCarSealValveMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    car_seal_valve = db.Column(db.String(250), nullable=False)
