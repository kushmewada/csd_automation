from sqlalchemy.orm import aliased

import constants
from common_packages import db
from scd_automation_app.common.models import (
    TblAreaMaster,
    TblUnitMaster,
    TblFunctionLocationMaster,
    TblCategoryMaster,
    TblScdPurposeMaster,
    TblPidDrawingMaster,
    TblCarSealMaster
)
from scd_automation_app.groups.models import TblPlannerGroupMaster, TblWorkCenterMaster
from scd_automation_app.samref.common_functions import (
    convert_timestamp_to_date, get_key_value_pair_list, paginate_records
)
from scd_automation_app.samref.common_helpers import add_or_update_record, delete_record_by_id
from scd_automation_app.samref.common_helpers import (
    format_list_api_data, check_for_existing_master_record, create_response
)
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster
from .helpers import get_reason_data, get_cs_property_data
from .models import TblChangeSourceMaster

"""
Master FL List Controller Functions Start. 
"""


def get_master_fl_records(request_data=None, logger=None):
    filter_fields = []
    fl_record_list = dict()
    page = int(request_data['page'])
    search_text = request_data.get('search_text', None)

    if search_text:
        filter_fields.append(TblFunctionLocationMaster.fl_name.contains(search_text.upper()))

    creation_user_alias = aliased(TblUserMaster)
    modification_user_alias = aliased(TblUserMaster)
    records = TblFunctionLocationMaster.query.join(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblFunctionLocationMaster.pid_drawing_id
    ).outerjoin(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).outerjoin(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).outerjoin(
        TblPlannerGroupMaster, TblPlannerGroupMaster.id == TblFunctionLocationMaster.planner_group_id
    ).outerjoin(
        creation_user_alias, creation_user_alias.id == TblFunctionLocationMaster.created_by
    ).outerjoin(
        modification_user_alias, modification_user_alias.id == TblFunctionLocationMaster.modified_by
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblCategoryMaster.category,
        TblScdPurposeMaster.scd_purpose,
        TblPidDrawingMaster.pid_drawing,
        TblPlannerGroupMaster.planner_group_name,
        TblWorkCenterMaster.work_center_name,
        TblWorkCenterMaster.category_id,
        creation_user_alias.id.label('creation_user_id'),
        creation_user_alias.name.label('creation_user_name'),
        modification_user_alias.id.label('modification_user_id'),
        modification_user_alias.name.label('modification_user_name'),
        TblFunctionLocationMaster.id,
        TblFunctionLocationMaster.unit_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.fl_type,
        TblFunctionLocationMaster.work_center_id,
        TblFunctionLocationMaster.scd_purpose_id,
        TblFunctionLocationMaster.pid_drawing_id,
        TblFunctionLocationMaster.planner_group_id,
        TblFunctionLocationMaster.work_center_id,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.equipments_protected,
        TblFunctionLocationMaster.main_working_category,
        TblFunctionLocationMaster.pm_plan_scheduled,
        TblFunctionLocationMaster.pid_drawing_updated,
        TblFunctionLocationMaster.sap_scd_class_updated,
        TblFunctionLocationMaster.abc_indicator,
        TblFunctionLocationMaster.system_status,
        TblFunctionLocationMaster.remarks,
        TblFunctionLocationMaster.created_time,
        TblFunctionLocationMaster.created_by,
        TblFunctionLocationMaster.modified_time,
        TblFunctionLocationMaster.modified_by
    ).filter(
        *filter_fields
    ).order_by(
        getattr(TblFunctionLocationMaster, constants.KEY_DICT['FL_NAME'])
    ).all()

    for record in records:
        fl_record_list[record.id] = {
            'id': record.id,
            'area': {'id': record.area_id, 'name': record.area},
            'unit': {'id': record.unit_id, 'name': record.unit_number},
            'function_location': record.fl_name,
            'description': record.fl_description,
            'scd': record.fl_type,
            'category': {'id': record.category_id, 'name': record.category},
            'critical_safe_guard': record.critical_safeguard,
            'scd_purpose': {'id': record.scd_purpose_id, 'name': record.scd_purpose},
            'pid_drawing': {'id': record.pid_drawing_id, 'name': record.pid_drawing},
            'planner_group': {'id': record.planner_group_id, 'name': record.planner_group_name},
            'main_work_center': {'id': record.work_center_id, 'name': record.work_center_name},
            'equipment_protected': record.equipments_protected,
            'main_working_category': record.main_working_category,
            'pm_plan_scheduled': record.pm_plan_scheduled,
            'pid_drawing_updated': record.pid_drawing_updated,
            'sap_scd_class_updated': record.sap_scd_class_updated,
            'abc_indicator': record.abc_indicator,
            'system_status': record.system_status,
            'remarks': record.remarks,
            'created_on': convert_timestamp_to_date(timestamp=record.created_time),
            'created_by': {
                'id': record.creation_user_id, 'name': record.creation_user_name
            } if record.creation_user_id else None,
            'changed_on': convert_timestamp_to_date(timestamp=record.modified_time),
            'changed_by': {
                'id': record.modification_user_id, 'name': record.modification_user_name
            } if record.modification_user_id else None
        }

    data = format_list_api_data(
        records=fl_record_list,
        list_name=constants.KEY_DICT['MASTER_FL_LIST'],
        page=page,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


"""
Master FL List Controller Functions End.
"""


"""
Reason controller functions start.
"""


def get_reason_records(request_data=None, logger=None):
    reason_type = int(request_data['reason_type'])
    page = int(request_data['page'])
    search_text = request_data.get('search_text')

    reason_table, reason_field = get_reason_data(reason_type=reason_type)
    reason_data = {
        record.id: {'id': record.id, 'name': getattr(record, reason_field), 'description': record.description}
        for record in reason_table.query.all()
    }

    data = format_list_api_data(
        records=reason_data,
        sort_key=constants.KEY_DICT['NAME'],
        list_name=constants.KEY_DICT['REASON_LIST'],
        search_key=constants.KEY_DICT['NAME'],
        search_text=search_text,
        page=page,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def create_or_update_reason_record(request_data=None, reason_id=None, request_type=None, logger=None):
    reason_type = int(request_data['reason_type'])
    reason_table, reason_field, reason_exists_error_msg = get_reason_data(
        reason_type=reason_type, reason_exists_error_message_required=True
    )
    reason = request_data['reason'].title()
    description = request_data['description'].capitalize()

    if request_type == constants.POST:
        record = check_for_existing_master_record(table=reason_table, key_field=reason_field, key_field_value=reason)
        if record:
            return create_response(status=constants.FAILURE, error_message=reason_exists_error_msg)
        record = reason_table(**{reason_field: reason, 'description': description})
        db.session.add(record)
        db.session.commit()
        response_params = {'post_success_response': True}

    elif request_type == constants.PUT:
        record = reason_table.query.filter(
            getattr(reason_table, 'id') != reason_id,
            getattr(reason_table, reason_field) == reason
        ).first()
        if record:
            return create_response(status=constants.FAILURE, error_message=reason_exists_error_msg)

        reason_table.query.filter_by(id=reason_id).update(dict({reason_field: reason, 'description': description}))
        db.session.commit()
        response_params = {'patch_success_response': True}
    return create_response(**response_params)


def delete_reason_record(request_data=None, reason_id=None, logger=None):
    reason_type = int(request_data['reason_type'])

    reason_table, reason_field, reason_not_found_error_msg, request_master_table, request_master_table_reason_id, \
        reason_associated_with_request_error_message = get_reason_data(
            reason_type=reason_type, reason_not_found_error_message_required=True, request_master_table_required=True
        )

    reason_obj = reason_table.query.filter_by(id=reason_id)
    reason_record = reason_obj.first()
    if reason_record is None:
        return create_response(status=constants.FAILURE, error_message=reason_not_found_error_msg)

    record = request_master_table.query.filter_by(**{request_master_table_reason_id: reason_id}).first()
    if record:
        return create_response(status=constants.FAILURE, error_message=reason_associated_with_request_error_message)
    reason_obj.delete()
    db.session.commit()
    return create_response(delete_success_response=True)


"""
Reason controller functions end.
"""


"""
Category controller functions start.
"""


def get_category_records(request_data=None, logger=None):
    category_dict = dict()
    page = int(request_data['page'])
    search_text = request_data.get('search_text')

    records = TblCategoryMaster.query.outerjoin(
        TblWorkCenterMaster, TblWorkCenterMaster.category_id == TblCategoryMaster.id
    ).add_columns(
        TblCategoryMaster.id,
        TblCategoryMaster.category,
        TblWorkCenterMaster.category_id
    ).all()

    for record in records:
        if record.id not in category_dict:
            category_dict[record.id] = {'id': record.id, 'name': record.category, 'work_center_count': 0}
        category_dict[record.id]['work_center_count'] += 1

    data = format_list_api_data(
        records=category_dict,
        sort_key=constants.KEY_DICT['NAME'],
        list_name=constants.KEY_DICT['CATEGORY_LIST'],
        search_key=constants.KEY_DICT['NAME'],
        search_text=search_text,
        page=page,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_category_work_center_records(request_data=None, category_id=None, logger=None):
    filters = [TblWorkCenterMaster.category_id == category_id]
    search_text = request_data.get('search_text')

    if search_text:
        filters.append(TblWorkCenterMaster.work_center_name.contains(search_text))

    records = TblWorkCenterMaster.query.filter(*filters).order_by(TblWorkCenterMaster.work_center_name).all()
    category_work_center_list = [{'id': record.id, 'name': record.work_center_name} for record in records]
    data = paginate_records(
        page=int(request_data['page']),
        record_list=category_work_center_list,
        list_name=constants.KEY_DICT['WORK_CENTER_LIST'],
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def create_or_update_category(request_data=None, category_id=None, request_type=None, logger=None):
    category = request_data['category'].title()
    status = check_for_existing_master_record(table=TblCategoryMaster, id_value=category_id,
                                              key_field=constants.KEY_DICT['CATEGORY'], key_field_value=category)
    if status:
        return create_response(status=constants.FAILURE, error_message=constants.CATEGORY_ALREADY_EXISTS)

    add_or_update_record(
        table=TblCategoryMaster, id_value=category_id, field=constants.KEY_DICT['CATEGORY'], field_value=category
    )
    if request_type == constants.POST:
        response_params = {'post_success_response': True}
    else:
        response_params = {'put_success_response': True}
    return create_response(**response_params)


def delete_category_record(request_data=None, category_id=None, logger=None):
    record = TblWorkCenterMaster.query.filter_by(id=category_id).first()
    if record:
        return create_response(status=constants.FAILURE, error_message=constants.WORK_CENTER_ASSOCIATED_WITH_CATEGORY)

    status = delete_record_by_id(table=TblCategoryMaster, id_value=category_id)
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.CATEGORY_DOES_NOT_EXISTS)

    return create_response(delete_success_response=constants.SUCCESS)


"""
Category controller functions end.
"""


"""
Change Source controller functions start.
"""


def get_change_source_records(request_data=None, logger=None):
    filter_fields = list()
    page = int(request_data['page'])
    search_text = request_data.get('search_text', None)

    if search_text:
        filter_fields.append(TblChangeSourceMaster.change_source.contains(search_text.title()))

    records = TblChangeSourceMaster.query.filter(*filter_fields).order_by(TblChangeSourceMaster.change_source).all()
    change_source_list = get_key_value_pair_list(
        record_list=records, record_key_2=constants.KEY_DICT['CHANGE_SOURCE_LOWER']
    )
    data = format_list_api_data(
        records=change_source_list,
        is_dict=False,
        list_name=constants.KEY_DICT['CHANGE_SOURCE_LIST'],
        page=page,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def create_or_update_change_source_record(request_data=None, change_source_id=None, logger=None):
    change_source = request_data['change_source'].title()
    status = check_for_existing_master_record(
        table=TblChangeSourceMaster,
        id_value=change_source_id,
        key_field=constants.KEY_DICT['CHANGE_SOURCE_LOWER'],
        key_field_value=request_data['change_source'].title()
    )
    if status:
        return create_response(status=constants.FAILURE, error_message=constants.CHANGE_SOURCE_ALREADY_EXISTS)

    add_or_update_record(
        table=TblChangeSourceMaster,
        id_value=change_source_id,
        field=constants.KEY_DICT['CHANGE_SOURCE_LOWER'],
        field_value=change_source
    )
    if change_source_id:
        response_params = {'put_success_response': True}
    else:
        response_params = {'post_success_response': True}
    return create_response(**response_params)


def delete_change_source_record(request_data=None, change_source_id=None, logger=None):
    record = TblFunctionLocationMaster.query.filter_by(change_source_id=change_source_id).first()
    if record:
        return create_response(
            status=constants.FAILURE, error_message=constants.CHANGE_SOURCE_ASSOCIATED_WITH_FUNCTION_LOCATION
        )

    status = delete_record_by_id(table=TblChangeSourceMaster, id_value=change_source_id)
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.CHANGE_SOURCE_DOES_NOT_EXISTS)

    return create_response(delete_success_response=constants.SUCCESS)


"""
Change Source controller functions end.
"""


"""
Car Seal Property controller functions start.
"""


def get_cs_property_records(request_data=None, logger=None):
    filters = list()
    page = int(request_data['page'])
    search_text = request_data.get('search_text')

    property_table, property_field, list_name = get_cs_property_data(
        property_type=int(request_data['property_type']), list_name_required=True
    )

    if search_text:
        filters.append(getattr(property_table, property_field).contains(search_text))

    records = property_table.query.filter(*filters).order_by(getattr(property_table, property_field)).all()
    data = format_list_api_data(
        records=get_key_value_pair_list(record_list=records, record_key_2=property_field),
        is_dict=False,
        list_name=list_name,
        page=page,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def create_or_update_cs_property_record(request_data=None, cs_property_id=None, logger=None):
    property_table, property_field, property_exists_message = get_cs_property_data(
        property_type=int(request_data['property_type']), property_exists_message_required=True
    )

    status = check_for_existing_master_record(
        table=property_table,
        id_value=cs_property_id,
        key_field=property_field,
        key_field_value=request_data['property']
    )
    if status:
        return create_response(status=constants.FAILURE, error_message=property_exists_message)

    add_or_update_record(
        table=property_table, id_value=cs_property_id, field=property_field, field_value=request_data['property']
    )
    if cs_property_id:
        response_params = {'put_success_response': True}
    else:
        response_params = {'post_success_response': True}
    return create_response(**response_params)


def delete_cs_property_record(request_data=None, cs_property_id=None, logger=None):
    property_table, property_does_not_exists_message, property_associated_with_cs_message, property_id_field = \
        get_cs_property_data(
            property_type=int(request_data['property_type']),
            property_field_required=False,
            property_does_not_exists_message_required=True,
            property_associated_with_cs_message_required=True,
            property_id_field_required=True
        )

    record = TblCarSealMaster.query.filter(getattr(TblCarSealMaster, property_id_field) == cs_property_id).first()
    if record:
        return create_response(status=constants.FAILURE, error_message=property_associated_with_cs_message)

    status = delete_record_by_id(table=property_table, id_value=cs_property_id)
    if status is constants.FAILURE:
        return create_response(status=status, error_message=property_does_not_exists_message)

    return create_response(delete_success_response=constants.SUCCESS)


"""
Car Seal Property controller functions end.
"""
