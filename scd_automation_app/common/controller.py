import constants
from scd_automation_app.cod.models import TblCodMaster, TblCodFunctionLocations
from scd_automation_app.groups.models import TblWorkCenterMaster
from scd_automation_app.plant.models import TblDefermentReasonMaster
from scd_automation_app.samref.common_functions import (
    get_key_value_pair_list, create_list_from_key_value_pair, paginate_records
)
from scd_automation_app.samref.common_helpers import create_response, format_list_api_data
from scd_automation_app.scr.models import TblScrMaster, TblScrFunctionLocations
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserAreas
from .helpers import get_suggestion_values
from .models import (
    TblAreaMaster,
    TblUnitMaster,
    TblFunctionLocationMaster,
    TblPidDrawingMaster,
    TblCategoryMaster,
    TblScdPurposeMaster,
    TblOperationModeMaster,
    TblCarSealMaster
)


def enable_system_record(request_data=None, logger=None):
    return create_response(post_success_response=True)


def disable_system_record(request_data=None, logger=None):
    return create_response(post_success_response=True)


def get_user_area_records(request_data=None, logger=None):
    filters = list()
    user_id = request_data.get('user_id', None)

    if user_id:
        filters.append(TblUserAreas.user_id == int(user_id))

    user_area_records = TblUserAreas.query.join(
        TblAreaMaster, TblAreaMaster.id == TblUserAreas.area_id
    ).add_columns(
        TblAreaMaster.id,
        TblAreaMaster.area
    ).filter(
        *filters
    ).all()

    area_list = get_key_value_pair_list(record_list=user_area_records, record_key_2=constants.KEY_DICT['AREA'])
    return create_response(get_success_response=True, data=area_list)


def get_area_unit_records(request_data=None, logger=None):
    filter_fields = [TblUnitMaster.area_id == int(request_data['area_id'])]

    search_text = request_data.get('search_text', None)
    if search_text:
        filter_fields.append(TblUnitMaster.unit_number.contains(search_text))

    unit_records = TblUnitMaster.query.join(
        TblAreaMaster, TblUnitMaster.area_id == TblAreaMaster.id
    ).filter(
        *filter_fields
    ).order_by(
        TblUnitMaster.unit_number
    ).all()
    unit_list = get_key_value_pair_list(record_list=unit_records, record_key_2=constants.KEY_DICT['UNIT_NUMBER'])
    data = paginate_records(
        page=int(request_data['page']),
        record_list=unit_list,
        list_name=constants.KEY_DICT['UNIT_LIST'],
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_fl_records(request_data=None, logger=None):
    filter_fields = list()
    unit_id = request_data.get('unit_id', None)
    request_type = int(request_data['request_type']) if request_data.get('request_type', None) else None
    change_type = int(request_data['change_type']) if request_data.get('change_type', None) else None
    category_id = request_data.get('category_id', None)
    search_text = request_data.get('search_text', None)

    if unit_id is None:
        return create_response(status=constants.FAILURE, error_message=constants.UNIT_NOT_SELECTED)
    filter_fields.append(TblUnitMaster.id == int(unit_id))

    if search_text:
        filter_fields.append(TblFunctionLocationMaster.fl_name.contains(search_text))

    if change_type == constants.FL_ADDITION:
        fl_type_list = [constants.DEMOLISHED_FL, constants.DELETED_FL, constants.NON_SCD]
    elif change_type in [constants.FL_DELETION, constants.FL_DEMOLITION]:
        fl_type_list = [constants.SCD]
    else:
        fl_type_list = list(constants.FL_TYPE_DICT.keys())
    filter_fields.append(TblFunctionLocationMaster.fl_type.in_(fl_type_list))

    if category_id:
        filter_fields.append(TblCategoryMaster.id == int(category_id))

    fl_objects = TblFunctionLocationMaster.query.join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).outerjoin(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblFunctionLocationMaster.pid_drawing_id
    ).outerjoin(
        TblOperationModeMaster, TblOperationModeMaster.id == TblFunctionLocationMaster.operation_mode_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).add_columns(
        TblCategoryMaster.category,
        TblWorkCenterMaster.category_id,
        TblWorkCenterMaster.work_center_name,
        TblScdPurposeMaster.scd_purpose,
        TblPidDrawingMaster.pid_drawing,
        TblOperationModeMaster.mode,
        TblFunctionLocationMaster.id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.scd_purpose_id,
        TblFunctionLocationMaster.pid_drawing_id,
        TblFunctionLocationMaster.operation_mode_id,
        TblFunctionLocationMaster.work_center_id,
    ).filter(
        *filter_fields
    ).order_by(
        TblFunctionLocationMaster.fl_name
    ).all()

    if request_type in [constants.SCR, constants.COD]:
        if request_type == constants.SCR:
            master_table, fl_table = TblScrMaster, TblScrFunctionLocations
        else:
            master_table, fl_table = TblCodMaster, TblCodFunctionLocations

        active_request_fl_records = master_table.query.join(
            fl_table, fl_table.request_group_number == master_table.request_group_number
        ).filter(
            master_table.request_status.not_in([constants.REJECTED, constants.CLOSED])
        ).add_columns(
            fl_table.function_location_id
        ).all()
        active_request_fl_id_list = [record.function_location_id for record in active_request_fl_records]
    else:
        active_request_fl_id_list = []

    fl_list = [
        {
            'id': fl_object.id,
            'name': fl_object.fl_name,
            'description': fl_object.fl_description,
            'work_center': {'id': fl_object.work_center_id, 'name': fl_object.work_center_name},
            'category': {'id': fl_object.category_id, 'name': fl_object.category},
            'scd_purpose': {'id': fl_object.scd_purpose_id, 'name': fl_object.scd_purpose},
            'pid_drawing': {'id': fl_object.pid_drawing_id, 'name': fl_object.pid_drawing},
            'mode': {
                'id': fl_object.operation_mode_id if fl_object.operation_mode_id else constants.NORMAL,
                'name': fl_object.mode if fl_object.mode else constants.OPERATION_MODE_DICT[constants.NORMAL]
            },
            'critical_safeguard': fl_object.critical_safeguard,
        }
        for fl_object in fl_objects if fl_object.id not in active_request_fl_id_list
    ]
    data = format_list_api_data(
        records=fl_list,
        is_dict=False,
        list_name=constants.KEY_DICT['FUNCTION_LOCATION_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_all_fl_records(request_data=None, logger=None):
    filter_fields = list()

    search_text = request_data.get('search_text', None)
    if search_text:
        filter_fields.append(TblFunctionLocationMaster.fl_name.contains(search_text))

    records = TblFunctionLocationMaster.query.filter_by(
        fl_type=constants.SCD
    ).filter(
        *filter_fields
    ).order_by(
        TblFunctionLocationMaster.fl_name
    ).all()

    data = paginate_records(
        page=int(request_data['page']),
        record_list=get_key_value_pair_list(record_list=records, record_key_2=constants.KEY_DICT['FL_NAME']),
        list_name=constants.KEY_DICT['FUNCTION_LOCATION_LIST'],
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_fl_details_master_records(request_data=None, logger=None):
    deferment_reason_objects = TblDefermentReasonMaster.query.order_by(TblDefermentReasonMaster.deferment_reason).all()
    work_center_objects = TblWorkCenterMaster.query.order_by(TblWorkCenterMaster.work_center_name).all()

    master_data = {
        'operation_mode_list': create_list_from_key_value_pair(record_dict=constants.OPERATION_MODE_DICT),
        'recommended_pm_interval': create_list_from_key_value_pair(record_dict=constants.RECOMMENDED_PM_INTERVAL_DICT),
        'deferment_reason_list':
            get_key_value_pair_list(record_list=deferment_reason_objects, record_key_2='deferment_reason'),
        'monitoring_frequency': create_list_from_key_value_pair(record_dict=constants.MONITORING_FREQUENCY_DICT),
        'main_work_center_list':
            get_key_value_pair_list(record_list=work_center_objects, record_key_2='work_center_name'),
        'cod_request_status_data': {
            'start_date': constants.NOT_APPLICABLE,
            'defeat_status': constants.NOT_APPLICABLE,
            'defeat_since_initial_defeat': constants.NOT_APPLICABLE,
            'expiring_on': constants.NOT_APPLICABLE
        },
        'pmdr_request_status_data': {'deferment_status': constants.NEW}
    }
    return create_response(get_success_response=True, data=master_data)


def get_car_seal_master_records(request_data=None, logger=None):
    cs_master_data = {
        'car_seal_type': create_list_from_key_value_pair(record_dict=constants.CAR_SEAL_TYPE_DICT),
        'change_type': create_list_from_key_value_pair(record_dict=constants.CHANGE_TYPE_DICT)
    }
    return create_response(get_success_response=True, data=cs_master_data)


def get_fl_suggestion_records(request_data=None, logger=None):
    master_data_type = int(request_data['master_data_type'])
    if master_data_type == constants.CATEGORY_SUGGESTION_ID:
        table, key, list_name = TblCategoryMaster, constants.KEY_DICT['CATEGORY'], constants.KEY_DICT['CATEGORY_LIST']
    elif master_data_type == constants.SCD_PURPOSE_SUGGESTION_ID:
        table, key, list_name = \
            TblScdPurposeMaster, constants.KEY_DICT['SCD_PURPOSE'], constants.KEY_DICT['SCD_PURPOSE_LIST']
    elif master_data_type == constants.PID_DRAWING_SUGGESTION_ID:
        table, key, list_name = \
            TblPidDrawingMaster, constants.KEY_DICT['PID_DRAWING'], constants.KEY_DICT['PID_DRAWING_LIST']

    data = paginate_records(
        page=int(request_data['page']),
        record_list=get_suggestion_values(table=table, key=key, search_key=request_data.get('search_text')),
        list_name=list_name,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def check_for_active_request_record(request_data=None, logger=None):
    fl_ids = request_data['function_location_ids']
    fl_id_list = [int(_id) for _id in fl_ids.split(',')]

    work_flow_id = int(request_data['work_flow_id'])
    if work_flow_id == constants.SCR:
        table = TblScrMaster
        fl_table = TblScrFunctionLocations
    else:
        table = TblCodMaster
        fl_table = TblCodFunctionLocations

    records = table.query.join(
        fl_table, fl_table.request_group_number == table.request_group_number
    ).filter(
        table.request_status.not_in([constants.REJECTED, constants.CLOSED]),
        fl_table.function_location_id.in_(fl_id_list)
    ).all()

    if records:
        status, message, error_message = False, None, constants.REQUEST_EXISTS_FOR_FL
    else:
        status, message, error_message = True, constants.DEFAULT_GET_API_SUCCESS_MESSAGE, None
    return create_response(status=status, message=message, error_message=error_message)


def fetch_last_car_seal_index(request_data=None, logger=None):
    area = TblAreaMaster.query.filter_by(id=int(request_data['area_id'])).first()
    unit = TblUnitMaster.query.filter_by(id=int(request_data['unit_id'])).first()
    car_seal_sub_str = area.area + '-' + unit.unit_number + '-CS-'

    cs_record = TblCarSealMaster.query.filter(
        TblCarSealMaster.car_seal_no.contains(car_seal_sub_str)
    ).order_by(
        TblCarSealMaster.car_seal_no.desc()
    ).first()
    data = {constants.KEY_DICT['LAST_CAR_SEAL_INDEX']: cs_record.car_seal_no.split('-')[-1] if cs_record else 0}

    return create_response(get_success_response=constants.SUCCESS, data=data)
