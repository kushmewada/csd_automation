from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblAreaMaster(BaseModel):
    area = db.Column(db.String(250), unique=True, nullable=False)


class TblUnitMaster(BaseModel):
    area_id = db.Column(db.Integer, db.ForeignKey('tbl_area_master.id'), nullable=False)
    unit_number = db.Column(db.String(250), nullable=False)


class TblCategoryMaster(BaseModel):
    category = db.Column(db.String(250), unique=True, nullable=False)


class TblScdPurposeMaster(BaseModel):
    scd_purpose = db.Column(db.String(250), nullable=False)


class TblOperationModeMaster(BaseModel):
    mode = db.Column(db.String(250), unique=True, nullable=False)


class TblPidDrawingMaster(BaseModel):
    pid_drawing = db.Column(db.String(250), nullable=False)


class TblTaskMaster(BaseModel):
    task_type = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    start_time = db.Column(db.Numeric(15), nullable=False)
    started_by = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)
    completion_time = db.Column(db.Numeric(15), nullable=True)
    completed_by = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)


class TblFunctionLocationMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    unit_id = db.Column(db.Integer, db.ForeignKey('tbl_unit_master.id'), nullable=True)
    fl_name = db.Column(db.String(250), unique=True, nullable=False)
    fl_type = db.Column(db.Integer, nullable=False)
    fl_description = db.Column(db.String(250), nullable=True)
    critical_safeguard = db.Column(db.Integer, nullable=True)
    operation_mode_id = db.Column(db.Integer, db.ForeignKey('tbl_operation_mode_master.id'), nullable=True)
    scd_purpose_id = db.Column(db.Integer, db.ForeignKey('tbl_scd_purpose_master.id'), nullable=True)
    pid_drawing_id = db.Column(db.Integer, db.ForeignKey('tbl_pid_drawing_master.id'), nullable=True)
    equipments_protected = db.Column(db.String(250), nullable=True)
    change_source_id = db.Column(db.String(250), nullable=True)
    source_reference_no = db.Column(db.String(250), nullable=True)
    consequence_level = db.Column(db.String(250), nullable=True)
    consequence_of_failure = db.Column(db.String(250), nullable=True)
    required_availability_target = db.Column(db.Integer, nullable=True)
    pm_plan_deactivated = db.Column(db.Integer, nullable=True)
    recommended_pm_interval = db.Column(db.Integer, nullable=True)
    new_or_updated_equipment_strategy = db.Column(db.Integer, nullable=True)
    recommended_plan = db.Column(db.String(250), nullable=True)
    actual_availability_target = db.Column(db.Integer, nullable=True)
    additional_risk_management_options = db.Column(db.String(250), nullable=True)
    pm_plan_scheduled = db.Column(db.Integer, nullable=True)
    pid_drawing_updated = db.Column(db.Integer, nullable=True)
    sap_scd_class_updated = db.Column(db.Integer, nullable=True)
    estimate_back_in_service_date = db.Column(db.Numeric(15), nullable=True)
    system_status = db.Column(db.String(250), nullable=True)
    main_working_category = db.Column(db.String(250), nullable=True)
    abc_indicator = db.Column(db.String(250), nullable=True)
    planner_group_id = db.Column(db.Integer, db.ForeignKey('tbl_planner_group_master.id'), nullable=True)
    work_center_id = db.Column(db.Integer, db.ForeignKey('tbl_work_center_master.id'), nullable=True)
    due_status = db.Column(db.String(250), nullable=True)
    remarks = db.Column(db.String(250), nullable=True)


class TblCarSealMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    unit_id = db.Column(db.Integer, db.ForeignKey('tbl_unit_master.id'), nullable=True)
    car_seal_no = db.Column(db.String(250), nullable=True)
    car_seal_type = db.Column(db.Integer, nullable=False)
    she_critical = db.Column(db.Integer, nullable=True)
    position_id = db.Column(db.Integer, db.ForeignKey('tbl_car_seal_position_master.id'), nullable=True)
    valve_id = db.Column(db.Integer, db.ForeignKey('tbl_car_seal_valve_master.id'), nullable=True)
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)
    pid_drawing_id = db.Column(db.Integer, db.ForeignKey('tbl_pid_drawing_master.id'), nullable=True)
    reason = db.Column(db.String(250), nullable=True)
    line_no = db.Column(db.String(250), nullable=True)
    change_date = db.Column(db.Numeric(15), nullable=True)
    last_verification_date = db.Column(db.Numeric(15), nullable=True)
    verified_by = db.Column(db.String(250), nullable=True)
