def get_suggestion_values(table=None, key=None, search_key=None):
    field = getattr(table, key)
    filters = [field.contains(search_key)] if search_key else list()
    records = table.query.filter(*filters).order_by(field).all()
    return [getattr(record, key) for record in records]
