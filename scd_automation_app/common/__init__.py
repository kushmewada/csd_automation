from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    enable_system_record,
    disable_system_record,
    get_user_area_records,
    get_area_unit_records,
    get_fl_records,
    get_all_fl_records,
    get_fl_details_master_records,
    get_car_seal_master_records,
    get_fl_suggestion_records,
    check_for_active_request_record,
    fetch_last_car_seal_index
)

blueprint_name = constants.BLUE_PRINT_DICT['COMMON']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['COMMON']['URL_PREFIX']
common_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['COMMON_LOG'])


api_enable_system = constants.API_DICT['COMMON']['ENABLE_SYSTEM']
api_disable_system = constants.API_DICT['COMMON']['DISABLE_SYSTEM']
api_get_user_areas = constants.API_DICT['COMMON']['GET_USER_AREAS']
api_get_area_units = constants.API_DICT['COMMON']['GET_AREA_UNITS']
api_get_fl_list = constants.API_DICT['COMMON']['GET_FL_LIST']
api_get_all_fl_list = constants.API_DICT['COMMON']['GET_ALL_FL_LIST']
api_get_fl_details_master_data = constants.API_DICT['COMMON']['GET_FL_DETAILS_MASTER_DATA']
api_get_car_seal_master_data = constants.API_DICT['COMMON']['GET_CAR_SEAL_MASTER_DATA']
api_get_fl_suggestion_data = constants.API_DICT['COMMON']['GET_FL_SUGGESTION_DATA']
api_check_for_active_request = constants.API_DICT['COMMON']['CHECK_FOR_ACTIVE_REQUEST']
api_get_last_car_seal_index = constants.API_DICT['COMMON']['GET_LAST_CAR_SEAL_INDEX']


@common_bp.route(api_enable_system, methods=[constants.POST])
def enable_system():
    return base_api(request=request, api_name=api_enable_system, api=enable_system_record, logger=logger)


@common_bp.route(api_disable_system, methods=[constants.POST])
def disable_system():
    return base_api(request=request, api_name=api_disable_system, api=disable_system_record, logger=logger)


@common_bp.route(api_get_user_areas, methods=[constants.GET])
def get_user_areas():
    return base_api(request=request, api_name=api_get_user_areas, api=get_user_area_records, logger=logger)


@common_bp.route(api_get_area_units, methods=[constants.GET])
def get_area_units():
    return base_api(request=request, api_name=api_get_area_units, api=get_area_unit_records, logger=logger)


@common_bp.route(api_get_fl_list, methods=[constants.GET])
def get_fl_list():
    return base_api(request=request, api_name=api_get_fl_list, api=get_fl_records, logger=logger)


@common_bp.route(api_get_all_fl_list, methods=[constants.GET])
def get_all_fl_list():
    return base_api(request=request, api_name=api_get_fl_list, api=get_all_fl_records, logger=logger)


@common_bp.route(api_get_fl_details_master_data, methods=[constants.GET])
def get_fl_details_master_data():
    return base_api(request=request, api_name=api_get_fl_details_master_data, api=get_fl_details_master_records,
                    logger=logger)


@common_bp.route(api_get_car_seal_master_data, methods=[constants.GET])
def get_car_seal_master_data():
    return base_api(request=request, api_name=api_get_car_seal_master_data, api=get_car_seal_master_records,
                    logger=logger)


@common_bp.route(api_get_fl_suggestion_data, methods=[constants.GET])
def get_fl_suggestion_data():
    return base_api(request=request, api_name=api_get_fl_suggestion_data, api=get_fl_suggestion_records,
                    logger=logger)


@common_bp.route(api_check_for_active_request, methods=[constants.GET])
def check_for_active_request():
    return base_api(request=request, api_name=api_check_for_active_request, api=check_for_active_request_record,
                    logger=logger)


@common_bp.route(api_get_last_car_seal_index, methods=[constants.GET])
def get_last_car_seal_index():
    return base_api(request=request, api_name=api_get_last_car_seal_index, api=fetch_last_car_seal_index,
                    logger=logger)
