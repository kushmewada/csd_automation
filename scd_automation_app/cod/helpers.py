import json

import constants
from common_packages import db
from scd_automation_app.cod.models import TblCodMaster
from scd_automation_app.common.models import (
    TblScdPurposeMaster, TblPidDrawingMaster, TblOperationModeMaster, TblFunctionLocationMaster, TblCarSealMaster
)
from scd_automation_app.plant.models import TblDefeatReasonMaster
from scd_automation_app.samref.common_functions import convert_time_to_timestamp, convert_date_to_timestamp
from scd_automation_app.samref.common_helpers import get_master_data_ids


def format_defeat_contingency_plan_data(request_data=None, controller_rgn_str=None, logger=None):
    monitoring_frequency = None
    monitoring_frequency_id = request_data.get('monitoring_frequency_id')
    if monitoring_frequency_id:
        monitoring_frequency_id = int(request_data['monitoring_frequency_id'])
        monitoring_frequency = request_data['other_frequency'].title() \
            if request_data['other_frequency'] else constants.MONITORING_FREQUENCY_DICT[monitoring_frequency_id]

    monitored_equipments = request_data.get('monitored_equipments')
    monitoring_role = request_data.get('monitoring_role')
    special_staffing_required = request_data.get('special_staffing_required')
    special_staffing = request_data.get('special_staffing')
    event_cause = request_data.get('event_cause')
    contingency_role = request_data.get('contingency_role')
    preventive_measures = request_data.get('preventive_measures')
    evaluated_unit_conditions = request_data.get('evaluated_unit_conditions')

    formatted_dcp_data = {
        'monitored_equipments': monitored_equipments.title() if monitoring_role else None,
        'monitoring_role': monitoring_role.title() if monitoring_role else None,
        'monitoring_frequency': monitoring_frequency,
        'special_staffing_required': int(special_staffing_required) if special_staffing_required else None,
        'special_staffing': special_staffing.title() if special_staffing else None,
        'event_cause': event_cause.title() if event_cause else None,
        'contingency_role': contingency_role.title() if contingency_role else None,
        'preventive_measures': preventive_measures.title() if preventive_measures else None,
        'evaluated_unit_conditions': evaluated_unit_conditions.title() if evaluated_unit_conditions else None
    }
    logger.info(controller_rgn_str + ' Formatted Defeat Contingency Data - ' + str(formatted_dcp_data))
    return formatted_dcp_data


def update_cod_master_record(
        request_group_number=None,
        request_data=None,
        request_status=None,
        defeat_status=None,
        section_id_key=None,
        section_id=None,
        controller_rgn_str=None,
        logger=None
):
    cod_master_params = format_defeat_contingency_plan_data(
        request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger
    )

    defeat_reason_id = int(request_data['defeat_reason_id'])
    record = TblDefeatReasonMaster.query.filter_by(id=int(request_data['defeat_reason_id'])).first()

    cod_master_params.update({
        'defeat_reason_id': defeat_reason_id,
        'original_defeat_reason': record.defeat_reason,
        'modified_time': convert_time_to_timestamp(),
        'modified_by': int(request_data['user_id'])
    })

    if request_status:
        cod_master_params.update({'request_status': request_status})

    if defeat_status:
        cod_master_params.update({'defeat_status': defeat_status})

    if section_id:
        cod_master_params.update({section_id_key: section_id})

    TblCodMaster.query.filter_by(id=request_group_number).update(dict(cod_master_params))
    db.session.commit()
    logger.info(controller_rgn_str + ' COD Master record updated.')


def update_fl_data(request_data=None, controller_rgn_str=None, logger=None):
    for fl in json.loads(request_data['fl_data']):
        estimate_back_in_service_date = fl['estimate_back_in_service_date']
        master_id_dict = get_master_data_ids(
            fl_data=fl, scd_purpose_table=TblScdPurposeMaster,
            pid_drawing_table=TblPidDrawingMaster,
            operation_mode_table=TblOperationModeMaster
        )

        TblFunctionLocationMaster.query.filter_by(id=int(fl['fl_id'])).update(dict(
            scd_purpose_id=master_id_dict['scd_purpose_id'],
            pid_drawing_id=master_id_dict['pid_drawing_id'],
            estimate_back_in_service_date=convert_date_to_timestamp(date=estimate_back_in_service_date)
        ))
        db.session.commit()
    logger.info(controller_rgn_str + ' Function Locations data updated.')


def update_car_seal_list(request_data=None, controller_rgn_str=None, logger=None):
    car_seal_data = request_data.get('car_seal_data')
    car_seal_list = json.loads(car_seal_data) if car_seal_data else list()

    for cs in car_seal_list:
        TblCarSealMaster.query.filter_by(id=cs['cs_id']).update(dict(
            car_seal_type=constants.TEMPORARY_CAR_SEAL_ID,
            position_id=int(cs['position_id']),
            valve_id=int(cs['valve_id']),
            function_location_id=int(cs['fl_id'])
        ))
    db.session.commit()
    logger.info(controller_rgn_str + ' Car Seal data updated.')

