from json import loads

import constants
from common_packages import db
from scd_automation_app.attachments.models import TblDocumentMaster, TblWorkFlowDocuments
from scd_automation_app.common.models import (
    TblAreaMaster,
    TblUnitMaster,
    TblFunctionLocationMaster,
    TblCategoryMaster,
    TblScdPurposeMaster,
    TblPidDrawingMaster,
    TblCarSealMaster
)
from scd_automation_app.groups.models import TblWorkCenterMaster
from scd_automation_app.plant.models import TblDefeatReasonMaster, TblCarSealPositionMaster, TblCarSealValveMaster
from scd_automation_app.samref.common_functions import (
    convert_time_to_timestamp,
    convert_date_to_timestamp,
    convert_dict_to_list,
    get_list_from_comma_separated_str,
    get_time_interval,
    get_time_delta,
    get_key_for_given_value,
    convert_timestamp_to_date,
    convert_timestamp_to_time
)
from scd_automation_app.samref.common_helpers import (
    check_request_permission_for_section_1,
    check_request_permissions,
    format_list_api_data,
    get_sort_data,
    get_unique_rgn_data,
    get_controller_rgn_str,
    get_attachment_list_for_request,
    get_user_data_from_request,
    create_or_update_user_active_work_flow,
    create_response,
    delete_section_documents,
    delete_user_active_work_flow_record,
    create_pending_action_record
)
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster, TblUserActiveWorkflows, TblPendingActions
from .helpers import (
    update_cod_master_record, update_fl_data, update_car_seal_list, format_defeat_contingency_plan_data
)
from .models import (
    TblCodMaster,
    TblCodFunctionLocations,
    TblCodCarSeals,
    TblCodInitialDefeatApprovalMaster,
    TblCodDefeatExtensionApprovalMaster,
    TblCodUserAcknowledgementMaster,
    TblCodUserAcknowledgements,
    TblCodLongTermDefeatMaster
)


def get_cod_records(request_data=None, logger=None):
    cod_data_dict = dict()
    filter_fields = list()
    search_text = request_data.get('search_text', None)
    request_type = int(request_data['request_type']) if request_data.get('request_type', None) else None

    if request_type and request_type == constants.ACTIVE_COD_REQUESTS:
        filter_fields = [TblCodMaster.request_status.not_in([constants.REJECTED, constants.CLOSED])]

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id == int(area_id))

    defeat_reason_id = request_data.get('defeat_reason_id', None)
    if defeat_reason_id:
        filter_fields.append(TblDefeatReasonMaster.id == int(defeat_reason_id))

    category_id = request_data.get('category_id', None)
    if category_id:
        filter_fields.append(TblCategoryMaster.id == int(category_id))

    request_defeat_status = request_data.get('request_defeat_status', None)
    if request_defeat_status:
        request_defeat_status = int(request_defeat_status)
        if request_defeat_status == constants.APPROVED_DEFEATS:
            filter_fields.append(TblCodMaster.initial_defeat_approval_id != None)
            filter_fields.append(TblCodMaster.defeat_extension_approval_id == None)
            filter_fields.append(TblCodMaster.long_term_defeat_id == None)
        elif request_defeat_status == constants.EXTENDED_DEFEATS:
            filter_fields.append(TblCodMaster.defeat_extension_approval_id != None)
            filter_fields.append(TblCodMaster.long_term_defeat_id == None)
        elif request_defeat_status == constants.LONG_TERM_DEFEATS:
            filter_fields.append(TblCodMaster.long_term_defeat_id != None)

    sort_id = int(request_data['sort_id']) if request_data.get('sort_id', None) else constants.REQUEST_ID_ASCENDING
    sort_obj = get_sort_data(sort_id=sort_id, table=TblCodMaster)

    cod_records = TblCodMaster.query.outerjoin(
        TblCodInitialDefeatApprovalMaster,
        TblCodInitialDefeatApprovalMaster.id == TblCodMaster.initial_defeat_approval_id
    ).join(
        TblCodFunctionLocations, TblCodFunctionLocations.request_group_number == TblCodMaster.request_group_number
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCodFunctionLocations.function_location_id
    ).join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).join(
        TblDefeatReasonMaster, TblDefeatReasonMaster.id == TblCodMaster.defeat_reason_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblCodMaster.id,
        TblCodMaster.unique_rgn_id,
        TblCodMaster.defeat_reason_id,
        TblCodMaster.defeat_status,
        TblCodMaster.request_status,
        TblCodMaster.request_group_number,
        TblCodMaster.created_time,
        TblCodInitialDefeatApprovalMaster.created_time.label('section_2_created_time'),
        TblCodFunctionLocations.function_location_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.estimate_back_in_service_date,
        TblDefeatReasonMaster.defeat_reason,
        TblCategoryMaster.category,
        TblWorkCenterMaster.category_id
    ).filter(
        *filter_fields
    ).order_by(
        sort_obj
    ).all()

    for record in cod_records:
        request_group_number = record.request_group_number
        if request_group_number not in cod_data_dict:
            pending_action = TblPendingActions.query.join(
                TblUserMaster, TblUserMaster.id == TblPendingActions.assigned_to
            ).filter(
                TblPendingActions.status == constants.PENDING_ACTION,
                TblPendingActions.pending_action_id != constants.COD_SECTION_4
            ).add_columns(
                TblUserMaster.name,
                TblPendingActions.assigned_to
            ).first()

            cod_data_dict[record.id] = {
                'id': request_group_number,
                'unique_rgn_id': record.unique_rgn_id,
                'area': record.fl_name.split('-')[1],
                'unit': record.fl_name.split('-')[2],
                'function_locations': [{
                    'id': record.function_location_id,
                    'name': record.fl_name,
                    'critical_safeguard': record.critical_safeguard,
                    'category': {'id': record.category_id, 'name': record.category}
                }],
                'defeat_reason': {'id': record.defeat_reason_id, 'name': record.defeat_reason},
                'defeat_status': record.defeat_status,
                'request_status': record.request_status,
                'currently_with':
                    {'id': pending_action.assigned_to, 'name': pending_action.name} if pending_action else None,
                'created_time': convert_timestamp_to_date(timestamp=record.created_time),
                'expiry_timestamp': str(record.section_2_created_time),
                'expiry_date': convert_timestamp_to_date(timestamp=record.section_2_created_time)
            }
        else:
            cod_data_dict[record.id]['function_locations'].append({
                'id': record.function_location_id,
                'name': record.fl_name,
                'critical_safeguard': record.critical_safeguard,
                'category': {'id': record.category_id, 'name': record.category}
            })

    data = format_list_api_data(
        records=cod_data_dict,
        list_name=constants.KEY_DICT['COD_REQUEST_LIST'],
        search_key=constants.KEY_DICT['UNIQUE_RGN_ID'],
        search_text=search_text,
        page=int(request_data['page']) if request_data.get('page', None) else None,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_cod_request_data(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['section_ids'])

    cod_fl_records = TblCodFunctionLocations.query.join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCodFunctionLocations.function_location_id
    ).join(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).join(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblFunctionLocationMaster.pid_drawing_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.id,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblFunctionLocationMaster.unit_id,
        TblCodFunctionLocations.function_location_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.scd_purpose_id,
        TblFunctionLocationMaster.pid_drawing_id,
        TblFunctionLocationMaster.estimate_back_in_service_date,
        TblScdPurposeMaster.scd_purpose,
        TblPidDrawingMaster.pid_drawing
    ).filter(
        TblCodFunctionLocations.request_group_number == request_group_number
    ).all()

    fl_dict = dict()
    fl_dict = {
        record.function_location_id:
            {
                'id': record.function_location_id,
                'name': record.fl_name,
                'description': record.fl_description,
                'critical_safeguard': record.critical_safeguard,
                'scd_purpose_data': {'id': record.scd_purpose_id, 'name': record.scd_purpose},
                'pid_drawing_data': {'id': record.pid_drawing_id, 'name': record.pid_drawing},
                'estimate_back_in_service_date':
                    convert_timestamp_to_date(timestamp=record.estimate_back_in_service_date)
            }
        for record in cod_fl_records if record.function_location_id not in fl_dict
    }
    fl_list = convert_dict_to_list(fl_dict)

    cod_car_seals_records = TblCodCarSeals.query.join(
        TblCarSealMaster, TblCarSealMaster.id == TblCodCarSeals.car_seal_id
    ).join(
        TblCarSealPositionMaster, TblCarSealPositionMaster.id == TblCarSealMaster.position_id
    ).join(
        TblCarSealValveMaster, TblCarSealValveMaster.id == TblCarSealMaster.valve_id
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCarSealMaster.function_location_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblFunctionLocationMaster.pid_drawing_id
    ).add_columns(
        TblFunctionLocationMaster.fl_name,
        TblPidDrawingMaster.pid_drawing,
        TblCarSealPositionMaster.car_seal_position,
        TblCarSealValveMaster.car_seal_valve,
        TblCarSealMaster.id,
        TblCarSealMaster.function_location_id,
        TblCarSealMaster.position_id,
        TblCarSealMaster.valve_id,
    ).filter(
        TblCodCarSeals.request_group_number == request_group_number
    ).all()

    car_seal_list = [
        {
            'id': cs[3],
            'fl_data': {'id': cs.function_location_id, 'name': cs.fl_name},
            'position_data': {'id': cs.position_id, 'name': cs.car_seal_position},
            'valve_data': {'id': cs.valve_id, 'name': cs.car_seal_valve}
        }
        for cs in cod_car_seals_records
    ]

    attachment_list = get_attachment_list_for_request(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=constants.COD_SECTION_1,
        document_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments
    )

    cod_master_record = TblCodMaster.query.join(
        TblDefeatReasonMaster, TblDefeatReasonMaster.id == TblCodMaster.defeat_reason_id
    ).add_columns(
        TblCodMaster.id,
        TblCodMaster.unique_rgn_id,
        TblCodMaster.defeat_reason_id,
        TblCodMaster.original_defeat_reason,
        TblCodMaster.defeat_status,
        TblCodMaster.initial_defeat_approval_id,
        TblCodMaster.defeat_extension_approval_id,
        TblCodMaster.long_term_defeat_id,
        TblCodMaster.monitoring_frequency,
        TblCodMaster.monitoring_role,
        TblCodMaster.monitored_equipments,
        TblCodMaster.event_cause,
        TblCodMaster.contingency_role,
        TblCodMaster.preventive_measures,
        TblCodMaster.evaluated_unit_conditions,
        TblCodMaster.special_staffing,
        TblCodMaster.special_staffing_required,
        TblCodMaster.section_status,
        TblCodMaster.created_time,
        TblCodMaster.created_by,
        TblCodMaster.modified_time,
        TblCodMaster.modified_by,
        TblDefeatReasonMaster.defeat_reason
    ).filter(
        TblCodMaster.request_group_number == request_group_number
    ).first()

    # This if condition is for the case when user is creating new request from and an existing request. FE directly
    # populates this data in the form. In case new request is initiated, defeat status, days_since_initial_defeat and
    # expiring_on should be NEW, N / A and N / A respectively. When data is fetched for an existing closed request,
    # these 3 fields will have values according to old request which cannot be populated in the FE.
    if len(section_id_list) == 1:
        defeat_status = constants.DEFEAT_STATUS_DICT['NEW']
        start_date = constants.NOT_APPLICABLE
        days_since_initial_defeat = constants.NOT_APPLICABLE
        expiring_on = constants.NOT_APPLICABLE
    else:
        defeat_status = cod_master_record.defeat_status
        start_date = convert_timestamp_to_date(timestamp=cod_master_record.created_time)
        time_interval = get_time_interval(start_timestamp=cod_master_record.created_time)
        days_since_initial_defeat = time_interval.days
        if defeat_status == constants.DEFEAT_STATUS_DICT['DEFEAT_EXPIRED']:
            expiring_on = constants.NOT_APPLICABLE
        elif cod_master_record.defeat_extension_approval_id:
            initial_defeat_approval = TblCodInitialDefeatApprovalMaster.query.filter_by(
                id=cod_master_record.initial_defeat_approval_id
            ).first()
            timestamp = get_time_delta(timestamp=initial_defeat_approval.created_time, days=1, get_timestamp=True)
            expiring_on = convert_timestamp_to_date(timestamp=timestamp)
        else:
            expiring_on = constants.NOT_APPLICABLE

    if cod_master_record.monitoring_frequency is not None:
        if cod_master_record.monitoring_frequency not in constants.MONITORING_FREQUENCY_DICT.values():
            monitoring_frequency_id = constants.OTHERS
        else:
            monitoring_frequency_id = get_key_for_given_value(
                dictionary=constants.MONITORING_FREQUENCY_DICT, value=cod_master_record.monitoring_frequency
            )
    else:
        monitoring_frequency_id = None

    special_staffing_data = {
        'special_staffing_required': cod_master_record.special_staffing_required
    }
    if cod_master_record.special_staffing_required:
        special_staffing_data['special_staffing'] = cod_master_record.special_staffing

    section_1_data_dict = {
        'id': cod_master_record.id,
        'section_data': {
            'area': {'id': cod_fl_records[0].area_id, 'name': cod_fl_records[0].area},
            'unit': {'id': cod_fl_records[0].unit_id, 'name': cod_fl_records[0].unit_number},
            'defeat_reason_list': {
                'id': cod_master_record.defeat_reason_id,
                'name': cod_master_record.defeat_reason,
                'original_name': cod_master_record.original_defeat_reason
            },
            'fl_list': fl_list,
            'defeat_contingency_plan': {
                'monitored_equipments': cod_master_record.monitored_equipments,
                'monitoring_role': cod_master_record.monitoring_role,
                'monitoring_frequency_data': {
                    'id': monitoring_frequency_id, 'name': cod_master_record.monitoring_frequency
                },
                'special_staffing_data': special_staffing_data,
                'event_cause': cod_master_record.event_cause,
                'contingency_role': cod_master_record.contingency_role,
                'preventive_measures': cod_master_record.preventive_measures,
                'evaluated_unit_conditions': cod_master_record.evaluated_unit_conditions
            },
            'car_seal_list': car_seal_list,
            'attachment_list': attachment_list
        },
        'section_status': cod_master_record.section_status
    }
    section_user_data = get_user_data_from_request(request_record=cod_master_record, user_table=TblUserMaster)
    section_1_data_dict.update(section_user_data)
    cod_request_data_dict = {
        'request_group_number': request_group_number,
        'unique_rgn_id': cod_master_record.unique_rgn_id,
        'defeat_status': defeat_status,
        'start_date': start_date,
        'days_since_initial_defeat': days_since_initial_defeat,
        'expiring_on': expiring_on,
        'section_1': section_1_data_dict
    }

    if constants.COD_SECTION_2 in section_id_list and cod_master_record.initial_defeat_approval_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.COD,
            work_flow_section_id=constants.COD_SECTION_2,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        defeat_approval_record = \
            TblCodInitialDefeatApprovalMaster.query.filter_by(id=cod_master_record.initial_defeat_approval_id).first()

        section_2_data_dict = {
            'id': defeat_approval_record.id,
            'section_data': {
                'immediate_ra_required': defeat_approval_record.immediate_ra_required,
                'remarks': defeat_approval_record.remarks,
                'attachment_list': attachment_list
            },
            'section_status': defeat_approval_record.section_status
        }
        section_user_data = \
            get_user_data_from_request(request_record=defeat_approval_record, user_table=TblUserMaster)
        section_2_data_dict.update(section_user_data)
        cod_request_data_dict['section_2'] = section_2_data_dict

    if constants.COD_SECTION_3 in section_id_list and cod_master_record.defeat_extension_approval_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.COD,
            work_flow_section_id=constants.COD_SECTION_3,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        defeat_extension_record = TblCodDefeatExtensionApprovalMaster.query.filter_by(
            id=cod_master_record.defeat_extension_approval_id
        ).first()

        section_3_data_dict = {
            'id': defeat_extension_record.id,
            'section_data': {
                'defeat_extension_reason': defeat_extension_record.defeat_extension_reason,
                'ra_required': defeat_extension_record.ra_required,
                'remarks': defeat_extension_record.remarks,
                'recycle_comments': defeat_extension_record.recycle_comments,
                'attachment_list': attachment_list
            },
            'section_status': defeat_extension_record.section_status
        }
        section_user_data = \
            get_user_data_from_request(request_record=defeat_extension_record, user_table=TblUserMaster)
        section_3_data_dict.update(section_user_data)
        cod_request_data_dict['section_3'] = section_3_data_dict

    if constants.COD_SECTION_4 in section_id_list:
        user_ack_records = TblCodUserAcknowledgementMaster.query.join(
            TblCodUserAcknowledgements,
            TblCodUserAcknowledgements.user_acknowledgement_id == TblCodUserAcknowledgementMaster.id
        ).join(
            TblUserMaster, TblUserMaster.id == TblCodUserAcknowledgementMaster.user_id
        ).add_columns(
            TblUserMaster.name,
            TblCodUserAcknowledgements.acknowledgement_time
        ).filter(
            TblCodUserAcknowledgementMaster.request_group_number == request_group_number
        ).order_by(
            TblCodUserAcknowledgements.id
        ).all()

        user_ack_list = [{
            'user_data': {'id': record[0].user_id, 'name': record.name},
            'acknowledgement_time': convert_timestamp_to_time(timestamp=record.acknowledgement_time)
        } for record in user_ack_records]

        if user_ack_list:
            cod_request_data_dict['section_4'] = {'section_data': {'user_acknowledgement_list': user_ack_list}}

    if constants.COD_SECTION_5 in section_id_list and cod_master_record.long_term_defeat_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.COD,
            work_flow_section_id=constants.COD_SECTION_5,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        long_term_defeat = TblCodLongTermDefeatMaster.query.filter_by(id=cod_master_record.long_term_defeat_id).first()
        section_5_data_dict = {
            'id': long_term_defeat.id,
            'section_data': {
                'long_term_defeat_reason': long_term_defeat.long_term_defeat_reason,
                'expiry_date': convert_timestamp_to_date(timestamp=long_term_defeat.expiry_date),
                'mitigation_present': long_term_defeat.mitigation_present,
                'recycle_comments': long_term_defeat.recycle_comments,
                'attachment_list': attachment_list
            },
            'section_status': long_term_defeat.section_status
        }
        section_user_data = \
            get_user_data_from_request(request_record=long_term_defeat, user_table=TblUserMaster)
        section_5_data_dict.update(section_user_data)
        cod_request_data_dict['section_5'] = section_5_data_dict

    return create_response(get_success_response=True, data=cod_request_data_dict)


def create_cod_record(request_data=None, logger=None):
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status'])
    defeat_reason_id = int(request_data['defeat_reason_id'])

    status, error_message = check_request_permission_for_section_1(
        request_data=request_data,
        data_key=constants.KEY_DICT['FL_DATA'],
        id_key=constants.KEY_DICT['FL_ID'],
        id_field=constants.KEY_DICT['FUNCTION_LOCATION_ID'],
        section_id=constants.COD_SECTION_1,
        master_table=TblCodMaster,
        id_table=TblCodFunctionLocations,
        pending_action_table=TblPendingActions,
        request_type=constants.POST
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=error_message)

    request_group_number, unique_rgn_id, controller_rgn_str = get_unique_rgn_data(
        work_flow_id=constants.COD, work_flow_section_id=constants.COD_SECTION_1, table=TblCodMaster
    )
    logger.info(controller_rgn_str)

    defeat_reason_record = TblDefeatReasonMaster.query.filter_by(id=defeat_reason_id).first()
    cod_master_params = {
        'request_group_number': request_group_number,
        'unique_rgn_id': unique_rgn_id,
        'defeat_reason_id': defeat_reason_id,
        'original_defeat_reason': defeat_reason_record.defeat_reason,
        'request_status': constants.COD_SECTION_1,
        'defeat_status': constants.DEFEAT_STATUS_DICT['NEW'],
        'section_status': section_status,
        'created_time': convert_time_to_timestamp(),
        'created_by': user_id
    }
    formatted_dcp_data = format_defeat_contingency_plan_data(request_data=request_data,
                                                             controller_rgn_str=controller_rgn_str, logger=logger)
    cod_master_params.update(formatted_dcp_data)
    cod_master_record = TblCodMaster(**cod_master_params)
    db.session.add(cod_master_record)
    db.session.commit()
    logger.info(controller_rgn_str + ' - New COD request created.')

    cod_fl_records = [
        TblCodFunctionLocations(request_group_number=request_group_number, function_location_id=fl['fl_id'])
        for fl in loads(request_data['fl_data'])
    ]
    db.session.add_all(cod_fl_records)
    db.session.commit()
    logger.info(controller_rgn_str + ' COD Function Locations added.')

    update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    for car_seal in loads(request_data['car_seal_data']):
        fl_id = int(car_seal['fl_id'])
        fl_record = TblFunctionLocationMaster.query.filter_by(id=fl_id).first()
        car_seal_record = TblCarSealMaster(
            unit_id=fl_record.unit_id,
            car_seal_type=constants.TEMPORARY_CAR_SEAL_ID,
            position_id=int(car_seal['position_id']),
            valve_id=int(car_seal['valve_id']),
            function_location_id=fl_id,
        )
        db.session.add(car_seal_record)
        db.session.commit()

        cod_car_seal_record = TblCodCarSeals(request_group_number=request_group_number, car_seal_id=car_seal_record.id)
        db.session.add(cod_car_seal_record)
        db.session.commit()
    logger.info(controller_rgn_str + ' COD Car Seals added.')

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_number=constants.COD_SECTION_1,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )
    return create_response(post_success_response=True,
                           data={'request_group_number': request_group_number, 'unit_id': int(request_data['unit_id'])})


def initial_defeat_approval_record(request_data=None, request_type=None, logger=None):
    request_group_number = request_data['request_group_number']
    user_id = int(request_data['user_id'])

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_master_table_key=constants.KEY_DICT['INITIAL_DEFEAT_APPROVAL_ID'],
        section_id=constants.COD_SECTION_2,
        master_table=TblCodMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=constants.COD_SECTION_2
    )
    logger.info(controller_rgn_str)
    logger.info(controller_rgn_str + ' Request type - ' + str(request_group_number))

    initial_defeat_approval_params = {'immediate_ra_required': int(request_data['immediate_ra_required'])}
    if constants.KEY_DICT['REMARKS'] in request_data:
        initial_defeat_approval_params.update({'remarks': request_data['remarks'].capitalize()})

    if request_type == constants.POST:
        initial_defeat_approval_params.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})

        initial_defeat_approval = TblCodInitialDefeatApprovalMaster(**initial_defeat_approval_params)
        db.session.add(initial_defeat_approval)
        db.session.commit()
        logger.info(controller_rgn_str + ' New Initial Defeat Approval record created.')
        initial_defeat_approval_id = initial_defeat_approval.id
        response_params = {'post_success_response': True}
    else:
        initial_defeat_approval_id = int(request_data['initial_defeat_approval_id'])
        initial_defeat_approval_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})

        TblCodInitialDefeatApprovalMaster.query.filter_by(id=initial_defeat_approval_id).update(dict(
            initial_defeat_approval_params
        ))
        logger.info(controller_rgn_str + ' Initial Defeat Approval record with ID {} updated.'
                    .format(str(initial_defeat_approval_id)))
        response_params = {'patch_success_response': True}

    update_cod_master_record(
        request_group_number=request_group_number,
        request_data=request_data,
        section_id_key=constants.KEY_DICT['INITIAL_DEFEAT_APPROVAL_ID'],
        section_id=initial_defeat_approval_id,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    update_car_seal_list(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_number=constants.COD_SECTION_2,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )
    return create_response(**response_params)


def defeat_extension_approval_record(request_data=None, request_type=None, logger=None):
    request_group_number = request_data['request_group_number']
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_master_table_key=constants.KEY_DICT['DEFEAT_EXTENSION_APPROVAL_ID'],
        section_id=constants.COD_SECTION_3,
        master_table=TblCodMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=constants.COD_SECTION_3
    )
    logger.info(controller_rgn_str)
    logger.info(controller_rgn_str + ' Request type - ' + str(request_group_number))

    defeat_extension_params = {'ra_required': int(request_data['ra_required'])}
    if section_status:
        defeat_extension_params.update({'section_status': section_status})
    if constants.KEY_DICT['DEFEAT_EXTENSION_REASON'] in request_data:
        defeat_extension_params.update({
            'defeat_extension_reason': request_data['defeat_extension_reason'].capitalize()
        })
    if constants.KEY_DICT['REMARKS'] in request_data:
        defeat_extension_params.update({'remarks': request_data['remarks'].capitalize()})
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        defeat_extension_params.update({'recycle_comments': request_data['recycle_comments'].capitalize()})

    if request_type == constants.POST:
        defeat_extension_params.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        extension_approval_record = TblCodDefeatExtensionApprovalMaster(**defeat_extension_params)
        db.session.add(extension_approval_record)
        db.session.commit()
        logger.info(controller_rgn_str + ' New Deferment Extension Approval record created.')
        defeat_extension_approval_id = extension_approval_record.id
        response_params = {'post_success_response': True}
    else:
        defeat_extension_approval_id = int(request_data['defeat_extension_approval_id'])
        defeat_extension_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        TblCodDefeatExtensionApprovalMaster.query.filter_by(id=defeat_extension_approval_id).update(dict(
            defeat_extension_params
        ))
        db.session.commit()
        logger.info(controller_rgn_str + ' Defeat Extension Approval record with ID {} updated.'
                    .format(str(defeat_extension_approval_id)))
        response_params = {'patch_success_response': True}

    cod_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'controller_rgn_str': controller_rgn_str,
        'section_id_key': constants.KEY_DICT['DEFEAT_EXTENSION_APPROVAL_ID'],
        'section_id': defeat_extension_approval_id,
        'logger': logger
    }
    if section_status == constants.RECYCLED:
        cod_master_params.update({'request_status': constants.COD_SECTION_3})
    update_cod_master_record(**cod_master_params)

    update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    update_car_seal_list(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_number=constants.COD_SECTION_3,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.RECYCLED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.COD,
            'current_section_id': constants.COD_SECTION_3,
            'pending_action_id': constants.COD_SECTION_2,
            'completed_action_id_list':
                str(constants.COD_SECTION_2) + ',' + str(constants.COD_SECTION_3) + ',' + str(constants.COD_SECTION_5),
            'section_status': constants.RECYCLED
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)
    return create_response(**response_params)


def create_defeat_acknowledgement_record(request_data=None, logger=None):
    user_id = int(request_data['user_id'])
    request_group_number = int(request_data['request_group_number'])

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=constants.COD_SECTION_4
    )
    logger.info(controller_rgn_str)

    user_ack_master_record = TblCodUserAcknowledgementMaster.query.filter_by(
        user_id=user_id, request_group_number=request_group_number
    ).first()

    if user_ack_master_record is None:
        user_ack_master_record = TblCodUserAcknowledgementMaster(
            user_id=user_id,
            request_group_number=request_group_number,
            acknowledgement_suppressed=constants.COD_ACKNOWLEDGEMENT_STATUS['UNSUPPRESSED'],
            created_time=convert_time_to_timestamp(),
            created_by=user_id
        )
        db.session.add(user_ack_master_record)
        db.session.commit()
        logger.info(controller_rgn_str + ' New COD User Acknowledgement record created for user ID - ' + str(user_id))

    user_ack_record = TblCodUserAcknowledgements(
        user_acknowledgement_id=user_ack_master_record.id,
        acknowledgement_time=convert_time_to_timestamp()
    )
    db.session.add(user_ack_record)
    db.session.commit()
    logger.info(controller_rgn_str + ' Acknowledgement added for user with ID - ' + str(user_id))
    return create_response(post_success_response=True)


def create_or_update_long_term_defeat_record(request_data=None, request_type=None, logger=None):
    request_group_number = request_data['request_group_number']
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_master_table_key=constants.KEY_DICT['LONG_TERM_DEFEAT_ID'],
        section_id=constants.COD_SECTION_5,
        master_table=TblCodMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=constants.COD_SECTION_5
    )
    logger.info(controller_rgn_str)

    section_fields = {
        'mitigation_present': int(request_data['mitigation_present']),
        'recycle_comments':
            request_data['recycle_comments'].capitalize() if request_data.get('recycle_comments', None) else None
    }
    if constants.KEY_DICT['LONG_TERM_DEFEAT_REASON'] in request_data:
        section_fields.update({'long_term_defeat_reason': request_data['long_term_defeat_reason'].capitalize()})
    if constants.KEY_DICT['EXPIRY_DATE'] in request_data:
        section_fields.update({'expiry_date': convert_date_to_timestamp(date=request_data['expiry_date'])})
    if section_status:
        section_fields.update({'section_status': section_status})

    if request_type == constants.POST:
        section_fields.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        long_term_defeat_record = TblCodLongTermDefeatMaster(**section_fields)
        db.session.add(long_term_defeat_record)
        db.session.commit()
        logger.info(controller_rgn_str + ' New Long Term Defeat record created.')
        long_term_defeat_id = long_term_defeat_record.id
    elif request_type == constants.PATCH:
        long_term_defeat_id = int(request_data['long_term_defeat_id'])
        section_fields.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        record = TblCodLongTermDefeatMaster.query.filter_by(id=long_term_defeat_id).update(dict(section_fields))
        db.session.commit()
        logger.info(controller_rgn_str + ' Long Term Defeat record updated.')

    cod_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'section_id_key': constants.KEY_DICT['LONG_TERM_DEFEAT_ID'],
        'section_id': long_term_defeat_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    update_cod_master_record(**cod_master_params)

    update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    update_car_seal_list(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        section_number=constants.COD_SECTION_5,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.RECYCLED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.COD,
            'current_section_id': constants.COD_SECTION_5,
            'pending_action_id': constants.COD_SECTION_2,
            'completed_action_id_list':
                str(constants.COD_SECTION_2) + ',' + str(constants.COD_SECTION_3) + ',' + str(constants.COD_SECTION_5),
            'section_status': constants.RECYCLED
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)
    return create_response(post_success_response=True)


def discard_request_data(request_data=None, logger=None):
    fl_master_fields = dict()
    section_master_table = None
    section_master_table_id = None
    request_group_number = int(request_data['request_group_number'])
    work_flow_section_id = int(request_data['work_flow_section_id'])

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number, work_flow_id=constants.COD, work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str)

    cod_function_location_objects = TblCodFunctionLocations.query.filter_by(request_group_number=request_group_number)
    fl_id_list = [record.function_location_id for record in cod_function_location_objects.all()]

    cod_master_obj = TblCodMaster.query.filter_by(request_group_number=request_group_number)
    cod_master_record = cod_master_obj.first()

    cod_car_seal_objects = TblCodCarSeals.query.filter_by(request_group_number=request_group_number)
    car_seal_id_list = [record.car_seal_id for record in cod_car_seal_objects.all()]

    if work_flow_section_id == constants.COD_SECTION_1:
        fl_master_fields.update({'scd_purpose_id': None, 'pid_drawing_id': None, 'estimate_back_in_service_date': None})

    if work_flow_section_id == constants.COD_SECTION_2:
        request_status = constants.COD_SECTION_1
        section_master_table = TblCodInitialDefeatApprovalMaster
        section_master_table_id = constants.KEY_DICT['INITIAL_DEFEAT_APPROVAL_ID']

    if work_flow_section_id == constants.COD_SECTION_3:
        request_status = constants.COD_SECTION_2
        section_master_table = TblCodDefeatExtensionApprovalMaster
        section_master_table_id = constants.KEY_DICT['DEFEAT_EXTENSION_APPROVAL_ID']

    if work_flow_section_id == constants.COD_SECTION_5:
        request_status = constants.COD_SECTION_3
        section_master_table = TblCodLongTermDefeatMaster
        section_master_table_id = constants.KEY_DICT['LONG_TERM_DEFEAT_ID']

    delete_section_documents(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=work_flow_section_id,
        document_master_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments,
        logger=logger
    )
    logger.info(controller_rgn_str + ' Attachments deleted.')

    delete_user_active_work_flow_record(
        table=TblUserActiveWorkflows,
        request_group_number=request_group_number,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str + ' User active workflow record deleted.')

    if fl_master_fields:
        TblFunctionLocationMaster.query.filter(TblFunctionLocationMaster.id.in_(fl_id_list)).update(fl_master_fields)
        db.session.commit()
        logger.info(controller_rgn_str + ' Function location properties set to NULL.')

    if work_flow_section_id == constants.COD_SECTION_1:
        cod_car_seal_objects.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' COD Car Seal records deleted.')

        TblCarSealMaster.query.filter(TblCarSealMaster.id.in_(car_seal_id_list)).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' Car Seal master records deleted.')

        cod_function_location_objects.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' COD Function Locations deleted.')

        cod_master_obj.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' COD master record deleted.')
    else:
        cod_master_obj.update(dict(request_status=request_status))
        db.session.commit()

    if section_master_table_id:
        section_master_table_id_value = getattr(cod_master_record, section_master_table_id)
        cod_master_obj.update(dict({section_master_table_id: None}))
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table ID set to None in COD master table.')

        section_master_table.query.filter_by(id=section_master_table_id_value).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table record deleted.')

    return create_response(delete_success_response=constants.SUCCESS)
