from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_cod_records,
    get_cod_request_data,
    create_cod_record,
    initial_defeat_approval_record,
    defeat_extension_approval_record,
    create_defeat_acknowledgement_record,
    create_or_update_long_term_defeat_record,
    discard_request_data
)

blueprint_name = constants.BLUE_PRINT_DICT['COD']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['COD']['URL_PREFIX']
cod_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['COD_LOG'])


api_get_cod_request_list = constants.API_DICT['COD']['GET_COD_REQUEST_LIST']
api_get_cod_request_details = constants.API_DICT['COD']['GET_COD_REQUEST_DETAILS']
api_create_cod_request = constants.API_DICT['COD']['CREATE_COD_REQUEST']
api_initial_defeat_approval = constants.API_DICT['COD']['INITIAL_DEFEAT_APPROVAL']
api_defeat_extension_approval = constants.API_DICT['COD']['DEFEAT_EXTENSION_APPROVAL']
api_create_defeat_acknowledgement = constants.API_DICT['COD']['CREATE_DEFEAT_ACKNOWLEDGEMENT']
api_long_term_defeat = constants.API_DICT['COD']['LONG_TERM_DEFEAT']
api_discard_form = constants.API_DICT['COD']['DISCARD_FORM']


@cod_bp.route(api_get_cod_request_list, methods=[constants.GET])
def get_cod_request_list():
    return base_api(request=request, api_name=api_get_cod_request_list, api=get_cod_records, logger=logger)


@cod_bp.route(api_get_cod_request_details, methods=[constants.GET])
def get_cod_request_details():
    return base_api(request=request, api_name=api_get_cod_request_details, api=get_cod_request_data, logger=logger)


@cod_bp.route(api_create_cod_request, methods=[constants.POST])
def create_cod_request():
    return base_api(request=request, api_name=api_create_cod_request, api=create_cod_record, logger=logger)


@cod_bp.route(api_initial_defeat_approval, methods=[constants.POST, constants.PATCH])
def initial_defeat_approval():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_initial_defeat_approval, api=initial_defeat_approval_record,
                    api_params=api_params, logger=logger)


@cod_bp.route(api_defeat_extension_approval, methods=[constants.POST, constants.PATCH])
def defeat_extension_approval():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_defeat_extension_approval, api=defeat_extension_approval_record,
                    api_params=api_params, logger=logger)


@cod_bp.route(api_create_defeat_acknowledgement, methods=[constants.POST])
def create_defeat_acknowledgement():
    return base_api(request=request, api_name=api_create_defeat_acknowledgement,
                    api=create_defeat_acknowledgement_record, logger=logger)


@cod_bp.route(api_long_term_defeat, methods=[constants.POST, constants.PATCH])
def long_term_defeat():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_long_term_defeat, api=create_or_update_long_term_defeat_record,
                    api_params=api_params, logger=logger)


@cod_bp.route(api_discard_form, methods=[constants.DELETE])
def discard_form():
    return base_api(request=request, api_name=api_discard_form, api=discard_request_data, logger=logger)
