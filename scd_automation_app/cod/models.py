from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblCodMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    unique_rgn_id = db.Column(db.String(250), nullable=True)
    defeat_reason_id = db.Column(db.Integer, db.ForeignKey('tbl_defeat_reason_master.id'), nullable=True)
    original_defeat_reason = db.Column(db.String(250), nullable=True)
    monitored_equipments = db.Column(db.String(250), nullable=True)
    monitoring_role = db.Column(db.String(250), nullable=True)
    monitoring_frequency = db.Column(db.String(250), nullable=True)
    special_staffing_required = db.Column(db.Integer, nullable=True)
    special_staffing = db.Column(db.String(250), nullable=True)
    event_cause = db.Column(db.String(250), nullable=True)
    contingency_role = db.Column(db.String(250), nullable=True)
    preventive_measures = db.Column(db.String(250), nullable=True)
    evaluated_unit_conditions = db.Column(db.String(250), nullable=True)
    initial_defeat_approval_id = \
        db.Column(db.Integer, db.ForeignKey('tbl_cod_initial_defeat_approval_master.id'), nullable=True)
    defeat_extension_approval_id = \
        db.Column(db.Integer, db.ForeignKey('tbl_cod_defeat_extension_approval_master.id'), nullable=True)
    long_term_defeat_id = db.Column(db.Integer, db.ForeignKey('tbl_cod_long_term_defeat_master.id'), nullable=True)
    request_status = db.Column(db.Integer, nullable=False)
    defeat_status = db.Column(db.String(250), nullable=False)
    section_status = db.Column(db.Integer, nullable=False)


class TblCodFunctionLocations(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)


class TblCodCarSeals(BaseModel):
    request_group_number = db.Column(db.Integer, nullable=False)
    car_seal_id = db.Column(db.Integer, db.ForeignKey('tbl_car_seal_master.id'), nullable=True)


class TblCodInitialDefeatApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    immediate_ra_required = db.Column(db.Integer, nullable=False)
    remarks = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblCodDefeatExtensionApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    defeat_extension_reason = db.Column(db.String(250), nullable=True)
    ra_required = db.Column(db.Integer, nullable=False)
    remarks = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblCodUserAcknowledgementMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)
    request_group_number = db.Column(db.Integer, nullable=False)
    acknowledgement_suppressed = db.Column(db.Integer, nullable=False)


class TblCodUserAcknowledgements(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    user_acknowledgement_id = \
        db.Column(db.Integer, db.ForeignKey('tbl_cod_user_acknowledgement_master.id'), nullable=True)
    acknowledgement_time = db.Column(db.Numeric(15), nullable=False)


class TblCodLongTermDefeatMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    long_term_defeat_reason = db.Column(db.String(250), nullable=True)
    expiry_date = db.Column(db.Numeric(15), nullable=True)
    mitigation_present = db.Column(db.Integer, nullable=False)
    section_status = db.Column(db.Integer, nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
