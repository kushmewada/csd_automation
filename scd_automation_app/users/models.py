from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin

class TblRoleMaster(BaseModel):
    role_name = db.Column(db.String(250), nullable=False, unique=True)


class TblPermissionMaster(BaseModel):
    module_id = db.Column(db.Integer, nullable=False)
    permission_type = db.Column(db.Integer, nullable=False)
    is_available = db.Column(db.Integer, nullable=False)


class TblShiftMaster(BaseModel):
    shift = db.Column(db.String(250), nullable=False, unique=True)
    start_time = db.Column(db.String(250), nullable=False)
    end_time = db.Column(db.String(250), nullable=False)


class TblUserAreas(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    area_id = db.Column(db.Integer, db.ForeignKey('tbl_area_master.id'), nullable=False)


class TblUserRoles(BaseModel):
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('tbl_role_master.id'), nullable=False)


class TblRolePermissions(BaseModel):
    role_id = db.Column(db.Integer, db.ForeignKey('tbl_role_master.id'), nullable=False)
    permission_id = db.Column(db.Integer, db.ForeignKey('tbl_permission_master.id'), nullable=False)


class TblPendingActions(BaseModel):
    unit_id = db.Column(db.Integer, db.ForeignKey('tbl_unit_master.id'), nullable=True)
    assigned_to_role_id = db.Column(db.Integer, db.ForeignKey('tbl_role_master.id'), nullable=True)
    assigned_to = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)
    request_group_number = db.Column(db.Integer, nullable=False)
    pending_action_id = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    created_time = db.Column(db.Integer, nullable=False)
    completed_time = db.Column(db.Integer, nullable=True)
    completed_by = db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)


class TblUserActiveWorkflows(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    work_flow_id = db.Column(db.Integer, nullable=False)
    section_number = db.Column(db.Integer, nullable=False)
    section_status = db.Column(db.Integer, nullable=False)


class TblUserMaster(BaseModel):
    name = db.Column(db.String(250), nullable=False)
    system_user_name = db.Column(db.String(250), nullable=False)
    personnel_no = db.Column(db.Integer, nullable=True)
    email_id = db.Column(db.String(250), nullable=True)
    extension_no = db.Column(db.Integer, nullable=True)
    shift_id = db.Column(db.Integer, db.ForeignKey('tbl_shift_master.id'), nullable=True)
    user_status = db.Column(db.Integer, nullable=False)

    scr_s1_created_by = db.relationship(
        'TblScrMaster', backref='tbl_user_master_created_by', foreign_keys='TblScrMaster.created_by'
    )
    scr_s1_modified_by = db.relationship(
        'TblScrMaster', backref='tbl_user_master_modified_by', foreign_keys='TblScrMaster.modified_by'
    )

    scr_s2_created_by = db.relationship(
        'TblScrSheReviewMaster', backref='tbl_user_master_created_by', foreign_keys='TblScrSheReviewMaster.created_by'
    )
    scr_s2_modified_by = db.relationship(
        'TblScrSheReviewMaster', backref='tbl_user_master_modified_by', foreign_keys='TblScrSheReviewMaster.modified_by'
    )

    scr_s3_created_by = db.relationship(
        'TblScrOimsAdminApprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblScrOimsAdminApprovalMaster.created_by'
    )
    scr_s4_modified_by = db.relationship(
        'TblScrOimsAdminApprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblScrOimsAdminApprovalMaster.modified_by'
    )

    scr_s4_created_by = db.relationship(
        'TblScrAreaBtmApprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblScrAreaBtmApprovalMaster.created_by'
    )
    scr_s4_modified_by = db.relationship(
        'TblScrAreaBtmApprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblScrAreaBtmApprovalMaster.modified_by'
    )

    scr_s5_created_by = db.relationship(
        'TblScrReliabilityAssessmentMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblScrReliabilityAssessmentMaster.created_by'
    )
    scr_s5_modified_by = db.relationship(
        'TblScrReliabilityAssessmentMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblScrReliabilityAssessmentMaster.modified_by'
    )

    cod_s1_created_by = db.relationship(
        'TblCodMaster', backref='tbl_user_master_created_by', foreign_keys='TblCodMaster.created_by'
    )
    cod_s1_modified_by = db.relationship(
        'TblCodMaster', backref='tbl_user_master_modified_by', foreign_keys='TblCodMaster.modified_by'
    )

    cod_s2_created_by = db.relationship(
        'TblCodInitialDefeatApprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblCodInitialDefeatApprovalMaster.created_by'
    )
    cod_s2_modified_by = db.relationship(
        'TblCodInitialDefeatApprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblCodInitialDefeatApprovalMaster.modified_by'
    )

    cod_s3_created_by = db.relationship(
        'TblCodDefeatExtensionApprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblCodDefeatExtensionApprovalMaster.created_by'
    )
    cod_s3_modified_by = db.relationship(
        'TblCodDefeatExtensionApprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblCodDefeatExtensionApprovalMaster.modified_by'
    )

    cod_s4_created_by = db.relationship(
        'TblCodUserAcknowledgementMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblCodUserAcknowledgementMaster.created_by'
    )
    cod_s4_modified_by = db.relationship(
        'TblCodUserAcknowledgementMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblCodUserAcknowledgementMaster.modified_by'
    )

    cod_s5_created_by = db.relationship(
        'TblCodLongTermDefeatMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblCodLongTermDefeatMaster.created_by'
    )
    cod_s5_modified_by = db.relationship(
        'TblCodLongTermDefeatMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblCodLongTermDefeatMaster.modified_by'
    )

    pmdr_s1_created_by = db.relationship(
        'TblPmdrMaster', backref='tbl_user_master_created_by', foreign_keys='TblPmdrMaster.created_by'
    )
    pmdr_s1_modified_by = db.relationship(
        'TblPmdrMaster', backref='tbl_user_master_modified_by', foreign_keys='TblPmdrMaster.modified_by'
    )

    pmdr_s2_created_by = db.relationship(
        'TblPmdrInitialDefermentMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblPmdrInitialDefermentMaster.created_by'
    )
    pmdr_s2_modified_by = db.relationship(
        'TblPmdrInitialDefermentMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblPmdrInitialDefermentMaster.modified_by'
    )

    pmdr_s3_created_by = db.relationship(
        'TblPmdrAreaBtmApprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblPmdrAreaBtmApprovalMaster.created_by'
    )
    pmdr_s3_modified_by = db.relationship(
        'TblPmdrAreaBtmApprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblPmdrAreaBtmApprovalMaster.modified_by'
    )

    pmdr_s4_created_by = db.relationship(
        'TblPmdrDefermentExtensionMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblPmdrDefermentExtensionMaster.created_by'
    )
    pmdr_s4_modified_by = db.relationship(
        'TblPmdrDefermentExtensionMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblPmdrDefermentExtensionMaster.modified_by'
    )

    pmdr_s5_created_by = db.relationship(
        'TblPmdrOperationsGmReapprovalMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblPmdrOperationsGmReapprovalMaster.created_by'
    )
    pmdr_s5_modified_by = db.relationship(
        'TblPmdrOperationsGmReapprovalMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblPmdrOperationsGmReapprovalMaster.modified_by'
    )

    pmdr_s6_created_by = db.relationship(
        'TblPmdrJobSchedulerMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblPmdrJobSchedulerMaster.created_by'
    )
    pmdr_s6_modified_by = db.relationship(
        'TblPmdrJobSchedulerMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblPmdrJobSchedulerMaster.modified_by'
    )

    pending_actions_assigned_to = db.relationship(
        'TblPendingActions', backref='tbl_user_master_assigned_to', foreign_keys='TblPendingActions.assigned_to'
    )
    pending_actions_completed_by = db.relationship(
        'TblPendingActions', backref='tbl_user_master_completed_by', foreign_keys='TblPendingActions.completed_by'
    )

    scr_lead_confirmation_master_created_by = db.relationship(
        'TblScrLeadConfirmationMaster',
        backref='tbl_user_master_created_by',
        foreign_keys='TblScrLeadConfirmationMaster.created_by'
    )
    scr_lead_confirmation_master_modified_by = db.relationship(
        'TblScrLeadConfirmationMaster',
        backref='tbl_user_master_modified_by',
        foreign_keys='TblScrLeadConfirmationMaster.modified_by'
    )
