import constants
from common_packages import db


def add_or_update_basic_user_data(request_data=None, user_id=None, user_table=None):
    user_fields = {
        'name': request_data['name'].capitalize(),
        'system_user_name': request_data['system_user_name'].upper(),
        'personnel_no': request_data['personnel_no'],
        'email_id': request_data['email_id'],
        'extension_no': int(request_data['extension_no']),
        'shift_id': int(request_data['shift_id']),
        'user_status': constants.USER_STATUS['ACTIVE']
    }

    if not user_id:
        record = user_table(**user_fields)
        db.session.add(record)
        db.session.commit()
        return record.id
    else:
        user_table.query.filter_by(id=user_id).update(dict(user_fields))
        db.session.commit()
        return user_id


def delete_user_data(table=None, user_id=None):
    table.query.filter_by(user_id=user_id).delete()
    db.session.commit()
