import constants
from common_packages import db
from scd_automation_app.common.models import TblAreaMaster
from scd_automation_app.groups.helpers import get_group_data
from scd_automation_app.groups.models import TblDepartmentMaster, TblUserDepartments, TblSectionMaster, TblUserSections
from scd_automation_app.groups.models import (
    TblGroupMaster, TblPlannerGroupMaster, TblWorkCenterMaster, TblUserGroups, TblUserPlannerGroups, TblUserWorkCenters
)
from scd_automation_app.samref.common_functions import (
    get_key_value_pair_list, get_list_from_comma_separated_str, convert_dict_to_list, sort_list_of_dictionaries
)
from scd_automation_app.samref.common_helpers import (
    format_list_api_data,
    create_response,
    check_user_associations,
    add_or_update_record,
    add_or_update_id_table,
    check_for_existing_master_record
)
from scd_automation_app.settings.models import TblSettingsMaster
from .helpers import delete_user_data, add_or_update_basic_user_data
from .models import (
    TblUserMaster,
    TblRoleMaster,
    TblPermissionMaster,
    TblShiftMaster,
    TblUserRoles,
    TblRolePermissions,
    TblUserAreas,
    TblUserActiveWorkflows
)

"""
Employee Controller Functions Start.
"""


def get_user_records(request_data=None, logger=None):
    user_data_dict = dict()
    filters = list()

    search_text = request_data.get('search_text', None)
    if search_text:
        filters.append(TblUserMaster.name.contains(search_text))

    query = TblUserMaster.query.join(
        TblUserAreas, TblUserAreas.user_id == TblUserMaster.id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUserAreas.area_id
    ).outerjoin(
        TblUserRoles, TblUserRoles.user_id == TblUserMaster.id
    ).outerjoin(
        TblRoleMaster, TblRoleMaster.id == TblUserRoles.role_id
    ).outerjoin(
        TblShiftMaster, TblShiftMaster.id == TblUserMaster.shift_id
    )

    group_type = request_data.get('group_type', None)
    if group_type:
        group_id = int(request_data.get('group_id'))
        group_table, user_group_table, group_id_field = get_group_data(
            group_type=int(group_type), group_name_field_required=False
        )

        filters.append(group_table.id == group_id)
        query = query.outerjoin(
            user_group_table, user_group_table.user_id == TblUserMaster.id
        ).outerjoin(
            group_table, group_table.id == getattr(user_group_table, group_id_field)
        )

    records = query.add_columns(
        TblUserMaster.name,
        TblUserMaster.user_status,
        TblUserMaster.shift_id,
        TblUserAreas.user_id,
        TblUserAreas.area_id,
        TblUserRoles.role_id,
        TblAreaMaster.area,
        TblRoleMaster.role_name,
        TblShiftMaster.shift
    ).filter(
        *filters
    ).order_by(
        TblUserMaster.name
    ).all()

    for record in records:
        if record.user_id not in user_data_dict:
            user_data_dict[record.user_id] = {
                'id': record.user_id,
                'name': record.name,
                'area_list': dict(),
                'role_list': dict(),
                'shift': {'id': record.shift_id, 'name': record.shift} if record.shift_id else dict(),
                'status': record.user_status
            }
        user_data_dict[record.user_id]['area_list'][record.area_id] = {'id': record.area_id, 'name': record.area}

        if record.role_id:
            user_data_dict[record.user_id]['role_list'][record.role_id] = {
                'id': record.role_id, 'name': record.role_name
            }

    user_data_list = convert_dict_to_list(records=user_data_dict)
    for user in user_data_list:
        user['area_list'] = convert_dict_to_list(records=user['area_list'])
        user['role_list'] = convert_dict_to_list(records=user['role_list'])

    data = format_list_api_data(
        records=user_data_list,
        is_dict=False,
        list_name=constants.KEY_DICT['USER_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_user_details(request_data=None, user_id=None, logger=None):
    user_data = dict()

    user_data_records = TblUserMaster.query.outerjoin(
        TblUserAreas, TblUserAreas.user_id == TblUserMaster.id
    ).outerjoin(
        TblAreaMaster, TblAreaMaster.id == TblUserAreas.area_id
    ).outerjoin(
        TblUserDepartments, TblUserDepartments.user_id == TblUserMaster.id
    ).outerjoin(
        TblDepartmentMaster, TblDepartmentMaster.id == TblUserDepartments.department_id
    ).outerjoin(
        TblUserSections, TblUserSections.user_id == TblUserMaster.id
    ).outerjoin(
        TblSectionMaster, TblSectionMaster.id == TblUserSections.section_id
    ).outerjoin(
        TblUserGroups, TblUserGroups.user_id == TblUserMaster.id
    ).outerjoin(
        TblGroupMaster, TblGroupMaster.id == TblUserGroups.group_id
    ).outerjoin(
        TblUserPlannerGroups, TblUserPlannerGroups.user_id == TblUserMaster.id
    ).outerjoin(
        TblPlannerGroupMaster, TblPlannerGroupMaster.id == TblUserPlannerGroups.planner_group_id
    ).outerjoin(
        TblUserWorkCenters, TblUserWorkCenters.user_id == TblUserMaster.id
    ).outerjoin(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblUserWorkCenters.work_center_id
    ).outerjoin(
        TblShiftMaster, TblShiftMaster.id == TblUserMaster.shift_id
    ).add_columns(
        TblUserMaster.id,
        TblUserMaster.name,
        TblUserMaster.system_user_name,
        TblUserMaster.personnel_no,
        TblUserMaster.email_id,
        TblUserMaster.extension_no,
        TblUserMaster.shift_id,
        TblUserMaster.user_status,
        TblUserGroups.group_id,
        TblGroupMaster.group_name,
        TblUserPlannerGroups.planner_group_id,
        TblPlannerGroupMaster.planner_group_name,
        TblUserWorkCenters.work_center_id,
        TblWorkCenterMaster.work_center_name,
        TblUserDepartments.department_id,
        TblDepartmentMaster.department_name,
        TblUserSections.section_id,
        TblSectionMaster.section_name,
        TblUserAreas.area_id,
        TblAreaMaster.area,
        TblShiftMaster.shift
    ).filter(
        TblUserMaster.id == user_id
    ).all()

    user_role_records = TblUserMaster.query.join(
        TblUserRoles, TblUserRoles.user_id == TblUserMaster.id
    ).join(
        TblRoleMaster, TblRoleMaster.id == TblUserRoles.role_id
    ).join(
        TblRolePermissions, TblRolePermissions.role_id == TblRoleMaster.id
    ).join(
        TblPermissionMaster, TblPermissionMaster.id == TblRolePermissions.permission_id
    ).add_columns(
        TblUserRoles.role_id,
        TblRoleMaster.role_name,
        TblRolePermissions.permission_id,
        TblRolePermissions.role_id,
        TblPermissionMaster.module_id,
        TblPermissionMaster.permission_type,
        TblPermissionMaster.is_available
    ).filter(
        TblUserMaster.id == user_id
    ).all()

    for record in user_data_records:
        if record.id not in user_data:
            user_data[record.id] = {
                'personnel_name': record.name,
                'system_user_name': record.system_user_name,
                'personnel_number': record.personnel_no,
                'email_id': record.email_id,
                'extension_no': record.extension_no,
                'shift': {'id': record.shift_id, 'name': record.shift} if record.shift_id else dict(),
                'area_list': dict(),
                'role_list': dict(),
                'department_list': dict(),
                'section_list': dict(),
                'group_list': dict(),
                'planner_group_list': dict(),
                'work_center_list': dict()
            }

        user_data[record.id]['area_list'][record.area_id] = {'id': record.area_id, 'name': record.area}
        if record.department_id:
            user_data[record.id]['department_list'][record.department_id] = {
                'id': record.department_id, 'name': record.department_name
            }

        if record.section_id:
            user_data[record.id]['section_list'][record.section_id] = {
                'id': record.section_id, 'name': record.section_name
            }

        if record.group_id:
            user_data[record.id]['group_list'][record.group_id] = {'id': record.group_id, 'name': record.group_name}

        if record.planner_group_id:
            user_data[record.id]['planner_group_list'][record.planner_group_id] = {
                'id': record.planner_group_id, 'name': record.planner_group_name
            }

        if record.work_center_id:
            user_data[record.id]['work_center_list'][record.work_center_id] = {
                'id': record.work_center_id, 'name': record.work_center_name
            }

    user_data = convert_dict_to_list(records=user_data)[0]
    for record in user_role_records:
        if record.role_id not in user_data['role_list']:
            user_data['role_list'][record.role_id] = {
                'id': record.role_id, 'name': record.role_name, 'permissions': dict()
            }

        if record.permission_id not in user_data['role_list'][record.role_id]['permissions']:
            user_data['role_list'][record.role_id]['permissions'][record.permission_id] = {
                'id': record.permission_id,
                'module_id': record.module_id,
                'permission_type': record.permission_type,
                'is_available': record.is_available
            }

    for role_id in user_data['role_list']:
        user_data['role_list'][role_id]['permissions'] = convert_dict_to_list(
            records=user_data['role_list'][role_id]['permissions']
        )

    for key in user_data:
        if type(user_data[key]) == dict and key != constants.KEY_DICT['SHIFT']:
            user_data[key] = convert_dict_to_list(records=user_data[key])

    return create_response(get_success_response=constants.SUCCESS, data=user_data)


def add_or_update_user_record(request_data=None, user_id=None, logger=None):
    user_id = add_or_update_basic_user_data(request_data=request_data, user_id=user_id, user_table=TblUserMaster)
    department_ids = request_data.get('department_ids')
    section_ids = request_data.get('section_ids')
    group_ids = request_data.get('group_ids')
    planner_group_ids = request_data.get('planner_group_ids')
    work_center_ids = request_data.get('work_center_ids')
    role_ids = request_data.get('role_ids')

    add_or_update_id_table(
        table=TblUserAreas,
        common_field=constants.KEY_DICT['USER_ID'],
        common_value=user_id,
        field=constants.KEY_DICT['AREA_ID'],
        current_ids=request_data['area_ids']
    )

    if department_ids:
        add_or_update_id_table(
            table=TblUserDepartments,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['DEPARTMENT_ID'],
            current_ids=request_data['department_ids']
        )

    if section_ids:
        add_or_update_id_table(
            table=TblUserSections,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['SECTION_ID'],
            current_ids=request_data['section_ids']
        )

    if group_ids:
        add_or_update_id_table(
            table=TblUserGroups,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['GROUP_ID'],
            current_ids=request_data['group_ids']
        )

    if planner_group_ids:
        add_or_update_id_table(
            table=TblUserPlannerGroups,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['PLANNER_GROUP_ID'],
            current_ids=request_data['planner_group_ids']
        )

    if work_center_ids:
        add_or_update_id_table(
            table=TblUserWorkCenters,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['WORK_CENTER_ID'],
            current_ids=request_data['work_center_ids']
        )

    if role_ids:
        add_or_update_id_table(
            table=TblUserRoles,
            common_field=constants.KEY_DICT['USER_ID'],
            common_value=user_id,
            field=constants.KEY_DICT['ROLE_ID'],
            current_ids=request_data['role_ids']
        )
    return create_response(post_success_response=constants.SUCCESS)


def change_user_status(request_data=None, user_id=None, logger=None):
    TblUserMaster.query.filter_by(id=user_id).update(dict(user_status=request_data['user_status']))
    db.session.commit()
    return create_response(patch_success_response=constants.SUCCESS)


def delete_user_records(request_data=None, logger=None):
    employee_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['user_ids'])
    for user_id in employee_id_list:
        change_user_status(
            request_data={'user_status': constants.USER_STATUS['DELETED']}, user_id=user_id, logger=logger
        )
        table_list = [TblUserAreas, TblUserDepartments, TblUserSections, TblUserGroups, TblUserPlannerGroups,
                      TblUserWorkCenters, TblUserRoles]
        for table in table_list:
            delete_user_data(table=table, user_id=user_id)
    return create_response(delete_success_response=constants.SUCCESS)


"""
Employee Controller Functions End.
"""


"""
Shift Controller Functions Start.
"""


def get_shift_record(request_data=None, shift_id=None, logger=None):
    shift = TblShiftMaster.query.filter_by(id=shift_id).first()
    data = {'id': shift.id, 'name': shift.shift, 'start_time': shift.start_time, 'end_time': shift.end_time}
    return create_response(get_success_response=True, data=data)


def get_shift_records(request_data=None, logger=None):
    filters = list()

    search_text = request_data.get('search_text')
    if search_text:
        filters.append(TblShiftMaster.shift.contains(search_text))

    shifts = TblShiftMaster.query.filter(*filters).order_by(TblShiftMaster.shift).all()
    shifts = [
        {'id': shift.id, 'name': shift.shift, 'start_time': shift.start_time, 'end_time': shift.end_time}
        for shift in shifts
    ]
    data = format_list_api_data(
        records=shifts,
        is_dict=False,
        list_name=constants.KEY_DICT['SHIFT_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def create_shift_record(request_data=None, logger=None):
    shift_record = TblShiftMaster(**request_data)
    db.session.add(shift_record)
    db.session.commit()
    return create_response(post_success_response=True)


def update_shift_record(request_data=None, shift_id=None, logger=None):
    TblShiftMaster.query.filter_by(id=shift_id).update(dict(request_data))
    db.session.commit()
    return create_response(patch_success_response=True)


def delete_shift_record(request_data=None, shift_id=None, logger=None):
    users = TblUserMaster.query.filter_by(shift_id=shift_id).all()
    if users:
        return create_response(status=constants.FAILURE, error_message=constants.SHIFT_ASSIGNED_TO_USERS)

    TblShiftMaster.query.filter_by(id=shift_id).delete()
    db.session.commit()
    return create_response(delete_success_response=True)


"""
Shift Controller Functions End.
"""


"""
Roles & Permissions Controller Functions Start.
"""


def get_role_records(request_data=None, logger=None):
    filters = list()
    search_text = request_data.get('search_text', None)

    if search_text:
        filters.append(TblRoleMaster.role_name.contains(search_text))

    records = TblRoleMaster.query.filter(*filters).order_by(TblRoleMaster.role_name).all()
    data = format_list_api_data(
        records=get_key_value_pair_list(record_list=records, record_key_2=constants.KEY_DICT['ROLE_NAME']),
        is_dict=False,
        list_name=constants.KEY_DICT['ROLE_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_role_record(request_data=None, role_id=None, logger=None):
    role = TblRoleMaster.query.filter_by(id=role_id).first()
    role_permissions = TblRolePermissions.query.filter_by(role_id=role_id).all()
    data = {
        'id': role_id,
        'name': role.role_name,
        'permission_list': [record.permission_id for record in role_permissions]
    }
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_permission_list(request_data=None, logger=None):
    permission_data = {'permissions': list()}

    for module in constants.MODULE_DETAILS:
        sub_module_id = None
        module_details = {
            'title': constants.MODULE_DETAILS[module]['title'],
            'identity': constants.MODULE_DETAILS[module]['identity'],
            'sortOrder': constants.MODULE_DETAILS[module]['sort_order'],
            'subModules': list()
        }

        if module == constants.WORK_FLOW_DICT[constants.SCR]:
            sub_module_id = constants.SCR
        elif module == constants.WORK_FLOW_DICT[constants.COD]:
            sub_module_id = constants.COD
        elif module == constants.WORK_FLOW_DICT[constants.PMDR]:
            sub_module_id = constants.PMDR
        elif module == constants.WORK_FLOW_DICT[constants.CSVR]:
            sub_module_id = constants.CSVR
        elif module == constants.WORK_FLOW_DICT[constants.MAIN_DASHBOARD].upper():
            sub_module_id = constants.MAIN_DASHBOARD
        elif module == constants.WORK_FLOW_DICT[constants.AREA_SHIFT_TEAM_DASHBOARD].upper():
            sub_module_id = constants.AREA_SHIFT_TEAM_DASHBOARD
        elif module == constants.WORK_FLOW_DICT[constants.MAINTENANCE_DASHBOARD].upper():
            sub_module_id = constants.MAINTENANCE_DASHBOARD
        elif module == constants.WORK_FLOW_DICT[constants.MASTERS].upper():
            sub_module_id = constants.MASTERS

        for section in constants.SUB_MODULE_DETAILS[sub_module_id]:
            permission_id = constants.SUB_MODULE_DETAILS[sub_module_id][section]['permission_id']
            permission_records = TblPermissionMaster.query.filter_by(module_id=permission_id).all()

            permissions = [
                {
                    'id': record.id,
                    'action': constants.BASIC_PERMISSIONS_DETAILS[record.permission_type]['ACTION'],
                    'description': '',
                    'is_available': record.is_available,
                    'sort_order': constants.BASIC_PERMISSIONS_DETAILS[record.permission_type]['SORT_ORDER']
                } for record in permission_records
            ]

            sub_module_details = {
                'title': constants.SUB_MODULE_DETAILS[sub_module_id][section]['title'],
                'module': constants.SUB_MODULE_DETAILS[sub_module_id][section]['module'],
                'permissions': sort_list_of_dictionaries(list_of_dict=permissions, key=constants.KEY_DICT['ID'])
            }
            module_details['subModules'].append(sub_module_details)

        permission_data['permissions'].append(module_details)
    return create_response(get_success_response=constants.SUCCESS, data=permission_data)


def create_role_record(request_data=None, logger=None):
    role_name = request_data['role_name'].title()
    status = check_for_existing_master_record(
        table=TblRoleMaster, key_field=constants.KEY_DICT['ROLE_NAME'], key_field_value=role_name
    )
    if status:
        return create_response(status=constants.FAILURE, error_message=constants.ROLE_ALREADY_EXISTS)

    role_id = add_or_update_record(table=TblRoleMaster, field=constants.KEY_DICT['ROLE_NAME'], field_value=role_name)
    add_or_update_id_table(
        table=TblRolePermissions,
        common_field=constants.KEY_DICT['ROLE_ID'],
        common_value=role_id,
        field=constants.KEY_DICT['PERMISSION_ID'],
        current_ids=request_data['permission_ids']
    )
    return create_response(post_success_response=constants.SUCCESS)


def update_role_record(request_data=None, role_id=None, logger=None):
    role_name = request_data['role_name'].title()

    status = check_for_existing_master_record(
        table=TblRoleMaster, id_value=role_id, key_field=constants.KEY_DICT['ROLE_NAME'], key_field_value=role_name
    )
    if status:
        return create_response(status=constants.FAILURE, error_message=constants.ROLE_ALREADY_EXISTS)

    add_or_update_record(
        table=TblRoleMaster, id_value=role_id, field=constants.KEY_DICT['ROLE_NAME'], field_value=role_name
    )
    TblRoleMaster.query.filter_by(id=role_id).update(dict(role_name=role_name))
    db.session.commit()

    add_or_update_id_table(
        table=TblRolePermissions,
        common_field=constants.KEY_DICT['ROLE_ID'],
        common_value=role_id,
        field=constants.KEY_DICT['PERMISSION_ID'],
        current_ids=request_data['permission_ids']
    )
    return create_response(put_success_response=constants.SUCCESS)


def delete_role_record(request_data=None, role_id=None, logger=None):
    status = check_user_associations(
        user_association_table=TblUserRoles,
        association_id_field=constants.KEY_DICT['ROLE_ID'],
        association_id_value=role_id
    )
    if status:
        return create_response(status=constants.FAILURE, error_message=constants.ROLE_ASSIGNED_TO_USERS)

    TblRolePermissions.query.filter_by(role_id=role_id).delete()
    db.session.commit() 

    TblRoleMaster.query.filter_by(id=role_id).delete()
    db.session.commit()
    return create_response(delete_success_response=constants.SUCCESS)


"""
Roles & Permissions Controller Functions End.
"""


"""
User Active Workflow Controller Functions Start.
"""


def get_user_active_work_flow_record(request_data=None, logger=None):
    work_flow_data = dict()
    request_group_number = request_data.get('request_group_number', None)
    work_flow_id = int(request_data['work_flow_id'])
    section_id = int(request_data['section_id'])
    created_by = int(request_data['user_id'])

    fields = {
        'work_flow_id': work_flow_id,
        'section_number': section_id,
        'section_status': constants.TEXTUAL_DATA_SUBMITTED,
        'created_by': created_by
    }
    if request_group_number:
        fields.update({'request_group_number': int(request_group_number)})

    record = TblUserActiveWorkflows.query.filter_by(**fields).first()
    if record:
        work_flow_data = {
            'request_group_number': record.request_group_number, 'section_fill_status': record.section_status
        }

    return create_response(get_success_response=constants.SUCCESS, data=work_flow_data)


"""
User Active Workflow Controller Functions End.
"""
