from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_user_details,
    get_user_records,
    add_or_update_user_record,
    change_user_status,
    delete_user_records,

    get_shift_record,
    get_shift_records,
    create_shift_record,
    update_shift_record,
    delete_shift_record,

    get_role_records,
    get_role_record,
    get_permission_list,
    create_role_record,
    update_role_record,
    delete_role_record,

    get_user_active_work_flow_record
)

blueprint_name = constants.BLUE_PRINT_DICT['MASTERS']['USERS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['MASTERS']['USERS']['URL_PREFIX']
users_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['USER_LOG'])


api_get_user = constants.API_DICT['MASTERS']['USERS']['GET_USER']
api_get_user_list = constants.API_DICT['MASTERS']['USERS']['GET_USER_LIST']
api_add_user = constants.API_DICT['MASTERS']['USERS']['ADD_USER']
api_update_user = constants.API_DICT['MASTERS']['USERS']['UPDATE_USER']
api_update_user_status = constants.API_DICT['MASTERS']['USERS']['UPDATE_USER_STATUS']
api_delete_users = constants.API_DICT['MASTERS']['USERS']['DELETE_USERS']

api_get_shift = constants.API_DICT['MASTERS']['USERS']['GET_SHIFT']
api_get_shift_list = constants.API_DICT['MASTERS']['USERS']['GET_SHIFT_LIST']
api_create_shift = constants.API_DICT['MASTERS']['USERS']['CREATE_SHIFT']
api_update_shift = constants.API_DICT['MASTERS']['USERS']['UPDATE_SHIFT']
api_delete_shift = constants.API_DICT['MASTERS']['USERS']['DELETE_SHIFT']

api_get_role_list = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['GET_ROLE_LIST']
api_get_role = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['GET_ROLE']
api_get_permissions = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['GET_PERMISSIONS']
api_create_role = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['CREATE_ROLE']
api_update_role = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['UPDATE_ROLE']
api_delete_role = constants.API_DICT['MASTERS']['USERS']['ROLES_AND_PERMISSIONS']['DELETE_ROLE']

api_get_user_active_work_flow = \
    constants.API_DICT['MASTERS']['USERS']['USER_ACTIVE_WORKFLOW']['GET_USER_ACTIVE_WORKFLOW']

"""
Employee APIs Start.
"""


@users_bp.route(api_get_user_list, methods=[constants.GET])
def get_user_list():
    return base_api(request=request, api_name=api_get_user_list, api=get_user_records, logger=logger)


@users_bp.route(api_get_user, methods=[constants.GET])
def get_user(record_id):
    return base_api(request=request, api_name=api_get_user, api=get_user_details,
                    api_params={'user_id': int(record_id)}, logger=logger)


@users_bp.route(api_add_user, methods=[constants.POST])
@users_bp.route(api_update_user, methods=[constants.PATCH])
def add_or_update_user(record_id=None):
    return base_api(request=request, api_name=api_add_user if record_id else api_update_user,
                    api=add_or_update_user_record, api_params={'user_id': int(record_id)} if record_id else None,
                    logger=logger)


@users_bp.route(api_update_user_status, methods=[constants.PATCH])
def update_user_status(record_id):
    return base_api(request=request, api_name=api_update_user_status, api=change_user_status,
                    api_params={'user_id': int(record_id)}, logger=logger)


@users_bp.route(api_delete_users, methods=[constants.DELETE])
def delete_users():
    return base_api(request=request, api_name=api_delete_users, api=delete_user_records, logger=logger)


"""
Employee APIs End.
"""


"""
Shift APIs Start.
"""


@users_bp.route(api_get_shift, methods=[constants.GET])
def get_shift(shift_id):
    return base_api(request=request, api_name=api_get_shift, api=get_shift_record,
                    api_params={'shift_id': int(shift_id)}, logger=logger)


@users_bp.route(api_get_shift_list, methods=[constants.GET])
def get_shift_list():
    return base_api(request=request, api_name=api_get_shift_list, api=get_shift_records, logger=logger)


@users_bp.route(api_create_shift, methods=[constants.POST])
def create_shift():
    return base_api(request=request, api_name=api_create_shift, api=create_shift_record, logger=logger)


@users_bp.route(api_update_shift, methods=[constants.PUT])
def update_shift(shift_id):
    return base_api(request=request, api_name=api_update_shift, api=update_shift_record,
                    api_params={'shift_id': int(shift_id)}, logger=logger)


@users_bp.route(api_delete_shift, methods=[constants.DELETE])
def delete_shift(shift_id):
    return base_api(request=request, api_name=api_delete_shift, api=delete_shift_record,
                    api_params={'shift_id': int(shift_id)}, logger=logger)


"""
Shift APIs End.
"""


"""
Roles & Permissions APIs Start.
"""


@users_bp.route(api_get_role_list, methods=[constants.GET])
def get_role_list():
    return base_api(request=request, api_name=api_get_role_list, api=get_role_records, logger=logger)


@users_bp.route(api_get_role, methods=[constants.GET])
def get_role(record_id):
    return base_api(request=request, api_name=api_get_role, api=get_role_record, api_params={'role_id': int(record_id)},
                    logger=logger)


@users_bp.route(api_get_permissions, methods=[constants.GET])
def get_permissions():
    return base_api(request=request, api_name=api_get_permissions, api=get_permission_list, logger=logger)


@users_bp.route(api_create_role, methods=[constants.POST])
def create_role():
    return base_api(request=request, api_name=api_get_permissions, api=create_role_record, logger=logger)


@users_bp.route(api_update_role, methods=[constants.PUT])
def update_role(record_id):
    return base_api(request=request, api_name=api_get_permissions, api=update_role_record,
                    api_params={'role_id': int(record_id)}, logger=logger)


@users_bp.route(api_delete_role, methods=[constants.DELETE])
def delete_role(record_id):
    return base_api(request=request, api_name=api_get_permissions, api=delete_role_record,
                    api_params={'role_id': int(record_id)}, logger=logger)


"""
Roles & Permissions APIs End.
"""


"""
User Active Work Flow APIs Start.
"""


@users_bp.route(api_get_user_active_work_flow, methods=[constants.GET])
def get_user_active_work_flow():
    return base_api(request=request, api_name=api_get_user_active_work_flow, api=get_user_active_work_flow_record,
                    logger=logger)


"""
User Active Work Flow APIs End.
"""
