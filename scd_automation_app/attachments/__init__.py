from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import get_document, upload_document, remove_attachment

blueprint_name = constants.BLUE_PRINT_DICT['ATTACHMENTS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['ATTACHMENTS']['URL_PREFIX']
attachment_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['ATTACHMENT_LOG'])


api_get_attachment = constants.API_DICT['ATTACHMENTS']['GET_ATTACHMENT']
api_upload_attachment = constants.API_DICT['ATTACHMENTS']['UPLOAD_ATTACHMENT']
api_delete_attachment = constants.API_DICT['ATTACHMENTS']['DELETE_ATTACHMENT']


@attachment_bp.route(api_get_attachment, methods=[constants.GET])
def get_attachment(document_id):
    return base_api(request=request, api_name=api_get_attachment, api=get_document,
                    api_params={'document_id': int(document_id)}, logger=logger)


@attachment_bp.route(api_upload_attachment, methods=[constants.POST])
def upload_attachment():
    return base_api(request=request, api_name=api_upload_attachment, api=upload_document,
                    api_params={'files': request.files.getlist('files')}, logger=logger)


@attachment_bp.route(api_delete_attachment, methods=[constants.DELETE])
def delete_attachment(document_id):
    return base_api(request=request, api_name=api_delete_attachment, api=remove_attachment,
                    api_params={'document_id': int(document_id)}, logger=logger)
