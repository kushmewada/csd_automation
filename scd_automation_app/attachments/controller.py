import constants
from common_packages import db
from scd_automation_app.samref.common_helpers import create_response, delete_document
from scd_automation_app.samref.document_store import store_attachments, get_file
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserActiveWorkflows
from .models import TblDocumentMaster, TblWorkFlowDocuments


def get_document(request_data=None, document_id=None, logger=None):
    document = TblDocumentMaster.query.filter_by(id=document_id).first()
    logger.info('Fetching file: original_name - ' + document.original_name + ' secured_name - ' + document.secured_name
                + 'directory_path - ' + str(document.directory_path))
    return get_file(document.directory_path, document.secured_name)


def upload_document(request_data=None, files=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    work_flow_id = int(request_data['work_flow_id'])
    work_flow_section_id = int(request_data['work_flow_section_id'])

    controller_rgn_str = \
        constants.WORK_FLOW_CONTROLLER_DICT[work_flow_id][work_flow_section_id] + ' - ' + str(request_group_number)
    logger.info(controller_rgn_str)

    work_flow_documents_fields = {
        'request_group_number': request_group_number,
        'work_flow_id': work_flow_id,
        'work_flow_section_id': work_flow_section_id
    }

    user_active_work_flow = TblUserActiveWorkflows.query.filter_by(
        request_group_number=request_group_number,
        work_flow_id=work_flow_id,
        section_number=work_flow_section_id,
        section_status=constants.TEXTUAL_DATA_SUBMITTED
    ).first()
    if user_active_work_flow is None:
        return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

    records = TblWorkFlowDocuments.query.filter_by(**work_flow_documents_fields).all()
    attachment_setting_record = TblSettingsMaster.query.filter_by(id=constants.ATTACHMENTS_PER_SECTION_ID).first()
    if len(records) >= attachment_setting_record.property_value:
        return create_response(status=constants.FAILURE, error_message=constants.MAX_UPLOAD_LIMIT_REACHED)

    status, storage_data = store_attachments(
        files=files, settings_table=TblSettingsMaster, controller_rgn_str=controller_rgn_str, logger=logger
    )
    logger.info(controller_rgn_str + ', status - ' + str(status) + ', attachment_data - ' + str(storage_data))
    if status:
        document_id_list = list()
        for file_data in storage_data:
            document = TblDocumentMaster(**file_data)
            db.session.add(document)
            db.session.commit()
            document_id_list.append(document.id)

            work_flow_documents_fields.update({'document_id': document.id})
            work_flow_document = TblWorkFlowDocuments(**work_flow_documents_fields)
            db.session.add(work_flow_document)
            db.session.commit()
        return create_response(post_success_response=True, data={'document_id_list': document_id_list})
    else:
        return create_response(status=status, data=storage_data)


def remove_attachment(request_data=None, document_id=None, logger=None):
    delete_document(document_master_table=TblDocumentMaster, work_flow_document_table=TblWorkFlowDocuments,
                    document_id=document_id, logger=logger)
    return create_response(delete_success_response=True)
