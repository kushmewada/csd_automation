from common_packages import db
from scd_automation_app.samref.base_model import BaseModel


class TblDocumentMaster(BaseModel):
    # original_name = db.Column(db.String(8000), nullable=False)
    original_name = db.Column(db.String(250), nullable=False)
    # secured_name = db.Column(db.String(8000), nullable=False)
    secured_name = db.Column(db.String(250), nullable=False)
    # directory_path = db.Column(db.String(8000), nullable=False)
    directory_path = db.Column(db.String(250), nullable=False)


class TblWorkFlowDocuments(BaseModel):
    document_id = db.Column(db.Integer, db.ForeignKey('tbl_document_master.id'), nullable=True)
    request_group_number = db.Column(db.Integer, nullable=False)
    work_flow_id = db.Column(db.Integer, nullable=False)
    work_flow_section_id = db.Column(db.Integer, nullable=False)
