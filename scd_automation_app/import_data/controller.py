from concurrent.futures import ThreadPoolExecutor

from pandas import read_excel

import constants
from common_packages import db, init_app
from scd_automation_app.common.models import TblTaskMaster
from scd_automation_app.pmdr.models import TblWorkOrderMaster
from scd_automation_app.samref.common_functions import convert_date_to_timestamp
from scd_automation_app.samref.common_helpers import create_response
from .helpers import add_or_update_user_data, add_or_update_fl, add_or_update_import_data_task, add_or_update_category

app = init_app()

EMPLOYEE_INDEX_DICT = {
    'PERSONNEL_NO': 1, 'NAME': 2, 'GROUP': 3, 'DEPARTMENT': 4, 'SECTION': 5, 'ROLE': 7, 'SYSTEM_USER_NAME': 8
}

FL_INDEX_DICT = {
    'FL': 1,
    'FL_DESCRIPTION': 2,
    'ABC_INDICATOR': 3,
    'MAIN_WORK_CENTER': 4,
    'PLANNER_GROUP': 5,
    'CREATED_ON': 6,
    'CREATED_BY': 7,
    'SYSTEM_STATUS': 8,
    'CHANGED_ON': 9,
    'CHANGED_BY': 10
}

PM_INDEX_DICT = {
    'CREATED_ON': 1,
    'SCHEDULED_START': 2,
    'ACTUAL_ORDER_FINISH_DATE': 3,
    'FL': 4,
    'ORDER_TYPE': 5,
    'ORDER': 6,
    'WO_DESCRIPTION': 7,
    'PLANNER_GROUP': 8,
    'MAIN_WORK_CENTER': 9,
    'ABC_INDICATOR': 10,
    'USER_STATUS': 11,
    'SYSTEM_STATUS': 12,
    'MAINTENANCE_PLAN': 13
}

SCD_PURPOSE_INDEX_DICT = {'FL': 1, 'PID_DRAWING': 2, 'SCD_PURPOSE': 3, 'REMARKS': 4}

WORK_CENTER_CATEGORY_INDEX_DICT = {'MAIN_WORK_CENTER': 1, 'CATEGORY': 3}


def get_import_data_task_status(request_data=None, logger=None):
    record = TblTaskMaster.query.filter_by(status=constants.IMPORT_TASK_IN_PROGRESS).first()
    task_status = {
        'task_status': constants.IMPORT_TASK_IN_PROGRESS if record else constants.NO_IMPORT_TASK_IN_PROGRESS,
        'system_status': constants.SYSTEM_DISABLED if record else constants.SYSTEM_ENABLED
    }
    return create_response(get_success_response=True, data=task_status)


def get_task_type_records(request_data=None, logger=None):
    data = {'task_type_list': [{'id': record, 'name': constants.IMPORT_TASK_DICT[record]}
                               for record in constants.IMPORT_TASK_DICT]}
    return create_response(get_success_response=True, data=data)


def start_import_data_process(request_data=None, file=None, logger=None):
    task_type = int(request_data['task_type'])
    user_id = int(request_data['user_id'])
    if task_type == constants.IMPORT_USER_DATA_TASK:
        index_dict = EMPLOYEE_INDEX_DICT
        func = update_user_data
    elif task_type == constants.IMPORT_MASTER_SCD_DATA_TASK:
        index_dict = FL_INDEX_DICT
        func = update_fl_master_data
    elif task_type == constants.IMPORT_PM_OVERDUE_DATA_TASK:
        index_dict = PM_INDEX_DICT
        func = update_pm_overdue_data
    elif task_type == constants.IMPORT_SCD_PURPOSE_DATA_TASK:
        index_dict = SCD_PURPOSE_INDEX_DICT
        func = update_fl_master_data
    elif task_type == constants.IMPORT_WORK_CENTER_CATEGORY_DATA:
        index_dict = WORK_CENTER_CATEGORY_INDEX_DICT
        func = update_work_center_category_data

    add_or_update_import_data_task(task_type=task_type, user_id=user_id, logger=logger)
    with ThreadPoolExecutor(max_workers=constants.DEFAULT_IMPORT_DATA_WORKERS) as executor:
        executor.submit(func, file=file, index_dict=index_dict, logger=logger)
    add_or_update_import_data_task(task_type=task_type, user_id=user_id, logger=logger)

    return create_response(patch_success_response=True)


def update_user_data(file=None, index_dict=None, logger=None):
    try:
        add_or_update_user_data(data_tuple=None, index_dict=index_dict, logger=logger)
        logger.info('update_user_data: User data updated.')
    except:
        logger.exception('update_user_data: ')


def update_fl_master_data(file=None, index_dict=None, logger=None):
    try:
        logger.info('update_fl_master_data: Updating FL data......')
        df = read_excel(file)
        for fl_tuple in df.itertuples():
            add_or_update_fl(data_tuple=fl_tuple, index_dict=index_dict, logger=logger)
        logger.info('update_fl_master_data: FL data updated.')
    except:
        logger.exception('update_fl_master_data: ')


def update_pm_overdue_data(file=None, index_dict=None, logger=None):
    try:
        logger.info('update_pm_overdue_data: Updating PM Overdue data.....')
        df = read_excel(file)

        for pm_tuple in df.itertuples():
            try:
                with app.app_context():
                    basic_start_date = str(pm_tuple[index_dict['CREATED_ON']]).split(' ')[0]
                    scheduled_start_date = str(pm_tuple[index_dict['SCHEDULED_START']]).split(' ')[0]
                    actual_order_finish_date = str(pm_tuple[index_dict['ACTUAL_ORDER_FINISH_DATE']]).split(' ')[0]
                    if actual_order_finish_date == constants.NAN:
                        actual_order_finish_date = None

                    work_order_fields = {
                        'work_order_description': pm_tuple[index_dict['WO_DESCRIPTION']],
                        'function_location_id':
                            add_or_update_fl(data_tuple=pm_tuple, index_dict=index_dict, logger=logger),
                        'work_order_type': pm_tuple[index_dict['ORDER_TYPE']],
                        'system_status': pm_tuple[index_dict['SYSTEM_STATUS']],
                        'user_status': pm_tuple[index_dict['USER_STATUS']],
                        'recommended_plan': pm_tuple[index_dict['MAINTENANCE_PLAN']],
                        'work_order_plan_date': convert_date_to_timestamp(
                            date=basic_start_date, date_format=constants.ISO_DATE_FORMAT
                        ),
                        'scheduled_start_date': convert_date_to_timestamp(
                            date=scheduled_start_date, date_format=constants.ISO_DATE_FORMAT
                        ),
                        'actual_order_finish_date': convert_date_to_timestamp(
                            date=actual_order_finish_date, date_format=constants.ISO_DATE_FORMAT
                        )
                    }
                    work_order_obj = TblWorkOrderMaster.query.filter_by(work_order=pm_tuple[index_dict['ORDER']])
                    if work_order_obj.first() is None:
                        work_order_fields.update({'work_order': pm_tuple[index_dict['ORDER']]})
                        work_order_record = TblWorkOrderMaster(**work_order_fields)
                        db.session.add(work_order_record)
                    else:
                        work_order_obj.update(work_order_fields)
                    db.session.commit()
            except:
                logger.exception('update_pm_overdue_data: data tuple error: ')
        logger.info('update_pm_overdue_data: PM Overdue data updated.')
    except:
        logger.exception('update_pm_overdue_data: ')


def update_work_center_category_data(file=None, index_dict=None, logger=None):
    try:
        logger.info('update_work_center_category_data: Updating category and work center data......')
        df = read_excel(file)
        for data_tuple in df.itertuples():
            add_or_update_category(data_tuple=data_tuple, index_dict=index_dict, logger=logger)
        logger.info('update_work_center_category_data: Category & work center data updated.')
    except:
        logger.exception('update_work_center_category_data: ')
