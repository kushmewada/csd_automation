from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import start_import_data_process, get_import_data_task_status, get_task_type_records

blueprint_name = constants.BLUE_PRINT_DICT['MASTERS']['IMPORT_DATA']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['MASTERS']['IMPORT_DATA']['URL_PREFIX']
import_data_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['IMPORT_DATA_LOG'])


api_get_task_status = constants.API_DICT['MASTERS']['IMPORT_DATA']['GET_TASK_STATUS']
api_get_task_type_list = constants.API_DICT['MASTERS']['IMPORT_DATA']['GET_TASK_TYPE_LIST']
api_import_data = constants.API_DICT['MASTERS']['IMPORT_DATA']['IMPORT_DATA']


@import_data_bp.route(api_get_task_status, methods=[constants.GET])
def get_task_status():
    return base_api(request=request, api_name=api_get_task_status, api=get_import_data_task_status, logger=logger)


@import_data_bp.route(api_get_task_type_list, methods=[constants.GET])
def get_task_type_list():
    return base_api(request=request, api_name=api_get_task_type_list, api=get_task_type_records, logger=logger)


@import_data_bp.route(api_import_data, methods=[constants.PATCH])
def import_data():
    return base_api(request=request, api_name=api_import_data, api=start_import_data_process,
                    api_params={'file': request.files['file']}, logger=logger)
