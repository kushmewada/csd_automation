import constants
from common_packages import db, init_app
from scd_automation_app.common.models import (
    TblAreaMaster,
    TblUnitMaster,
    TblFunctionLocationMaster,
    TblTaskMaster,
    TblScdPurposeMaster,
    TblPidDrawingMaster,
    TblCategoryMaster
)
from scd_automation_app.groups.models import TblPlannerGroupMaster, TblWorkCenterMaster
from scd_automation_app.samref.common_functions import convert_date_to_timestamp, convert_time_to_timestamp
from scd_automation_app.samref.common_helpers import check_and_add_record
from scd_automation_app.users.models import TblUserMaster

app = init_app()


def add_or_update_user_data(data_tuple=None, index_dict=None, logger=None):
    from time import sleep
    sleep(5)


def add_or_update_fl(data_tuple=None, index_dict=None, logger=None):
    try:
        with app.app_context():
            fl_fields = dict()
            fl_name = data_tuple[index_dict['FL']]
            fl_name = fl_name.upper()
            fl_obj = TblFunctionLocationMaster.query.filter_by(fl_name=fl_name)

            if constants.KEY_DICT['CREATED_ON_UPPER'] in index_dict:
                created_date = str(data_tuple[index_dict['CREATED_ON']]).split(' ')[0]
                fl_fields.update({
                    'created_time': convert_date_to_timestamp(date=created_date, date_format=constants.ISO_DATE_FORMAT)
                })

            if constants.KEY_DICT['CREATED_BY_UPPER'] in index_dict:
                fl_fields.update({'created_by': get_user_id(user=data_tuple[index_dict['CREATED_BY']])})

            if constants.KEY_DICT['CHANGED_ON_UPPER'] in index_dict:
                modified_time = str(data_tuple[index_dict['CHANGED_ON']]).split(' ')[0]
                modified_timestamp = convert_date_to_timestamp(
                    date=modified_time, date_format=constants.ISO_DATE_FORMAT
                )
                fl_fields.update({'modified_time': modified_timestamp})

            if constants.KEY_DICT['CHANGED_BY_UPPER'] in index_dict:
                fl_fields.update({'modified_by': get_user_id(user=data_tuple[index_dict['CHANGED_BY']])})

            if constants.KEY_DICT['FL_DESCRIPTION_UPPER'] in index_dict:
                fl_fields.update({'fl_description': data_tuple[index_dict['FL_DESCRIPTION']].capitalize()})

            if constants.KEY_DICT['SYSTEM_STATUS_UPPER'] in index_dict:
                fl_fields.update({'system_status': data_tuple[index_dict['SYSTEM_STATUS']].title()})

            if constants.KEY_DICT['SYSTEM_STATUS_UPPER'] in index_dict:
                fl_fields.update({'system_status': data_tuple[index_dict['SYSTEM_STATUS']].title()})

            if constants.KEY_DICT['SCD_PURPOSE_UPPER'] in index_dict:
                scd_purpose_id = check_and_add_record(
                    table=TblScdPurposeMaster,
                    field=constants.KEY_DICT['SCD_PURPOSE'],
                    field_value=str(data_tuple[index_dict['SCD_PURPOSE']]).title()
                )
                fl_fields.update({'scd_purpose_id': scd_purpose_id})

            if constants.KEY_DICT['PID_DRAWING_UPPER'] in index_dict:
                pid_drawing_id = check_and_add_record(
                    table=TblPidDrawingMaster,
                    field=constants.KEY_DICT['PID_DRAWING'],
                    field_value=str(data_tuple[index_dict['PID_DRAWING']]).upper()
                )
                fl_fields.update({'pid_drawing_id': pid_drawing_id})

            if constants.KEY_DICT['MAIN_WORK_CENTER_UPPER'] in index_dict:
                work_center_id = check_and_add_record(
                    table=TblWorkCenterMaster,
                    field=constants.KEY_DICT['WORK_CENTER_NAME'],
                    field_value=str(data_tuple[index_dict['MAIN_WORK_CENTER']]).upper()
                )
                fl_fields.update({'work_center_id': work_center_id})

            if constants.KEY_DICT['PLANNER_GROUP_UPPER'] in index_dict:
                planner_group_id = check_and_add_record(
                    table=TblPlannerGroupMaster,
                    field=constants.KEY_DICT['PLANNER_GROUP_NAME'],
                    field_value=str(data_tuple[index_dict['PLANNER_GROUP']]).upper()
                )
                fl_fields.update({'planner_group_id': planner_group_id})

            if constants.KEY_DICT['REMARKS_UPPER'] in index_dict:
                remarks = str(data_tuple[index_dict['REMARKS']])
                if remarks != constants.NAN:
                    fl_fields.update({'remarks': remarks.capitalize()})

            if constants.KEY_DICT['ABC_INDICATOR_UPPER'] in index_dict:
                fl_fields.update({'abc_indicator': str(data_tuple[index_dict['ABC_INDICATOR']]).upper()})

            if fl_obj.first() is None:
                area_id = check_and_add_record(
                    table=TblAreaMaster, field=constants.KEY_DICT['AREA'], field_value=fl_name.split('-')[1]
                )
                fl_fields.update({
                    'unit_id': add_or_fetch_unit(area_id=area_id, unit=fl_name.split('-')[2].zfill(3)),
                    'fl_name': fl_name,
                    'fl_type': constants.SCD
                })
                fl_record = TblFunctionLocationMaster(**fl_fields)
                db.session.add(fl_record)

            else:
                fl_obj.update(fl_fields)
                fl_record = fl_obj.first()
            db.session.commit()
            return fl_record.id
    except:
        logger.error('fl_name: ' + str(data_tuple[index_dict['FL']]))
        logger.exception('add_or_update_fl: ')


def add_or_update_category(data_tuple=None, index_dict=None, logger=None):
    try:
        with app.app_context():
            work_center_name = data_tuple[index_dict['MAIN_WORK_CENTER']].upper()

            category_id = check_and_add_record(
                table=TblCategoryMaster, field=constants.KEY_DICT['CATEGORY'], field_value=data_tuple[index_dict['CATEGORY']]
            )

            work_center_obj = TblWorkCenterMaster.query.filter_by(work_center_name=work_center_name)
            updated_fields = {'category_id': category_id}
            if work_center_obj.first():
                work_center_obj.update(dict(updated_fields))
                db.session.commit()
            else:
                updated_fields.update({'work_center_name': work_center_obj})
                record = TblWorkCenterMaster(work_center_name=work_center_name, category_id=category_id)
                db.session.add(record)
                db.session.commit()
    except:
        logger.error('fl_name: ', data_tuple[index_dict['FL']])
        logger.exception('add_or_update_category: ')


def add_or_update_import_data_task(task_type=None, user_id=None, logger=None):
    task_obj = TblTaskMaster.query.filter_by(task_type=task_type, status=constants.IMPORT_TASK_IN_PROGRESS)
    if task_obj.first() is None:
        task_record = TblTaskMaster(
            task_type=task_type,
            status=constants.IMPORT_TASK_IN_PROGRESS,
            start_time=convert_time_to_timestamp(),
            started_by=user_id
        )
        db.session.add(task_record)
        db.session.commit()
        logger.info('add_or_update_import_data_task: User ID - ' + str(user_id) + ' Task type ' + str(task_type)
                    + ' added in DB.')
    else:
        task_obj.update(dict(status=constants.NO_IMPORT_TASK_IN_PROGRESS, completion_time=convert_time_to_timestamp()))
        db.session.commit()
        logger.info('add_or_update_import_data_task: User ID - ' + str(user_id) + ' Task Type ' + str(task_type)
                    + ' updated in DB.')


def get_user_id(user=None):
    record = TblUserMaster.query.filter_by(name=user).first()
    if record is None:
        record = TblUserMaster(name=user, system_user_name=user, user_status=constants.USER_STATUS['DELETED'])
        db.session.add(record)
        db.session.commit()
    return record.id


def add_or_fetch_unit(area_id=None, unit=None):
    unit_record = TblUnitMaster.query.filter_by(area_id=area_id, unit_number=unit).first()
    print('unit, record: ', unit, unit_record)
    if unit_record is None:
        unit_record = TblUnitMaster(unit_number=unit, area_id=area_id)
        db.session.add(unit_record)
        db.session.commit()
    return unit_record.id
