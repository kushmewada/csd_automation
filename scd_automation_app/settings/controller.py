import constants
from common_packages import db
from scd_automation_app.samref.common_helpers import create_response
from scd_automation_app.settings.models import TblSettingsMaster


def get_setting_records(request_data=None, logger=None):
    data = [{'id': record.id, 'name': constants.SETTING_DICT[record.id], 'value': record.setting_value}
            for record in TblSettingsMaster.query.all()]
    return create_response(get_success_response=constants.SUCCESS, data=data)


def update_setting_record(request_data=None, setting_id=None, logger=None):
    TblSettingsMaster.query.filter_by(id=setting_id).update(dict(setting_value=int(request_data['setting_value'])))
    db.session.commit()
    return create_response(put_success_response=constants.SUCCESS)
