from .models import TblSettingsMaster
import constants
from flask import Blueprint, request
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import get_setting_records, update_setting_record
from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import get_setting_records, update_setting_record
from .models import TblSettingsMaster

blueprint_name = constants.BLUE_PRINT_DICT['SETTINGS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['SETTINGS']['URL_PREFIX']
settings_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['SETTING_LOG'])


api_get_setting_list = constants.API_DICT['SETTINGS']['GET_SETTING_LIST']
api_update_setting = constants.API_DICT['SETTINGS']['UPDATE_SETTING']


@settings_bp.route(api_get_setting_list, methods=[constants.GET])
def get_setting_list():
    return base_api(request=request, api_name=api_get_setting_list, api=get_setting_records, logger=logger)


@settings_bp.route(api_update_setting, methods=[constants.PUT])
def update_setting(record_id):
    return base_api(request=request, api_name=api_update_setting, api=update_setting_record,
                    api_params={'setting_id': int(record_id)}, logger=logger)
