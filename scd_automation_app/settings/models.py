from common_packages import db
from scd_automation_app.samref.base_model import BaseModel


class TblSettingsMaster(BaseModel):
    setting_name = db.Column(db.String(250), nullable=False, unique=True)
    setting_value = db.Column(db.Integer, nullable=True)
