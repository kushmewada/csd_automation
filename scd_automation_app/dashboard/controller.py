import constants
from scd_automation_app.cod.models import TblCodMaster, TblCodFunctionLocations
from scd_automation_app.common.models import (
    TblAreaMaster, TblUnitMaster, TblFunctionLocationMaster, TblCategoryMaster
)
from scd_automation_app.groups.models import TblWorkCenterMaster
from scd_automation_app.plant.models import TblDefeatReasonMaster, TblDefermentReasonMaster
from scd_automation_app.pmdr.helpers import calculate_due_status
from scd_automation_app.pmdr.models import TblPmdrMaster, TblPmdrWorkOrders, TblWorkOrderMaster
from scd_automation_app.samref.common_helpers import create_response
from scd_automation_app.scr.models import TblScrMaster, TblScrFunctionLocations
from .helpers import get_unique_request_count, get_id_wise_count


def get_scr_request_count_data(request_data=None, logger=None):
    filter_fields = list()
    request_count_dict = {'total': list(), 'active': list()}
    area_id = request_data.get('area_id', None)

    if area_id:
        filter_fields.append(TblAreaMaster.id.in_([int(area_id)]))

    records = TblScrMaster.query.join(
        TblScrFunctionLocations, TblScrFunctionLocations.request_group_number == TblScrMaster.request_group_number
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblScrFunctionLocations.function_location_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).filter(
        *filter_fields
    ).all()

    for record in records:
        request_count_dict['total'].append(record.id)
        if record.request_status not in [constants.CLOSED, constants.REJECTED]:
            request_count_dict['active'].append(record.id)

    data = get_unique_request_count(request_count_dict=request_count_dict)
    return create_response(get_success_response=constants.SUCCESS, data={'request_counts': data})


def get_cod_request_count(request_data=None, logger=None):
    filter_fields = [TblCodMaster.request_status.not_in([constants.REJECTED, constants.CLOSED])]
    request_count_dict = {
        'defeat_status_wise_count': {
            'approved_defeats': list(), 'extended_defeats': list(), 'long_term_defeats': list()
        },
        'defeat_reason_wise_count_data': {},
        'defeat_reason_wise_count': {},
        'category_wise_count_data': {},
        'category_wise_count': {}
    }

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id.in_([int(area_id)]))

    defeat_reasons = TblDefeatReasonMaster.query.all()
    for record in defeat_reasons:
        request_count_dict['defeat_reason_wise_count'].update({
            record.defeat_reason: {'id': record.id, 'name': record.defeat_reason, 'count': 0}
        })
        request_count_dict['defeat_reason_wise_count_data'].update({record.defeat_reason: list()})

    categories = TblCategoryMaster.query.all()
    for record in categories:
        request_count_dict['category_wise_count'].update({
            record.category: {'id': record.id, 'name': record.category, 'count': 0}
        })
        request_count_dict['category_wise_count_data'].update({record.category: list()})

    records = TblCodFunctionLocations.query.join(
        TblCodMaster, TblCodMaster.request_group_number == TblCodFunctionLocations.request_group_number
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCodFunctionLocations.function_location_id
    ).join(
        TblDefeatReasonMaster, TblDefeatReasonMaster.id == TblCodMaster.defeat_reason_id
    ).join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblCodMaster.id,
        TblCodMaster.initial_defeat_approval_id,
        TblCodMaster.defeat_extension_approval_id,
        TblCodMaster.long_term_defeat_id,
        TblCodMaster.defeat_reason_id,
        TblDefeatReasonMaster.defeat_reason,
        TblCategoryMaster.category
    ).filter(
        *filter_fields
    ).all()

    for record in records:
        if record.long_term_defeat_id:
            request_count_dict['defeat_status_wise_count']['long_term_defeats'].append(record.id)
        elif record.defeat_extension_approval_id:
            request_count_dict['defeat_status_wise_count']['extended_defeats'].append(record.id)
        elif record.initial_defeat_approval_id:
            request_count_dict['defeat_status_wise_count']['approved_defeats'].append(record.id)

        if record.defeat_reason:
            request_count_dict['defeat_reason_wise_count_data'][record.defeat_reason].append(record.id)
        if record.category:
            request_count_dict['category_wise_count_data'][record.category].append(record.id)

    request_count_dict['defeat_status_wise_count'] = get_unique_request_count(
        request_count_dict=request_count_dict['defeat_status_wise_count']
    )

    request_count_dict['defeat_reason_wise_count'] = get_id_wise_count(
        id_dict=request_count_dict['defeat_reason_wise_count_data'],
        count_dict=request_count_dict['defeat_reason_wise_count']
    )
    del request_count_dict['defeat_reason_wise_count_data']

    request_count_dict['category_wise_count'] = get_id_wise_count(
        id_dict=request_count_dict['category_wise_count_data'],
        count_dict=request_count_dict['category_wise_count']
    )
    del request_count_dict['category_wise_count_data']

    return create_response(get_success_response=constants.SUCCESS, data={'request_counts': request_count_dict})


def get_pmdr_request_count(request_data=None, logger=None):
    filter_fields = [TblWorkOrderMaster.system_status != constants.SYSTEM_STATUS_CNF,
                     TblWorkOrderMaster.actual_order_finish_date == None]
    request_count_dict = {
        'deferment_status_wise_count': {
            'approved': list(),
            'overdue': list(),
            'deferred': list(),
            'due_in_one_week': list(),
            'due_in_two_weeks': list(),
            'due_in_three_weeks': list(),
            'due_in_four_weeks': list(),
            'due_in_four_to_twelve_weeks': list()
        },
        'deferment_reason_wise_count': dict(),
        'deferment_reason_wise_count_data': dict(),
        'category_wise_count': dict(),
        'category_wise_count_data': dict(),
    }

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id.in_([int(area_id)]))

    deferment_reasons = TblDefermentReasonMaster.query.all()
    for record in deferment_reasons:
        request_count_dict['deferment_reason_wise_count'].update({
            record.deferment_reason: {'id': record.id, 'name': record.deferment_reason, 'count': 0}
        })
        request_count_dict['deferment_reason_wise_count_data'].update({record.deferment_reason: list()})

    categories = TblCategoryMaster.query.all()
    for record in categories:
        request_count_dict['category_wise_count'].update({
            record.category: {'id': record.id, 'name': record.category, 'count': 0}
        })
        request_count_dict['category_wise_count_data'].update({record.category: list()})

    records = TblWorkOrderMaster.query.outerjoin(
        TblPmdrWorkOrders, TblPmdrWorkOrders.work_order_id == TblWorkOrderMaster.id
    ).join(
        TblPmdrMaster, TblPmdrMaster.request_group_number == TblPmdrWorkOrders.request_group_number
    ).join(
        TblDefermentReasonMaster, TblDefermentReasonMaster.id == TblPmdrMaster.deferment_reason_id
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id
    ).join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblPmdrMaster.id,
        TblPmdrMaster.deferment_extension_id,
        TblPmdrMaster.area_btm_approval_id,
        TblPmdrMaster.deferment_extension_id,
        TblPmdrMaster.operations_gm_reapproval_id,
        TblWorkOrderMaster.actual_order_finish_date,
        TblWorkOrderMaster.work_order_plan_date,
        TblPmdrWorkOrders.work_order_id,
        TblCategoryMaster.category,
        TblDefermentReasonMaster.deferment_reason
    ).filter(
        *filter_fields
    ).all()

    for record in records:
        if record.operations_gm_reapproval_id:
            request_count_dict['deferment_status_wise_count']['deferred'].append(record.id)
        elif record.deferment_extension_id and not record.operations_gm_reapproval_id:
            request_count_dict['deferment_status_wise_count']['overdue'].append(record.id)
        elif record.area_btm_approval_id:
            request_count_dict['deferment_status_wise_count']['approved'].append(record.id)
        else:
            due_status = calculate_due_status(planned_date=record.work_order_plan_date)
            if due_status == constants.OVER_DUE_MESSAGE_DICT['OVERDUE']:
                request_count_dict['deferment_status_wise_count']['overdue'].append(record.id)
            elif due_status == constants.OVER_DUE_MESSAGE_DICT['ONE_WEEK']:
                request_count_dict['deferment_status_wise_count']['due_in_one_week'].append(record.id)
            elif due_status == constants.OVER_DUE_MESSAGE_DICT['TWO_WEEKS']:
                request_count_dict['deferment_status_wise_count']['due_in_two_weeks'].append(record.id)
            elif due_status == constants.OVER_DUE_MESSAGE_DICT['THREE_WEEKS']:
                request_count_dict['deferment_status_wise_count']['due_in_three_weeks'].append(record.id)
            elif due_status in [constants.OVER_DUE_MESSAGE_DICT['FOUR_WEEKS']]:
                request_count_dict['deferment_status_wise_count']['due_in_four_weeks'].append(record.id)
            elif due_status in [constants.OVER_DUE_MESSAGE_DICT['TWO_MONTHS'],
                                constants.OVER_DUE_MESSAGE_DICT['THREE_MONTHS']]:
                request_count_dict['deferment_status_wise_count']['due_in_four_to_twelve_weeks'].append(record.id)

        if record.deferment_reason:
            request_count_dict['deferment_reason_wise_count_data'][record.deferment_reason].append(record.id)
        if record.category:
            request_count_dict['category_wise_count_data'][record.category].append(record.id)

    request_count_dict['deferment_status_wise_count'] = get_unique_request_count(
        request_count_dict=request_count_dict['deferment_status_wise_count']
    )

    request_count_dict['deferment_reason_wise_count'] = get_id_wise_count(
        id_dict=request_count_dict['deferment_reason_wise_count_data'],
        count_dict=request_count_dict['deferment_reason_wise_count']
    )
    del request_count_dict['deferment_reason_wise_count_data']

    request_count_dict['category_wise_count'] = get_id_wise_count(
        id_dict=request_count_dict['category_wise_count_data'],
        count_dict=request_count_dict['category_wise_count']
    )
    del request_count_dict['category_wise_count_data']

    return create_response(get_success_response=constants.SUCCESS, data={'request_counts': request_count_dict})
