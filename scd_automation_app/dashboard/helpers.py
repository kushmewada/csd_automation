from scd_automation_app.samref.common_functions import convert_dict_to_list


def get_unique_request_count(request_count_dict=None):
    for key in request_count_dict:
        request_count_dict[key] = len(set(request_count_dict[key])) if request_count_dict[key] else 0
    return request_count_dict


def get_id_wise_count(id_dict=None, count_dict=None):
    request_count_dict = get_unique_request_count(request_count_dict=id_dict)
    for record in request_count_dict:
        count_dict[record]['count'] = request_count_dict[record]
    return convert_dict_to_list(records=count_dict)
