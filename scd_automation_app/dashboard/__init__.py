from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import get_scr_request_count_data, get_cod_request_count, get_pmdr_request_count

blueprint_name = constants.BLUE_PRINT_DICT['DASHBOARDS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['DASHBOARDS']['URL_PREFIX']
dashboard_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['DASHBOARD_LOG'])


api_get_scr_request_count = constants.API_DICT['DASHBOARDS']['GET_SCR_REQUEST_COUNT']
api_get_active_cod_request_count = constants.API_DICT['DASHBOARDS']['GET_ACTIVE_COD_REQUEST_COUNT']
api_get_active_pmdr_request_count = constants.API_DICT['DASHBOARDS']['GET_ACTIVE_PMDR_REQUEST_COUNT']


@dashboard_bp.route(api_get_scr_request_count, methods=[constants.GET])
def get_scr_request_count():
    return base_api(request=request, api_name=api_get_scr_request_count, api=get_scr_request_count_data, logger=logger)


@dashboard_bp.route(api_get_active_cod_request_count, methods=[constants.GET])
def get_active_cod_request_count():
    return base_api(request=request, api_name=api_get_active_cod_request_count, api=get_cod_request_count,
                    logger=logger)


@dashboard_bp.route(api_get_active_pmdr_request_count, methods=[constants.GET])
def get_active_pmdr_request_count():
    return base_api(request=request, api_name=api_get_active_pmdr_request_count, api=get_pmdr_request_count,
                    logger=logger)
