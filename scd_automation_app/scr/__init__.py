from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_fl_lead_confirmation_records,
    get_fl_property_history_records,
    get_scr_request_records,
    get_scr_request_data,
    scr_record,
    she_review_record,
    oims_admin_approval_record,
    area_btm_approval_record,
    create_ra_record,
    create_or_update_lead_confirmation_record,
    update_fl_record,
    discard_request_data
)

blueprint_name = constants.BLUE_PRINT_DICT['SCR']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['SCR']['URL_PREFIX']
scr_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['SCR_LOG'])


api_get_fl_lead_confirmation_list = constants.API_DICT['SCR']['GET_FL_LEAD_CONFIRMATION_LIST']
api_get_scr_request_list = constants.API_DICT['SCR']['GET_SCR_REQUEST_LIST']
api_get_scr_request_details = constants.API_DICT['SCR']['GET_SCR_REQUEST_DETAILS']
api_get_fl_property_history = constants.API_DICT['SCR']['GET_FL_PROPERTY_HISTORY']
api_scr_request = constants.API_DICT['SCR']['SCR_REQUEST']
api_she_review = constants.API_DICT['SCR']['SHE_REVIEW']
api_oims_admin_approval = constants.API_DICT['SCR']['OIMS_ADMIN_APPROVAL']
api_area_btm_approval = constants.API_DICT['SCR']['AREA_BTM_APPROVAL']
api_create_ra = constants.API_DICT['SCR']['CREATE_RA']
api_create_or_update_lead_confirmation = constants.API_DICT['SCR']['CREATE_LEAD_CONFIRMATION']
api_update_fl_property = constants.API_DICT['SCR']['UPDATE_FL_PROPERTY']
api_discard_form = constants.API_DICT['SCR']['DISCARD_FORM']


@scr_bp.route(api_get_fl_lead_confirmation_list, methods=[constants.GET])
def get_fl_lead_confirmation_list():
    return base_api(request=request, api_name=api_get_fl_lead_confirmation_list, api=get_fl_lead_confirmation_records,
                    logger=logger)


@scr_bp.route(api_get_fl_property_history, methods=[constants.GET])
def get_fl_property_history(record_id):
    return base_api(request=request, api_name=api_get_fl_property_history, api=get_fl_property_history_records,
                    api_params={'fl_id': int(record_id)}, logger=logger)


@scr_bp.route(api_get_scr_request_list, methods=[constants.GET])
def get_scr_request_list():
    return base_api(request=request, api_name=api_get_scr_request_list, api=get_scr_request_records, logger=logger)


@scr_bp.route(api_get_scr_request_details, methods=[constants.GET])
def get_scr_request_details():
    return base_api(request=request, api_name=api_get_scr_request_details, api=get_scr_request_data, logger=logger)


@scr_bp.route(api_scr_request, methods=[constants.POST, constants.PATCH])
def scr_request():
    return base_api(request=request, api_name=api_scr_request, api=scr_record,
                    api_params={'request_type': request.method}, logger=logger)


@scr_bp.route(api_she_review, methods=[constants.POST, constants.PATCH])
def she_review():
    return base_api(request=request, api_name=api_she_review, api=she_review_record,
                    api_params={'request_type': request.method}, logger=logger)


@scr_bp.route(api_oims_admin_approval, methods=[constants.POST, constants.PATCH])
def oims_admin_approval():
    return base_api(request=request, api_name=api_oims_admin_approval, api=oims_admin_approval_record,
                    api_params={'request_type': request.method}, logger=logger)


@scr_bp.route(api_area_btm_approval, methods=[constants.POST, constants.PATCH])
def area_btm_approval():
    return base_api(request=request, api_name=api_area_btm_approval, api=area_btm_approval_record,
                    api_params={'request_type': request.method}, logger=logger)


@scr_bp.route(api_create_ra, methods=[constants.POST])
def create_ra():
    return base_api(request=request, api_name=api_create_ra, api=create_ra_record, logger=logger)


@scr_bp.route(api_create_or_update_lead_confirmation, methods=[constants.POST])
def create_or_update_lead_confirmation():
    return base_api(request=request, api_name=api_create_or_update_lead_confirmation,
                    api=create_or_update_lead_confirmation_record, logger=logger)


@scr_bp.route(api_update_fl_property, methods=[constants.PATCH])
def update_fl_property(record_id):
    return base_api(request=request, api_name=api_update_fl_property, api=update_fl_record,
                    api_params={'fl_id': int(record_id)}, logger=logger)


@scr_bp.route(api_discard_form, methods=[constants.DELETE])
def discard_form():
    return base_api(request=request, api_name=api_discard_form, api=discard_request_data, logger=logger)
