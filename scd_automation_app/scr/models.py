from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblScrMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    unique_rgn_id = db.Column(db.String(250), nullable=True)
    change_type = db.Column(db.Integer, nullable=False)
    deletion_reason = db.Column(db.String(250), nullable=True)
    request_status = db.Column(db.Integer, nullable=False)
    section_status = db.Column(db.Integer, nullable=False)
    she_review_id = db.Column(db.Integer, db.ForeignKey('tbl_scr_she_review_master.id'), nullable=True)
    oims_admin_approval_id = db.Column(
        db.Integer, db.ForeignKey('tbl_scr_oims_admin_approval_master.id'), nullable=True
    )
    area_btm_approval_id = db.Column(db.Integer, db.ForeignKey('tbl_scr_area_btm_approval_master.id'), nullable=True)
    reliability_assessment_id = db.Column(
        db.Integer, db.ForeignKey('tbl_scr_reliability_assessment_master.id'), nullable=True
    )


class TblScrFunctionLocations(BaseModel):
    request_group_number = db.Column(db.Integer, nullable=False)
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)


class TblScrSheReviewMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    she_engineer_comments = db.Column(db.String(250))
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=False)


class TblScrOimsAdminApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    oims_admin_comments = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=False)


class TblScrAreaBtmApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    area_btm_comments = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=False)


class TblScrReliabilityAssessmentMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    section_status = db.Column(db.Integer, nullable=False)


class TblScrLeadConfirmationMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)
    status = db.Column(db.Integer, nullable=False)


class TblScrFlLeadConfirmationMaster(BaseModel, CreationTimestampMixin):
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)
    property_id = db.Column(db.Integer, nullable=False)
    property_value = db.Column(db.Integer, nullable=False)
