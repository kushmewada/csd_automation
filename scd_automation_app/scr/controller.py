from json import loads

import constants
from common_packages import db
from scd_automation_app.attachments.models import TblDocumentMaster, TblWorkFlowDocuments
from scd_automation_app.common.models import (
    TblAreaMaster,
    TblUnitMaster,
    TblFunctionLocationMaster,
    TblCategoryMaster,
    TblScdPurposeMaster,
    TblOperationModeMaster,
    TblPidDrawingMaster
)
from scd_automation_app.groups.models import TblWorkCenterMaster
from scd_automation_app.plant.models import TblChangeSourceMaster
from scd_automation_app.samref.common_functions import (
    convert_time_to_timestamp, get_list_from_comma_separated_str, convert_timestamp_to_time
)
from scd_automation_app.samref.common_helpers import (
    check_request_permission_for_section_1,
    check_request_permissions,
    get_sort_data,
    get_sort_obj,
    format_list_api_data,
    get_user_data_from_request,
    get_attachment_list_for_request,
    get_unique_rgn_data,
    get_controller_rgn_str,
    create_or_update_user_active_work_flow,
    create_response,
    delete_user_active_work_flow_record,
    delete_section_documents,
    convert_dict_to_list
)
from scd_automation_app.samref.common_helpers import create_pending_action_record, paginate_records
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster, TblUserActiveWorkflows, TblPendingActions
from .helpers import (
    update_scr_master,
    update_fl_data,
    add_or_update_she_review,
    add_or_update_oims_admin_approval,
    add_or_update_area_btm_approval,
    get_property_key
)
from .models import (
    TblScrMaster,
    TblScrFunctionLocations,
    TblScrSheReviewMaster,
    TblScrOimsAdminApprovalMaster,
    TblScrAreaBtmApprovalMaster,
    TblScrReliabilityAssessmentMaster,
    TblScrLeadConfirmationMaster,
    TblScrFlLeadConfirmationMaster
)


def get_fl_lead_confirmation_records(request_data=None, logger=None):
    fl_lead_confirmation_dict = dict()
    filter_fields = [
        TblScrMaster.request_status == constants.CLOSED, TblFunctionLocationMaster.fl_type == constants.SCD
    ]

    area_id = int(request_data['area_id']) if request_data.get('area_id', None) else None
    if area_id:
        filter_fields.append(TblAreaMaster.id == area_id)

    filter_id = int(request_data['filter_id']) if request_data.get('filter_id', None) else None
    if filter_id == constants.LEAD_CONFIRMED_REQUESTS:
        filter_fields.extend([
            TblFunctionLocationMaster.pm_plan_scheduled == constants.PM_PLAN_SCHEDULED,
            TblFunctionLocationMaster.pid_drawing_id == constants.PID_DRAWING_UPDATED,
            TblFunctionLocationMaster.sap_scd_class_updated == constants.SAP_SCD_CLASS_UPDATED
        ])
    elif filter_id == constants.LEAD_UNCONFIRMED_REQUESTS:
        filter_fields.append(
            (TblFunctionLocationMaster.pm_plan_scheduled == constants.PM_PLAN_NOT_SCHEDULED) |
            (TblFunctionLocationMaster.pid_drawing_id == constants.PID_DRAWING_NOT_UPDATED) |
            (TblFunctionLocationMaster.sap_scd_class_updated == constants.SAP_SCD_CLASS_NOT_UPDATED)
        )

    sort_type = int(request_data.get('sort_id', constants.ASCENDING))
    sort_obj = get_sort_obj(
        sort_field=constants.KEY_DICT['FL_NAME'], sort_type=sort_type, table=TblFunctionLocationMaster
    )

    records = TblFunctionLocationMaster.query.join(
        TblScrLeadConfirmationMaster, TblScrLeadConfirmationMaster.function_location_id == TblFunctionLocationMaster.id
    ).join(
        TblScrFunctionLocations, TblScrFunctionLocations.function_location_id == TblFunctionLocationMaster.id
    ).join(
        TblScrMaster, TblScrMaster.request_group_number == TblScrFunctionLocations.request_group_number
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblScrMaster.request_group_number,
        TblScrMaster.unique_rgn_id,
        TblScrLeadConfirmationMaster.status,
        TblScrFunctionLocations.function_location_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.pm_plan_scheduled,
        TblFunctionLocationMaster.pid_drawing_updated,
        TblFunctionLocationMaster.sap_scd_class_updated
    ).filter(
        *filter_fields
    ).order_by(
        sort_obj
    ).all()

    for record in records:
        if record.function_location_id not in fl_lead_confirmation_dict:
            fl_lead_confirmation_dict[record.function_location_id] = {
                'id': record.function_location_id,
                'name': record.fl_name,
                'pm_plan_scheduled': record.pm_plan_scheduled,
                'pid_drawing_updated': record.pid_drawing_updated,
                'sap_scd_class_updated': record.sap_scd_class_updated,
                'all_tasks_status': record.status,
                'request_list': {
                    record.request_group_number: {'id': record.request_group_number, 'name': record.unique_rgn_id}
                }
            }

    for fl_id in fl_lead_confirmation_dict:
        fl_lead_confirmation_dict[fl_id]['request_list'] = convert_dict_to_list(records=fl_lead_confirmation_dict[fl_id]['request_list'])

    data = format_list_api_data(
        records=fl_lead_confirmation_dict,
        list_name=constants.KEY_DICT['FUNCTION_LOCATION_LIST'],
        search_key=constants.KEY_DICT['NAME'],
        search_text=request_data.get('search_text', None),
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def get_fl_property_history_records(request_data=None, fl_id=None, logger=None):
    property_id = int(request_data['property_id'])
    property_field = get_property_key(property_id=property_id)
    fl_record = TblFunctionLocationMaster.query.filter_by(id=fl_id).first()

    records = TblScrLeadConfirmationMaster.query.join(
        TblScrFlLeadConfirmationMaster,
        TblScrFlLeadConfirmationMaster.function_location_id == TblScrLeadConfirmationMaster.function_location_id
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblScrLeadConfirmationMaster.function_location_id
    ).join(
        TblUserMaster, TblUserMaster.id == TblScrFlLeadConfirmationMaster.created_by
    ).add_columns(
        TblScrLeadConfirmationMaster.function_location_id,
        TblScrLeadConfirmationMaster.status,
        TblFunctionLocationMaster.fl_name,
        getattr(TblFunctionLocationMaster, property_field),
        TblScrFlLeadConfirmationMaster.property_id,
        TblScrFlLeadConfirmationMaster.property_value,
        TblScrFlLeadConfirmationMaster.created_time,
        TblScrFlLeadConfirmationMaster.created_by,
        TblUserMaster.name
    ).filter(
        TblFunctionLocationMaster.id == fl_id,
        TblScrFlLeadConfirmationMaster.property_id == property_id
    ).order_by(
        TblScrFlLeadConfirmationMaster.created_time.desc()
    ).all()

    change_history_list = [{
        'status': record.property_value,
        'created_time': convert_timestamp_to_time(timestamp=record.created_time),
        'created_by': {'id': record.created_by, 'name': record.name}
    } for record in records]

    fl_property_history = {'id': fl_id, 'name': fl_record.fl_name}
    fl_property_history.update(paginate_records(
        page=int(request_data['page']),
        record_list=change_history_list,
        list_name=constants.KEY_DICT['CHANGE_HISTORY'],
        settings_table=TblSettingsMaster
    ))
    return create_response(get_success_response=True, data=fl_property_history)


def get_scr_request_records(request_data=None, logger=None):
    scr_data_dict = dict()
    filter_fields = list()
    search_text = request_data.get('search_text', None)

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id == int(area_id))

    request_type = request_data.get('request_type', None)
    if request_type and int(request_type) == constants.ACTIVE_SCR_REQUESTS:
        filter_fields.append(TblScrMaster.request_status.not_in([constants.REJECTED, constants.CLOSED]))

    sort_id = int(request_data['sort_id']) if request_data.get('sort_id', None) else constants.REQUEST_ID_ASCENDING
    sort_obj = get_sort_data(sort_id=sort_id, table=TblScrMaster)

    scr_records = TblScrMaster.query.join(
        TblScrFunctionLocations, TblScrFunctionLocations.request_group_number == TblScrMaster.request_group_number
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblScrFunctionLocations.function_location_id
    ).outerjoin(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).outerjoin(
        TblOperationModeMaster, TblOperationModeMaster.id == TblFunctionLocationMaster.operation_mode_id
    ).outerjoin(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).outerjoin(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblScrMaster.request_group_number,
        TblScrMaster.unique_rgn_id,
        TblScrMaster.change_type,
        TblScrMaster.request_status,
        TblScrFunctionLocations.function_location_id,
        TblCategoryMaster.category,
        TblOperationModeMaster.mode,
        TblScdPurposeMaster.scd_purpose,
        TblWorkCenterMaster.category_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.operation_mode_id,
        TblFunctionLocationMaster.scd_purpose_id
    ).filter(
        *filter_fields
    ).order_by(
        sort_obj
    ).all()

    for record in scr_records:
        request_group_number = record.request_group_number
        if request_group_number not in scr_data_dict:
            scr_data_dict[request_group_number] = {
                'id': request_group_number,
                'unique_rgn_id': record.unique_rgn_id,
                'function_locations': [{
                    'id': record.function_location_id,
                    'name': record.fl_name,
                    'operation_mode': {'id': record.operation_mode_id, 'name': record.mode},
                    'scd_purpose': {'id': record.scd_purpose_id, 'name': record.scd_purpose}
                }],
                'change_type': record.change_type,
                'category_data': {'id': record.category_id, 'name': record.category},
                'critical_safeguard': record.critical_safeguard,
                'status': record.request_status,
            }
        else:
            scr_data_dict[request_group_number]['function_locations'].append({
                'id': record.function_location_id,
                'name': record.fl_name,
                'operation_mode': {'id': record.operation_mode_id, 'name': record.mode},
                'scd_purpose': {'id': record.scd_purpose_id, 'name': record.scd_purpose}
            })

    data = format_list_api_data(
        records=scr_data_dict,
        list_name=constants.KEY_DICT['SCR_REQUEST_LIST'],
        search_key=constants.KEY_DICT['UNIQUE_RGN_ID'],
        search_text=search_text,
        page=int(request_data['page']) if request_data.get('page', None) else None,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_scr_request_data(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['section_ids'])
    section_id_list.sort()

    scr_fl_records = TblScrFunctionLocations.query.join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblScrFunctionLocations.function_location_id
    ).outerjoin(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).outerjoin(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).outerjoin(
        TblOperationModeMaster, TblOperationModeMaster.id == TblFunctionLocationMaster.operation_mode_id
    ).outerjoin(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblFunctionLocationMaster.pid_drawing_id
    ).outerjoin(
        TblChangeSourceMaster, TblChangeSourceMaster.id == TblFunctionLocationMaster.change_source_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblCategoryMaster.category,
        TblWorkCenterMaster.work_center_name,
        TblOperationModeMaster.mode,
        TblScdPurposeMaster.scd_purpose,
        TblPidDrawingMaster.pid_drawing,
        TblChangeSourceMaster.change_source,
        TblWorkCenterMaster.category_id,
        TblScrFunctionLocations.function_location_id,
        TblFunctionLocationMaster.unit_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.work_center_id,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.operation_mode_id,
        TblFunctionLocationMaster.scd_purpose_id,
        TblFunctionLocationMaster.equipments_protected,
        TblFunctionLocationMaster.pid_drawing_id,
        TblFunctionLocationMaster.change_source_id,
        TblFunctionLocationMaster.source_reference_no,
        TblFunctionLocationMaster.consequence_level,
        TblFunctionLocationMaster.consequence_of_failure,
        TblFunctionLocationMaster.required_availability_target,
        TblFunctionLocationMaster.pm_plan_deactivated,
        TblFunctionLocationMaster.recommended_pm_interval,
        TblFunctionLocationMaster.new_or_updated_equipment_strategy,
        TblFunctionLocationMaster.recommended_plan,
        TblFunctionLocationMaster.actual_availability_target,
        TblFunctionLocationMaster.additional_risk_management_options,
        TblFunctionLocationMaster.pm_plan_scheduled,
        TblFunctionLocationMaster.pid_drawing_updated,
        TblFunctionLocationMaster.sap_scd_class_updated
    ).filter(
        TblScrFunctionLocations.request_group_number == request_group_number
    ).all()

    fl_data = [
        {
            'fl_id': record.function_location_id,
            'fl_name': record.fl_name,
            'fl_description': record.fl_description,
            'category': {'id': record.category_id, 'name': record.category},
            'work_center': {'id': record.work_center_id, 'name': record.work_center_name},
            'critical_safeguard': record.critical_safeguard,
            'operation_mode': {'id': record.operation_mode_id, 'name': record.mode},
            'equipments_protected': record.equipments_protected,
            'scd_purpose': {'id': record.scd_purpose_id, 'name': record.scd_purpose},
            'pid_drawing': {'id': record.pid_drawing_id, 'name': record.pid_drawing},
            'change_source': {'id': record.change_source_id, 'name': record.change_source},
            'source_reference_no': record.source_reference_no
        } for record in scr_fl_records
    ]

    attachment_list = get_attachment_list_for_request(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        work_flow_section_id=constants.SCR_SECTION_1,
        document_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments
    )

    scr_master_record = TblScrMaster.query.filter_by(request_group_number=request_group_number).first()
    section_data_dict = {
        'id': scr_master_record.id,
        'section_status': scr_master_record.section_status,
        'section_data': {
            'area': {'id': scr_fl_records[0].area_id, 'name': scr_fl_records[0].area},
            'unit': {'id': scr_fl_records[0].unit_id, 'name': scr_fl_records[0].unit_number},
            'change_type': scr_master_record.change_type,
            'deletion_reason': scr_master_record.deletion_reason,
            'fl_data': fl_data,
            'attachment_list': attachment_list
        }
    }
    section_user_data = get_user_data_from_request(request_record=scr_master_record, user_table=TblUserMaster)
    section_data_dict.update(section_user_data)

    scr_request_data_dict = {
        'request_group_number': request_group_number,
        'unique_rgn_id': scr_master_record.unique_rgn_id,
        'section_1': section_data_dict
    }

    if constants.SCR_SECTION_2 in section_id_list and scr_master_record.she_review_id:
        she_review = TblScrSheReviewMaster.query.filter_by(id=scr_master_record.she_review_id).first()
        fl_data = [
            {
                'fl_id': record.function_location_id,
                'fl_name': record.fl_name,
                'consequence_level': record.consequence_level,
                'consequence_of_failure': record.consequence_of_failure,
                'required_availability_target': record.required_availability_target
            } for record in scr_fl_records
        ]

        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.SCR,
            work_flow_section_id=constants.SCR_SECTION_2,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        section_data_dict = {
            'id': she_review.id,
            'section_status': she_review.section_status,
            'section_data': {
                'she_engineer_comments': she_review.she_engineer_comments,
                'recycle_comments': she_review.recycle_comments,
                'fl_data': fl_data,
                'attachment_list': attachment_list
            }
        }
        section_user_data = get_user_data_from_request(request_record=she_review, user_table=TblUserMaster)
        section_data_dict.update(section_user_data)
        scr_request_data_dict.update({'section_2': section_data_dict})

    if constants.SCR_SECTION_3 in section_id_list and scr_master_record.oims_admin_approval_id:
        oims_approval = \
            TblScrOimsAdminApprovalMaster.query.filter_by(id=scr_master_record.oims_admin_approval_id).first()

        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.SCR,
            work_flow_section_id=constants.SCR_SECTION_3,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        section_data_dict = {
            'id': oims_approval.id,
            'section_status': oims_approval.section_status,
            'section_data': {
                'oims_admin_comments': oims_approval.oims_admin_comments,
                'recycle_comments': oims_approval.recycle_comments,
                'attachment_list': attachment_list
            }
        }
        section_user_data = get_user_data_from_request(request_record=oims_approval, user_table=TblUserMaster)
        section_data_dict.update(section_user_data)
        scr_request_data_dict.update({'section_3': section_data_dict})

    if constants.SCR_SECTION_4 in section_id_list and scr_master_record.area_btm_approval_id:
        area_btm_approval = \
            TblScrAreaBtmApprovalMaster.query.filter_by(id=scr_master_record.area_btm_approval_id).first()

        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.SCR,
            work_flow_section_id=constants.SCR_SECTION_4,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        section_data_dict = {
            'id': area_btm_approval.id,
            'section_status': area_btm_approval.section_status,
            'section_data': {
                'area_btm_comments': area_btm_approval.area_btm_comments,
                'recycle_comments': area_btm_approval.recycle_comments,
                'attachment_list': attachment_list
            }
        }
        section_user_data = get_user_data_from_request(request_record=area_btm_approval, user_table=TblUserMaster)
        section_data_dict.update(section_user_data)
        scr_request_data_dict.update({'section_4': section_data_dict})

    if constants.SCR_SECTION_5 in section_id_list and scr_master_record.reliability_assessment_id:
        ra_record = \
            TblScrReliabilityAssessmentMaster.query.filter_by(id=scr_master_record.reliability_assessment_id).first()

        fl_data = [
            {
                'fl_id': record.function_location_id,
                'fl_name': record.fl_name,
                'pm_plan_deactivated': record.pm_plan_deactivated,
                'recommended_pm_interval': {
                    'id': record.recommended_pm_interval,
                    'name': constants.RECOMMENDED_PM_INTERVAL_DICT[record.recommended_pm_interval]
                    if record.recommended_pm_interval else None
                },
                'new_or_updated_equipment_strategy': record.new_or_updated_equipment_strategy,
                'recommended_plan': record.recommended_plan,
                'required_availability_target': record.required_availability_target,
                'actual_availability_target': record.actual_availability_target,
                'additional_risk_management_options': record.additional_risk_management_options
            } for record in scr_fl_records
        ]

        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.SCR,
            work_flow_section_id=constants.SCR_SECTION_5,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        section_data_dict = {
            'id': ra_record.id,
            'section_status': ra_record.section_status,
            'section_data': {'fl_data': fl_data, 'attachment_list': attachment_list}
        }
        section_user_data = get_user_data_from_request(request_record=ra_record, user_table=TblUserMaster)
        section_data_dict.update(section_user_data)
        scr_request_data_dict.update({'section_5': section_data_dict})

    if constants.SCR_MAINTENANCE_LEAD_CONFIRMATION in section_id_list:
        fl_lead_confirmation_data_dict = dict()
        fl_id_list = [record.function_location_id for record in scr_fl_records]

        records = TblScrLeadConfirmationMaster.query.join(
            TblScrFlLeadConfirmationMaster,
            TblScrFlLeadConfirmationMaster.function_location_id == TblScrLeadConfirmationMaster.function_location_id
        ).join(
            TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblScrLeadConfirmationMaster.function_location_id
        ).join(
            TblUserMaster, TblUserMaster.id == TblScrFlLeadConfirmationMaster.created_by
        ).add_columns(
            TblScrLeadConfirmationMaster.function_location_id,
            TblScrLeadConfirmationMaster.status,
            TblFunctionLocationMaster.fl_name,
            TblFunctionLocationMaster.pm_plan_scheduled,
            TblFunctionLocationMaster.pid_drawing_updated,
            TblFunctionLocationMaster.sap_scd_class_updated,
            TblScrFlLeadConfirmationMaster.property_id,
            TblScrFlLeadConfirmationMaster.property_value,
            TblScrFlLeadConfirmationMaster.created_time,
            TblScrFlLeadConfirmationMaster.created_by,
            TblUserMaster.name
        ).filter(
            TblFunctionLocationMaster.id.in_(fl_id_list)
        ).all()

        for _id in fl_id_list:
            fl_record = TblFunctionLocationMaster.query.filter_by(id=_id).first()
            lead_confirmation_record = TblScrLeadConfirmationMaster.query.filter_by(function_location_id=_id).first()
            fl_lead_confirmation_data_dict[_id] = {
                'id': _id,
                'name': fl_record.fl_name,
                'lead_confirmation_status': lead_confirmation_record.status,
                'pm_plan_scheduled': {'current_status': fl_record.pm_plan_scheduled, 'change_history': list()},
                'pid_drawing_updated': {'current_status': fl_record.pid_drawing_updated, 'change_history': list()},
                'sap_scd_class_updated': {'current_status': fl_record.sap_scd_class_updated, 'change_history': list()}
            }

        for record in records:
            key = get_property_key(property_id=record.property_id)
            fl_lead_confirmation_data_dict[record.function_location_id][key]['change_history'].append({
                'property_value': record.property_value,
                'created_time': convert_timestamp_to_time(timestamp=record.created_time),
                'created_by': {'id': record.created_by, 'name': record.name}
            })

        fl_lead_confirmation_data_list = convert_dict_to_list(records=fl_lead_confirmation_data_dict)
        scr_request_data_dict.update({
            'lead_confirmations': {'section_data': {'fl_data': fl_lead_confirmation_data_list}}
        })
    return create_response(get_success_response=True, data=scr_request_data_dict)


def scr_record(request_data=None, request_type=None, logger=None):
    status, error_message = check_request_permission_for_section_1(
        request_data=request_data,
        data_key=constants.KEY_DICT['FL_DATA'],
        id_key=constants.KEY_DICT['FL_ID'],
        id_field=constants.KEY_DICT['FUNCTION_LOCATION_ID'],
        section_id=constants.SCR_SECTION_1,
        master_table=TblScrMaster,
        id_table=TblScrFunctionLocations,
        pending_action_table=TblPendingActions,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=error_message)

    if request_type == constants.POST:
        request_group_number, unique_rgn_id, controller_rgn_str = get_unique_rgn_data(
            work_flow_id=constants.SCR, work_flow_section_id=constants.SCR_SECTION_1, table=TblScrMaster
        )
        response_params = {
            'post_success_response': True,
            'data': {'request_group_number': request_group_number, 'unit_id': int(request_data['unit_id'])}
        }
    else:
        request_group_number = int(request_data['request_group_number'])
        controller_rgn_str = get_controller_rgn_str(
            request_group_number=request_group_number,
            work_flow_id=constants.SCR,
            work_flow_section_id=constants.SCR_SECTION_1
        )
        response_params = {'patch_success_response': True}
    logger.info(controller_rgn_str)

    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status'])
    if request_type == constants.POST:
        scr_master_record = TblScrMaster(
            request_group_number=request_group_number,
            unique_rgn_id=unique_rgn_id,
            change_type=int(request_data['change_type']),
            deletion_reason=request_data['deletion_reason'],
            request_status=constants.SCR_SECTION_1,
            section_status=section_status,
            created_time=convert_time_to_timestamp(),
            created_by=user_id
        )
        db.session.add(scr_master_record)
        logger.info(controller_rgn_str + ' - New SCR request created.')
    else:
        TblScrMaster.query.filter_by(request_group_number=request_group_number).update({
            'modified_time': convert_time_to_timestamp(), 'modified_by': user_id
        })
        logger.info(controller_rgn_str + ' - Existing SCR request updated.')
    db.session.commit()

    if request_type == constants.POST:
        scr_function_locations = [
            TblScrFunctionLocations(request_group_number=request_group_number, function_location_id=fl['fl_id'])
            for fl in loads(request_data['fl_data'])
        ]
        db.session.add_all(scr_function_locations)
        db.session.commit()
        logger.info(controller_rgn_str + str(request_group_number) + ' - SCR function locations added.')

    update_fl_data(
        request_data=request_data,
        controller_rgn_str=controller_rgn_str,
        controller_name=constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_1],
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_number=constants.SCR_SECTION_1,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )
    return create_response(**response_params)


def she_review_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_status = int(request_data['section_status'])

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_master_table_key=constants.KEY_DICT['SHE_REVIEW_ID'],
        section_id=constants.SCR_SECTION_2,
        master_table=TblScrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.SCR, work_flow_section_id=constants.SCR_SECTION_2
    )
    logger.info(controller_rgn_str)

    she_review_id = add_or_update_she_review(
        request_data=request_data,
        request_group_number=request_group_number,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    scr_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'she_review_id': she_review_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if section_status == constants.REJECTED:
        scr_master_params['request_status'] = constants.REJECTED
    elif section_status in [constants.APPROVED, constants.RECYCLED]:
        scr_master_params['request_status'] = constants.SCR_SECTION_2
    update_scr_master(**scr_master_params)

    update_fl_data(
        request_data=request_data,
        controller_rgn_str=controller_rgn_str,
        controller_name=constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_2],
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_number=constants.SCR_SECTION_2,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=int(request_data['user_id']),
        table=TblUserActiveWorkflows
    )

    if section_status in [constants.RECYCLED, constants.REJECTED]:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': int(request_data['user_id']),
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.SCR,
            'current_section_id': constants.SCR_SECTION_2,
            'pending_action_id': constants.SCR_SECTION_1 if section_status == constants.RECYCLED else None,
            'completed_action_id_list': str(constants.SCR_SECTION_2)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    if request_type == constants.POST:
        return create_response(post_success_response=True)
    else:
        return create_response(patch_success_response=True)


def oims_admin_approval_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_status = int(request_data['section_status'])

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_master_table_key=constants.KEY_DICT['OIMS_ADMIN_APPROVAL_ID'],
        section_id=constants.SCR_SECTION_3,
        master_table=TblScrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.SCR, work_flow_section_id=constants.SCR_SECTION_3
    )
    logger.info(controller_rgn_str)

    oims_admin_approval_id = add_or_update_oims_admin_approval(
        request_data=request_data,
        request_group_number=request_group_number,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    scr_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'oims_admin_approval_id': oims_admin_approval_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if section_status == constants.REJECTED:
        scr_master_params['request_status'] = constants.REJECTED
    else:
        scr_master_params['request_status'] = constants.SCR_SECTION_3
    update_scr_master(**scr_master_params)

    update_fl_data(
        request_data=request_data,
        controller_name=constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_3],
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_number=constants.SCR_SECTION_3,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=int(request_data['user_id']),
        table=TblUserActiveWorkflows
    )

    if section_status in [constants.RECYCLED, constants.REJECTED]:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': int(request_data['user_id']),
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.SCR,
            'current_section_id': constants.SCR_SECTION_3,
            'pending_action_id': constants.SCR_SECTION_2 if section_status == constants.RECYCLED else None,
            'completed_action_id_list': str(constants.SCR_SECTION_3)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    if request_type == constants.POST:
        return create_response(post_success_response=True)
    else:
        return create_response(patch_success_response=True)


def area_btm_approval_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_status = int(request_data['section_status'])

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_master_table_key=constants.KEY_DICT['AREA_BTM_APPROVAL_ID'],
        section_id=constants.SCR_SECTION_4,
        master_table=TblScrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.SCR, work_flow_section_id=constants.SCR_SECTION_4
    )
    logger.info(controller_rgn_str)

    area_btm_approval_id = add_or_update_area_btm_approval(
        request_data=request_data,
        request_group_number=request_group_number,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    scr_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'request_status': constants.SCR_SECTION_4,
        'area_btm_approval_id': area_btm_approval_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    update_scr_master(**scr_master_params)

    update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_number=constants.SCR_SECTION_4,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=int(request_data['user_id']),
        table=TblUserActiveWorkflows
    )

    if section_status == constants.RECYCLED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': int(request_data['user_id']),
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.SCR,
            'current_section_id': constants.SCR_SECTION_4,
            'pending_action_id': constants.SCR_SECTION_3,
            'completed_action_id_list': str(constants.SCR_SECTION_4)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    if request_type == constants.POST:
        return create_response(post_success_response=True)
    else:
        return create_response(patch_success_response=True)


def create_ra_record(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_master_table_key=constants.KEY_DICT['RELIABILITY_ASSESSMENT_ID'],
        section_id=constants.SCR_SECTION_5,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        master_table=TblScrMaster
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.SCR, work_flow_section_id=constants.SCR_SECTION_5
    )
    logger.info(controller_rgn_str)

    ra_record = TblScrReliabilityAssessmentMaster(
        section_status=constants.SUBMITTED, created_time=convert_time_to_timestamp(), created_by=user_id
    )
    db.session.add(ra_record)
    db.session.commit()
    logger.info(controller_rgn_str + ' - New Reliability Assessment created.')

    scr_master_params = {
        'request_data': request_data,
        'request_group_number': request_group_number,
        'request_status': constants.CLOSED,
        'reliability_assessment_id': ra_record.id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    update_scr_master(**scr_master_params)

    update_fl_data(
        request_group_number=request_group_number,
        request_data=request_data,
        lead_confirmation_func=create_or_update_lead_confirmation_record,
        controller_name=constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_5],
        controller_rgn_str=controller_rgn_str,
        user_id=user_id,
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        section_number=constants.SCR_SECTION_5,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    return create_response(post_success_response=True)


def create_or_update_lead_confirmation_record(request_data=None, logger=None):
    fl_id = int(request_data['function_location_id'])
    status = int(request_data['status'])
    fields = {'status': status}

    record = TblFunctionLocationMaster.query.filter_by(id=fl_id).first()
    if (not record.pm_plan_scheduled or not record.pid_drawing_id or not record.sap_scd_class_updated) and \
            status == constants.ALL_LEAD_CONFIRMATIONS_RECEIVED:
        return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

    record_obj = TblScrLeadConfirmationMaster.query.filter_by(function_location_id=fl_id)
    if not record_obj.first():
        fields.update({
            'function_location_id': fl_id,
            'created_by': int(request_data['user_id']),
            'created_time': convert_time_to_timestamp()
        })
        record = TblScrLeadConfirmationMaster(**fields)
        db.session.add(record)
        db.session.commit()
    else:
        fields.update({'modified_by': int(request_data['user_id']), 'modified_time': convert_time_to_timestamp()})
        record_obj.update(dict(fields))
        db.session.commit()

    return create_response(post_success_response=constants.SUCCESS)


def update_fl_record(request_data=None, fl_id=None, logger=None):
    fields = dict()
    fl_field_id = int(request_data['fl_field_id'])
    fl_field_status = int(request_data['fl_field_status'])

    record = TblScrLeadConfirmationMaster.query.filter_by(function_location_id=fl_id).first()
    if record.status == constants.ALL_LEAD_CONFIRMATIONS_RECEIVED:
        return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

    if fl_field_id == constants.PM_PLAN_SCHEDULED_ID:
        fields.update({'pm_plan_scheduled': fl_field_status})
    elif fl_field_id == constants.PID_DRAWING_UPDATED_ID:
        fields.update({'pid_drawing_updated': fl_field_status})
    elif fl_field_id == constants.SAP_CLASS_UPDATED_ID:
        fields.update({'sap_scd_class_updated': fl_field_status})

    TblFunctionLocationMaster.query.filter_by(id=fl_id).update(dict(fields))
    db.session.commit()

    record = TblScrFlLeadConfirmationMaster(
        function_location_id=fl_id,
        property_id=fl_field_id,
        property_value=fl_field_status,
        created_time=convert_time_to_timestamp(),
        created_by=int(request_data['user_id'])
    )
    db.session.add(record)
    db.session.commit()
    return create_response(patch_success_response=constants.SUCCESS)


def discard_request_data(request_data=None, logger=None):
    fl_master_fields = dict()
    section_master_table = None
    section_master_table_id = None
    request_group_number = int(request_data['request_group_number'])
    work_flow_section_id = int(request_data['work_flow_section_id'])

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number, work_flow_id=constants.SCR, work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str)

    scr_master_obj = TblScrMaster.query.filter_by(request_group_number=request_group_number)
    scr_master_record = scr_master_obj.first()
    scr_function_locations = TblScrFunctionLocations.query.filter_by(request_group_number=request_group_number)

    if work_flow_section_id == constants.SCR_SECTION_1:
        fl_master_fields.update({
            'work_center_id': None,
            'operation_mode_id': None,
            'scd_purpose_id': None,
            'pid_drawing_id': None,
            'equipments_protected': None,
            'change_source_id': None,
            'source_reference_no': None,
            'critical_safeguard': None
        })

    if work_flow_section_id == constants.SCR_SECTION_2:
        request_status = constants.SCR_SECTION_1
        section_master_table = TblScrSheReviewMaster
        section_master_table_id = constants.KEY_DICT['SHE_REVIEW_ID']
        fl_master_fields.update({
            'consequence_level': None, 'consequence_of_failure': None, 'required_availability_target': None,
        })

    if work_flow_section_id == constants.SCR_SECTION_3:
        request_status = constants.SCR_SECTION_2
        section_master_table = TblScrOimsAdminApprovalMaster
        section_master_table_id = constants.KEY_DICT['OIMS_ADMIN_APPROVAL_ID']

    if work_flow_section_id == constants.SCR_SECTION_4:
        request_status = constants.SCR_SECTION_3
        section_master_table = TblScrAreaBtmApprovalMaster
        section_master_table_id = constants.KEY_DICT['AREA_BTM_APPROVAL_ID']

    if work_flow_section_id == constants.SCR_SECTION_5:
        request_status = constants.SCR_SECTION_4
        section_master_table = TblScrReliabilityAssessmentMaster
        section_master_table_id = constants.KEY_DICT['RELIABILITY_ASSESSMENT_ID']

        if scr_master_record.change_type == constants.FL_ADDITION:
            fl_master_fields.update({'pm_plan_deactivated': None, 'recommended_pm_interval': None})
        else:
            fl_master_fields.update({'new_or_updated_equipment_strategy': None, 'recommended_plan': None})
        fl_master_fields.update({'actual_availability_target': None, 'additional_risk_management_options': None})

    delete_section_documents(
        request_group_number=request_group_number,
        work_flow_id=constants.SCR,
        work_flow_section_id=work_flow_section_id,
        document_master_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments,
        logger=logger
    )
    logger.info(controller_rgn_str + ' Attachments deleted.')

    delete_user_active_work_flow_record(
        table=TblUserActiveWorkflows,
        request_group_number=request_group_number,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str + ' User active workflow record deleted.')

    fl_id_list = [record.function_location_id for record in scr_function_locations.all()]
    if fl_master_fields:
        TblFunctionLocationMaster.query.filter(TblFunctionLocationMaster.id.in_(fl_id_list)).update(fl_master_fields)
        db.session.commit()
        logger.info(controller_rgn_str + ' Function location properties set to NULL.')

    if work_flow_section_id == constants.SCR_SECTION_1:
        TblScrFunctionLocations.query.filter_by(request_group_number=request_group_number).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' SCR function location records deleted.')

        scr_master_obj.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' SCR master record deleted.')
    else:
        scr_master_obj.update(dict(request_status=request_status))
        db.session.commit()

    if section_master_table_id:
        section_master_table_id_value = getattr(scr_master_record, section_master_table_id)
        scr_master_obj.update(dict({section_master_table_id: None}))
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table ID set to None in SCR master table.')

        section_master_table.query.filter_by(id=section_master_table_id_value).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table record deleted.')

    return create_response(delete_success_response=constants.SUCCESS)
