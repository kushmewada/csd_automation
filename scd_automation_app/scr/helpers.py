from json import loads

import constants
from common_packages import db
from scd_automation_app.common.models import (
    TblFunctionLocationMaster,
    TblOperationModeMaster,
    TblScdPurposeMaster,
    TblPidDrawingMaster
)
from scd_automation_app.samref.common_functions import convert_time_to_timestamp
from scd_automation_app.samref.common_helpers import get_master_data_ids
from .models import TblScrMaster, TblScrSheReviewMaster, TblScrOimsAdminApprovalMaster, TblScrAreaBtmApprovalMaster


def update_scr_master(
        request_data=None,
        request_group_number=None,
        request_status=None,
        she_review_id=None,
        oims_admin_approval_id=None,
        area_btm_approval_id=None,
        reliability_assessment_id=None,
        controller_rgn_str=None,
        logger=None
):
    update_params = {
        'request_status': request_status,
        'modified_time': convert_time_to_timestamp(),
        'modified_by': int(request_data['user_id'])
    }

    if she_review_id:
        update_params.update({'she_review_id': she_review_id})

    if oims_admin_approval_id:
        update_params.update({'oims_admin_approval_id': oims_admin_approval_id})

    if area_btm_approval_id:
        update_params.update({'area_btm_approval_id': area_btm_approval_id})

    if reliability_assessment_id:
        update_params.update({'reliability_assessment_id': reliability_assessment_id})

    TblScrMaster.query.filter_by(request_group_number=request_group_number).update(dict(update_params))
    db.session.commit()
    logger.info(controller_rgn_str + ' - SCR master record updated.')


def update_fl_data(
        request_group_number=None,
        request_data=None,
        lead_confirmation_func=None,
        controller_rgn_str=None,
        controller_name=None,
        user_id=None,
        logger=None
):
    scr_master_record = TblScrMaster.query.filter_by(request_group_number=request_group_number).first()
    for fl_dict in loads(request_data['fl_data']):
        fl_id = int(fl_dict['fl_id'])
        fl_obj = TblFunctionLocationMaster.query.filter_by(id=fl_id)

        update_params = {
            'critical_safeguard': int(fl_dict['critical_safeguard']),
            'equipments_protected': fl_dict['equipments_protected'].capitalize()
        }
        master_id_dict = get_master_data_ids(
            work_flow_id=constants.SCR,
            fl_data=fl_dict,
            scd_purpose_table=TblScdPurposeMaster,
            pid_drawing_table=TblPidDrawingMaster,
            operation_mode_table=TblOperationModeMaster
        )
        update_params.update(master_id_dict)

        if controller_name == constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_1]:
            update_params.update({
                'change_source_id': int(fl_dict['change_source_id']),
                'source_reference_no': fl_dict['source_reference_no']
            })

        if controller_name in [constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_2],
                               constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_3]]:

            if constants.KEY_DICT['CONSEQUENCE_LEVEL'] in fl_dict:
                update_params.update({'consequence_level': fl_dict['consequence_level'].capitalize()})
            if constants.KEY_DICT['CONSEQUENCE_OF_FAILURE'] in fl_dict:
                update_params.update({'consequence_of_failure': fl_dict['consequence_of_failure'].capitalize()})
            if constants.KEY_DICT['REQUIRED_AVAILABILITY_TARGET'] in fl_dict:
                update_params.update({'required_availability_target': int(fl_dict['availability_target'])})

        if controller_name == constants.WORK_FLOW_CONTROLLER_DICT[constants.SCR][constants.SCR_SECTION_5]:
            if constants.KEY_DICT['PM_PLAN_DEACTIVATED'] in fl_dict:
                update_params.update({'pm_plan_deactivated': int(fl_dict['pm_plan_deactivated'])})
            if constants.KEY_DICT['RECOMMENDED_PM_INTERVAL'] in fl_dict:
                update_params.update({'recommended_pm_interval': int(fl_dict['recommended_pm_interval'])})
            if constants.KEY_DICT['NEW_OR_UPDATED_EQUIPMENT_STRATEGY'] in fl_dict:
                update_params.update({
                    'new_or_updated_equipment_strategy': int(fl_dict['new_or_updated_equipment_strategy'])
                })
            if constants.KEY_DICT['RECOMMENDED_PLAN'] in fl_dict:
                update_params.update({'recommended_plan': fl_dict['recommended_plan'].capitalize()})
            if constants.KEY_DICT['ADDITIONAL_RISK_MANAGEMENT_OPTIONS'] in fl_dict:
                update_params.update({
                    'additional_risk_management_options': fl_dict['additional_risk_management_options'].capitalize()
                })

            if scr_master_record.change_type == constants.FL_ADDITION:
                fl_type = constants.SCD
            elif scr_master_record.change_type == constants.FL_DELETION:
                fl_type = constants.DELETED_FL
            elif scr_master_record.change_type == constants.FL_DEMOLITION:
                fl_type = constants.DEMOLISHED_FL

            update_params.update({
                'fl_type': fl_type,
                'actual_availability_target': int(fl_dict['actual_availability_target']),
                'pm_plan_scheduled': constants.PM_PLAN_NOT_SCHEDULED,
                'pid_drawing_updated': constants.PID_DRAWING_NOT_UPDATED,
                'sap_scd_class_updated': constants.SAP_SCD_CLASS_NOT_UPDATED
            })

            lead_data_dict = {
                'function_location_id': fl_id,
                'status': constants.ALL_LEAD_CONFIRMATIONS_NOT_RECEIVED,
                'user_id': user_id
            }
            lead_confirmation_func(request_data=lead_data_dict, logger=logger)

        if update_params:
            fl_obj.update(dict(update_params))
            db.session.commit()

    logger.info(controller_rgn_str + ' - Function Location data updated.')


def add_or_update_she_review(request_data=None, request_group_number=None, controller_rgn_str=None, logger=None):
    scr_master_record = TblScrMaster.query.filter_by(request_group_number=request_group_number).first()
    query_params = {'section_status': int(request_data['section_status'])}
    if constants.KEY_DICT['SHE_ENGINEER_COMMENTS'] in request_data:
        query_params.update({'she_engineer_comments': request_data['she_engineer_comments'].capitalize()})
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        query_params.update({'recycle_comments': request_data['recycle_comments'].capitalize()})

    if scr_master_record.she_review_id:
        query_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': int(request_data['user_id'])})
        she_review = TblScrSheReviewMaster.query.filter_by(id=scr_master_record.she_review_id)
        she_review.update(dict(query_params))
        she_review = she_review.first()
        db.session.commit()
        logger.info(controller_rgn_str + ' - SHE Review updated.')
    else:
        query_params.update({'created_time': convert_time_to_timestamp(), 'created_by': int(request_data['user_id'])})
        she_review = TblScrSheReviewMaster(**query_params)
        db.session.add(she_review)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New SHE Review created.')

    return she_review.id


def add_or_update_oims_admin_approval(
        request_data=None, request_group_number=None, controller_rgn_str=None, logger=None
):
    scr_master_record = TblScrMaster.query.filter_by(request_group_number=request_group_number).first()

    query_params = {'section_status': int(request_data['section_status'])}
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        query_params.update({'recycle_comments': request_data['recycle_comments'].capitalize()})
    if constants.KEY_DICT['OIMS_ADMIN_COMMENTS'] in request_data:
        query_params.update({'oims_admin_comments': request_data['oims_admin_comments'].capitalize()})

    if scr_master_record.oims_admin_approval_id is None:
        query_params.update({'created_time': convert_time_to_timestamp(), 'created_by': int(request_data['user_id'])})
        record = TblScrOimsAdminApprovalMaster(**query_params)
        db.session.add(record)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New OIMS Admin Approval created.')
    else:
        query_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': int(request_data['user_id'])})
        record = TblScrOimsAdminApprovalMaster.query.filter_by(id=scr_master_record.oims_admin_approval_id)
        record.update(dict(query_params))
        db.session.commit()
        record = record.first()
        logger.info(controller_rgn_str + ' - Existing OIMS Admin Approval updated.')
    return record.id


def add_or_update_area_btm_approval(request_data=None, request_group_number=None, controller_rgn_str=None, logger=None):
    scr_master_record = TblScrMaster.query.filter_by(request_group_number=request_group_number).first()
    query_params = {'section_status': int(request_data['section_status'])}
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        query_params.update({'recycle_comments': request_data['recycle_comments'].capitalize()})
    if constants.KEY_DICT['AREA_BTM_COMMENTS'] in request_data:
        query_params.update({'area_btm_comments': request_data['area_btm_comments'].capitalize()})

    if scr_master_record.area_btm_approval_id is None:
        query_params.update({'created_time': convert_time_to_timestamp(), 'created_by': int(request_data['user_id'])})
        record = TblScrAreaBtmApprovalMaster(**query_params)
        db.session.add(record)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New Area BTM Approval created.')
    else:
        query_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': int(request_data['user_id'])})
        record = TblScrAreaBtmApprovalMaster.query.filter_by(id=scr_master_record.area_btm_approval_id)
        record.update(dict(query_params))
        db.session.commit()
        record = record.first()
        logger.info(controller_rgn_str + ' - Existing Area BTM Approval updated.')
    return record.id


def get_property_key(property_id=None):
    if property_id == constants.PM_PLAN_SCHEDULED_ID:
        return constants.KEY_DICT['PM_PLAN_SCHEDULED']
    elif property_id == constants.PID_DRAWING_UPDATED_ID:
        return constants.KEY_DICT['PID_DRAWING_UPDATED']
    elif property_id == constants.SAP_CLASS_UPDATED_ID:
        return constants.KEY_DICT['SAP_SCD_CLASS_UPDATED']
