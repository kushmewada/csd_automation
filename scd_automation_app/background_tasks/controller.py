import constants
from scd_automation_app.cod.models import TblCodMaster, TblCodInitialDefeatApprovalMaster, TblCodLongTermDefeatMaster
from scd_automation_app.common.models import TblUnitMaster, TblFunctionLocationMaster
from scd_automation_app.pmdr.models import (
    TblWorkOrderMaster, TblPmdrMaster, TblPmdrWorkOrders, TblPmdrInitialDefermentMaster
)
from scd_automation_app.samref.common_functions import get_time_interval, convert_time_to_timestamp
from scd_automation_app.samref.common_helpers import create_pending_action_record
from scd_automation_app.users.models import TblPendingActions
from scd_automation_app.users.models import TblUserActiveWorkflows
from .helpers import log_pending_actions_for_cod


def update_cod_requests(logger=None):
    cod_master_records = TblCodMaster.query.filter(
        TblCodMaster.initial_defeat_approval_id != None,
        TblCodMaster.request_status != constants.CLOSED
    ).all()

    for record in cod_master_records:
        cod_initial_defeat_record = TblCodInitialDefeatApprovalMaster.query.filter_by(
            id=record.initial_defeat_approval_id
        ).first()
        time_interval = get_time_interval(start_timestamp=cod_initial_defeat_record.created_time)

        if record.long_term_defeat_id is not None:
            pending_actions = TblPendingActions.query.filter_by(
                request_group_number=record.request_group_number,
                pending_action_id=constants.COD_SECTION_5,
                status=constants.PENDING_ACTION
            ).all()

            if pending_actions:
                continue

            long_term_defeat = TblCodLongTermDefeatMaster.query.filter_by(id=record.long_term_defeat_id).first()
            time_interval = convert_time_to_timestamp() - long_term_defeat.expiry_date

            if long_term_defeat.section_status == constants.RECYCLED and time_interval < 0:
                controller_rgn_str = constants.WORK_FLOW_CONTROLLER_DICT['BACKGROUND_TASKS'] \
                                     + str(record.request_group_number)

                logger.info(controller_rgn_str + 'COD Record ID: ' + str(record.id)
                            + ' - Long Term Defeat pending action created.')
                logger.info(controller_rgn_str + ' Role ID: ' + str(1) + ' User ID: ' + str(1))

                log_pending_actions_for_cod(request_group_number=record.request_group_number,
                                            pending_action_id=constants.COD_SECTION_5, logger=logger)

        elif time_interval.days > 7 and time_interval.seconds > 0:
            cod_master_record = TblCodMaster.query.filter_by(id=record.id).first()
            controller_rgn_str = constants.WORK_FLOW_CONTROLLER_DICT['BACKGROUND_TASKS'] \
                                 + str(cod_master_record.request_group_number)

            pending_actions = TblPendingActions.query.filter_by(
                request_group_number=cod_master_record.request_group_number,
                pending_action_id=constants.COD_SECTION_5,
                status=constants.PENDING_ACTION
            ).all()

            if pending_actions:
                continue

            logger.info('COD Record ID: ' + str(record.id) + ' - Long Term Defeat pending action created.')
            logger.info(controller_rgn_str + ' Role ID: ' + str(1) + ' User ID: ' + str(1))

            log_pending_actions_for_cod(request_group_number=cod_master_record.request_group_number,
                                        pending_action_id=constants.COD_SECTION_5, logger=logger)

        elif time_interval.days > 1 and time_interval.seconds > 0:
            cod_master_obj = TblCodMaster.query.filter_by(id=record.id)
            cod_master_record = cod_master_obj.first()

            controller_rgn_str = constants.WORK_FLOW_CONTROLLER_DICT['BACKGROUND_TASKS'] \
                                 + str(cod_master_record.request_group_number)

            pending_actions = TblPendingActions.query.filter_by(
                request_group_number=cod_master_record.request_group_number,
                pending_action_id=constants.COD_SECTION_3,
                status=constants.PENDING_ACTION
            ).all()
            if pending_actions:
                continue

            cod_master_obj.update(dict(defeat_status=constants.DEFEAT_STATUS_DICT['DEFEAT_EXPIRED']))
            logger.info('COD Record ID: ' + str(record.id) + ' - Defeat Extension Approval pending action created.')
            logger.info(controller_rgn_str + ' Role ID: ' + str(1) + ' User ID: ' + str(1))

            log_pending_actions_for_cod(request_group_number=cod_master_record.request_group_number,
                                        pending_action_id=constants.COD_SECTION_3, logger=logger)


def update_pmdr_requests(logger=None):
    pmdr_master_records = TblPmdrMaster.query.filter(
        TblPmdrMaster.initial_deferment_id != None,
        TblPmdrMaster.request_status.not_in([constants.REJECTED, constants.CLOSED])
    ).all()

    for record in pmdr_master_records:
        pending_actions = TblPendingActions.query.filter(
            TblPendingActions.request_group_number == record.request_group_number,
            TblPendingActions.pending_action_id.in_(
                [constants.PMDR_SECTION_4, constants.PMDR_SECTION_5, constants.PMDR_SECTION_6]
            ),
            TblPendingActions.status == constants.PENDING_ACTION
        ).first()

        if pending_actions:
            continue

        initial_deferment = TblPmdrInitialDefermentMaster.query.filter_by(id=record.initial_deferment_id).first()
        time_interval = get_time_interval(start_timestamp=initial_deferment.proposed_deferment_date)

        if time_interval.days >= 0:
            controller_rgn_str = \
                constants.WORK_FLOW_CONTROLLER_DICT['BACKGROUND_TASKS'] + str(record.request_group_number)

            pmdr_work_order = TblPmdrWorkOrders.query.join(
                TblWorkOrderMaster, TblWorkOrderMaster.id == TblPmdrWorkOrders.work_order_id
            ).join(
                TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id
            ).join(
                TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
            ).add_columns(
                TblFunctionLocationMaster.unit_id
            ).filter(
                TblPmdrWorkOrders.request_group_number == record.request_group_number
            ).first()

            request_data = {
                'unit_id': pmdr_work_order.unit_id,
                'request_group_number': record.request_group_number,
                'completed_action_id_list': str(constants.PMDR_SECTION_3),
                'pending_action_id': str(constants.PMDR_SECTION_4),
                'assigned_to_role_id': 1,
                'role_id': 1,
                'assigned_to': 1,
                'user_id': 1
            }

            TblPmdrMaster.query.filter_by(id=record.id).update(dict(
                deferment_status=constants.PMDR_REQUEST_STATUS_DICT['EXPIRED']
            ))
            logger.info(controller_rgn_str + ' PMDR record ID - ' + str(record.id) + 'updated.')
            create_pending_action_record(
                request_data=request_data,
                user_active_work_flow_table=TblUserActiveWorkflows,
                pending_action_table=TblPendingActions,
                background_task=True,
                logger=logger
            )
