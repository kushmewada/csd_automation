import constants
from scd_automation_app.cod.models import TblCodFunctionLocations
from scd_automation_app.common.models import TblUnitMaster, TblFunctionLocationMaster
from scd_automation_app.samref.common_helpers import create_pending_action_record
from scd_automation_app.users.models import TblUserActiveWorkflows, TblPendingActions


def get_unit_id(request_group_number=None):
    record = TblCodFunctionLocations.query.join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCodFunctionLocations.function_location_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).add_columns(
        TblUnitMaster.id
    ).filter(
        TblCodFunctionLocations.request_group_number == request_group_number
    ).first()
    return record[1]


def log_pending_actions_for_cod(request_group_number=None, pending_action_id=None, logger=None):
    request_data = {
        'unit_id': get_unit_id(request_group_number=request_group_number),
        'request_group_number': request_group_number,
        'pending_action_id': pending_action_id,
        'completed_action_id_list':
            str(constants.COD_SECTION_2) + ',' + str(constants.COD_SECTION_3) + ',' + str(constants.COD_SECTION_5),
        'assigned_to_role_id': 1,
        'assigned_to': 1,
        'role_id': 1,
        'user_id': 1
    }
    create_pending_action_record(
        request_data=request_data,
        user_active_work_flow_table=TblUserActiveWorkflows,
        pending_action_table=TblPendingActions,
        background_task=True,
        logger=logger
    )
