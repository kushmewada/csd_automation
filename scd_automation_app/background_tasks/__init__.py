from time import sleep

import constants
from common_packages import db, init_app
from logging_module import setup_logger
from .controller import update_cod_requests, update_pmdr_requests

app = init_app()
logger = setup_logger(constants.LOGGER_DICT['BACKGROUND_TASKS_LOG'])


def background_tasks(stop_thread=True):
    try:
        while stop_thread:
            with app.app_context():
                update_cod_requests(logger=logger)
                update_pmdr_requests(logger=logger)
                db.session.commit()
                sleep(constants.BACKGROUND_TASK_SLEEP_INTERVAL)
    except:
        logger.exception('background_tasks: ')
