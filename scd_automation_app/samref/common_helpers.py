import os
from datetime import datetime
from json import loads, dumps

import constants
from common_packages import db
from .common_functions import (
    convert_dict_to_list,
    sort_list_of_dictionaries,
    paginate_records,
    convert_timestamp_to_date,
    get_list_from_comma_separated_str,
    convert_time_to_timestamp
)


def get_controller_rgn_str(request_group_number=None, work_flow_id=None, work_flow_section_id=None):
    return constants.WORK_FLOW_CONTROLLER_DICT[work_flow_id][work_flow_section_id] + ': rgn - ' \
           + str(request_group_number)


def get_unique_rgn_data(work_flow_id=None, work_flow_section_id=None, table=None):
    request_group_number = 1
    last_record = table.query.order_by(table.id.desc()).first()
    if last_record:
        request_group_number += last_record.request_group_number

    unique_rgn_id = \
        str(datetime.now().year) + '-' + constants.WORK_FLOW_DICT[work_flow_id] + '-' + str(request_group_number)
    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number, work_flow_id=work_flow_id, work_flow_section_id=work_flow_section_id
    )
    return request_group_number, unique_rgn_id, controller_rgn_str


def get_user_data_from_request(request_record=None, user_table=None):
    creation_user_record = user_table.query.filter_by(id=request_record.created_by).first()
    user_data = {
        'created_time': convert_timestamp_to_date(timestamp=request_record.created_time),
        'created_by': {'id': creation_user_record.id, 'name': creation_user_record.name},
        'modified_time': None,
        'modified_by': None
    }

    if request_record.modified_by:
        modification_user_record = user_table.query.filter_by(id=request_record.modified_by).first()
        user_data.update({
            'modified_time': convert_timestamp_to_date(timestamp=request_record.modified_time),
            'modified_by': {'id': modification_user_record.id, 'name': modification_user_record.name}
        })
    return user_data


def check_request_permission_for_section_1(
        request_data=None,
        data_key=None,
        id_key=None,
        id_field=None,
        section_id=None,
        master_table=None,
        id_table=None,
        pending_action_table=None,
        request_type=None,
):
    request_id_list = [record[id_key] for record in loads(request_data[data_key])]
    records = master_table.query.join(
        id_table, id_table.request_group_number == master_table.request_group_number
    ).add_columns(
        getattr(id_table, id_field)
    ).filter(
        master_table.request_status.not_in([constants.REJECTED, constants.CLOSED]),
        getattr(id_table, id_field).in_(request_id_list)
    ).all()

    if request_type == constants.POST:
        if records:
            status, error_message = constants.FAILURE, constants.REQUEST_EXISTS_FOR_FL
        else:
            status, error_message = constants.SUCCESS, None
    else:
        request_group_number = int(request_data['request_group_number'])
        record = pending_action_table.query.filter_by(
            request_group_number=request_group_number, pending_action_id=section_id, status=constants.PENDING_ACTION
        ).first()
        if record is None:
            status, error_message = constants.FAILURE, constants.REQUEST_NOT_ALLOWED
        else:
            status, error_message = constants.SUCCESS, None
    return status, error_message


def check_request_permission_for_csvr_section_1(
        request_data=None,
        change_type=None,
        car_seal_id_list=None,
        csvr_master_table=None,
        csvr_car_seal_table=None,
        car_seal_master_table=None
):
    if change_type == constants.CAR_SEAL_ADDITION_ID:
        request_id_list = [record['car_seal_no'].upper() for record in loads(request_data['car_seal_data'])]
        car_seals = car_seal_master_table.query.filter(car_seal_master_table.car_seal_no.in_(request_id_list)).all()
        if car_seals:
            return constants.FAILURE, constants.ADDITION_REQUEST_EXISTS_FOR_CAR_SEAL
        return constants.SUCCESS, None
    elif change_type == constants.CAR_SEAL_DELETION_ID:
        records = csvr_car_seal_table.query.join(
            csvr_master_table, csvr_master_table.request_group_number == csvr_car_seal_table.request_group_number
        ).filter(
            csvr_car_seal_table.car_seal_id.in_(car_seal_id_list)
        ).add_columns(
            csvr_master_table.change_type,
            csvr_car_seal_table.car_seal_id
        ).all()
        existing_car_seal_id_list = [record.car_seal_id for record in records]
        existing_car_seal_id_list.sort()

        car_seal_id_list.sort()
        if car_seal_id_list != existing_car_seal_id_list:
            return constants.FAILURE, constants.CAR_SEALS_DO_NOT_EXISTS
        elif records[0].change_type == constants.CAR_SEAL_DELETION_ID:
            return constants.FAILURE, constants.DELETION_REQUEST_EXISTS_FOR_CAR_SEAL
        else:
            return constants.SUCCESS, None


def check_request_permissions(
        request_group_number=None,
        work_flow_id=None,
        section_master_table_key=None,
        section_id=None,
        master_table=None,
        pending_action_table=None,
        user_active_flow_table=None,
        request_type=constants.POST
):
    record = master_table.query.filter_by(request_group_number=request_group_number).first()
    pending_action = pending_action_table.query.filter_by(
        request_group_number=request_group_number, pending_action_id=section_id, status=constants.PENDING_ACTION
    ).first()

    if pending_action is None:
        return constants.FAILURE

    if request_type == constants.POST and getattr(record, section_master_table_key):
        return constants.FAILURE

    if request_type in [constants.PUT, constants.PATCH] and getattr(record, section_master_table_key) is None:
        return constants.FAILURE

    active_work_flow_record = user_active_flow_table.query.filter_by(
        request_group_number=request_group_number,
        work_flow_id=work_flow_id,
        section_number=section_id,
        section_status=constants.TEXTUAL_DATA_SUBMITTED
    ).first()
    if active_work_flow_record:
        return constants.FAILURE

    return constants.SUCCESS


def get_attachment_list_for_request(
        request_group_number=None,
        work_flow_id=None,
        work_flow_section_id=None,
        document_table=None,
        work_flow_document_table=None
):
    records = work_flow_document_table.query.join(
        document_table, document_table.id == work_flow_document_table.document_id
    ).add_columns(
        work_flow_document_table.id,
        work_flow_document_table.document_id,
        document_table.original_name,
    ).filter(
        work_flow_document_table.request_group_number == request_group_number,
        work_flow_document_table.work_flow_id == work_flow_id,
        work_flow_document_table.work_flow_section_id == work_flow_section_id
    ).all()

    attachment_list = [
        {
            'document_id': record.document_id,
            'work_flow_document_id': record.id,
            'original_name': record.original_name
        } for record in records
    ]
    return attachment_list


def create_or_update_user_active_work_flow(
        request_group_number=None, work_flow_id=None, section_number=None, section_status=None, user_id=None, table=None
):
    fields = {'request_group_number': request_group_number, 'work_flow_id': work_flow_id,
              'section_number': section_number, 'section_status': section_status}
    if section_status == constants.TEXTUAL_DATA_SUBMITTED:
        fields.update({'created_by': user_id, 'created_time': convert_time_to_timestamp()})
        record = table(**fields)
        db.session.add(record)
        db.session.commit()
    else:
        fields.update({'modified_by': user_id, 'modified_time': convert_time_to_timestamp()})
        table.query.filter_by(
            request_group_number=request_group_number, work_flow_id=work_flow_id, section_number=section_number
        ).update(dict(fields))
        db.session.commit()


def get_search_results(records=None, search_key=None, search_text=None):
    search_text = search_text.lower()
    return [record for record in records if search_text in record[search_key].lower()]


def format_list_api_data(
        records=None,
        is_dict=True,
        sort_key=None,
        sort_reverse=False,
        list_name=None,
        search_key=None,
        search_text=None,
        page=None,
        settings_table=None
):
    if is_dict:
        record_list = convert_dict_to_list(records=records)
    else:
        record_list = records

    if sort_key:
        record_list = sort_list_of_dictionaries(list_of_dict=record_list, key=sort_key, reverse=sort_reverse)
    if search_text:
        record_list = get_search_results(records=record_list, search_key=search_key, search_text=search_text)
    if page:
        record_list = paginate_records(
            page=page, record_list=record_list, list_name=list_name, settings_table=settings_table
        )
    return record_list


def add_or_update_record(table=None, id_value=None, field=None, field_value=None):
    if id_value is None:
        record = table(**{field: field_value})
        db.session.add(record)
        db.session.commit()
    else:
        record = table.query.filter_by(id=id_value)
        record.update(dict({field: field_value}))
        db.session.commit()
        record = record.first()
    return record.id


def check_and_add_record(table=None, id_value=None, field=None, field_value=None):
    if id_value:
        record = table.query.filter_by(id=id_value).first()
    else:
        record = table.query.filter_by(**{field: field_value}).first()
    if not record or not getattr(record, field).lower() == field_value.lower():
        record = table(**{field: field_value})
        db.session.add(record)
        db.session.commit()
    return record.id


def get_master_data_ids(work_flow_id=None, fl_data=None, scd_purpose_table=None, pid_drawing_table=None,
                        operation_mode_table=None):
    scd_purpose_id = check_and_add_record(
        table=scd_purpose_table, field=constants.KEY_DICT['SCD_PURPOSE'], field_value=fl_data['scd_purpose'].title()
    )

    pid_drawing_id = check_and_add_record(
        table=pid_drawing_table, field=constants.KEY_DICT['PID_DRAWING'], field_value=fl_data['pid_drawing'].title()
    )

    master_id_dict = {'scd_purpose_id': scd_purpose_id, 'pid_drawing_id': pid_drawing_id}
    if work_flow_id == constants.SCR:
        operation_mode_id = check_and_add_record(
            table=operation_mode_table, field=constants.KEY_DICT['MODE'], field_value=fl_data['operation_mode'].title()
        )
        master_id_dict.update({'operation_mode_id': operation_mode_id})

    return master_id_dict


def check_for_existing_master_record(table=None, id_value=None, key_field=None, key_field_value=None):
    record = table.query.filter_by(id=id_value).first()
    if record and getattr(record, key_field) == key_field_value:
        return False
    else:
        if table.query.filter_by(**{key_field: key_field_value}).first():
            return True
        else:
            return False


def delete_record_by_id(table=None, id_value=None):
    record_obj = table.query.filter_by(id=id_value)
    if record_obj.first() is None:
        return constants.FAILURE

    record_obj.delete()
    db.session.commit()
    return constants.SUCCESS


def delete_master_group_record(group_table=None, user_group_table=None, group_id_field=None, group_id=None):
    records = user_group_table.query.filter_by(**{group_id_field: group_id}).all()
    if records:
        return constants.FAILURE
    else:
        group_table.query.filter_by(id=group_id).delete()
        db.session.commit()
        return constants.SUCCESS


def get_sort_data(sort_id=None, table=None):
    if sort_id == constants.START_DATE_ASCENDING:
        sort_obj = table.created_time
    elif sort_id == constants.START_DATE_DESCENDING:
        sort_obj = table.created_time.desc()
    elif sort_id == constants.REQUEST_ID_ASCENDING:
        sort_obj = table.unique_rgn_id
    elif sort_id == constants.REQUEST_ID_DESCENDING:
        sort_obj = table.unique_rgn_id.desc()
    return sort_obj


def get_sort_obj(sort_field=None, sort_type=constants.ASCENDING, table=None):
    sort_obj = getattr(table, sort_field)
    if sort_type == constants.DESCENDING:
        sort_obj = sort_obj.desc()
    return sort_obj


def create_response(
        get_success_response=False,
        post_success_response=False,
        put_success_response=False,
        patch_success_response=False,
        delete_success_response=False,
        default_failure_response=False,
        status=None,
        data=None,
        message=None,
        error_message=None
):
    if status is None:
        status = constants.SUCCESS

    if get_success_response:
        message = constants.DEFAULT_GET_API_SUCCESS_MESSAGE
    elif post_success_response:
        message = constants.DEFAULT_POST_API_SUCCESS_MESSAGE
    elif patch_success_response or put_success_response:
        message = constants.DEFAULT_UPDATE_API_SUCCESS_MESSAGE
    elif delete_success_response:
        message = constants.DEFAULT_DELETE_API_SUCCESS_MESSAGE
    elif default_failure_response:
        status = constants.FAILURE
        error_message = constants.DEFAULT_FAILURE_MESSAGE
    return dumps({'status': status, 'data': data, 'message': message, 'error_message': error_message})


def add_or_update_id_table(table=None, common_field=None, common_value=None, field=None, current_ids=None):
    records = table.query.filter_by(**{common_field: common_value}).all()
    previous_id_list = [getattr(record, field) for record in records]
    previous_id_list.sort()

    current_id_list = get_list_from_comma_separated_str(comma_separated_str=current_ids)
    current_id_list.sort()

    if previous_id_list != current_id_list:
        incorrect_ids = list(set(previous_id_list) - set(current_id_list))

        for _id in incorrect_ids:
            table.query.filter_by(**{common_field: common_value, field: _id}).delete()
        db.session.commit()

    new_id_list = list(set(current_id_list) - set(previous_id_list))
    new_instances = [table(**{common_field: common_value, field: _id}) for _id in new_id_list]
    db.session.add_all(new_instances)
    db.session.commit()


def delete_user_active_work_flow_record(table=None, request_group_number=None, work_flow_section_id=None):
    table.query.filter_by(request_group_number=request_group_number, section_number=work_flow_section_id).delete()
    db.session.commit()


def delete_section_documents(
        request_group_number=None,
        work_flow_id=None,
        work_flow_section_id=None,
        document_master_table=None,
        work_flow_document_table=None,
        logger=None
):
    work_flow_documents = work_flow_document_table.query.filter_by(
        request_group_number=request_group_number,
        work_flow_id=work_flow_id,
        work_flow_section_id=work_flow_section_id
    )
    for record in work_flow_documents:
        delete_document(
            document_master_table=document_master_table,
            work_flow_document_table=work_flow_document_table,
            document_id=record.document_id,
            logger=logger
        )


def delete_document(document_master_table=None, work_flow_document_table=None, document_id=None, logger=None):
    document_obj = document_master_table.query.filter_by(id=document_id)
    document = document_obj.first()

    document_path = os.path.join(document.directory_path, document.secured_name)
    if not os.path.exists(document_path):
        return create_response(status=constants.FAILURE, error_message=constants.FILE_NOT_FOUND)

    logger.info('Deleting attachment: original_name - ' + document.original_name + ' , secured_name - '
                + document.secured_name + ' document_path - ' + str(document_path))
    os.remove(document_path)
    logger.info('Attachment deleted.')

    work_flow_document_table.query.filter_by(document_id=document_id).delete()
    document_obj.delete()
    db.session.commit()

    return create_response(delete_success_response=True)


def create_pending_action_record(
        request_data=None,
        background_task=False,
        user_active_work_flow_table=None,
        pending_action_table=None,
        api_call=False,
        logger=None
):
    user_id = int(request_data['user_id'])
    assigned_to_role_id = int(request_data['role_id'])
    request_group_number = int(request_data['request_group_number'])
    work_flow_id = int(request_data['work_flow_id']) if request_data.get('work_flow_id', None) else None
    current_section_id = \
        int(request_data['current_section_id']) if request_data.get('current_section_id', None) else None
    pending_action_id = int(request_data['pending_action_id']) if request_data.get('pending_action_id', None) else None
    completed_action_ids = request_data.get('completed_action_id_list', None)
    controller_rgn_str = 'rgn - ' + str(request_group_number)

    completed_action_id_list = list()
    if completed_action_ids:
        completed_action_id_list = [int(_id) for _id in completed_action_ids.split(',')]

    if not background_task:
        user_active_work_flow = user_active_work_flow_table.query.filter_by(
            request_group_number=request_group_number,
            work_flow_id=work_flow_id,
            section_number=current_section_id,
            section_status=constants.TEXTUAL_DATA_SUBMITTED,
            created_by=user_id
        ).first()
        if user_active_work_flow is None:
            if api_call is False:
                return constants.FAILURE
            return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

    if pending_action_id:
        existing_record = pending_action_table.query.filter_by(
            request_group_number=request_group_number,
            pending_action_id=pending_action_id,
            status=constants.PENDING_ACTION
        ).first()
        if existing_record:
            if api_call is False:
                return constants.FAILURE
            return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

    if completed_action_id_list:
        pending_action_obj = pending_action_table.query.filter(
            pending_action_table.request_group_number == request_group_number,
            pending_action_table.pending_action_id.in_(completed_action_id_list)
        )
        if pending_action_obj.first() is None:
            if api_call is False:
                return constants.FAILURE
            return create_response(status=constants.FAILURE, error_message=constants.REQUEST_NOT_ALLOWED)

        pending_action_obj.update(dict(
            status=constants.PENDING_ACTION_COMPLETED, completed_time=convert_time_to_timestamp(), completed_by=user_id
        ))
        logger.info(controller_rgn_str + ', user_id - ' + str(user_id) + ', completed_action_id_list - '
                    + str(completed_action_id_list))

    if current_section_id == constants.COD_SECTION_2 and constants.COD_SECTION_4 not in completed_action_id_list:
        pending_action_record = pending_action_table.query.filter_by(
            request_group_number=request_group_number, pending_action_id=constants.COD_SECTION_4
        ).first()
        if pending_action_record is None:
            if api_call is False:
                return constants.FAILURE
            pending_action_id = constants.COD_SECTION_4

    if pending_action_id:
        pending_action_record = pending_action_table(
            unit_id=int(request_data['unit_id']),
            assigned_to_role_id=assigned_to_role_id,
            assigned_to=user_id,
            request_group_number=request_group_number,
            pending_action_id=pending_action_id,
            status=constants.PENDING_ACTION,
            created_time=convert_time_to_timestamp()
        )
        db.session.add(pending_action_record)
        logger.info(controller_rgn_str + ', user_id - ' + str(user_id) + ', role_id - ' + str(assigned_to_role_id) +
                    ', pending_action_id - ' + str(pending_action_id))

    if not background_task:
        create_or_update_user_active_work_flow(
            request_group_number=request_group_number,
            work_flow_id=int(request_data['work_flow_id']),
            section_number=current_section_id,
            section_status=constants.ATTACHMENTS_SUBMITTED,
            user_id=user_id,
            table=user_active_work_flow_table
        )
    db.session.commit()


def update_section_master_table(
        section_master_table=None, request_master_record=None, section_id=None, section_status=None
):
    section_master_table.query.filter_by(
        id=getattr(request_master_record, section_id)
    ).update(dict(
        section_status=section_status
    ))
    db.session.commit()


def check_user_associations(user_association_table=None, association_id_field=None, association_id_value=None):
    records = user_association_table.query.filter(
        getattr(user_association_table, association_id_field) == association_id_value
    ).all()
    if records:
        return False
