import os

from flask import send_from_directory
from werkzeug.utils import secure_filename

import constants
import settings
from common_packages import init_app

app = init_app()


def get_file(directory_path=None, secured_name=None):
    return send_from_directory(directory_path, secured_name)


def check_extension(file):
    file_name = file.filename
    file_ext = file_name.split('.')[-1].lower()
    if file_ext not in app.config['UPLOAD_EXTENSIONS']:
        return constants.UNSUPPORTED_FILE_FORMAT
    else:
        return constants.SUPPORTED_FILE_FORMAT


def get_directory_size(directory_path=None):
    return sum(os.path.getsize(os.path.join(directory_path, f)) for f in os.listdir(directory_path))


def get_secured_file_name(file=None, directory_path=None):
    file_name_postfix = '_1'
    flask_secured_name = secure_filename(file.filename)

    file_list = os.listdir(directory_path)
    if file_list:
        # abc_2.jpg => file.split('.'), [abc_2, jpg] => file.split('.')[-2], abc_2 => file.split('.')[-2][-1], 2
        file_suffix_number_list = [int(file.split('.')[-2][-1]) for file in file_list]
        file_suffix_number_list.sort(reverse=True)
        file_name_postfix = file_suffix_number_list[0] + 1
        file_name_postfix = '_' + str(file_name_postfix)

    name_string_list = flask_secured_name.split('.')
    file_ext = name_string_list[-1].lower()
    secured_name_string_list = name_string_list[:-1] + [file_name_postfix] + ['.'] + [file_ext]

    return ''.join(secured_name_string_list)


def get_storage_directory():
    directory_list = os.listdir(settings.DOCUMENT_STORE_PATH)
    if directory_list:
        directory_list.sort(reverse=True)
        storage_directory = directory_list[0]
        storage_directory_path = os.path.join(settings.DOCUMENT_STORE_PATH, storage_directory)
        directory_size = get_directory_size(directory_path=storage_directory_path)
        total_files = len(os.listdir(storage_directory_path))

        if total_files >= settings.MAX_FILES_IN_ATTACHMENT_DIRECTORY or \
                directory_size >= settings.ATTACHMENT_DIRECTORY_MAX_SIZE:
            storage_directory = int(storage_directory) + 1
            storage_directory = str(storage_directory)
            os.mkdir(os.path.join(settings.DOCUMENT_STORE_PATH, storage_directory))
    else:
        storage_directory = '1'
        os.mkdir(os.path.join(settings.DOCUMENT_STORE_PATH, storage_directory))
    return storage_directory


def store_attachments(files=None, settings_table=None, controller_rgn_str=None, logger=None):
    error_dict = {'size_limit': [], 'unsupported_format': [], 'corrupted_file': []}
    attachment_size_setting_record = settings_table.query.filter_by(id=constants.MAX_ATTACHMENT_SIZE_ID).first()

    for file in files:
        file_name = file.filename

        file.seek(0, os.SEEK_END)
        size = file.tell()
        file.seek(0)
        if size >= attachment_size_setting_record.property_value:
            error_dict['size_limit'].append(file_name)
        else:
            extension_code = check_extension(file)
            if extension_code == constants.UNSUPPORTED_FILE_FORMAT:
                error_dict['unsupported_format'].append(file_name)
            else:
                file_ext = file_name.split('.')[-1].lower()
                file_mime_type = file.content_type
                if file_mime_type not in constants.MIME_TYPES[file_ext]:
                    error_dict['corrupted_file'].append(file_name)

    if error_dict['size_limit'] or error_dict['unsupported_format'] or error_dict['corrupted_file']:
        return constants.FAILURE, error_dict
    else:
        attachment_path_list = list()
        for file in files:
            directory_path = os.path.join(settings.DOCUMENT_STORE_PATH, get_storage_directory())
            secured_name = get_secured_file_name(file=file, directory_path=directory_path)
            document_path = os.path.join(directory_path, secured_name)

            logger.info(controller_rgn_str + ', original_name - ' + str(file.filename) + ', secured_name - '
                        + secured_name + ' & document_path - ' + str(document_path) + '.')
            file.save(document_path)
            logger.info(controller_rgn_str + ' File saved.')

            attachment_path_list.append({
                'original_name': file.filename,
                'secured_name': secured_name,
                'directory_path': directory_path
            })
        return constants.SUCCESS, attachment_path_list
