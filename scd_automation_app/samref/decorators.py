import constants
from common_packages import args_logger, resp_logger, init_app
from scd_automation_app.samref.common_helpers import create_response, get_list_from_comma_separated_str

app = init_app()


def base_api(request=None, api_name=None, api=None, api_params=None, logger=None):
    with app.app_context():
        try:
            if False:
                if not authenticate_user():
                    return create_response(
                        status=constants.FAILURE, error_message=constants.INVALID_USERNAME_OR_PASSWORD
                    )
                if not authorize_user():
                    return create_response(status=constants.FAILURE, error_message=constants.PERMISSION_DENIED)

            args_logger.info(api_name + ': ' + str(request))
            if request.method in [constants.GET, constants.DELETE]:
                args = request.args
            elif request.method in [constants.POST, constants.PUT, constants.PATCH]:
                args = request.form
            args_logger.info(api_name + ': ' + str(args))

            controller_params = {'request_data': args, 'logger': logger}
            if api_params:
                controller_params.update(**api_params)

            api_response = api(**{'logger': logger, **controller_params})
        except:
            api_response = create_response(default_failure_response=True)
            logger.exception(api_name + ': ')
        resp_logger.info(api_name + ': ' + str(api_response))
        return api_response


def authenticate_user():
    return constants.SUCCESS


def authorize_user(request_data=None, permission_id=None):
    return constants.SUCCESS
    # user_id = int(request_data['user_id'])
    # records = TblUserMaster.query.join(
    #     TblUserRoles, TblUserRoles.user_id == TblUserMaster.id
    # ).join(
    #     TblRoleMaster, TblRoleMaster.id == TblUserRoles.role_id
    # ).join(
    #     TblRolePermissions, TblRolePermissions.role_id == TblRoleMaster.id
    # ).add_columns(
    #     TblRolePermissions.permission_id
    # ).filter(
    #     TblUserMaster.id == user_id
    # ).all()
    # permission_id_list = [record.permission_id for record in records]
    #
    # if permission_id not in permission_id_list:
    #     return constants.FAILURE
    #
    # status_list = list()
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserAreas,
    #     group_ids=request_data['area_ids'],
    #     group_id_field=constants.KEY_DICT['AREA_ID'],
    #     user_id=user_id
    # ))
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserGroups,
    #     group_ids=request_data['group_ids'],
    #     group_id_field=constants.KEY_DICT['GROUP_ID'],
    #     user_id=user_id
    # ))
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserPlannerGroups,
    #     group_ids=request_data['planner_group_ids'],
    #     group_id_field=constants.KEY_DICT['PLANNER_GROUP_ID'],
    #     user_id=user_id
    # ))
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserWorkCenters,
    #     group_ids=request_data['work_center_ids'],
    #     group_id_field=constants.KEY_DICT['WORK_CENTER_ID'],
    #     user_id=user_id
    # ))
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserSections,
    #     group_ids=request_data['section_ids'],
    #     group_id_field=constants.KEY_DICT['SECTION_ID'],
    #     user_id=user_id
    # ))
    # status_list.append(check_group_permissions(
    #     user_group_table=TblUserDepartments,
    #     group_ids=request_data['departments_ids'],
    #     group_id_field=constants.KEY_DICT['DEPARTMENT_ID'],
    #     user_id=user_id
    # ))
    # if False in status_list:
    #     return constants.FAILURE
    # return constants.SUCCESS


def check_group_permissions(user_group_table=None, group_ids=None, group_id_field=None, user_id=None):
    if group_ids is None:
        return None

    api_group_id_list = get_list_from_comma_separated_str(comma_separated_str=group_ids)
    api_group_id_list = api_group_id_list.sort()

    records = user_group_table.query.filter_by(user_id=user_id).all()
    existing_group_id_list = [getattr(record, group_id_field) for record in records]
    existing_group_id_list = existing_group_id_list.sort()

    if api_group_id_list != existing_group_id_list:
        return False
    return True
