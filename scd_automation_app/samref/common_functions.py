from datetime import datetime, timedelta

import pagecalc

import constants


def convert_time_to_timestamp(time=None):
    if not time:
        time = datetime.now()
    return int(datetime.timestamp(time))


def convert_timestamp_to_time(timestamp):
    if timestamp:
        return datetime.fromtimestamp(timestamp).strftime("%b %d %Y %H:%M:%S")
    return None


def convert_timestamp_to_date(timestamp=None):
    if timestamp is None:
        return None
    return str(datetime.fromtimestamp(timestamp).date())


def convert_date_to_timestamp(date=None, date_format='%d/%m/%Y'):
    if date is None:
        return None
    date_object = datetime.strptime(date, date_format)
    time_stamp = int(datetime.timestamp(date_object))
    return time_stamp


def paginate_records(page=None, record_list=None, records_per_page=None, list_name=None, settings_table=None):
    page = page - 1

    if records_per_page is None:
        record = settings_table.query.filter_by(id=constants.PAGE_SIZE_ID).first()
        records_per_page = record.setting_value

    paginator = pagecalc.Paginator(total=len(record_list), by=records_per_page)
    page_details = paginator.paginate(page + 1)

    starting_record = page * records_per_page
    end_record = page * records_per_page + records_per_page
    required_record_list = record_list[starting_record: end_record]

    required_page_details = {
        'page': {
            'page': page_details['page']['current'],
            'pageSize': records_per_page,
            'totalEntrys': page_details['item']['totalCount'],
            'totalpage': page_details['page']['count'],
            'start': starting_record + 1,
            'end': starting_record + len(required_record_list)
        },
        list_name: required_record_list
    }

    return required_page_details


def sort_list_of_dictionaries(list_of_dict=None, key=None, reverse=False):
    return sorted(list_of_dict, key=lambda record: record[key], reverse=reverse)


def get_search_results(records=None, search_key=None, search_text=None):
    search_text = search_text.lower()
    required_list = [
        record for record in records if search_text in record[search_key].lower()
    ]
    return required_list


def convert_dict_to_list(records=None):
    return [records[record] for record in records]


def get_key_value_pair_list(
        record_list=None,
        key_1=constants.KEY_DICT['ID'],
        record_key_1=constants.KEY_DICT['ID'],
        key_2=constants.KEY_DICT['NAME'],
        record_key_2=None
):
    return [{key_1: getattr(record, record_key_1), key_2: getattr(record, record_key_2)} for record in record_list]


def get_list_from_comma_separated_str(comma_separated_str=None):
    if not comma_separated_str:
        return list()
    data_list = comma_separated_str.split(',')
    data_list = [int(elem) for elem in data_list]
    return data_list


def get_value_list(records=None, key=None):
    return [record[key] for record in records]


def create_list_from_key_value_pair(record_dict=None, key_1='id', key_2='name'):
    record_list = [
        {
            key_1: _id,
            key_2: record_dict[_id]
        }
        for _id in record_dict
    ]
    return record_list


def get_time_interval(start_timestamp=None):
    start_date_obj = datetime.fromtimestamp(start_timestamp)
    end_date_obj = datetime.now()
    time_interval = end_date_obj - start_date_obj
    return time_interval


def get_time_delta(timestamp=None, days=None, get_timestamp=False, get_datetime_obj=False):
    date_obj = datetime.fromtimestamp(timestamp)
    date_obj = date_obj + timedelta(days)

    if get_timestamp:
        return date_obj.timestamp()

    if get_datetime_obj:
        return date_obj


def get_key_for_given_value(dictionary=None, value=None):
    return list(dictionary.keys())[list(dictionary.values()).index(value)]
