import constants

from flask_sqlalchemy.model import declared_attr

from common_packages import db


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    is_deleted = db.Column(db.Integer, nullable=True, default=constants.NOT_DELETED)


class CreationTimestampMixin(object):
    created_time = db.Column(db.Numeric(15), nullable=True)

    @declared_attr
    def created_by(cls):
        return db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)


class ModificationTimestampMixin(object):
    modified_time = db.Column(db.Numeric(15), nullable=True)

    @declared_attr
    def modified_by(cls):
        return db.Column(db.Integer, db.ForeignKey('tbl_user_master.id'), nullable=True)
