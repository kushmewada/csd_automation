from json import loads

import constants
from common_packages import db
from scd_automation_app.common.models import (
    TblCarSealMaster, TblPidDrawingMaster, TblScdPurposeMaster, TblFunctionLocationMaster
)
from scd_automation_app.csvr.models import TblCsvrMaster, TblCsvrCarSeals
from scd_automation_app.samref.common_functions import convert_time_to_timestamp
from scd_automation_app.samref.common_helpers import check_and_add_record


def add_or_update_csvr_record(
        request_group_number=None,
        unique_rgn_id=None,
        request_data=None,
        ocl_review_id=None,
        area_btm_approval_id=None,
        request_status=None,
        controller_rgn_str=None,
        logger=None
):
    csvr_master_fields = dict()
    if ocl_review_id:
        csvr_master_fields.update({'ocl_review_id': ocl_review_id})
    if area_btm_approval_id:
        csvr_master_fields.update({'area_btm_approval_id': area_btm_approval_id})

    if request_status:
        csvr_master_fields.update({'request_status': request_status})

    csvr_master = TblCsvrMaster.query.filter_by(request_group_number=request_group_number)
    if csvr_master.first() is None:
        csvr_master_fields.update({
            'request_group_number': request_group_number,
            'unique_rgn_id': unique_rgn_id,
            'change_type': int(request_data['change_type']),
            'change_reason': request_data['change_reason'].capitalize(),
            'section_status': constants.SUBMITTED,
            'created_time': convert_time_to_timestamp(),
            'created_by': int(request_data['user_id'])
        })
        csvr_master_record = TblCsvrMaster(**csvr_master_fields)
        db.session.add(csvr_master_record)
        logger.info(controller_rgn_str + ' CSVR Master record created.')
    else:
        csvr_master_fields.update({
            'modified_time': convert_time_to_timestamp(),
            'modified_by': int(request_data['user_id'])
        })
        csvr_master.update(csvr_master_fields)
        logger.info(controller_rgn_str + ' CSVR Master record updated.')
    db.session.commit()


def update_fl_data(request_data=None, controller_rgn_str=None, logger=None):
    car_seal_list = loads(request_data['car_seal_data'])
    for cs in car_seal_list:
        fl_id = int(cs['fl_id'])
        if fl_id == -1:
            continue
        TblFunctionLocationMaster.query.filter_by(id=fl_id).update({
            'scd_purpose_id': check_and_add_record(
                table=TblScdPurposeMaster, field=constants.KEY_DICT['SCD_PURPOSE'], field_value=cs['scd_purpose']
            )
        })
        db.session.commit()
    logger.info(controller_rgn_str + ' FL Data updated.')


def update_car_seal_data(request_group_number=None, request_data=None, controller_rgn_str=None, logger=None):
    unit_id = int(request_data['unit_id'])
    car_seal_list = loads(request_data['car_seal_data'])

    for cs in car_seal_list:
        pid_drawing_id = None
        pid_drawing = cs.get('pid_drawing', None)

        if pid_drawing:
            pid_drawing_id = check_and_add_record(
                table=TblPidDrawingMaster, field=constants.KEY_DICT['PID_DRAWING'], field_value=pid_drawing
            )

        car_seal_fields = {
            'unit_id': unit_id,
            'car_seal_type': constants.PERMANENT_CAR_SEAL_ID,
            'she_critical': int(cs['she_critical']),
            'position_id': int(cs['position_id']),
            'valve_id': int(cs['valve_id']),
            'function_location_id': int(cs['fl_id']) if int(cs['fl_id']) != -1 else None,
            'pid_drawing_id': pid_drawing_id,
            'line_no': cs['line_no'].upper()
        }

        if constants.KEY_DICT['CAR_SEAL_NO'] in cs:
            car_seal_fields.update({'car_seal_no': cs['car_seal_no'].upper()})
            cs_record = TblCarSealMaster(**car_seal_fields)
            db.session.add(cs_record)
            db.session.commit()

            add_csvr_car_seal(request_group_number=request_group_number, car_seal_id=cs_record.id)
        else:
            TblCarSealMaster.query.filter_by(id=int(cs['cs_id'])).update(car_seal_fields)
            db.session.commit()
    logger.info(controller_rgn_str + ' Car Seal data updated.')


def add_csvr_car_seal(request_group_number=None, car_seal_id=None):
    csvr_cs_record = TblCsvrCarSeals(request_group_number=request_group_number, car_seal_id=car_seal_id)
    db.session.add(csvr_cs_record)
    db.session.commit()


def mark_car_seals_as_deleted(request_group_number=None):
    records = TblCsvrCarSeals.query.filter_by(request_group_number=request_group_number).all()
    cs_id_list = [record.car_seal_id for record in records]
    TblCarSealMaster.query.filter(TblCarSealMaster.id.in_(cs_id_list)).update(dict({'is_deleted': constants.DELETED}))
    db.session.commit()
