from flask import Blueprint, request

import constants
from logging_module import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_car_seal_records,
    get_csvr_request_data,
    create_csvr_record,
    ocl_review_record,
    area_btm_approval_record,
    discard_request_data
)

blueprint_name = constants.BLUE_PRINT_DICT['CSVR']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['CSVR']['URL_PREFIX']
csvr_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['CSVR_LOG'])


api_get_car_seal_list = constants.API_DICT['CSVR']['GET_CAR_SEAL_LIST']
api_get_csvr_request_details = constants.API_DICT['CSVR']['GET_CSVR_REQUEST_DETAILS']
api_create_csv_change_request = constants.API_DICT['CSVR']['CREATE_CSVR_REQUEST']
api_ocl_review = constants.API_DICT['CSVR']['OCL_REVIEW']
api_area_btm_approval = constants.API_DICT['CSVR']['AREA_BTM_APPROVAL']
api_discard_form = constants.API_DICT['CSVR']['DISCARD_FORM']


@csvr_bp.route(api_get_car_seal_list, methods=[constants.GET])
def get_car_seal_list():
    return base_api(request=request, api_name=api_get_car_seal_list, api=get_car_seal_records, logger=logger)


@csvr_bp.route(api_get_csvr_request_details, methods=[constants.GET])
def get_csvr_request_details():
    return base_api(request=request, api_name=api_get_csvr_request_details, api=get_csvr_request_data, logger=logger)


@csvr_bp.route(api_create_csv_change_request, methods=[constants.POST])
def create_csv_change_request():
    return base_api(request=request, api_name=api_create_csv_change_request, api=create_csvr_record, logger=logger)


@csvr_bp.route(api_ocl_review, methods=[constants.POST, constants.PUT])
def ocl_review():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_ocl_review, api=ocl_review_record, api_params=api_params,
                    logger=logger)


@csvr_bp.route(api_area_btm_approval, methods=[constants.POST, constants.PUT])
def area_btm_approval():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_area_btm_approval, api=area_btm_approval_record,
                    api_params=api_params, logger=logger)


@csvr_bp.route(api_discard_form, methods=[constants.DELETE])
def discard_form():
    return base_api(request=request, api_name=api_discard_form, api=discard_request_data, logger=logger)
