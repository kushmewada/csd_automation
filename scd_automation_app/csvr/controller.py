import constants
from common_packages import db
from scd_automation_app.attachments.models import TblDocumentMaster, TblWorkFlowDocuments
from scd_automation_app.common.models import (
    TblAreaMaster,
    TblUnitMaster,
    TblPidDrawingMaster,
    TblFunctionLocationMaster,
    TblCarSealMaster,
    TblScdPurposeMaster
)
from scd_automation_app.plant.models import TblCarSealPositionMaster, TblCarSealValveMaster
from scd_automation_app.samref.common_functions import (
    convert_time_to_timestamp, convert_timestamp_to_date, get_list_from_comma_separated_str
)
from scd_automation_app.samref.common_helpers import (
    check_request_permission_for_csvr_section_1,
    check_request_permissions,
    format_list_api_data,
    get_unique_rgn_data,
    get_controller_rgn_str,
    get_user_data_from_request,
    get_attachment_list_for_request,
    create_or_update_user_active_work_flow,
    create_response,
    delete_section_documents,
    delete_user_active_work_flow_record,
    create_pending_action_record
)
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster, TblUserActiveWorkflows, TblPendingActions
from .helpers import add_or_update_csvr_record, update_fl_data, update_car_seal_data, add_csvr_car_seal
from .models import TblCsvrMaster, TblCsvrCarSeals, TblCsvrOclMaster, TblCsvrAreaBtmApprovalMaster


def get_car_seal_records(request_data=None, logger=None):
    filter_fields = [
        TblCarSealMaster.car_seal_type == constants.PERMANENT_CAR_SEAL_ID,
        TblCsvrMaster.request_status == constants.CLOSED
    ]

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id == int(area_id))

    unit_id = request_data.get('unit_id', None)
    if unit_id:
        filter_fields.append(TblUnitMaster.id == int(unit_id))

    change_type = request_data.get('change_type', None)
    if change_type:
        filter_fields.append(TblCsvrMaster.change_type == int(change_type))

    sort_id = request_data.get('sort_id', None)
    if sort_id == constants.CAR_SEAL_NO_ASCENDING or not sort_id:
        sort_obj = TblCarSealMaster.car_seal_no
    else:
        sort_obj = TblCarSealMaster.car_seal_no.desc()

    records = TblCarSealMaster.query.join(
        TblCsvrCarSeals, TblCsvrCarSeals.car_seal_id == TblCarSealMaster.id
    ).join(
        TblCsvrMaster, TblCsvrMaster.request_group_number == TblCsvrCarSeals.request_group_number
    ).join(
        TblCarSealPositionMaster, TblCarSealPositionMaster.id == TblCarSealMaster.position_id
    ).join(
        TblCarSealValveMaster, TblCarSealValveMaster.id == TblCarSealMaster.valve_id
    ).outerjoin(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblCarSealMaster.function_location_id
    ).outerjoin(
        TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblCarSealMaster.pid_drawing_id
    ).outerjoin(
        TblUserMaster, TblUserMaster.id == TblCarSealMaster.verified_by
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblCarSealMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.scd_purpose_id,
        TblScdPurposeMaster.scd_purpose,
        TblPidDrawingMaster.pid_drawing,
        TblCarSealPositionMaster.car_seal_position,
        TblCarSealValveMaster.car_seal_valve,
        TblUserMaster.name,
        TblCarSealMaster.id,
        TblCarSealMaster.car_seal_no,
        TblCarSealMaster.car_seal_type,
        TblCarSealMaster.she_critical,
        TblCarSealMaster.car_seal_no,
        TblCarSealMaster.position_id,
        TblCarSealMaster.valve_id,
        TblCarSealMaster.function_location_id,
        TblCarSealMaster.pid_drawing_id,
        TblCarSealMaster.line_no,
        TblCarSealMaster.change_date,
        TblCarSealMaster.last_verification_date,
        TblCarSealMaster.verified_by
    ).filter(
        *filter_fields
    ).order_by(
        sort_obj
    ).all()

    car_seal_data_dict = {
        record.id: {
            'id': record.id,
            'name': record.car_seal_no,
            'car_seal_type': record.car_seal_type,
            'she_critical': record.she_critical,
            'position': {'id': record.position_id, 'name': record.car_seal_position},
            'valve': {'id': record.valve_id, 'name': record.car_seal_valve},
            'function_location': {
                'id': record.function_location_id,
                'name': record.fl_name,
                'scd_purpose': {'id': record.scd_purpose_id, 'name': record.scd_purpose}
            },
            'pid_drawing': {'id': record.pid_drawing_id, 'name': record.pid_drawing},
            'line_no': record.line_no,
            'change_date': convert_timestamp_to_date(timestamp=record.change_date),
            'last_verification_date': convert_timestamp_to_date(timestamp=record.last_verification_date),
            'last_verified_by': {'id': record.verified_by, 'name': record.name}

        } for record in records
    }

    data = format_list_api_data(
        records=car_seal_data_dict,
        sort_key=constants.KEY_DICT['NAME'],
        list_name=constants.KEY_DICT['CAR_SEAL_LIST'],
        search_key=constants.KEY_DICT['NAME'],
        search_text=request_data.get('search_text', None),
        page=int(request_data['page']) if request_data.get('page', None) else None,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_csvr_request_data(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['section_ids'])

    csvr_cs_records = TblCsvrCarSeals.query.join(
        TblCarSealMaster, TblCarSealMaster.id == TblCsvrCarSeals.car_seal_id
    ).join(
        TblCarSealPositionMaster, TblCarSealPositionMaster.id == TblCarSealMaster.position_id
    ).join(
        TblCarSealValveMaster, TblCarSealValveMaster.id == TblCarSealMaster.valve_id
    ).outerjoin(
        TblPidDrawingMaster, TblPidDrawingMaster.id == TblCarSealMaster.pid_drawing_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblCarSealMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblPidDrawingMaster.pid_drawing,
        TblCarSealPositionMaster.car_seal_position,
        TblCarSealValveMaster.car_seal_valve,
        TblCarSealMaster.unit_id,
        TblCarSealMaster.car_seal_no,
        TblCarSealMaster.she_critical,
        TblCarSealMaster.position_id,
        TblCarSealMaster.valve_id,
        TblCarSealMaster.line_no,
        TblCarSealMaster.function_location_id,
        TblCarSealMaster.pid_drawing_id,
        TblCsvrCarSeals.car_seal_id
    ).filter(
        TblCsvrCarSeals.request_group_number == request_group_number
    ).all()

    car_seal_list = list()
    for cs in csvr_cs_records:
        if cs.function_location_id is not None:
            fl_record = TblFunctionLocationMaster.query.join(
                TblScdPurposeMaster, TblScdPurposeMaster.id == TblFunctionLocationMaster.scd_purpose_id
            ).add_columns(
                TblFunctionLocationMaster.fl_name,
                TblFunctionLocationMaster.scd_purpose_id,
                TblScdPurposeMaster.scd_purpose
            ).filter(
                TblFunctionLocationMaster.id == cs.function_location_id
            ).first()

            fl_id = cs.function_location_id
            fl_name = fl_record.fl_name
            scd_purpose_id = fl_record.scd_purpose_id
            scd_purpose = fl_record.scd_purpose
        else:
            fl_id = -1
            fl_name = None
            scd_purpose_id = None
            scd_purpose = None

        car_seal_list.append({
            'id': cs.car_seal_id,
            'car_seal_no': cs.car_seal_no,
            'fl_data': {
                'id': fl_id,
                'name': fl_name,
                'scd_purpose': {'id': scd_purpose_id, 'name': scd_purpose},
                'pid_drawing': {'id': cs.pid_drawing_id, 'name': cs.pid_drawing}
            },
            'she_critical': cs.she_critical,
            'position_data': {'id': cs.position_id, 'name': cs.car_seal_position},
            'valve_data': {'id': cs.valve_id, 'name': cs.car_seal_valve},
            'line_no': cs.line_no
        })

    attachment_list = get_attachment_list_for_request(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        work_flow_section_id=constants.CSVR_SECTION_1,
        document_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments
    )

    csvr_record = TblCsvrMaster.query.filter_by(request_group_number=request_group_number).first()
    section_1_user_data = get_user_data_from_request(request_record=csvr_record, user_table=TblUserMaster)

    section_1_data_dict = {
        'id': csvr_record.id,
        'section_data': {
            'area': {
                'id': csvr_cs_records[0].area_id,
                'name': csvr_cs_records[0].area
            },
            'unit': {
                'id': csvr_cs_records[0].unit_id,
                'name': csvr_cs_records[0].unit_number
            },
            'change_type': csvr_record.change_type,
            'change_reason': csvr_record.change_reason,
            'car_seal_list': car_seal_list,
            'attachment_list': attachment_list
        },
        'section_status': csvr_record.section_status,
    }
    section_1_data_dict.update(section_1_user_data)

    csvr_request_data_dict = {
        'request_group_number': request_group_number,
        'unique_rgn_id': csvr_record.unique_rgn_id,
        'request_status': csvr_record.request_status,
        'section_1': section_1_data_dict
    }

    if constants.CSVR_SECTION_2 in section_id_list and csvr_record.ocl_review_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.CSVR,
            work_flow_section_id=constants.CSVR_SECTION_2,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        section_2 = TblCsvrOclMaster.query.filter_by(id=csvr_record.ocl_review_id).first()
        section_2_user_data = \
            get_user_data_from_request(request_record=section_2, user_table=TblUserMaster)

        section_2_data_dict = {
            'id': section_2.id,
            'section_data': {'comments': section_2.ocl_comments, 'attachment_list': attachment_list},
            'section_status': section_2.section_status,
        }
        section_2_data_dict.update(section_2_user_data)
        csvr_request_data_dict['section_2'] = section_2_data_dict

    if constants.CSVR_SECTION_3 in section_id_list and csvr_record.area_btm_approval_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.CSVR,
            work_flow_section_id=constants.CSVR_SECTION_3,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )
        section_3 = TblCsvrAreaBtmApprovalMaster.query.filter_by(id=csvr_record.area_btm_approval_id).first()
        section_3_user_data = get_user_data_from_request(request_record=section_3, user_table=TblUserMaster)
        section_3_data_dict = {
            'id': section_3.id,
            'section_data': {
                'comments': section_3.area_btm_comments,
                'recycle_comments': section_3.recycle_comments,
                'attachment_list': attachment_list
            },
            'section_status': section_3.section_status
        }
        section_3_data_dict.update(section_3_user_data)
        csvr_request_data_dict['section_3'] = section_3_data_dict

    return create_response(get_success_response=True, data=csvr_request_data_dict)


def create_csvr_record(request_data=None, logger=None):
    change_type = int(request_data['change_type'])
    request_permission_params = {
        'request_data': request_data, 'change_type': change_type, 'car_seal_master_table': TblCarSealMaster
    }

    if change_type == constants.CAR_SEAL_DELETION_ID:
        car_seal_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['car_seal_ids'])
        request_permission_params.update({
            'car_seal_id_list': car_seal_id_list,
            'csvr_master_table': TblCsvrMaster,
            'csvr_car_seal_table': TblCsvrCarSeals
        })

    status, message = check_request_permission_for_csvr_section_1(**request_permission_params)
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.ADDITION_REQUEST_EXISTS_FOR_CAR_SEAL)

    request_group_number, unique_rgn_id, controller_rgn_str = get_unique_rgn_data(
        work_flow_id=constants.CSVR, work_flow_section_id=constants.CSVR_SECTION_1, table=TblCsvrMaster
    )
    logger.info(controller_rgn_str)

    add_or_update_csvr_record(
        request_group_number=request_group_number,
        unique_rgn_id=unique_rgn_id,
        request_data=request_data,
        request_status=constants.CSVR_SECTION_1,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    if change_type == constants.CAR_SEAL_ADDITION_ID:
        update_car_seal_data(
            request_group_number=request_group_number,
            request_data=request_data,
            controller_rgn_str=controller_rgn_str,
            logger=logger
        )
        update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)
    elif change_type == constants.CAR_SEAL_DELETION_ID:
        for _id in car_seal_id_list:
            add_csvr_car_seal(request_group_number=request_group_number, car_seal_id=_id)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        section_number=constants.CSVR_SECTION_1,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=int(request_data['user_id']),
        table=TblUserActiveWorkflows
    )

    data = {'request_group_number': request_group_number, 'unit_id': int(request_data['unit_id'])}
    return create_response(post_success_response=True, data=data)


def ocl_review_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        section_master_table_key=constants.KEY_DICT['OCL_REVIEW_ID'],
        section_id=constants.CSVR_SECTION_2,
        master_table=TblCsvrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        work_flow_section_id=constants.CSVR_SECTION_2
    )
    logger.info(controller_rgn_str)

    ocl_review_fields = dict()
    if constants.KEY_DICT['COMMENTS'] in request_data:
        ocl_review_fields.update({'ocl_comments': request_data['comments'].capitalize()})

    if request_type == constants.POST:
        ocl_review_fields.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        ocl_review = TblCsvrOclMaster(**ocl_review_fields)
        db.session.add(ocl_review)
        logger.info(controller_rgn_str + ' New OCL Review / Endorsement record created.')

    else:
        ocl_review_fields.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        ocl_review = TblCsvrOclMaster.query.filter_by(id=int(request_data['ocl_review_id']))
        ocl_review.update(ocl_review_fields)
        ocl_review = ocl_review.first()
        logger.info(
            controller_rgn_str + ' OCL Review / Endorsement record with ID {} updated.'.format(str(ocl_review.id))
        )
    db.session.commit()

    if int(request_data['change_type']) == constants.CAR_SEAL_ADDITION_ID:
        update_car_seal_data(
            request_group_number=request_group_number,
            request_data=request_data,
            controller_rgn_str=controller_rgn_str,
            logger=logger
        )
        update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    add_or_update_csvr_record(
        request_group_number=request_group_number,
        request_data=request_data,
        ocl_review_id=ocl_review.id,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        section_number=constants.CSVR_SECTION_2,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.REJECTED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.CSVR,
            'current_section_id': constants.CSVR_SECTION_2,
            'completed_action_id_list': str(constants.CSVR_SECTION_2),
            'section_status': section_status
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    if request_type == constants.POST:
        return create_response(post_success_response=True)
    else:
        return create_response(patch_success_response=True)


def area_btm_approval_record(request_type=None, request_data=None, logger=None):
    request_status = None
    request_group_number = int(request_data['request_group_number'])
    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        section_master_table_key=constants.KEY_DICT['AREA_BTM_APPROVAL_ID'],
        section_id=constants.CSVR_SECTION_3,
        master_table=TblCsvrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    section_status = int(request_data['section_status'])
    user_id = int(request_data['user_id'])
    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        work_flow_section_id=constants.CSVR_SECTION_3
    )
    logger.info(controller_rgn_str)

    area_btm_approval_fields = dict()
    if constants.KEY_DICT['COMMENTS'] in request_data:
        area_btm_approval_fields.update({'area_btm_comments': request_data['comments'].capitalize()})
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        area_btm_approval_fields.update({'recycle_comments': request_data['recycle_comments'].capitalize()})

    if section_status == constants.REJECTED:
        area_btm_approval_fields.update({'section_status': section_status})
        request_status = constants.REJECTED

    if request_type == constants.POST:
        area_btm_approval_fields.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        area_btm_approval = TblCsvrAreaBtmApprovalMaster(**area_btm_approval_fields)
        db.session.add(area_btm_approval)
        logger.info(controller_rgn_str + ' New Area BTM Approval record created.')

    else:
        area_btm_approval_fields.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        area_btm_approval = TblCsvrAreaBtmApprovalMaster.query.filter_by(id=int(request_data['area_btm_approval_id']))
        area_btm_approval.update(area_btm_approval_fields)
        area_btm_approval = area_btm_approval.first()
        logger.info(
            controller_rgn_str + ' Area BTM Approval record with ID {} updated.'.format(str(area_btm_approval.id))
        )
    db.session.commit()

    if int(request_data['change_type'] == constants.CAR_SEAL_ADDITION_ID):
        update_car_seal_data(
            request_group_number=request_group_number,
            request_data=request_data,
            controller_rgn_str=controller_rgn_str,
            logger=logger
        )
        update_fl_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    add_or_update_csvr_record(
        request_group_number=request_group_number,
        request_data=request_data,
        area_btm_approval_id=area_btm_approval.id,
        request_status=request_status,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        section_number=constants.CSVR_SECTION_3,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status in [constants.RECYCLED, constants.REJECTED]:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.CSVR,
            'current_section_id': constants.CSVR_SECTION_3,
            'pending_action_id': constants.CSVR_SECTION_2 if section_status == constants.RECYCLED else None,
            'completed_action_id_list': str(constants.CSVR_SECTION_3),
            'section_status': section_status
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    if request_type == constants.POST:
        return create_response(post_success_response=True)
    else:
        return create_response(patch_success_response=True)


def discard_request_data(request_data=None, logger=None):
    request_status = None
    section_master_table = None
    section_master_table_id = None
    request_group_number = int(request_data['request_group_number'])
    work_flow_section_id = int(request_data['work_flow_section_id'])

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str)

    csvr_car_seal_objects = TblCsvrCarSeals.query.filter_by(request_group_number=request_group_number)
    car_seal_id_list = [record.car_seal_id for record in csvr_car_seal_objects.all()]

    csvr_master_obj = TblCsvrMaster.query.filter_by(request_group_number=request_group_number)
    csvr_master_record = csvr_master_obj.first()

    if work_flow_section_id == constants.CSVR_SECTION_2:
        section_master_table = TblCsvrOclMaster
        section_master_table_id = constants.KEY_DICT['OCL_REVIEW_ID']

    if work_flow_section_id == constants.CSVR_SECTION_3:
        section_master_table = TblCsvrAreaBtmApprovalMaster
        section_master_table_id = constants.KEY_DICT['AREA_BTM_APPROVAL_ID']
        if csvr_master_record.request_status == constants.REJECTED:
            request_status = constants.CSVR_SECTION_2

    delete_section_documents(
        request_group_number=request_group_number,
        work_flow_id=constants.CSVR,
        work_flow_section_id=work_flow_section_id,
        document_master_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments,
        logger=logger
    )
    logger.info(controller_rgn_str + ' Attachments deleted.')

    delete_user_active_work_flow_record(
        table=TblUserActiveWorkflows,
        request_group_number=request_group_number,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str + ' User active workflow record deleted.')

    if work_flow_section_id == constants.CSVR_SECTION_1:
        csvr_car_seal_objects.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' CSVR Car Seal records deleted.')

        if csvr_master_record.change_type == constants.CAR_SEAL_ADDITION_ID:
            TblCarSealMaster.query.filter(TblCarSealMaster.id.in_(car_seal_id_list)).delete()
            db.session.commit()
            logger.info(controller_rgn_str + ' Car Seal master records deleted.')

        csvr_master_obj.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' CSVR master record deleted.')

    if request_status:
        csvr_master_obj.update(dict(request_status=request_status))
        db.session.commit()

    if section_master_table_id:
        section_master_table_id_value = getattr(csvr_master_record, section_master_table_id)
        csvr_master_obj.update(dict({section_master_table_id: None}))
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table ID set to None in CSVR master table.')

        section_master_table.query.filter_by(id=section_master_table_id_value).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table record deleted.')

    return create_response(delete_success_response=constants.SUCCESS)
