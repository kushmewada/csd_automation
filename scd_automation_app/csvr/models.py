from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblCsvrMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    unique_rgn_id = db.Column(db.String(250), nullable=False)
    change_type = db.Column(db.Integer, nullable=False)
    change_reason = db.Column(db.String(250), nullable=True)
    ocl_review_id = db.Column(db.Integer, db.ForeignKey('tbl_csvr_ocl_master.id'), nullable=True)
    area_btm_approval_id = db.Column(db.Integer, db.ForeignKey('tbl_csvr_area_btm_approval_master.id'), nullable=True)
    request_status = db.Column(db.Integer, nullable=False)
    section_status = db.Column(db.Integer, nullable=False)


class TblCsvrCarSeals(BaseModel):
    request_group_number = db.Column(db.Integer, nullable=False)
    car_seal_id = db.Column(db.Integer, db.ForeignKey('tbl_car_seal_master.id'), nullable=True)


class TblCsvrOclMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    ocl_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblCsvrAreaBtmApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    area_btm_comments = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)
