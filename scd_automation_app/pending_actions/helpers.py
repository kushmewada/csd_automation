import constants
from common_packages import db
from scd_automation_app.cod.models import (
    TblCodMaster, TblCodInitialDefeatApprovalMaster, TblCodDefeatExtensionApprovalMaster, TblCodLongTermDefeatMaster
)
from scd_automation_app.csvr.helpers import mark_car_seals_as_deleted
from scd_automation_app.csvr.models import TblCsvrMaster, TblCsvrOclMaster, TblCsvrAreaBtmApprovalMaster
from scd_automation_app.pmdr.models import (
    TblPmdrMaster,
    TblPmdrInitialDefermentMaster,
    TblPmdrAreaBtmApprovalMaster,
    TblPmdrDefermentExtensionMaster,
    TblPmdrOperationsGmReapprovalMaster
)
from scd_automation_app.samref.common_helpers import update_section_master_table


def update_request_statuses(request_group_number=None, work_flow_id=None, current_section_id=None, section_status=None):
    if work_flow_id == constants.COD and current_section_id != constants.COD_SECTION_1 \
            and section_status != constants.RECYCLED:
        cod_master_obj = TblCodMaster.query.filter_by(request_group_number=request_group_number)
        cod_master_record = cod_master_obj.first()

        if current_section_id == constants.COD_SECTION_2:
            section_id = constants.KEY_DICT['INITIAL_DEFEAT_APPROVAL_ID']
            section_master_table = TblCodInitialDefeatApprovalMaster
            request_status = constants.COD_SECTION_2
        elif current_section_id == constants.COD_SECTION_3:
            section_id = constants.KEY_DICT['DEFEAT_EXTENSION_APPROVAL_ID']
            section_master_table = TblCodDefeatExtensionApprovalMaster
            request_status = constants.COD_SECTION_3
        elif current_section_id == constants.COD_SECTION_5:
            section_id = constants.KEY_DICT['LONG_TERM_DEFEAT_ID']
            section_master_table = TblCodLongTermDefeatMaster
            request_status = constants.COD_SECTION_5

        if section_status == constants.CLOSED:
            defeat_status = constants.DEFEAT_STATUS_DICT['DEFEAT_CLOSED']
            request_status = constants.CLOSED
        elif section_status in [constants.ENDORSED, constants.APPROVED]:
            defeat_status = constants.DEFEAT_STATUS_DICT['DEFEAT_APPROVED']

        cod_master_obj.update(dict({'defeat_status': defeat_status, 'request_status': request_status}))
        db.session.commit()

        update_section_master_table(
            section_master_table=section_master_table,
            request_master_record=cod_master_record,
            section_id=section_id,
            section_status=section_status
        )

    if work_flow_id == constants.PMDR and current_section_id in \
            [constants.PMDR_SECTION_2, constants.PMDR_SECTION_3, constants.PMDR_SECTION_4, constants.PMDR_SECTION_5]:
        pmdr_master_obj = TblPmdrMaster.query.filter_by(request_group_number=request_group_number)
        pmdr_master_record = pmdr_master_obj.first()

        if current_section_id == constants.PMDR_SECTION_2:
            section_id = constants.KEY_DICT['INITIAL_DEFERMENT_ID']
            section_master_table = TblPmdrInitialDefermentMaster

        elif current_section_id == constants.PMDR_SECTION_3:
            section_id = constants.KEY_DICT['INITIAL_DEFERMENT_ID']
            section_master_table = TblPmdrAreaBtmApprovalMaster

        elif current_section_id == constants.PMDR_SECTION_4:
            section_id = constants.KEY_DICT['DEFERMENT_EXTENSION_ID']
            section_master_table = TblPmdrDefermentExtensionMaster

        elif current_section_id == constants.PMDR_SECTION_5:
            section_id = constants.KEY_DICT['OPERATIONS_GM_REAPPROVAL_ID']
            section_master_table = TblPmdrOperationsGmReapprovalMaster

        if section_status == constants.CLOSED:
            request_status = constants.CLOSED
            deferment_status = constants.PMDR_REQUEST_STATUS_DICT['CLOSED']
        else:
            request_status = current_section_id
            deferment_status = constants.PMDR_REQUEST_STATUS_DICT['APPROVED']

        pmdr_master_obj.update(dict({'deferment_status': deferment_status, 'request_status': request_status}))
        db.session.commit()

        update_section_master_table(
            section_master_table=section_master_table,
            request_master_record=pmdr_master_record,
            section_id=section_id,
            section_status=section_status
        )

    if work_flow_id == constants.CSVR and current_section_id != constants.CSVR_SECTION_1:
        csvr_master_obj = TblCsvrMaster.query.filter_by(request_group_number=request_group_number)
        csvr_master_record = csvr_master_obj.first()

        if current_section_id == constants.CSVR_SECTION_2:
            section_id = constants.KEY_DICT['OCL_REVIEW_ID']
            section_master_table = TblCsvrOclMaster
        elif current_section_id == constants.CSVR_SECTION_3:
            section_id = constants.KEY_DICT['AREA_BTM_APPROVAL_ID']
            section_master_table = TblCsvrAreaBtmApprovalMaster

        if section_status in [constants.CLOSED, constants.APPROVED]:
            request_status = constants.CLOSED
            if csvr_master_record.change_type == constants.CAR_SEAL_DELETION_ID:
                mark_car_seals_as_deleted(request_group_number=request_group_number)
        else:
            request_status = current_section_id
        csvr_master_obj.update(dict({'request_status': request_status}))
        db.session.commit()

        update_section_master_table(
            section_master_table=section_master_table,
            request_master_record=csvr_master_record,
            section_id=section_id,
            section_status=section_status
        )
