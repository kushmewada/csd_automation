from flask import Blueprint, request

import constants
from logging_module import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import get_pending_action_records, log_pending_action

blueprint_name = constants.BLUE_PRINT_DICT['PENDING_ACTIONS']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['PENDING_ACTIONS']['URL_PREFIX']
pending_actions_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['PENDING_ACTION_LOG'])


api_get_pending_actions = constants.API_DICT['PENDING_ACTIONS']['GET_PENDING_ACTIONS']
api_create_pending_action = constants.API_DICT['PENDING_ACTIONS']['CREATE_PENDING_ACTION']


@pending_actions_bp.route(api_get_pending_actions, methods=[constants.GET])
def get_pending_actions():
    return base_api(request=request, api_name=api_get_pending_actions, api=get_pending_action_records, logger=logger)


@pending_actions_bp.route(api_create_pending_action, methods=[constants.POST])
def create_pending_action():
    return base_api(request=request, api_name=api_create_pending_action, api=log_pending_action, logger=logger)
