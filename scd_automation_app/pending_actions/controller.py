import constants
from scd_automation_app.cod.models import TblCodMaster
from scd_automation_app.csvr.models import TblCsvrMaster
from scd_automation_app.pmdr.models import TblPmdrMaster
from scd_automation_app.samref.common_helpers import create_response, create_pending_action_record, format_list_api_data
from scd_automation_app.scr.models import TblScrMaster
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblPendingActions, TblUserActiveWorkflows
from .helpers import update_request_statuses


def get_pending_action_records(request_data=None, logger=None):
    pending_action_list = list()

    pending_action_records = TblPendingActions.query.filter_by(
        assigned_to_role_id=int(request_data['role_id']),
        assigned_to=int(request_data['user_id']),
        status=constants.PENDING_ACTION
    ).all()

    for pending_action in pending_action_records:
        work_flow_id = pending_action.pending_action_id - (pending_action.pending_action_id % 10)

        if work_flow_id == 10:
            table = TblScrMaster
        elif work_flow_id == 20:
            table = TblCodMaster
        elif work_flow_id == 30:
            table = TblPmdrMaster
        elif work_flow_id == 40:
            table = TblCsvrMaster

        master_record = table.query.filter_by(request_group_number=pending_action.request_group_number).first()
        pending_action_list.append({
            'unique_rgn_id': master_record.unique_rgn_id,
            'request_group_number': pending_action.request_group_number,
            'work_flow_id': work_flow_id,
            'work_flow': constants.WORK_FLOW_DICT[work_flow_id],
            'work_flow_section_id': pending_action.pending_action_id,
            'work_flow_section': constants.WORK_FLOW_DICT[pending_action.pending_action_id],
            'pending_action_id': pending_action.pending_action_id,
            'request_type':
                getattr(master_record, 'change_type') if work_flow_id in [constants.SCR, constants.CSVR] else None,
            'created_time': pending_action.created_time
        })

    data = format_list_api_data(
        records=pending_action_list,
        is_dict=False,
        search_key=constants.KEY_DICT['WORK_FLOW_SECTION'],
        search_text=request_data.get('search_text', None),
        list_name=constants.KEY_DICT['PENDING_ACTION_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=constants.SUCCESS, data=data)


def log_pending_action(request_data=None, logger=None):
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None
    create_pending_action_record(
        request_data=request_data,
        user_active_work_flow_table=TblUserActiveWorkflows,
        pending_action_table=TblPendingActions,
        api_call=True,
        logger=logger
    )
    update_request_statuses(
        request_group_number=int(request_data['request_group_number']),
        work_flow_id=int(request_data['work_flow_id']),
        current_section_id=int(request_data['current_section_id']),
        section_status=section_status
    )
    return create_response(post_success_response=constants.SUCCESS)
