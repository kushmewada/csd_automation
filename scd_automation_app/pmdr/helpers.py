from json import loads

import constants
from common_packages import db
from scd_automation_app.common.models import TblFunctionLocationMaster
from scd_automation_app.plant.models import TblDefermentReasonMaster
from scd_automation_app.samref.common_functions import get_time_interval, convert_time_to_timestamp
from .models import TblPmdrMaster, TblWorkOrderMaster


def update_pmdr_record(
        request_group_number=None,
        request_data=None,
        request_status=None,
        deferment_status=None,
        controller_rgn_str=None,
        logger=None,
        **kwargs
):

    pmdr_master_params = {'modified_time': convert_time_to_timestamp(), 'modified_by': int(request_data['user_id'])}
    pmdr_master_params.update(**kwargs)

    deferment_reason_id = \
        int(request_data['deferment_reason_id']) if request_data.get('deferment_reason_id', None) else None

    if deferment_reason_id:
        deferment_reason = TblDefermentReasonMaster.query.filter_by(id=deferment_reason_id).first()
        pmdr_master_params.update({
            'deferment_reason_id': deferment_reason_id, 'deferment_reason': deferment_reason.deferment_reason
        })

    if request_status:
        pmdr_master_params.update({'request_status': request_status})

    if deferment_status:
        pmdr_master_params.update({'deferment_status': deferment_status})

    TblPmdrMaster.query.filter_by(request_group_number=request_group_number).update(dict(pmdr_master_params))
    db.session.commit()
    logger.info(controller_rgn_str + ' - PMDR record updated.')


def update_fl_work_order_data(request_data=None, controller_rgn_str=None, logger=None):
    fl_work_order_data = loads(request_data['fl_work_order_data'])

    for record in fl_work_order_data:
        TblWorkOrderMaster.query.filter_by(id=int(record['work_order_id'])).update(dict(
            recommended_plan=record['maintenance_plan']
        ))

        TblFunctionLocationMaster.query.filter_by(id=int(record['fl_id'])).update(dict(
            work_center_id=int(record['work_center_id']), recommended_pm_interval=int(record['testing_interval'])
        ))
    logger.info(controller_rgn_str + ' - Function Location and Work Order data updated.')


def calculate_due_status(planned_date=None):
    if planned_date is None:
        return constants.NULL_BASIC_START_DATE
    datetime_obj = get_time_interval(start_timestamp=planned_date)
    overdue_days = 90 - datetime_obj.days

    if overdue_days <= 0:
        return constants.OVER_DUE_MESSAGE_DICT['OVERDUE']
    elif 0 < overdue_days <= 7:
        return constants.OVER_DUE_MESSAGE_DICT['ONE_WEEK']
    elif 7 < overdue_days <= 14:
        return constants.OVER_DUE_MESSAGE_DICT['TWO_WEEKS']
    elif 14 < overdue_days <= 21:
        return constants.OVER_DUE_MESSAGE_DICT['THREE_WEEKS']
    elif 21 < overdue_days <= 28:
        return constants.OVER_DUE_MESSAGE_DICT['FOUR_WEEKS']
    elif 28 < overdue_days <= 59:
        return constants.OVER_DUE_MESSAGE_DICT['TWO_MONTHS']
    elif 59 < overdue_days <= 90:
        return constants.OVER_DUE_MESSAGE_DICT['THREE_MONTHS']
    else:
        return constants.OVER_DUE_MESSAGE_DICT['NOT_OVERDUE']
