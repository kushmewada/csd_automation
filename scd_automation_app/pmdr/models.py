from common_packages import db
from scd_automation_app.samref.base_model import BaseModel, CreationTimestampMixin, ModificationTimestampMixin


class TblPmdrMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    request_group_number = db.Column(db.Integer, nullable=False)
    unique_rgn_id = db.Column(db.String(250), nullable=False)
    request_type = db.Column(db.Integer, nullable=False)
    deferment_reason_id = db.Column(db.Integer, db.ForeignKey('tbl_deferment_reason_master.id'), nullable=True)
    deferment_reason = db.Column(db.String(250), nullable=False)
    initial_deferment_id = db.Column(db.Integer, db.ForeignKey('tbl_pmdr_initial_deferment_master.id'), nullable=True)
    area_btm_approval_id = db.Column(db.Integer, db.ForeignKey('tbl_pmdr_area_btm_approval_master.id'), nullable=True)
    deferment_extension_id = \
        db.Column(db.Integer, db.ForeignKey('tbl_pmdr_deferment_extension_master.id'), nullable=True)
    operations_gm_reapproval_id = \
        db.Column(db.Integer, db.ForeignKey('tbl_pmdr_operations_gm_reapproval_master.id'), nullable=True)
    job_scheduler_id = db.Column(db.Integer, db.ForeignKey('tbl_pmdr_job_scheduler_master.id'), nullable=True)
    request_status = db.Column(db.Integer, nullable=False)
    deferment_status = db.Column(db.String(250), nullable=False)
    section_status = db.Column(db.Integer, nullable=False)


class TblPmdrWorkOrders(BaseModel):
    request_group_number = db.Column(db.Integer, nullable=False)
    work_order_id = db.Column(db.Integer, db.ForeignKey('tbl_work_order_master.id'), nullable=True)


class TblPmdrInitialDefermentMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    proposed_deferment_date = db.Column(db.Numeric(15), nullable=True)
    ra_required = db.Column(db.Integer, nullable=False)
    remarks = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblPmdrAreaBtmApprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    risk_mitigated = db.Column(db.Integer, nullable=False)
    remarks = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblPmdrDefermentExtensionMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    extension_mitigated = db.Column(db.Integer, nullable=False)
    remarks = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblPmdrOperationsGmReapprovalMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    remarks = db.Column(db.String(250), nullable=True)
    recycle_comments = db.Column(db.String(250), nullable=True)
    section_status = db.Column(db.Integer, nullable=True)


class TblPmdrJobSchedulerMaster(BaseModel, CreationTimestampMixin, ModificationTimestampMixin):
    remarks = db.Column(db.String(250), nullable=True)


class TblWorkOrderMaster(BaseModel):
    work_order = db.Column(db.String(250), nullable=False)
    function_location_id = db.Column(db.Integer, db.ForeignKey('tbl_function_location_master.id'), nullable=True)
    work_order_type = db.Column(db.String(250), nullable=True)
    work_order_description = db.Column(db.String(250), nullable=True)
    recommended_plan = db.Column(db.String(250), nullable=True)
    work_order_plan_date = db.Column(db.Numeric(15), nullable=True)
    scheduled_start_date = db.Column(db.Numeric(15), nullable=True)
    actual_order_finish_date = db.Column(db.Numeric(15), nullable=True)
    user_status = db.Column(db.String(250), nullable=True)
    system_status = db.Column(db.String(250), nullable=True)
