from json import loads

import constants
from common_packages import db
from scd_automation_app.attachments.models import TblDocumentMaster, TblWorkFlowDocuments
from scd_automation_app.common.models import TblAreaMaster, TblUnitMaster, TblFunctionLocationMaster, TblCategoryMaster
from scd_automation_app.groups.models import TblWorkCenterMaster, TblPlannerGroupMaster
from scd_automation_app.plant.models import TblDefermentReasonMaster
from scd_automation_app.samref.common_functions import (
    convert_time_to_timestamp, convert_date_to_timestamp, convert_timestamp_to_date, get_list_from_comma_separated_str
)
from scd_automation_app.samref.common_helpers import (
    check_request_permission_for_section_1,
    check_request_permissions,
    get_sort_data,
    format_list_api_data,
    get_unique_rgn_data,
    get_controller_rgn_str,
    get_user_data_from_request,
    get_attachment_list_for_request,
    create_or_update_user_active_work_flow,
    create_response,
    delete_section_documents,
    delete_user_active_work_flow_record,
    create_pending_action_record
)
from scd_automation_app.settings.models import TblSettingsMaster
from scd_automation_app.users.models import TblUserMaster, TblUserActiveWorkflows, TblPendingActions
from .helpers import update_pmdr_record, update_fl_work_order_data, calculate_due_status
from .models import (
    TblPmdrMaster,
    TblPmdrWorkOrders,
    TblPmdrInitialDefermentMaster,
    TblPmdrAreaBtmApprovalMaster,
    TblPmdrDefermentExtensionMaster,
    TblPmdrOperationsGmReapprovalMaster,
    TblPmdrJobSchedulerMaster,
    TblWorkOrderMaster
)


def get_pmdr_request_records(request_data=None, logger=None):
    pmdr_data_dict = dict()
    filter_fields = list()
    search_text = request_data.get('search_text', None)

    area_id = request_data.get('area_id', None)
    if area_id:
        filter_fields.append(TblAreaMaster.id == int(area_id))

    deferment_reason_id = request_data.get('deferment_reason_id', None)
    if deferment_reason_id:
        filter_fields.append(TblDefermentReasonMaster.id == int(deferment_reason_id))

    category_id = request_data.get('category_id', None)
    if category_id:
        filter_fields.append(TblCategoryMaster.id == int(category_id))

    request_deferment_status = request_data.get('request_deferment_status', None)
    if request_deferment_status:
        request_deferment_status = int(request_deferment_status)
        if request_deferment_status == constants.APPROVED_DEFERMENTS:
            filter_fields.append(TblPmdrMaster.area_btm_approval_id != None)
            filter_fields.append(TblPmdrMaster.deferment_extension_id == None)
            filter_fields.append(TblPmdrMaster.operations_gm_reapproval_id == None)
        elif request_deferment_status == constants.OVERDUE_DEFERMENTS:
            filter_fields.append(TblPmdrMaster.deferment_extension_id != None)
            filter_fields.append(TblPmdrMaster.operations_gm_reapproval_id == None)
        elif request_deferment_status == constants.DEFERRED_DEFERMENTS:
            filter_fields.append(TblPmdrMaster.operations_gm_reapproval_id != None)
        else:
            data = format_list_api_data(
                records=dict(),
                list_name=constants.KEY_DICT['PMDR_REQUEST_LIST'],
                search_key=constants.KEY_DICT['UNIQUE_RGN_ID'],
                search_text=search_text, page=int(request_data['page']),
                settings_table=TblSettingsMaster
            )
            return create_response(get_success_response=True, data=data)

    sort_id = int(request_data['sort_id']) if request_data.get('sort_id', None) else constants.REQUEST_ID_ASCENDING
    sort_obj = get_sort_data(sort_id=sort_id, table=TblPmdrMaster)

    records = TblPmdrMaster.query.join(
        TblPmdrWorkOrders, TblPmdrWorkOrders.request_group_number == TblPmdrMaster.request_group_number
    ).join(
        TblWorkOrderMaster, TblWorkOrderMaster.id == TblPmdrWorkOrders.work_order_id
    ).join(
        TblDefermentReasonMaster, TblDefermentReasonMaster.id == TblPmdrMaster.deferment_reason_id
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id
    ).outerjoin(
        TblPlannerGroupMaster, TblPlannerGroupMaster.id == TblFunctionLocationMaster.planner_group_id
    ).join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblCategoryMaster, TblCategoryMaster.id == TblWorkCenterMaster.category_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblPmdrWorkOrders.work_order_id,
        TblWorkCenterMaster.category_id,
        TblPmdrMaster.request_group_number,
        TblPmdrMaster.unique_rgn_id,
        TblPmdrMaster.deferment_status,
        TblPmdrMaster.deferment_reason_id,
        TblPmdrMaster.created_by,
        TblPmdrMaster.created_time,
        TblWorkOrderMaster.work_order,
        TblWorkOrderMaster.function_location_id,
        TblWorkOrderMaster.work_order_description,
        TblFunctionLocationMaster.unit_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.planner_group_id,
        TblCategoryMaster.category,
        TblPlannerGroupMaster.planner_group_name,
        TblDefermentReasonMaster.deferment_reason
    ).filter(
        *filter_fields
    ).order_by(
        sort_obj
    ).all()

    for record in records:
        request_group_number = record.request_group_number

        if request_group_number not in pmdr_data_dict:
            pending_action = TblPendingActions.query.join(
                TblUserMaster, TblUserMaster.id == TblPendingActions.assigned_to
            ).filter(
                TblPendingActions.status == constants.PENDING_ACTION,
            ).add_columns(
                TblUserMaster.name,
                TblPendingActions.assigned_to
            ).first()

            pmdr_data_dict[record.unique_rgn_id] = {
                'id': request_group_number,
                'unique_rgn_id': record.unique_rgn_id,
                'area': {'id': record.area_id, 'name': record.area},
                'unit': {'id': record.unit_id, 'name': record.unit_number},
                'function_locations': [{
                    'id': record.function_location_id,
                    'name': record.fl_name,
                    'critical_safeguard': record.critical_safeguard,
                    'category': {'id': record.category_id, 'name': record.category},
                    'planner_group': {'id': record.planner_group_id, 'name': record.planner_group_name},
                    'work_order': {
                        'id': record.work_order_id,
                        'name': record.work_order,
                        'description': record.work_order_description
                    }
                }],
                'deferment_reason': {'id': record.deferment_reason_id, 'name': record.deferment_reason},
                'request_status': record.deferment_status,
                'currently_with':
                    {'id': pending_action.assigned_to, 'name': pending_action.name} if pending_action else None,
                'created_time': convert_timestamp_to_date(timestamp=record.created_time)
            }
        else:
            pmdr_data_dict[record.unique_rgn_id]['function_locations'].append({
                'id': record.function_location_id,
                'name': record.fl_name,
                'critical_safeguard': record.critical_safeguard,
                'category': {'id': record.category_id, 'name': record.category},
                'planner_group': {'id': record.planner_group_id, 'name': record.planner_group_name},
                'work_order': {
                    'id': record.work_order_id,
                    'name': record.work_order,
                    'description': record.work_order_description
                }
            })

    data = format_list_api_data(
        records=pmdr_data_dict,
        list_name=constants.KEY_DICT['PMDR_REQUEST_LIST'],
        search_key=constants.KEY_DICT['UNIQUE_RGN_ID'],
        search_text=search_text,
        page=int(request_data['page']) if request_data.get('page', None) else None,
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_unit_work_order_records(request_data=None, logger=None):
    filter_fields = list()
    unit_id = int(request_data['unit_id']) if request_data.get('unit_id', None) else None
    work_order_type = int(request_data['work_order_type']) if request_data.get('work_order_type', None) else None
    search_text = request_data.get('search_text', None)

    if unit_id is None:
        return create_response(status=constants.FAILURE, error_message=constants.UNIT_NOT_SELECTED)

    if search_text:
        filter_fields.append(TblWorkOrderMaster.work_order.contains(search_text))

    if work_order_type is None:
        fl_type_list = list(constants.FL_TYPE_DICT.keys())
    elif work_order_type == 1:
        fl_type_list = [constants.SCD]
    else:
        fl_type_list = [constants.DEMOLISHED_FL, constants.DELETED_FL, constants.NON_SCD]

    filter_fields.extend([
        TblWorkOrderMaster.actual_order_finish_date == None,
        TblWorkOrderMaster.system_status.contains(constants.SYSTEM_STATUS_CNF),
        TblFunctionLocationMaster.fl_type.in_(fl_type_list),
        TblUnitMaster.id == unit_id
    ])

    work_order_records = TblWorkOrderMaster.query.join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).add_columns(
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.system_status,
        TblWorkOrderMaster.id,
        TblWorkOrderMaster.work_order,
        TblWorkOrderMaster.function_location_id,
        TblWorkOrderMaster.work_order_plan_date,
        TblWorkOrderMaster.scheduled_start_date,
        TblWorkOrderMaster.actual_order_finish_date
    ).filter(
        *filter_fields
    ).all()

    active_request_fl_id_list = list()
    if work_order_type:
        active_request_fl_records = TblPmdrMaster.query.join(
            TblPmdrWorkOrders, TblPmdrWorkOrders.request_group_number == TblPmdrMaster.request_group_number
        ).join(
            TblWorkOrderMaster, TblWorkOrderMaster.id == TblPmdrWorkOrders.work_order_id
        ).join(
            TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id,
        ).filter(
            TblPmdrMaster.request_status.not_in([constants.REJECTED, constants.CLOSED])
        ).add_columns(
            TblWorkOrderMaster.function_location_id
        ).all()
        active_request_fl_id_list = [record.function_location_id for record in active_request_fl_records]

    work_order_list = [
        {
            'id': record.id,
            'name': record.work_order,
            'fl_id': record.function_location_id,
            'fl_name': record.fl_name,
            'fl_description': record.fl_description,
            'critical_safeguard': record.critical_safeguard,
            'system_status': record.system_status,
            'work_order_plan_date': convert_timestamp_to_date(timestamp=record.work_order_plan_date),
            'scheduled_start_date': convert_timestamp_to_date(timestamp=record.scheduled_start_date),
            'actual_order_finish_date': convert_timestamp_to_date(timestamp=record.actual_order_finish_date),
            'due_status': calculate_due_status(planned_date=record.work_order_plan_date)
        } for record in work_order_records if record.function_location_id not in active_request_fl_id_list
    ]

    data = format_list_api_data(
        records=work_order_list,
        is_dict=False,
        list_name=constants.KEY_DICT['WORK_ORDER_LIST'],
        page=int(request_data['page']),
        settings_table=TblSettingsMaster
    )
    return create_response(get_success_response=True, data=data)


def get_pmdr_request_data(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    section_id_list = get_list_from_comma_separated_str(comma_separated_str=request_data['section_ids'])

    pmdr_work_orders = TblPmdrWorkOrders.query.join(
        TblWorkOrderMaster, TblWorkOrderMaster.id == TblPmdrWorkOrders.work_order_id
    ).join(
        TblFunctionLocationMaster, TblFunctionLocationMaster.id == TblWorkOrderMaster.function_location_id
    ).join(
        TblWorkCenterMaster, TblWorkCenterMaster.id == TblFunctionLocationMaster.work_center_id
    ).join(
        TblUnitMaster, TblUnitMaster.id == TblFunctionLocationMaster.unit_id
    ).join(
        TblAreaMaster, TblAreaMaster.id == TblUnitMaster.area_id
    ).add_columns(
        TblAreaMaster.area,
        TblUnitMaster.area_id,
        TblUnitMaster.unit_number,
        TblFunctionLocationMaster.unit_id,
        TblFunctionLocationMaster.fl_name,
        TblFunctionLocationMaster.fl_description,
        TblFunctionLocationMaster.critical_safeguard,
        TblFunctionLocationMaster.system_status,
        TblFunctionLocationMaster.work_center_id,
        TblFunctionLocationMaster.recommended_pm_interval,
        TblWorkOrderMaster.function_location_id,
        TblWorkOrderMaster.work_order,
        TblWorkOrderMaster.work_order_type,
        TblWorkOrderMaster.recommended_plan,
        TblWorkOrderMaster.work_order_plan_date,
        TblWorkOrderMaster.scheduled_start_date,
        TblWorkOrderMaster.actual_order_finish_date,
        TblWorkCenterMaster.work_center_name,
        TblPmdrWorkOrders.work_order_id
    ).filter(
        TblPmdrWorkOrders.request_group_number == request_group_number
    ).all()

    fl_list = [
        {
            'work_order_id': record.work_order_id,
            'work_order_name': record.work_order,
            'work_order_type': record.work_order_type,
            'fl_id': record.function_location_id,
            'fl_name': record.fl_name,
            'fl_description': record.fl_description,
            'critical_safeguard': record.critical_safeguard,
            'main_work_center': {'id': record.work_center_id, 'name': record.work_center_name},
            'system_status': record.system_status,
            'due_status': calculate_due_status(planned_date=record.work_order_plan_date),
            'maintenance_plan': record.recommended_plan,
            'testing_interval': record.recommended_pm_interval,
            'work_order_plan_date': convert_timestamp_to_date(timestamp=record.work_order_plan_date),
            'scheduled_start_date': convert_timestamp_to_date(timestamp=record.scheduled_start_date),
            'actual_order_finish_date': convert_timestamp_to_date(timestamp=record.actual_order_finish_date)
        }
        for record in pmdr_work_orders
    ]

    attachment_list = get_attachment_list_for_request(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        work_flow_section_id=constants.PMDR_SECTION_1,
        document_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments
    )

    pmdr_master_record = TblPmdrMaster.query.filter_by(request_group_number=request_group_number).first()
    deferment_reason = TblDefermentReasonMaster.query.filter_by(id=pmdr_master_record.deferment_reason_id).first()
    user_data = get_user_data_from_request(request_record=pmdr_master_record, user_table=TblUserMaster)

    section_1_data_dict = {
        'id': pmdr_master_record.id,
        'section_data': {
            'area': {
                'id': pmdr_work_orders[0].area_id,
                'name': pmdr_work_orders[0].area
            },
            'unit': {
                'id': pmdr_work_orders[0].unit_id,
                'name': pmdr_work_orders[0].unit_number
            },
            'work_order_type': pmdr_master_record.request_type,
            'deferment_reason': {
                'id': deferment_reason.id,
                'name': deferment_reason.deferment_reason,
                'original_defeat_reason': pmdr_master_record.deferment_reason
            },
            'fl_list': fl_list,
            'attachment_list': attachment_list
        },
        'section_status': pmdr_master_record.section_status
    }
    section_1_data_dict.update(user_data)

    pmdr_request_data_dict = {
        'request_group_number': request_group_number,
        'unique_rgn_id': pmdr_master_record.unique_rgn_id,
        'request_type': pmdr_master_record.request_type,
        'deferment_status': pmdr_master_record.deferment_status,
        'section_1': section_1_data_dict
    }

    initial_deferment_id = pmdr_master_record.initial_deferment_id
    if constants.PMDR_SECTION_2 in section_id_list and initial_deferment_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.PMDR,
            work_flow_section_id=constants.PMDR_SECTION_2,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        initial_deferment = TblPmdrInitialDefermentMaster.query.filter_by(id=initial_deferment_id).first()
        user_data = get_user_data_from_request(request_record=initial_deferment, user_table=TblUserMaster)

        section_2_data_dict = {
            'id': initial_deferment.id,
            'section_data': {
                'proposed_deferment_date':
                    convert_timestamp_to_date(timestamp=initial_deferment.proposed_deferment_date),
                'ra_required': initial_deferment.ra_required,
                'remarks': initial_deferment.remarks,
                'attachment_list': attachment_list
            },
            'section_status': initial_deferment.section_status,
        }
        section_2_data_dict.update(user_data)
        pmdr_request_data_dict['section_2'] = section_2_data_dict

    area_btm_approval_id = pmdr_master_record.area_btm_approval_id
    if constants.PMDR_SECTION_3 in section_id_list and area_btm_approval_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.PMDR,
            work_flow_section_id=constants.PMDR_SECTION_3,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        area_btm_approval = TblPmdrAreaBtmApprovalMaster.query.filter_by(id=area_btm_approval_id).first()
        user_data = get_user_data_from_request(request_record=area_btm_approval, user_table=TblUserMaster)
        section_3_data_dict = {
            'id': area_btm_approval.id,
            'section_data': {
                'risk_mitigated': area_btm_approval.risk_mitigated,
                'remarks': area_btm_approval.remarks,
                'attachment_list': attachment_list
            },
            'section_status': area_btm_approval.section_status
        }
        section_3_data_dict.update(user_data)
        pmdr_request_data_dict['section_3'] = section_3_data_dict

    deferment_extension_id = pmdr_master_record.deferment_extension_id
    if constants.PMDR_SECTION_4 in section_id_list and deferment_extension_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.PMDR,
            work_flow_section_id=constants.PMDR_SECTION_4,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        deferment_extension = TblPmdrDefermentExtensionMaster.query.filter_by(id=deferment_extension_id).first()
        user_data = get_user_data_from_request(request_record=deferment_extension, user_table=TblUserMaster)

        section_4_data_dict = {
            'id': deferment_extension.id,
            'section_data': {
                'extension_mitigated': deferment_extension.extension_mitigated,
                'remarks': deferment_extension.remarks,
                'recycle_comments': deferment_extension.recycle_comments,
                'attachment_list': attachment_list
            },
            'section_status': deferment_extension.section_status
        }
        section_4_data_dict.update(user_data)
        pmdr_request_data_dict['section_4'] = section_4_data_dict

    operations_gm_reapproval_id = pmdr_master_record.operations_gm_reapproval_id
    if constants.PMDR_SECTION_5 in section_id_list and operations_gm_reapproval_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.PMDR,
            work_flow_section_id=constants.PMDR_SECTION_5,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        operations_gm_reapproval = \
            TblPmdrOperationsGmReapprovalMaster.query.filter_by(id=operations_gm_reapproval_id).first()
        user_data = get_user_data_from_request(request_record=operations_gm_reapproval, user_table=TblUserMaster)

        section_5_data_dict = {
            'id': operations_gm_reapproval.id,
            'section_data': {
                'remarks': operations_gm_reapproval.remarks,
                'recycle_comments': operations_gm_reapproval.recycle_comments,
                'attachment_list': attachment_list
            },
            'section_status': operations_gm_reapproval.section_status,
        }
        section_5_data_dict.update(user_data)
        pmdr_request_data_dict['section_5'] = section_5_data_dict

    job_scheduler_id = pmdr_master_record.job_scheduler_id
    if constants.PMDR_SECTION_6 in section_id_list and job_scheduler_id:
        attachment_list = get_attachment_list_for_request(
            request_group_number=request_group_number,
            work_flow_id=constants.PMDR,
            work_flow_section_id=constants.PMDR_SECTION_6,
            document_table=TblDocumentMaster,
            work_flow_document_table=TblWorkFlowDocuments
        )

        job_scheduler = TblPmdrJobSchedulerMaster.query.filter_by(id=job_scheduler_id).first()
        user_data = get_user_data_from_request(request_record=job_scheduler, user_table=TblUserMaster)

        section_6_data_dict = {
            'id': job_scheduler.id,
            'section_data': {'remarks': job_scheduler.remarks, 'attachment_list': attachment_list},
            'section_status': constants.CONFIRMED,
            'created_time': convert_timestamp_to_date(timestamp=job_scheduler.created_time)
        }
        section_6_data_dict.update(user_data)
        pmdr_request_data_dict['section_6'] = section_6_data_dict

    return create_response(get_success_response=True, data=pmdr_request_data_dict)


def create_pmdr_record(request_data=None, logger=None):
    status, error_message = check_request_permission_for_section_1(
        request_data=request_data,
        data_key=constants.KEY_DICT['FL_WORK_ORDER_DATA'],
        id_key=constants.KEY_DICT['WORK_ORDER_ID'],
        id_field=constants.KEY_DICT['WORK_ORDER_ID'],
        section_id=constants.PMDR_SECTION_1,
        master_table=TblPmdrMaster,
        id_table=TblPmdrWorkOrders,
        pending_action_table=TblPendingActions,
        request_type=constants.POST
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=error_message)

    user_id = int(request_data['user_id'])
    request_group_number, unique_rgn_id, controller_rgn_str = get_unique_rgn_data(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_1, table=TblPmdrMaster
    )
    logger.info(controller_rgn_str)

    deferment_reason_id = int(request_data['deferment_reason_id'])
    deferment_reason = TblDefermentReasonMaster.query.filter_by(id=deferment_reason_id).first()
    pmdr_record = TblPmdrMaster(
        request_group_number=request_group_number,
        unique_rgn_id=unique_rgn_id,
        request_type=int(request_data['work_order_type']),
        deferment_reason_id=deferment_reason_id,
        deferment_reason=deferment_reason.deferment_reason,
        request_status=constants.PMDR_SECTION_1,
        section_status=int(request_data['section_status']),
        deferment_status=constants.PMDR_REQUEST_STATUS_DICT['NEW'],
        created_time=convert_time_to_timestamp(),
        created_by=user_id
    )
    db.session.add(pmdr_record)
    db.session.commit()
    logger.info(controller_rgn_str + ' - New PMDR request created.')

    pmdr_work_orders_records = [
        TblPmdrWorkOrders(request_group_number=request_group_number, work_order_id=record['work_order_id'])
        for record in loads(request_data['fl_work_order_data'])
    ]
    db.session.add_all(pmdr_work_orders_records)
    db.session.commit()
    logger.info(controller_rgn_str + ' - Work Orders added.')

    update_fl_work_order_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_1,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )
    return create_response(post_success_response=True,
                           data={'request_group_number': request_group_number, 'unit_id': int(request_data['unit_id'])})


def create_initial_deferment_record(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_master_table_key=constants.KEY_DICT['INITIAL_DEFERMENT_ID'],
        section_id=constants.PMDR_SECTION_2,
        master_table=TblPmdrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=constants.POST
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_2
    )
    logger.info(controller_rgn_str)

    initial_deferment_params = {
        'ra_required': int(request_data['ra_required']),
        'created_time': convert_time_to_timestamp(),
        'created_by': user_id
    }
    if section_status:
        initial_deferment_params.update({'section_status': section_status})
    if constants.KEY_DICT['PROPOSED_DEFERMENT_DATE'] in request_data:
        initial_deferment_params.update({
            'proposed_deferment_date': convert_date_to_timestamp(date=request_data['proposed_deferment_date'])
        })
    if constants.KEY_DICT['REMARKS'] in request_data:
        initial_deferment_params.update({'remarks': request_data['remarks'].capitalize()})

    initial_deferment = TblPmdrInitialDefermentMaster(**initial_deferment_params)
    db.session.add(initial_deferment)
    db.session.commit()
    logger.info(controller_rgn_str + ' - New initial deferment created.')

    pmdr_master_params = {
        'request_group_number': request_group_number,
        'request_data': request_data,
        'initial_deferment_id': initial_deferment.id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if section_status == constants.REJECTED:
        pmdr_master_params.update({
            'request_status': constants.CLOSED, 'deferment_status': constants.PMDR_REQUEST_STATUS_DICT['REJECTED']
        })

    update_pmdr_record(**pmdr_master_params)

    update_fl_work_order_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_2,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.REJECTED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.PMDR,
            'current_section_id': constants.PMDR_SECTION_2,
            'completed_action_id_list': str(constants.PMDR_SECTION_2)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    return create_response(post_success_response=True)


def area_btm_approval_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_master_table_key=constants.KEY_DICT['AREA_BTM_APPROVAL_ID'],
        section_id=constants.PMDR_SECTION_3,
        master_table=TblPmdrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_2
    )
    logger.info(controller_rgn_str + ', request_type - ' + str(request_type))

    btm_approval_params = {'risk_mitigated': int(request_data['risk_mitigated'])}
    if section_status:
        btm_approval_params.update({'section_status': section_status})
    if constants.KEY_DICT['REMARKS'] in request_data:
        btm_approval_params.update({'remarks': request_data['remarks'].capitalize()})

    if request_type == constants.POST:
        btm_approval_params.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        btm_approval_record = TblPmdrAreaBtmApprovalMaster(**btm_approval_params)
        db.session.add(btm_approval_record)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New area BTM approval created.')
        area_btm_approval_id = btm_approval_record.id
        return_params = {'post_success_response': True}
    else:
        area_btm_approval_id = int(request_data['area_btm_approval_id'])
        btm_approval_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        TblPmdrAreaBtmApprovalMaster.query.filter_by(id=area_btm_approval_id).update(dict(btm_approval_params))
        logger.info(controller_rgn_str + ' - Area BTM approval updated.')
        return_params = {'patch_success_response': True}

    pmdr_master_params = {
        'request_group_number': request_group_number,
        'request_data': request_data,
        'area_btm_approval_id': area_btm_approval_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if request_data.get('section_status', None):
        pmdr_master_params.update({
            'section_status': int(request_data['section_status']),
            'deferment_status': constants.PMDR_REQUEST_STATUS_DICT['REJECTED']
        })

    update_pmdr_record(**pmdr_master_params)

    update_fl_work_order_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_3,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.REJECTED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.PMDR,
            'current_section_id': constants.PMDR_SECTION_3,
            'completed_action_id_list': str(constants.PMDR_SECTION_3)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    return create_response(**return_params)


def deferment_extension_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_master_table_key=constants.KEY_DICT['DEFERMENT_EXTENSION_ID'],
        section_id=constants.PMDR_SECTION_4,
        master_table=TblPmdrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_2
    )
    logger.info(controller_rgn_str + ', request_type - ' + str(request_type))

    extension_params = {'extension_mitigated': int(request_data['extension_mitigated'])}
    if section_status:
        extension_params.update({'section_status': section_status})
    if constants.KEY_DICT['REMARKS'] in request_data:
        extension_params.update({'remarks': request_data['remarks'].capitalize()})
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        extension_params.update({'recycle_comments': request_data['recycle_comments'].capitalize()})

    if request_type == constants.POST:
        extension_params.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        deferment_extension = TblPmdrDefermentExtensionMaster(**extension_params)
        db.session.add(deferment_extension)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New deferment extension created.')
        deferment_extension_id = deferment_extension.id
        response_params = {'post_success_response': True}
    else:
        deferment_extension_id = int(request_data['deferment_extension_id'])
        extension_params.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        TblPmdrDefermentExtensionMaster.query.filter_by(id=deferment_extension_id).update(dict(extension_params))
        logger.info(controller_rgn_str + ' - Deferment Extension updated.')
        response_params = {'patch_success_response': True}

    pmdr_master_params = {
        'request_group_number': request_group_number,
        'request_data': request_data,
        'deferment_extension_id': deferment_extension_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if section_status == constants.RECYCLED:
        pmdr_master_params.update({'request_status': constants.PMDR_SECTION_4})

    update_pmdr_record(**pmdr_master_params)

    update_fl_work_order_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_4,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.RECYCLED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.PMDR,
            'current_section_id': constants.PMDR_SECTION_4,
            'pending_action_id': constants.PMDR_SECTION_3,
            'completed_action_id_list': str(constants.PMDR_SECTION_3) + ',' + str(constants.PMDR_SECTION_4)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    return create_response(**response_params)


def gm_reapproval_record(request_data=None, request_type=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    user_id = int(request_data['user_id'])
    section_status = int(request_data['section_status']) if request_data.get('section_status', None) else None

    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_master_table_key=constants.KEY_DICT['OPERATIONS_GM_REAPPROVAL_ID'],
        section_id=constants.PMDR_SECTION_5,
        master_table=TblPmdrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=request_type
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_5
    )
    logger.info(controller_rgn_str)

    gm_reapproval_fields = dict()
    if section_status:
        gm_reapproval_fields.update({'section_status': section_status})
    if constants.KEY_DICT['REMARKS'] in request_data:
        gm_reapproval_fields.update({'remarks': request_data['remarks'].capitalize()})
    if constants.KEY_DICT['RECYCLE_COMMENTS'] in request_data:
        gm_reapproval_fields.update({'recycle_comments': request_data['recycle_comments'].capitalize()})

    if request_type == constants.POST:
        gm_reapproval_fields.update({'created_time': convert_time_to_timestamp(), 'created_by': user_id})
        gm_reapproval = TblPmdrOperationsGmReapprovalMaster(**gm_reapproval_fields)
        db.session.add(gm_reapproval)
        db.session.commit()
        logger.info(controller_rgn_str + ' - New GM Re - Approval created.')
        gm_reapproval_id = gm_reapproval.id
        response_params = {'post_success_response': True}
    elif request_type == constants.PUT:
        gm_reapproval_id = int(request_data['gm_reapproval_id'])
        gm_reapproval_fields.update({'modified_time': convert_time_to_timestamp(), 'modified_by': user_id})
        TblPmdrOperationsGmReapprovalMaster.query.filter_by(id=gm_reapproval_id).update(dict(gm_reapproval_fields))
        logger.info(controller_rgn_str + ' - GM Re - Approval record updated.')
        response_params = {'put_success_response': True}

    pmdr_master_params = {
        'request_group_number': request_group_number,
        'request_data': request_data,
        'operations_gm_reapproval_id': gm_reapproval_id,
        'controller_rgn_str': controller_rgn_str,
        'logger': logger
    }
    if section_status == constants.RECYCLED:
        pmdr_master_params.update({'section_status': section_status})

    update_pmdr_record(**pmdr_master_params)

    update_fl_work_order_data(request_data=request_data, controller_rgn_str=controller_rgn_str, logger=logger)

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_5,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    if section_status == constants.RECYCLED:
        pending_action_data = {
            'unit_id': int(request_data['unit_id']),
            'user_id': user_id,
            'role_id': 1,
            'request_group_number': request_group_number,
            'work_flow_id': constants.PMDR,
            'current_section_id': constants.PMDR_SECTION_5,
            'pending_action_id': constants.PMDR_SECTION_4,
            'completed_action_id_list': str(constants.PMDR_SECTION_5)
        }
        status = create_pending_action_record(
            request_data=pending_action_data,
            user_active_work_flow_table=TblUserActiveWorkflows,
            pending_action_table=TblPendingActions,
            logger=logger
        )
        if status == constants.FAILURE:
            return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    return create_response(**response_params)


def create_job_scheduler_record(request_data=None, logger=None):
    request_group_number = int(request_data['request_group_number'])
    status = check_request_permissions(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_master_table_key=constants.KEY_DICT['JOB_SCHEDULER_ID'],
        section_id=constants.PMDR_SECTION_6,
        master_table=TblPmdrMaster,
        pending_action_table=TblPendingActions,
        user_active_flow_table=TblUserActiveWorkflows,
        request_type=constants.POST
    )
    if status is constants.FAILURE:
        return create_response(status=status, error_message=constants.REQUEST_NOT_ALLOWED)

    user_id = int(request_data['user_id'])
    controller_rgn_str = get_controller_rgn_str(
        work_flow_id=constants.PMDR, work_flow_section_id=constants.PMDR_SECTION_2
    )
    logger.info(controller_rgn_str)

    job_scheduler_params = {'created_time': convert_time_to_timestamp(), 'created_by': user_id}
    if constants.KEY_DICT['REMARKS'] in request_data:
        job_scheduler_params.update({'remarks': request_data['remarks'].capitalize()})
    job_scheduler_record = TblPmdrJobSchedulerMaster(**job_scheduler_params)
    db.session.add(job_scheduler_record)
    db.session.commit()
    logger.info(controller_rgn_str + ' - New Job Scheduler request created.')

    update_pmdr_record(
        request_group_number=request_group_number,
        request_data=request_data,
        request_status=constants.CLOSED,
        job_scheduler_id=job_scheduler_record.id,
        controller_rgn_str=controller_rgn_str,
        logger=logger
    )

    create_or_update_user_active_work_flow(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        section_number=constants.PMDR_SECTION_6,
        section_status=constants.TEXTUAL_DATA_SUBMITTED,
        user_id=user_id,
        table=TblUserActiveWorkflows
    )

    return create_response(post_success_response=True)


def discard_request_data(request_data=None, logger=None):
    fl_master_fields = dict()
    work_center_fields = dict()
    section_master_table = None
    section_master_table_id = None
    request_group_number = int(request_data['request_group_number'])
    work_flow_section_id = int(request_data['work_flow_section_id'])

    controller_rgn_str = get_controller_rgn_str(
        request_group_number=request_group_number,
        work_flow_id=constants.PMDR,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str)

    pmdr_work_order_objects = TblPmdrWorkOrders.query.filter_by(request_group_number=request_group_number)
    work_order_id_list = [record.work_order_id for record in pmdr_work_order_objects.all()]

    work_order_objects = TblWorkOrderMaster.query.filter(TblWorkOrderMaster.id.in_(work_order_id_list))
    fl_id_list = [record.function_location_id for record in work_order_objects.all()]

    pmdr_master_obj = TblPmdrMaster.query.filter_by(request_group_number=request_group_number)
    pmdr_master_record = pmdr_master_obj.first()

    if work_flow_section_id == constants.PMDR_SECTION_1:
        fl_master_fields.update({'work_center_id': None, 'recommended_pm_interval': None})
        work_center_fields.update({'recommended_plan': None})

    if work_flow_section_id == constants.PMDR_SECTION_2:
        request_status = constants.PMDR_SECTION_1
        section_master_table = TblPmdrInitialDefermentMaster
        section_master_table_id = constants.KEY_DICT['INITIAL_DEFERMENT_ID']

    if work_flow_section_id == constants.PMDR_SECTION_3:
        request_status = constants.PMDR_SECTION_2
        section_master_table = TblPmdrAreaBtmApprovalMaster
        section_master_table_id = constants.KEY_DICT['AREA_BTM_APPROVAL_ID']

    if work_flow_section_id == constants.PMDR_SECTION_4:
        request_status = constants.PMDR_SECTION_2
        section_master_table = TblPmdrDefermentExtensionMaster
        section_master_table_id = constants.KEY_DICT['DEFERMENT_EXTENSION_ID']

    if work_flow_section_id == constants.PMDR_SECTION_5:
        request_status = constants.PMDR_SECTION_4
        section_master_table = TblPmdrOperationsGmReapprovalMaster
        section_master_table_id = constants.KEY_DICT['OPERATIONS_GM_REAPPROVAL_ID']

    if work_flow_section_id == constants.PMDR_SECTION_6:
        if pmdr_master_record.operations_gm_reapproval_id:
            request_status = constants.PMDR_SECTION_5
        elif pmdr_master_record.area_btm_approval_id:
            request_status = constants.PMDR_SECTION_3
        section_master_table = TblPmdrOperationsGmReapprovalMaster
        section_master_table_id = constants.KEY_DICT['JOB_SCHEDULER_ID']

    delete_section_documents(
        request_group_number=request_group_number,
        work_flow_id=constants.COD,
        work_flow_section_id=work_flow_section_id,
        document_master_table=TblDocumentMaster,
        work_flow_document_table=TblWorkFlowDocuments,
        logger=logger
    )
    logger.info(controller_rgn_str + ' Attachments deleted.')

    delete_user_active_work_flow_record(
        table=TblUserActiveWorkflows,
        request_group_number=request_group_number,
        work_flow_section_id=work_flow_section_id
    )
    logger.info(controller_rgn_str + ' User active workflow record deleted.')

    if fl_master_fields:
        TblFunctionLocationMaster.query.filter(
            TblFunctionLocationMaster.id.in_(fl_id_list)
        ).update(dict(
            fl_master_fields
        ))
        db.session.commit()
        logger.info(controller_rgn_str + ' Function location properties set to NULL.')

        work_order_objects.update(dict(work_center_fields))
        db.session.commit()
        logger.info(controller_rgn_str + ' Work Center properties set to NULL.')

    if work_flow_section_id == constants.PMDR_SECTION_1:
        pmdr_work_order_objects.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' PMDR Work Centers deleted.')

        pmdr_master_obj.delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' PMDR master record deleted.')
    else:
        pmdr_master_obj.update(dict(request_status=request_status))
        db.session.commit()

    if section_master_table_id:
        section_master_table_id_value = getattr(pmdr_master_record, section_master_table_id)
        pmdr_master_obj.update(dict({section_master_table_id: None}))
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table ID set to None in PMDR master table.')

        section_master_table.query.filter_by(id=section_master_table_id_value).delete()
        db.session.commit()
        logger.info(controller_rgn_str + ' Section Master table record deleted.')

    return create_response(delete_success_response=constants.SUCCESS)
