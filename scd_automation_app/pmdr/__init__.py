from flask import Blueprint, request

import constants
from logging_module import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import (
    get_pmdr_request_records,
    get_unit_work_order_records,
    get_pmdr_request_data,
    create_pmdr_record,
    create_initial_deferment_record,
    area_btm_approval_record,
    deferment_extension_record,
    gm_reapproval_record,
    create_job_scheduler_record,
    discard_request_data
)

blueprint_name = constants.BLUE_PRINT_DICT['PMDR']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['PMDR']['URL_PREFIX']
pmdr_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['PMDR_LOG'])


api_get_pmdr_request_list = constants.API_DICT['PMDR']['GET_PMDR_REQUEST_LIST']
api_get_pmdr_request_details = constants.API_DICT['PMDR']['GET_PMDR_REQUEST_DETAILS']
api_get_unit_work_order_details = constants.API_DICT['PMDR']['GET_UNIT_WORK_ORDER_DETAILS']
api_create_pmdr_request = constants.API_DICT['PMDR']['CREATE_PMDR_REQUEST']
api_create_initial_deferment = constants.API_DICT['PMDR']['CREATE_INITIAL_DEFERMENT']
api_area_btm_approval = constants.API_DICT['PMDR']['AREA_BTM_APPROVAL']
api_deferment_extension = constants.API_DICT['PMDR']['DEFERMENT_EXTENSION']
api_gm_reapproval = constants.API_DICT['PMDR']['GM_REAPPROVAL']
api_create_job_scheduler_request = constants.API_DICT['PMDR']['CREATE_JOB_SCHEDULER_REQUEST']
api_discard_form = constants.API_DICT['PMDR']['DISCARD_FORM']


@pmdr_bp.route(api_get_pmdr_request_list, methods=[constants.GET])
def get_pmdr_request_list():
    return base_api(request=request, api_name=api_get_pmdr_request_list, api=get_pmdr_request_records, logger=logger)


@pmdr_bp.route(api_get_pmdr_request_details, methods=[constants.GET])
def get_get_pmdr_request_details():
    return base_api(request=request, api_name=api_get_pmdr_request_details, api=get_pmdr_request_data, logger=logger)


@pmdr_bp.route(api_get_unit_work_order_details, methods=[constants.GET])
def get_unit_work_order_details():
    return base_api(request=request, api_name=api_get_unit_work_order_details, api=get_unit_work_order_records,
                    logger=logger)


@pmdr_bp.route(api_create_pmdr_request, methods=[constants.POST])
def create_pmdr_request():
    return base_api(request=request, api_name=api_create_pmdr_request, api=create_pmdr_record, logger=logger)


@pmdr_bp.route(api_create_initial_deferment, methods=[constants.POST])
def create_initial_deferment():
    return base_api(request=request, api_name=api_create_initial_deferment, api=create_initial_deferment_record,
                    logger=logger)


@pmdr_bp.route(api_area_btm_approval, methods=[constants.POST, constants.PATCH])
def area_btm_approval():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_area_btm_approval, api=area_btm_approval_record,
                    api_params=api_params, logger=logger)


@pmdr_bp.route(api_deferment_extension, methods=[constants.POST, constants.PATCH])
def deferment_extension():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_deferment_extension, api=deferment_extension_record,
                    api_params=api_params, logger=logger)


@pmdr_bp.route(api_gm_reapproval, methods=[constants.POST, constants.PATCH])
def gm_reapproval():
    api_params = {'request_type': request.method}
    return base_api(request=request, api_name=api_gm_reapproval, api=gm_reapproval_record, api_params=api_params,
                    logger=logger)


@pmdr_bp.route(api_create_job_scheduler_request, methods=[constants.POST])
def create_job_scheduler_request():
    return base_api(request=request, api_name=api_create_job_scheduler_request, api=create_job_scheduler_record,
                    logger=logger)


@pmdr_bp.route(api_discard_form, methods=[constants.DELETE])
def discard_form():
    return base_api(request=request, api_name=api_discard_form, api=discard_request_data, logger=logger)
