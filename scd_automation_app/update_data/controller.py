import constants
from common_packages import db
from scd_automation_app.common.models import (
    TblCategoryMaster, TblScdPurposeMaster, TblPidDrawingMaster, TblOperationModeMaster, TblFunctionLocationMaster
)
from scd_automation_app.samref.common_helpers import create_response, check_and_add_record


def update_fl_record(request_data=None, fl_id=None, logger=None):
    category_id = check_and_add_record(
        table=TblCategoryMaster, field=constants.KEY_DICT['CATEGORY'], field_value=request_data['category']
    )
    scd_purpose_id = check_and_add_record(
        table=TblScdPurposeMaster, field=constants.KEY_DICT['SCD_PURPOSE'], field_value=request_data['scd_purpose']
    )
    pid_drawing_id = check_and_add_record(
        table=TblPidDrawingMaster, field=constants.KEY_DICT['PID_DRAWING'], field_value=request_data['pid_drawing']
    )
    operation_mode_id = check_and_add_record(
        table=TblOperationModeMaster, field=constants.KEY_DICT['MODE'], field_value=request_data['mode']
    )

    TblFunctionLocationMaster.query.filter_by(id=fl_id).update(dict(
        # category_id=category_id,
        critical_safeguard=int(request_data['critical_safeguard']),
        scd_purpose_id=scd_purpose_id,
        pid_drawing_id=pid_drawing_id,
        operation_mode_id=operation_mode_id,
        equipments_protected=request_data['equipments_protected'].capitalize(),
        pm_plan_scheduled=int(request_data['pm_plan_scheduled']),
        pid_drawing_updated=int(request_data['pid_drawing_updated']),
        sap_scd_class_updated=int(request_data['sap_scd_class_updated']),
        remarks=request_data['remarks'].capitalize()
    ))
    db.session.commit()

    return create_response(patch_success_response=True)
