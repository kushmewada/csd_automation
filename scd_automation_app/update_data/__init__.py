from flask import Blueprint, request

import constants
from common_packages import setup_logger
from scd_automation_app.samref.decorators import base_api
from .controller import update_fl_record

blueprint_name = constants.BLUE_PRINT_DICT['MASTERS']['UPDATE_DATA']['BLUE_PRINT']
url_prefix = constants.BLUE_PRINT_DICT['MASTERS']['UPDATE_DATA']['URL_PREFIX']
update_data_bp = Blueprint(blueprint_name, __name__, url_prefix=url_prefix)
logger = setup_logger(logger_name=constants.LOGGER_DICT['UPDATE_DATA_LOG'])


api_update_fl_data = constants.API_DICT['MASTERS']['UPDATE_DATA']['FUNCTION_LOCATION']


@update_data_bp.route(api_update_fl_data, methods=[constants.PATCH])
def update_fl_data(record_id):
    return base_api(request=request, api_name=api_update_fl_data, api=update_fl_record,
                    api_params={'fl_id': int(record_id)}, logger=logger)
