# from __future__ import absolute_import, print_function, unicode_literals
import json
from logging import (config, handlers, getLogger, Formatter)
import os
import traceback
import constants
import settings
from .dynamic_dictionary import (config_initial, update_config_dictionary)

# print("++++ Current working directory for logging_module module:", constants.LOGGER_PATH)
base_path = os.path.join(constants.LOGGER_PATH, constants.LOGS_FOLDER)

# Configuration_File path
config_file_path = os.path.join(constants.LOGGER_PATH, constants.CONFIG_FOLDER, constants.CONFIG_JSON_FILE)

# Mapping List file path
mapping_list_path = os.path.join(constants.LOGGER_PATH, constants.MAPPING_LIST_FOLDER, constants.MAPPING_LIST_JSON_FILE)

# Global Dictionary 'logger_list'
logger_mapping_dictionary = {}
mapping_dictionary_updated = True
previous_config_read = False


def create_and_load_config_file():
    """
    Create a Config folder in root directory of project and a config.json file in it. This file
    will be used as the configuration file.

    Config.json will be loaded with initial basic configuration by calling the config_initial
    method of dynamic_dictionary module.

    Returns:
        configuration_dictionary [ which can further be passed to dictConfig() ]
    """

    # Creating Config folder
    config_folder = os.path.join(constants.LOGGER_PATH, constants.CONFIG_FOLDER)
    if not os.path.exists(config_folder):
        os.makedirs(config_folder)

    if not os.path.exists(base_path):
        os.makedirs(base_path)

    # Config.json file creation when not there
    if not os.path.isfile(config_file_path):
        open(config_file_path, 'a').close()

        # Always delete the existing mapping_list when creating the new
        # config.json file
        if os.path.isfile(mapping_list_path):
            # print('**** Deleting existing mapping_list ****')
            os.remove(mapping_list_path)

        # Load the initial configuration in the 'Config_settings' dictionary
        return config_initial(config_file_path)

    # Returning the config_dictionary if config.json is there
    else:
        try:
            with open(config_file_path, constants.READ_MODE) as fp:
                global previous_config_read
                previous_config_read = True
                return dict(json.load(fp))

        except ValueError as e:
            print("create_and_load_config_file: {0}".format(str(e)))
            print(">>>>>>config.json --> Configuration file is empty or missing !!")

            # Simply delete the config.json
            # This file will be created automatically on next run
            os.remove(config_file_path)

            print("create_and_load_config_file: >>>> Exiting - Restart.....")
            exit()


def create_logger(
        logger_name,
        handler_type_list,
        logging_level=constants.LOGGING_LEVEL_DICT['DEBUG'],
        rotating_max_bytes=settings.MAX_BYTES,
        rotating_backup_count=settings.DEFAULT_ROTATING_BACKUP_COUNT,
):
    """
    First searches in a mapping list. If mapping is there for the given logger_name, it means
    we have the configuration already available in configuration_dictionary.

    Otherwise, we will create a new logger instance and set up its configuration.
    Add the configuration in the config.json file.

    :param logger_name: name with which logger is to be created
    :param handler_type_list: types of handler to be used for the logger [pass as tuple or list]
    :param logging_level: Optional parameter [to pass the level of logging_module]
                           DEFAULT LEVEL : DEBUG
    :param rotating_max_bytes: For ROTATING_FILE_HANDLER's maxBytes arg [optional, Default: 102400 Bytes]
    :param rotating_backup_count: For ROTATING_FILE_HANDLER's backupCount arg [optional, Default: 2]

    :return: logger instance
    """

    logger = None
    for handler_type in handler_type_list:
        result = check_logger_and_handler_exist(logger_name, handler_type)

        if result:
            logger = getLogger(logger_name)

        else:
            logger = getLogger(logger_name)
            logger.setLevel(logging_level)
            handler = get_handler(logger_name, rotating_max_bytes, rotating_backup_count)

            formatter = Formatter(constants.LOG_FORMAT)
            handler.setFormatter(formatter)
            handler.setLevel(logging_level)
            logger.addHandler(handler)

            # Add the newly created logger instance in config.json file
            update_config_dictionary(
                logger_name,
                base_path,
                config_file_path,
                handler_type,
                logging_level,
                rotating_max_bytes,
                rotating_backup_count
            )

            # Update the dictionary 'logger_list'

            # Fetch the list of handlers associated with a particular logger_name
            # and update the handlers list

            key = logger_name
            value = list()

            # If logger exists in the dictionary, retrieve the already available handlers
            #  then append the new handler_type for the logger
            if key in list(logger_mapping_dictionary.keys()):
                value = logger_mapping_dictionary[key]

            value.append(handler_type)
            # New handler type added to the specific logger's handlers list
            logger_mapping_dictionary[key] = value

            update_mapping_list()

    return logger


def get_mapping_list():
    """
    Create a file for storing the name of loggers that have been configured.

    If file exists, load the list from file and simply return the list.
    Otherwise create a file and return empty list.

    :return: List containing loggers name.
    """
    mapping_list_folder = os.path.join(constants.LOGGER_PATH, constants.MAPPING_LIST_FOLDER)

    if not os.path.exists(mapping_list_folder):
        os.makedirs(os.path.join(mapping_list_folder))

    # List for storing the instances
    if not os.path.isfile(mapping_list_path):
        # print('**** Creating Mapping Dictionary ****')

        # Checking if the previous configuration available, then overwrite it with the default configuration
        # This happens when mapping_list.json doesn't exist and config.json
        # exists with some previous configuration

        if previous_config_read:
            # Overwrite the config.json with initial config
            # print("Overwriting the Previous Configuration....")
            config_initial(config_file_path)

        # creates a json file for dictionary
        open(mapping_list_path, constants.APPEND_MODE).close()
        return {}  # Returning the empty dictionary

    else:
        # Load the mapping_list with previous loggers
        # print('**** Loading the Mapping Dictionary ****')
        try:
            with open(mapping_list_path, constants.READ_MODE) as json_file:
                return json.load(json_file)
        except ValueError as e:
            print("get_mapping_list: {}".format(str(e)))
            print("get_mapping_list: >>>>>> mapping_list.json --> File is empty or missing !!")
            os.remove(mapping_list_path)
            os.remove(config_file_path)
            print("get_mapping_list: >>>> Exiting - Restart.....")
            exit()


def update_mapping_list():
    """
    Updates the file storing the list with loggers name and the associated handlers
    :return: None
    """
    # print('****** Updating mapping.json ******')
    try:
        with open(mapping_list_path, constants.WRITE_MODE) as json_file:
            json.dump(logger_mapping_dictionary, json_file)
    except ValueError as e:
        print("update_mapping_list: {}".format(str(e)))
        exit()
    else:
        # Set mapping_dictionary_updated to TRUE , so that new mapping list can
        # be loaded
        global mapping_dictionary_updated
        mapping_dictionary_updated = True

    return 0


def get_handler(logger_name, rotating_max_bytes, rotating_backup_count):
    """
    Create handler of appropriate type

    :param rotating_max_bytes:
    :param rotating_backup_count:
    :param logger_name: name of the logger to which handler will be attached

    :return: required handler type.
    """
    log_path = os.path.join(base_path, logger_name + ".log")
    rotating_file_handler = handlers.RotatingFileHandler(
        log_path, constants.APPEND_MODE, maxBytes=rotating_max_bytes, backupCount=rotating_backup_count
    )
    return rotating_file_handler


def check_logger_and_handler_exist(logger_name, handler):
    """
    Check for the logger and the associated handlers with it.

    Returns TRUE if the logger and the required handler type exists in the dictionary,
    Else returns FALSE if either logger doesn't exist or [if logger exists but the required handler_type doesn't exists]

    :param logger_name: Name of the logger
    :param handler: Handler type
    :return: boolean value
    """
    global logger_mapping_dictionary
    global mapping_dictionary_updated

    if mapping_dictionary_updated:

        # Retrieving the mapping list for first time
        logger_mapping_dictionary = get_mapping_list()

        # Mapping dictionary will not be loaded again unless updated
        mapping_dictionary_updated = False

    if logger_name in list(logger_mapping_dictionary.keys()):
        if handler in logger_mapping_dictionary[logger_name]:
            return True
        else:
            return False
    else:
        return False


def config_log():
    try:
        if not constants.IS_LOG_SETUP_IMPORTED:
            config.dictConfig(create_and_load_config_file())
            constants.IS_LOG_SETUP_IMPORTED = True
            print("Logger Configuration Imported.")
    except:
        print("config_log:" + str(traceback.print_exc()))


config_log()
