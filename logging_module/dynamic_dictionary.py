# from __future__ import absolute_import, print_function, unicode_literals
import json
import os
import constants
import settings


def config_initial(config_file_path):
    """
    Loads the config.json file with basic initial configuration.
    Creates a 'root.log' file for root logger.
    :param config_file_path: path
    :return: configuration_dictionary
    """
    log_setting = {
        'version': 1,
        'handlers':
            {
                'hand_root':
                    {
                        'class': constants.ROTATING_FILE_HANDLER_CLASS,
                        'level': constants.LOGGING_LEVEL_DICT['INFO'],
                        'formatter': constants.FORM_ROOT,
                        'filename': os.path.join(constants.LOGGER_PATH, constants.LOGS_FOLDER, constants.ROOT_LOG_FILE),
                        'mode': constants.APPEND_MODE,
                        'maxBytes': settings.MAX_BYTES,
                        'backupCount': settings.ROOT_ROTATING_BACKUP_COUNT
                    },
            },
        'formatters':
            {
                'form_root':
                    {
                        'format': constants.LOG_FORMAT,
                        'class': constants.FORMATTER_CLASS
                    }
            },
        'loggers': {},
        'root':
            {
                'level': constants.LOGGING_LEVEL_DICT['INFO'],
                'handlers': ['hand_root', ]
            }
    }

    # Saving Dictionary to json file
    try:
        with open(config_file_path, constants.WRITE_MODE) as config:
            json.dump(log_setting, config)
    except ValueError as e:
        print("config_initial: {0}".format(str(e)))
        exit()
    else:
        # Return the dictionary
        return log_setting


def update_config_dictionary(
        logger_name,
        base_path,
        config_file_path,
        handler_type,
        logging_level,
        rotating_max_bytes,
        rotating_backup_count
):
    """
    Updates the config.json file and adds the configuration for the new logger instance.
    Reads the config.json first to fetch the dictionary and then appends the new information in it
    and writes it back to config.json file.

    :param rotating_backup_count:
    :param rotating_max_bytes:
    :param logger_name: name of the logger
    :param base_path: path where log record will be created
    :param config_file_path: path where the config.json is stored
    :param handler_type: type of the handler to be associated with logger instance
    :param logging_level: Level of logging_module required [ OPTIONAL --> DEFAULT: INFO]
    :return: Nothing
    """

    options_dictionary = dict()  # For Handlers
    logger_options_dictionary = dict()  # For loggers

    # Retrieve the dictionary
    fp = open(config_file_path, constants.READ_MODE)
    config_dictionary = dict(json.load(fp))
    fp.close()

    handler_name = 'hand' + '_' + logger_name + '_' + handler_type

    options_dictionary = {
        'class': 'logging.handlers.RotatingFileHandler',
        'level': logging_level,
        'formatter': constants.FORM_ROOT,
        'filename': os.path.join(base_path, logger_name + '.log'),
        'mode': constants.APPEND_MODE,
        'maxBytes': rotating_max_bytes,
        'backupCount': rotating_backup_count
    }

    # Add the new handler to config_dictionary
    (config_dictionary['handlers'])[handler_name] = options_dictionary

    # keep the formatter same -- as same format for each and every logger
    # Retrieving list of available loggers in config dictionary
    loggers_list = list((config_dictionary['loggers']).keys())

    if logger_name in loggers_list:
        """
        logger available -- only append the new handler name
        """
        handlers_list = ((config_dictionary['loggers'])[logger_name])['handlers']  # Fetching the current handlers list
        handlers_list.append(handler_name)  # Appending the new handler name

        # print('Handlers associated with ' + logger_name + ': ', handlers_list)
        ((config_dictionary['loggers'])[logger_name])['handlers'] = handlers_list  # Updating the logger's configuration

    else:
        """
        No logger available - create a new one and update the configuration
        """
        logger_options_dictionary['level'] = logging_level
        logger_options_dictionary['handlers'] = [handler_name, ]
        logger_options_dictionary['propagate'] = 0
        logger_options_dictionary['qualname'] = 'xyz'
        (config_dictionary['loggers'])[logger_name] = logger_options_dictionary

    # Saving Dictionary to json file
    try:
        with open(config_file_path, constants.WRITE_MODE) as config:
            json.dump(config_dictionary, config)
    except ValueError as e:
        print("update_config_dictionary: {0}".format(str(e)))
        exit()
    else:
        return 0
