from .logging_module import create_logger
import constants


def setup_logger(logger_name, level=constants.LOGGING_LEVEL_DICT['DEBUG']):
    logger = create_logger(logger_name, [constants.ROTATING_FILE_HANDLER], logging_level=level)
    return logger
